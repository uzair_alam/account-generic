#!/bin/bash


bold=$(tput bold)
normal=$(tput sgr0)

if [ -z "$PROFILE" ]; then
    echo "[ERROR] ${bold}environment variable 'PROFILE' not initialized.${normal}"
    echo "[INFO] To initialize, run command ==> export PROFILE=[profile name]"
    exit 1;
fi


echo "[INFO] Setting up maven parameters...[MAVEN_OPTS]"

export MAVEN_OPTS="-Xmx3000m"

echo "[INFO] Pulling code from rep. Please enter password..."

git pull origin banaras

STATUS=$?
if [ $STATUS -eq 0 ]; then
    echo "[SUCCESS] Code Updated from repository"
else
    echo "[FAIL] Failed to Update code"
    exit 1
fi

echo "[INFO] Stoping tomcat..."
sudo service tomcat stop

STATUS=$?
if [ $STATUS -eq 0 ]; then
    echo "[SUCCESS] Tomcat stopped."
else
    echo "[FAIL] Fail to stop tomcat"
    exit 1
fi


echo "[INFO] Initializing Deployment"

mvn clean install -DskipTests -P $PROFILE

STATUS=$?
if [ $STATUS -eq 0 ]; then
    echo "[SUCCESS] Build Successful"
else
    echo "[FAIL] Deployment Failed"
    exit 1
fi

read -p "[>>>] Do you want to Copy .war file to Webapp? [y/n] " -n 1 -r
echo # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "[INFO] Copying War file to tomcat"
    mv target/$WAR_FILE /opt/tomcat/latest/webapps/
    STATUS=$?
    if [ $STATUS -eq 0 ]; then
        echo "[SUCCESS] Copy Successful";
    else
        echo "[FAIL] Copy Failed"
        exit 1
    fi
else
    echo "[ABORTED] Copying .war file aborted by user."
fi

echo "[INFO] Re starting tomcat"
sudo service tomcat start

STATUS=$?
if [ $STATUS -eq 0 ]; then
    echo "[SUCCESS] Tomcat started."
else
    echo "[FAIL] Fail to start tomcat"
    exit 1
fi


tail -f /opt/tomcat/latest/logs/catalina.out