<div class="container">

    <nav class="navbar navbar-inverse bg-inverse ">




        <li class="dropdown  navbar-brand" >
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: #a9a9a9" >Add Accounts
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a  href="${contextPath}/Account/addAccount">Add Account</a></li>
                <li><a  href="${contextPath}/Account/addCostCentre">Add Cost Centre</a></li>
            </ul>
        </li>
        <li class="dropdown  navbar-brand" >
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: #a9a9a9" >Add Details
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a  href="${contextPath}/User/addBankDetails">Add Bank Details</a></li>
                <li><a  href="${contextPath}/Account/addTaxDetails">Add Tax details</a></li>
            </ul>
        </li>


        <li class="dropdown  navbar-brand" >
            <a class="dropdown-toggle" data-toggle="dropdown"  href="#" style="color: #a9a9a9" >Vouchers
                <span class="caret"></span></a>
            <ul class="dropdown-menu" >
                <li><a  href="${contextPath}/Account/createPaymentVoucher">Payment Voucher</a></li>
                <li><a  href="${contextPath}/Account/createReceiptVoucher">Receipt Voucher</a></li>
                <li><a  href="${contextPath}/Account/createJournalVoucher">Journal Voucher</a></li>
            </ul>
        </li>

        <a class="navbar-brand" href="${contextPath}/Account/bankReconciliation">Reconciliation</a>
        <a  class="navbar-brand" href="${contextPath}/Account/addAccountNote">Add Note</a>
        <a  class="navbar-brand" href="${contextPath}/User/generateStatement">BS/PL </a>


        <form action="${contextPath}/logout" method="post">
            <input  class="navbar-brand pull-right" type="submit" value="Logout"/>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <li class="dropdown  navbar-brand pull-right" >
            <a class="dropdown-toggle" data-toggle="dropdown"  href="#" style="color: #a9a9a9" >Reports
                <span class="caret"></span></a>
            <ul class="dropdown-menu" >
                <li><a  href="${contextPath}/Reports/chartOfAccount">Chart Of Account</a></li>
                <li><a  href="${contextPath}/Reports/costCentres">Cost Centre</a></li>
                <li><a  href="${contextPath}/Reports/paymentVoucher">Payment Voucher</a></li>
                <li><a  href="${contextPath}/Reports/receiptVoucher">Receipt Voucher</a></li>
                <li><a  href="${contextPath}/Reports/journalVoucher">Journal Voucher</a></li>
                <li><a  href="${contextPath}/Reports/ledgerReport">Customer Ledger</a></li>
                <li><a  href="${contextPath}/Reports/ledgerReportBank">Bank Ledger</a></li>
                <li><a  href="${contextPath}/Reports/trialBalanceBasic">Trial Balance (Basic)</a></li>
                <li><a  href="${contextPath}/Reports/trialBalanceDetail">Trial Balance (Periodic)</a></li>
                <li><a  href="${contextPath}/Reports/noteSummary">Note Summary</a></li>
                <li><a  href="${contextPath}/Reports/sheetReport">Balance Sheet</a></li>

            </ul>
        </li>

    </nav>

</div>