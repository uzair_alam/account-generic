<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>${distributor.name}'s Customers</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${distributorAddSuccess}
        ${customerRemoveSuccess}

        <h2 class="heading-main">${distributor.name} Distributor Customers
            <span class="addIcon" >
                <form method="POST" class="form-inline">
                <label for="customerToAdd">Select Distributor :</label>
                    <select id="customerToAdd" name="customerId" class="form-control">
                        <c:forEach  items="${availableCustomers}" var="customer">
                            <option value="${customer.customerId}">${customer.customerAccount.title}</option>
                        </c:forEach>
                    </select>
                    <input type="hidden" name="distributorId" value="${distributor.distributorId}" />
                    <button type="submit"  > Add</button>
            </form>
            </span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th width="12%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${distributorCustomers}" var="customer" varStatus="distributorStatus">

                            <tr>
                                <td>${customer.customerAccount.title}</td>
                                <td>${customer.mobile}</td>

                                <td>
                                    <form method="post" id="distributorCustomerRemove${distributorStatus.index}"
                                          action="${contextPath}/Distributor/removeCustomer">
                                        <input type="hidden" name="customerId"
                                               value="${customer.customerId}"/>
                                        <input type="hidden" name="distributorId"
                                               value="${distributor.distributorId}"/>
                                    </form>

                                    <ul class="list-inline">
                                        <li><a href="#" onclick="remove(${distributorStatus.index})" ><img
                                                src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>




<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });

    function remove(id) {
        if (confirm("Are you sure, you want to remove Customer?")) {
            $('#distributorCustomerRemove' + id).submit();
        }
    }
</script>
</body>
</html>
