<header>

    <div class="container">
        <div class="header-main">
            <div class="leftNav">
                <nav>
                    <ul>
                        <li><a href="${contextPath}/home">Home</a></li>
                        <li class="dropdown"><a href="#" id="drop4" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false">Master File </a>
                            <ul class="dropdown-menu" id="menu1" aria-labelledby="drop4">
                                <li><a href="${contextPath}/Account/addAccount">Add Account</a></li>
                                <li><a href="${contextPath}/Account/AccountListing">Account List</a></li>
                                <li><a href="${contextPath}/Account/addCostCentre">Add Cost Centre</a></li>
                                <li><a href="${contextPath}/User/addBankDetails">Add Bank Details</a></li>
                                <li><a href="${contextPath}/Account/addTaxDetails">Add Tax details</a></li>
                                <li><a href="${contextPath}/Account/addAccountNote">Add Note</a></li>
                                <li><a href="${contextPath}/User/generateStatement">BS/PL </a></li>
                                <li><a href="${contextPath}/Account/addExpense">Add Expense Details </a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" id="drop5" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false">Transaction File </a>
                            <ul class="dropdown-menu" id="menu2" aria-labelledby="drop4">
                                <li><a href="${contextPath}/Account/createPaymentVoucher">Payment Voucher</a></li>
                                <li><a href="${contextPath}/Account/createReceiptVoucher">Receipt Voucher</a></li>
                                <li><a href="${contextPath}/Account/viewJournalVoucher">Journal Voucher</a></li>
                                <li><a href="${contextPath}/Account/bankReconciliation">Reconciliation</a></li>
                                <li><a href="${contextPath}/Account/createPdcJournalVoucher">PDC Voucher</a></li>
                                <li><a href="${contextPath}/Account/createBankChequeReturn">Cheque Return Voucher</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" id="drop6" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false">Reports </a>
                            <ul class="dropdown-menu" id="menu3" aria-labelledby="drop4">
                                <li><a href="${contextPath}/Reports/chartOfAccount">Chart Of Account</a></li>
                                <li><a href="${contextPath}/Reports/costCentres">Cost Centre</a></li>
                                <li><a href="${contextPath}/Reports/paymentVoucher">Payment Voucher</a></li>
                                <li><a href="${contextPath}/Reports/receiptVoucher">Receipt Voucher</a></li>
                                <li><a href="${contextPath}/Reports/journalVoucher">Journal Voucher</a></li>
                                <li><a href="${contextPath}/Reports/ledgerReport">Customer Ledger</a></li>
                                <li><a href="${contextPath}/Reports/ledgerReportBank">Bank Ledger</a></li>
                                <li><a href="${contextPath}/Reports/trialBalanceBasic">Trial Balance (Basic)</a></li>
                                <li><a href="${contextPath}/Reports/trialBalanceDetail">Trial Balance (Periodic)</a>
                                </li>
                                <li><a href="${contextPath}/Reports/noteSummary">Note Summary</a></li>
                                <li><a href="${contextPath}/Reports/sheetReport">Balance Sheet</a></li>
                                <li><a href="${contextPath}/Reports/ledgerBillReport">Bill Summary</a></li>
                                <li><a href="${contextPath}/Reports/productLedger">Product Ledger</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" id="drop7" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false">Stock </a>
                            <ul class="dropdown-menu" id="menu4" aria-labelledby="drop4">
                                <li><a href="${contextPath}/Stock/addStock">Add Stock</a></li>
                                <li><a href="${contextPath}/Stock/addVendor">Add Vendor</a></li>
                                <li><a href="${contextPath}/Stock/addCustomer">Add Customer</a></li>
                                <li><a href="${contextPath}/Stock/addStockCategory">Add Category</a></li>

                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" id="drop8" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false">Purchase/Sale </a>
                            <ul class="dropdown-menu" id="menu5" aria-labelledby="drop4">
                                <li><a href="${contextPath}/Purchase/createPurchaseVoucher">add Purchase Voucher</a>
                                </li>
                                <li><a href="${contextPath}/Purchase/viewPurchaseVoucher">Purchase Vouchers</a></li>
                                <li><a href="${contextPath}/Sales/createSaleVoucher">add Sales Voucher</a></li>
                                <li><a href="${contextPath}/Sales/viewSalesVoucher">Sales Vouchers</a></li>

                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" id="drop9" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false">Nozzle </a>
                            <ul class="dropdown-menu" id="menu6" aria-labelledby="drop4">
                                <li><a href="${contextPath}/Nozzle/addNozzleMaster">Add Nozzle</a></li>
                                <li><a href="${contextPath}/Nozzle/addReading">Add Reading</a></li>
                                <li><a href="${contextPath}/Nozzle/addDayEnd">Add Day End</a></li>
                                <li><a href="${contextPath}/Nozzle/dayEndFinal">Day End Report</a></li>


                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" id="drop9" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false">Setting </a>
                            <ul class="dropdown-menu" id="menu6" aria-labelledby="drop4">
                                <li><a href="${contextPath}/staticinfo">Edit Company Info</a></li>


                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" id="drop92" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false">Calibration </a>
                            <ul class="dropdown-menu" id="menu36" aria-labelledby="drop4">
                                <li><a href="${contextPath}/Calibration/tankList">Manage Tanks</a></li>
                                <li><a href="${contextPath}/Calibration/calibrationList">Manage Calibrations</a></li>


                            </ul>
                        </li>

                        <li class="dropdown"><a href="#" id="drop111" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Distributor / Rider </a>
                            <ul class="dropdown-menu" id="menu110" aria-labelledby="drop4">
                                <li><a  href="${contextPath}/Rider/addRider">Manage Rider</a></li>
                                <li><a  href="${contextPath}/Rider/customerBalance">Rider Customer Balance</a></li>
                                <li><a  href="${contextPath}/Distributor/addDistributor">Manage Distributor</a></li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>
            <div class="rightNav">
                <form action="${contextPath}/logout" method="post">
                    <input type="submit" value="Logout"/>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </form>
            </div>
        </div>
    </div>
</header>