<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create PDC Journal Voucher</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">
    ${voucherAddSuccess}

    <h2 class="form-signin-heading">PDC Journal Voucher</h2>
    <div class="container">
        <form method="POST" action="${contextPath}/Account/createPdcJournalVoucher" class="form-inline formSaleVoucher"
              style="margin-bottom: 15px; ">
            <div class="pull-left">
                <div class="form-Row">
                    <label>Voucher Number :</label>
                    <input id="voucherNumber" type="text" style="width: 150px" class="form-control" readonly="true"
                           autofocus="true" value="${voucherNumber}"/>
                </div>

            </div>
            <label>Current Date : </label>
            <input type="date" style="width: 160px" class="form-control " id="currentDate"
                   autofocus="true" value="${currentDate}" required="required"/>

            <label>From Date : </label>
            <input type="date" name="dateFrom" style="width: 160px" onchange="this.form.submit()" class="form-control "
                   id="fromDate"
                   autofocus="true" value="${dateFrom}" required="required"/>

            <label>To Date : </label>
            <input type="date" name="dateTo" style="width: 160px" onchange="this.form.submit()" class="form-control "
                   id="toDate"
                   autofocus="true" value="${dateTo}" required="required"/>

        </form>

    </div>


    <form:form method="POST" modelAttribute="voucherList" id="formid"
               action="${contextPath}/Account/savePdcJournalVoucher">
        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>

                    <th width="20%">Customer Name</th>
                    <th width="10%">Cheque Number</th>
                    <th width="20%">Amount</th>
                    <th width="25%">Bank Account</th>
                    <th width="5%">PDC Check</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${voucherList.voucherList}" var="vouchers" varStatus="status">

                    <tr>

                        <form:input type="hidden" path="voucherList[${status.index}].voucherNumberForPdc"
                                    class="form-control" value="${vouchers.voucherNumber}" readonly="true"></form:input>

                        <form:input id="voucherNumber" type="hidden" path="voucherList[${status.index}].voucherNumber"
                                    class="form-control"
                                    autofocus="true" value="${voucherNumber}"></form:input>

                        <td>

                            <form:input type="hidden" path="voucherList[${status.index}].accountCode"
                                        class="form-control" readonly="true"></form:input>
                            <input class="form-control" value="${accountMap.get(vouchers.accountCode)}" readonly/>
                        </td>
                        <td>
                            <form:input type="text" path="voucherList[${status.index}].chequeNumber"
                                        value="${vouchers.chequeNumber}" class="form-control debitList"
                                        readonly="true"></form:input>
                        </td>
                        <td>
                            <form:input type="text" path="voucherList[${status.index}].credit"
                                        class="form-control debitList" readonly="true"></form:input>


                        </td>
                        <td>

                            <form:select path="voucherList[${status.index}].bankAccount" class="form-control"  id="bankAccount${status.index}" onchange="changePdcStatus(${status.index}) ">
                                <c:forEach items="${bankAccounts}" var="bankAccount">
                                    <form:option value="${bankAccount.account.accountCode}">${bankAccount.account.title}
                                        <c:choose>
                                            <c:when test="${bankAccount.account.parentAccount != null }"> - ${bankAccount.account.parentAccount.title}
                                            </c:when>
                                        </c:choose>
                                    </form:option>
                                </c:forEach>
                            </form:select>
                        </td>
                        <td>
                            <form:checkbox path="voucherList[${status.index}].check"  id="checkPdc${status.index}"></form:checkbox>
                        </td>

                        <form:input type="date" cssStyle="display: none" path="voucherList[${status.index}].voucherDate"
                                    class="form-control theDate"
                                    autofocus="true" value="${currentDate}" required="required"></form:input>


                    </tr>
                </c:forEach>
                </tbody>

            </table>
        </div>
        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function () {
        $('#currentDate').change(function () {
            $('.theDate').val($('#currentDate').val());

        });
        $('#formid').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                alert("Enter key not Allowed.Please use submit button");
                e.preventDefault();
                return false;
            }
        });
    });
function changePdcStatus(id) {
    document.getElementById('checkPdc' + id).checked = true;
}


</script>
</body>
</html>
