<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title>Payment Voucher</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">
        <h2 class="heading-main">Payment Voucher <span class="addIcon addForm" data-toggle="modal"
                                                       data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th onclick="refreshPage()" width="10%">V-No </br> V-Date</th>
                            <th style="display: none">Date</th>
                            <th width="15%">Bank</br> Chq-No</th>
                            <th width="20%">Acc</br> Cost Centre</th>
                            <th width="20%">Tax Acc</br> Remarks</th>
                            <th width="15%">Bill Amt</br> Tax Amt</th>
                            <th width="15%">Net Payment</th>
                            <th width="5%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${paymentVouchers}" var="voucher" varStatus="voucherStatus">
                            <c:choose>


                                <c:when test="${voucher.bankAccount != 0}">
                                    <tr>

                                        <td>${voucher.voucherNumber} </br><fmt:formatDate pattern="dd/MM/yyyy" value="${voucher.voucherDate}"/></td>
                                        <td style="display: none">${voucher.voucherDate}</td>
                                        <td>${accountMap.get(voucher.bankAccount)} </br>${voucher.chequeNumber}</td>
                                        <td>${accountMap.get(voucher.accountCode)} </br>${costCentreMap.get(voucher.costCentre)}</td>
                                        <td>${accountMap.get(voucher.taxAccount)} </br>${voucher.remarks}</td>
                                        <td><fmt:formatNumber maxFractionDigits="2" value="${voucher.billAmount}"/> </br>
                                                ${voucher.taxAmount}
                                        </td>
                                        <td><fmt:formatNumber maxFractionDigits="2" value="${voucher.debit}"/></td>
                                        <td>
                                            <form method="post" id="voucherRemove${voucherStatus.index}" action="${contextPath}/Account/deleteVoucher?${_csrf.parameterName}=${_csrf.token}">
                                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                                <input type="hidden" name="voucherNumber" value="${voucher.voucherNumber}"/>
                                                <input type="hidden" name="redirectUrl" value="Account/createPaymentVoucher"/>
                                            </form>
                                            <ul class="list-inline">
                                                <li><a title="Edit" href="#" onclick="edit(${voucher.voucherId})" class="editForm"><img src="${contextPath}/resources/img/editIcon.png" alt=""></a></li>
                                                <li><a title="Delete" href="#" onclick="remove(${voucherStatus.index});"><img src="${contextPath}/resources/img/deleteIcon.png" alt=""></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </c:when>
                            </c:choose>
                        </c:forEach>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" onclick="refreshPage()" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Payment Voucher</h2>
            </div>
            <form:form method="post" modelAttribute="voucherForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="voucherNumber">

                                <form:label path="voucherNumber">Voucher Number</form:label>
                                <form:input id="voucherNumber" type="text" path="voucherNumber" readonly="true"
                                            autofocus="true" value="${voucherNumber}"></form:input>
                                <form:errors path="voucherNumber"></form:errors>

                            </spring:bind>
                            <spring:bind path="voucherDate">

                                <form:label path="voucherDate">Voucher Date</form:label>
                                <form:input type="date" path="voucherDate"
                                            autofocus="true" value="${currentDate}" required="required"></form:input>
                                <form:errors path="voucherDate"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankAccount">

                                <form:label path="bankAccount">Bank</form:label>
                                <form:select path="bankAccount">

                                    <c:forEach items="${bankDetailAccounts}" var="account">
                                        <form:option value="${account.accountCode}">${account.title}</form:option>
                                    </c:forEach>
                                </form:select>


                            </spring:bind>

                            <spring:bind path="accountCode">

                                <form:label path="accountCode">Account</form:label>
                                <form:select path="accountCode">

                                    <c:forEach items="${accounts}" var="account">
                                        <form:option value="${account.accountCode}">${account.title}
                                            <c:choose>
                                                <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                </c:when>
                                            </c:choose></form:option>
                                    </c:forEach>
                                </form:select>


                            </spring:bind>

                            <spring:bind path="costCentre">

                                <form:label path="costCentre">Cost Centre</form:label>
                                <form:select path="costCentre">
                                    <form:option value="">NIL</form:option>
                                    <c:forEach items="${costCentres}" var="centre">
                                        <form:option value="${centre.accountCode}">${centre.title}</form:option>
                                    </c:forEach>
                                </form:select>


                            </spring:bind>

                        </div>
                        <div class="col-sm-6">
                            <spring:bind path="taxAccount">

                                <form:label path="taxAccount">Tax Account</form:label>
                                <form:select path="taxAccount"  id="taxDrop">

                                    <form:option value="">NIL</form:option>
                                    <c:forEach items="${taxationAccounts}" var="taxationAccount" varStatus="status">
                                        <form:option value="${taxationAccount.accountCode}">${taxationAccount.title}</form:option>
                                    </c:forEach>

                                </form:select>
                            </spring:bind>
                            <div id="taxForm" style="display: none">
                                <spring:bind path="taxRate">
                                    <form:label path="taxRate">Select Account Type </form:label>
                                    <input type="hidden" value="" id="taxRateID"></input>
                                    <form:select path="taxRate" id="taxRate">
                                        <c:choose>
                                            <c:when test="${taxDetail.size()>0}">
                                                <option id="filer" value="${taxDetail.get(0).filerAmount}">Filer</option>
                                                <option id="non-filer" value="${taxDetail.get(0).nonFilerAmount}">Non-filer</option>
                                            </c:when>
                                        </c:choose>
                                    </form:select>
                                </spring:bind>
                            </div>

                            <spring:bind path="billAmount">

                                <form:label path="billAmount">Bill Amount</form:label>
                                <form:input id="billAmount" type="text" path="billAmount" placeholder="Bill Amount"
                                            autofocus="true" value="0.00"></form:input>
                                <form:errors path="billAmount"></form:errors>

                            </spring:bind>
                            <spring:bind path="chequeNumber">

                                <form:label path="chequeNumber">Cheque Number</form:label>
                                <form:input type="text" path="chequeNumber" placeholder="Cheque Number"
                                            autofocus="true"></form:input>
                                <form:errors path="chequeNumber"></form:errors>

                            </spring:bind>
                            <spring:bind path="taxAmount">

                                <form:label path="taxAmount">Tax Amount</form:label>
                                <form:input id="taxAmount" type="text" path="taxAmount" placeholder="Tax Amount"
                                            autofocus="true" value="0.00" readonly="true"></form:input>
                                <form:errors path="taxAmount"></form:errors>

                            </spring:bind>
                            <spring:bind path="debit">

                                <form:label path="debit">Net Payment</form:label>
                                <form:input type="text" path="debit" placeholder="Net Payment" id="netPayment"
                                            autofocus="true" value="0.00" readonly="true"></form:input>
                                <form:errors path="debit"></form:errors>

                            </spring:bind>
                        </div>


                    </div>
                    <div align="center">
                        <spring:bind path="remarks">
                            <div align="center">
                                <form:label path="remarks">Remarks</form:label></div>
                            <form:textarea cssStyle="width: 700px" type="text" path="remarks"
                                           placeholder="Enter Remarks if any."
                                           autofocus="true"></form:textarea>
                            <form:errors path="remarks"></form:errors>

                        </spring:bind>
                    </div>
                </div>

                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="button" onclick="addVoucherForm()">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" onclick="refreshPage()">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>
    var form_action = "add";
    $(document).ready(function () {

        $('#taxDrop').change(function () {
            changeTaxAmount();
        });

        $(".addForm").click(function () {
            form_action = 'add';
        });

        $(".editForm").click(function () {
            form_action = 'edit';
        });


        $('#taxRate').change(function () {
            var currentTaxRate = $('#taxRate').val();
            taxRateID = currentTaxRate;
            var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
            $('#taxAmount').val(taxAmount.toFixed(2));
            $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));

        });

        $('#billAmount').blur(function () {
            if ($('#taxDrop').val() != '') {
                var currentTaxRate = $('#taxRate').val();
                var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
                $('#taxAmount').val(taxAmount.toFixed(2));
                $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));
            } else {
                $('#netPayment').val($('#billAmount').val());
            }
        });

    });

    function changeTaxAmount() {

        $.ajax({
            url: '${contextPath}/Account/ajax',
            contentType: 'application/json',

            data: {
                taxAccountId: $('#taxDrop').val()
            },
            success: function (response) {


                if ($('#taxDrop').val() == '') {
                    $('#taxForm').css("display", "none");
                    $('#taxAmount').val("0.00");
                    $('#netPayment').val($('#billAmount').val());

                } else {
                    $('#filer').val(response[0]);
                    $('#non-filer').val(response[1]);
                    $('#filer').text('Filer - ' + response[0] + '%');
                    $('#non-filer').text('Non-Filer - ' + response[1] + '%');
                    $('#taxForm').css("display", "block");

                    var currentTaxRate = $('#taxRate').val();
                    var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
                    $('#taxAmount').val(taxAmount.toFixed(2));
                    $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));
                }

            }
        });
    }

    function remove(id) {
        if (confirm("Are you sure, you want to delete the voucher?")) {
            $('#voucherRemove' + id).submit();
        }
    }

    function getParameterByName(name, url) {
        if (!url)
            url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results)
            return null;
        if (!results[2])
            return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $(document).ready(function () {
        console.log('test')
        if (getParameterByName('id')) {
            $('html,body').animate({
                    scrollTop: $("#" + getParameterByName('id')).offset().top
                },
                'slow');
        }

    })
    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[1, "desc"]],
            "pageLength": 7
        });
    });

    function addVoucherForm() {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $(document).ajaxSend(function (e, xhr, options) {
            xhr.setRequestHeader(header, token);
        });
        var fd = $("#voucherForm").serialize();
        $.ajax({
            type: 'POST',
            async: false,
            url: '${contextPath}/Account/ajax/addVoucher',
            data: fd + "&prefix=PV",
            timeout: 600000,
            success: function (data) {
                if (form_action == "edit") {
                    refreshPage();
                }
                else {
                    updateForm(data);
                }
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }

    function updateForm(data) {
        $('#myModal').find('form').trigger('reset');
        $('#myModal').find('form').find('#voucherNumber').val(data);
    }

    function refreshPage() {
        window.location.reload();
    }

    function edit(voucherNumber) {
        console.log("hello");
        $.ajax({
            url: '${contextPath}/Account/ajax/editVoucher',
            contentType: 'application/json',
            data: {
                voucherId: voucherNumber

            },
            success: function (response) {
                editForm(response);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }

    function editForm(response) {
        $("#myModal").modal();
        $('#myModal').find('form').trigger('reset');
        $('#myModal').find('form').find('#voucherNumber').val(response.voucherNumber);
        $('#myModal').find('form').find('#bankAccount').val(response.bankAccount);
        $('#myModal').find('form').find('#accountCode').val(response.accountCode);
        $('#myModal').find('form').find('#costCentre').val(response.costCentre);
        $('#myModal').find('form').find('#taxDrop').val(response.taxAccount);
        $('#myModal').find('form').find('#billAmount').val(response.billAmount);
        $('#myModal').find('form').find('#voucherDate').val(response.voucherDate);
        $('#myModal').find('form').find('#chequeNumber').val(response.chequeNumber);
        $('#myModal').find('form').find('#taxAmount').val(response.taxAmount);
        $('#myModal').find('form').find('#debit').val(response.debit);
        $('#myModal').find('form').find('#remarks').val(response.remarks);
        $('#myModal').find('form').find('#taxRate').val(response.taxRate);
        $('#billAmount').trigger("blur");
        changeTaxAmount();

    }


</script>
</body>
</html>