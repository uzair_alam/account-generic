<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bank Reconciliation</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>

<div class="container">
    ${attendanceGenerationSuccess}
    <h2>Bank Reconciliation</h2>

        <div>
        <form method="post" action="${contextPath}/Account/reconciliation_post?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-right">

            <label> Bank Account :</label>
            <select name="account" class="form-control">
                <c:forEach items="${accounts}" var="account">
                    <option value="${account.accountCode}">${account.title}</option>

                </c:forEach>
            </select>

            <label> Voucher Status :</label>
            <select name="voucherStatus" class="form-control">

                <option value="Pending">Pending</option>
                <option value="Cleared">Cleared</option>

            </select>

           <label> Date :</label>
            <input type="Date"  class="form-control" name ="dateTill" value="${currentDate}"  required="required" />

            <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
            <input class="btn btn-primary" type="submit" value="Apply"/>
        </form>

        </div>



</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
