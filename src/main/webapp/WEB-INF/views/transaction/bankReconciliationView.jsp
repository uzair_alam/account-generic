<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bank Reconciliation</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>

<section class="main">
    <div class="container">
        <h2 class="heading-main">Bank Reconciliation</h2>

        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <div class="tableStyle">
                        <form method="post" action="${contextPath}/Account/reconciliation_post?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-right">

                        <div class="table-cell-style">
                            <p>${status} Vouchers</p>
                        </div>
                            <div class="table-cell-style">
                                <div class="fieldWithLabel Cell">
                                    <label>Bank Account</label>
                                    <select name="account" onchange="this.form.submit()">
                                        <c:forEach items="${accounts}" var="account">
                                            <c:choose>
                                                <c:when test="${account.accountCode == postAccount}">
                                                    <option value="${account.accountCode}" selected>${account.title}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${account.accountCode}">${account.title}</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        <div class="table-cell-style">
                            <div class="fieldWithLabel Cell">
                                <label>Voucher Status</label>
                                <select name="voucherStatus"  onchange="this.form.submit()" >
                                    <c:choose>
                                        <c:when test="${status == 'Pending'}">
                                            <option value="Pending" selected>Pending</option>
                                            <option value="Cleared">Cleared</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="Pending" >Pending</option>
                                            <option value="Cleared" selected>Cleared</option>
                                        </c:otherwise>
                                    </c:choose>
                                </select>
                            </div>
                        </div>
                            <div class="table-cell-style">
                            <div class="fieldWithLabel Cell">
                                <label>Date Till</label>
                                <input type="Date"   name ="dateTill" value="${currentDate}"  required="required" onchange="this.form.submit()" />
                            </div>
                        </div>
                        </form>

                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Voucher Number</th>
                            <th>Date</th>
                            <th>Account</th>
                            <th>Cheque Number</th>
                            <th>Amount</th>
                            <c:choose>
                                <c:when test="${status == 'Cleared'}">
                                    <th>Clearing Date</th>
                                </c:when>
                            </c:choose>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${vouchers}" var="voucher">

                            <tr>

                                <td>${voucher.voucherNumber}</td>
                                <td>${voucher.voucherDate}</td>
                                <td>${oMap.get(voucher.accountCode)}</td>
                                <td>${voucher.chequeNumber}</td>
                                <td>${voucher.debit + voucher.credit}</td>
                                <c:choose>
                                    <c:when test="${status == 'Cleared'}">
                                        <td>${voucher.clearingDate}</td>
                                    </c:when>
                                </c:choose>
                                <td><form method="post" action="${contextPath}/Account/voucherAction?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-right">

                                    <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                                    <input type="hidden"  name="voucherStatus"   value="${status}"/>
                                    <input type="hidden"  name="account"   value="${postAccount}"/>
                                    <input type="hidden"  name="clearDate"   value="${currentDate}"/>
                                    <input type="hidden"  name="voucherId"   value="${voucher.voucherId}"/>
                                    <c:choose>
                                        <c:when test="${status == 'Pending'}">
                                            <input class="btn btn-sm btn-success" type="submit" value="Clear"/>
                                        </c:when>
                                        <c:otherwise>
                                            <input class="btn btn-sm btn-danger" type="submit" value="Revert"/>
                                        </c:otherwise>
                                    </c:choose>

                                </form></td>
                                </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
