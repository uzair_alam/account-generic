<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title>Journal Voucher</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">
        <h2 class="heading-main">Journal Vouchers<a href="${contextPath}/Account/createJournalVoucher"><span
                class="addIcon"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></a></h2>

        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Voucher Number</th>
                            <th  onclick="refreshPage()" width="15%">Voucher Date</th>
                            <th style="display: none">Date</th>
                            <th>Debit Account</th>
                            <th>Credit Account</th>
                            <th>Amount</th>
                            <th width="10%">Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${journalVouchers}" var="voucher" varStatus="voucherStatus">
                            <c:choose>

                                <c:when test="${voucher.credit != 0.0}">
                                    <tr>
                                        <td id="${voucher.voucherNumber}"> ${voucher.voucherNumber}</td>
                                        <td><fmt:formatDate pattern="dd/MM/yyyy" value="${voucher.voucherDate}"/></td>
                                        <td style="display: none">{voucher.voucherDate}</td>
                                        <td>${accountMap.get(voucher.creditAccount)}</td>
                                        <td>${accountMap.get(voucher.accountCode)}</td>

                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${voucher.credit}"/></td>
                                        <td>
                                            <form method="post" id="voucherRemove${voucherStatus.index}"
                                                  action="${contextPath}/Account/deleteVoucher?${_csrf.parameterName}=${_csrf.token}">
                                                <input type="hidden" name="${_csrf.parameterName}"
                                                       value="${_csrf.token}"/>
                                                <input type="hidden" name="voucherNumber"
                                                       value="${voucher.voucherNumber}"/>
                                                <input type="hidden" name="redirectUrl"
                                                       value="Account/viewJournalVoucher"/>
                                            </form>
                                            <ul class="list-inline">
                                                <li><a title="Delete" href="#"
                                                       onclick="remove(${voucherStatus.index});"><img
                                                        src="${contextPath}/resources/img/deleteIcon.png" alt=""></a>
                                                </li>
                                                <li><a title="Edit"
                                                       href="${contextPath}/Account/editJournalVoucher?voucherNumber=${voucher.voucherNumber}"><img
                                                        src="${contextPath}/resources/img/editIcon.png" alt=""></a></li>
                                            </ul>
                                        </td>

                                    </tr>
                                </c:when>
                            </c:choose>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[2, "desc"]],
            "pageLength": 7
        });
    });

    function remove(id) {
        if (confirm("Are you sure, you want to delete the voucher?")) {
            $('#voucherRemove' + id).submit();
        }
    }
    function refreshPage() {
        window.location.reload();
    }

</script>
</body>
</html>
