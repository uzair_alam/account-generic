<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Nozzle</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">

        ${nozzleEditSuccess}
        ${nozzleRemoveSuccess}
        ${nozzleAddSuccess}

        <h2 class="heading-main">Nozzle Register <span class="addIcon" data-toggle="modal"
                                                       data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Date</th>
                            <th>Measure Unit</th>
                            <th>Opening Reading</th>
                            <th>Nozzle Item</th>
                            <th>Sort Order</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${nozzles}" var="nozzle" varStatus="statusNozzle">

                            <tr>
                                <td>${nozzle.nozzleName}</td>
                                <td>${nozzle.date}</td>
                                <td>${nozzle.measureUnit}</td>
                                <td>${nozzle.openingReading}</td>
                                <td>${nozzle.nozzleItem.name}</td>
                                <td>${nozzle.sortOrder}</td>
                                <td>
                                    <form method="post" id="nozzleRemove${statusNozzle.index}"
                                          action="${contextPath}/Nozzle/removeNozzle?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="nozzleCode" value="${nozzle.nozzleCode}"/>
                                    </form>
                                    <form method="post" id="nozzleEdit${statusNozzle.index}"
                                          action="${contextPath}/Nozzle/editNozzle?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="nozzleCode" value="${nozzle.nozzleCode}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a href="#" onclick="remove(${statusNozzle.index});"><img
                                                src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Nozzle Detail</h2>
            </div>

            <form:form method="POST" modelAttribute="nozzleForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="center-block">
                        <spring:bind path="nozzleName">
                            <form:label path="nozzleName">Nozzle Name</form:label>
                            <form:input id="nozzleName" type="text" path="nozzleName" placeholder="Nozzle Name"
                                        autofocus="true"></form:input>
                            <form:errors path="nozzleName"></form:errors>
                        </spring:bind>

                        <spring:bind path="openingReading">
                            <form:label path="openingReading">Opening Reading </form:label>
                            <form:input type="text" path="openingReading" placeholder="Opening Reading"
                                        autofocus="true"></form:input>
                            <form:errors path="openingReading"></form:errors>
                        </spring:bind>
                        <form:select path="stockId">
                            <c:forEach items="${stockItems}" var="item">
                                <form:option value="${item.stockId}">${item.name}</form:option>
                            </c:forEach>
                        </form:select>

                        <spring:bind path="measureUnit">
                            <form:label path="measureUnit">Mesuring Unit </form:label>
                            <form:input type="text" path="measureUnit" placeholder="Mesuring Unit"
                                        autofocus="true"></form:input>
                            <form:errors path="measureUnit"></form:errors>

                        </spring:bind>

                        <spring:bind path="sortOrder">
                            <form:label path="sortOrder">Sort Order </form:label>
                            <form:input type="text" path="sortOrder" placeholder="Sort Order"
                                        autofocus="true" value="${maxOrder}"></form:input>
                            <form:errors path="sortOrder"></form:errors>

                        </spring:bind>

                        <spring:bind path="date">
                            <form:label path="date">Nozzle Date</form:label>
                            <form:input type="date" path="date"
                                        autofocus="true" value="${currentDate}"></form:input>
                            <form:errors path="date"></form:errors>

                        </spring:bind>
                    </div>
                    </div>

                </div>

                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function () {


    });

    function remove(id, name) {
        if (confirm("Are you sure, you want to delete the Nozzle ?")) {
            $('#nozzleRemove' + id).submit();
        }
    }

    function edit(id) {
        //$('#nozzleEdit').submit();
    }

</script>
</body>
</html>
