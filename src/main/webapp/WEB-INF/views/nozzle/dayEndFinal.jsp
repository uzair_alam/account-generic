<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Day End Final</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>

<section class="main">
    <div class="container">

        <h2 class="heading-main">Daily Sales </h2>
        <div class="row">
            <div class="dayEndSec">
                <div class="col-sm-3">
                    <fieldset>
                        <legend>Cash Balance</legend>

                        <section class="">
                            <form class="form-horizontal">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Meter Sale(Litres)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" disabled value="${meterSale}">
                                    </div>
                                </div>
                            </form>
                        </section>
                    </fieldset>
                    <fieldset>
                        <legend>Cash Balance</legend>

                        <section class="">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Opening cash</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" disabled value="${cashModel.openingCash}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cash sale(Fuel/oil)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" disabled value="${cashSale}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">receipt Amount</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" disabled value="${cashModel.receiptAmount}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Total Receipt</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" disabled value="${cashModel.receiptAmount + cashSale}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Payment Deposit</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" disabled value="${cashModel.paymentDeposit}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Withdrawal</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" disabled value="${cashModel.withdrawal}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cash payments</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" disabled value="${cashModel.cashPayment}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Closing cash</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" disabled
                                               value="<fmt:formatNumber type="number" maxFractionDigits="2" value="${cashModel.openingCash + cashSale + cashModel.withdrawal + cashModel.receiptAmount - cashModel.paymentDeposit - cashModel.cashPayment}"/>">
                                    </div>
                                </div>
                            </form>
                        </section>
                    </fieldset>
                </div>
                <div class="col-sm-9">
                    <c:choose>
                    <c:when test="${showUpdate == true}">
                    <form action="${contextPath}/Nozzle/updateNozzleEntry?${_csrf.parameterName}=${_csrf.token}" method="post">
                        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                        <input type="date"   name="reportDate" id="myDate" style="display: none" />
                        <button type="submit" >Correction</button>
                    </form>
                    </c:when>
                    </c:choose>
                    <form action="${contextPath}/Nozzle/dayEndFinalPost?${_csrf.parameterName}=${_csrf.token}" method="post">
                        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                        <input type="date" class="form-control"  name="reportDate" id="currentDate" onchange="this.form.submit()" value="${currentDate}">
                    </form>
                        <section class="row">

                            <section class="col-sm-12">
                                <section class="dayEndTableScrollSec">
                                    <div class="main-table">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Test Qty</th>
                                                <th>Sale Qty</th>
                                                <th>Cash</th>
                                                <th>Credit</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${cashCreditList}" var="item">
                                                <tr>

                                                    <td>${item.item}</td>
                                                    <td>${item.testQuantity}</td>
                                                    <td>${item.saleQuantity}</td>
                                                    <td>${item.cash}</td>
                                                    <td>${item.credit}</td>
                                                    <td>${item.total}</td>
                                                </tr>
                                            </c:forEach>

                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </section>

                        </section>
                    </section>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function() {
        $('#myDate').val($('#currentDate').val());
        $('#currentDate').change(function() {
            $('#myDate').val($('#currentDate').val());

        });

    });

</script>
</body>
</html>
