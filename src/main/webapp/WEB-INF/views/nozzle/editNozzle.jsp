<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Account</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>


<section class="main">
    <div class="container">

        <div class="chartAcc">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="heading-light">Edit Nozzle</h2>
                    <div class="inputContainer">
                        <form:form method="POST" modelAttribute="nozzleForm" action="${contextPath}/Nozzle/doEditNozzle">
                        <div class="">
                            <spring:bind path="nozzleName">
                                <form:label path="nozzleName">Nozzle Name</form:label>
                                <form:input   type="text" path="nozzleName"  placeholder="Nozzle Name"></form:input>
                                <form:errors path="nozzleName"></form:errors>
                            </spring:bind>

                            <spring:bind path="openingReading">
                                <form:label path="openingReading">Opening Reading </form:label>
                                <form:input type="text" path="openingReading"  placeholder="Opening Reading"
                                            autofocus="true"></form:input>
                                <form:errors path="openingReading"></form:errors>
                            </spring:bind>

                            <spring:bind path="measureUnit">
                                <form:label path="measureUnit">Mesuring Unit </form:label>
                                <form:input type="text" path="measureUnit"  placeholder="Mesuring Unit"
                                            autofocus="true"></form:input>
                                <form:errors path="measureUnit"></form:errors>

                            </spring:bind>

                            <spring:bind path="date">
                                <form:label path="date">Nozzle Date</form:label>
                                <form:input type="date" path="date"  placeholder="Contact Person"
                                            autofocus="true" value="${currentDate}"></form:input>
                                <form:errors path="date"></form:errors>

                            </spring:bind>
                            <button type="submit">submit</button>
                            </form:form>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>


</body>
</html>
