<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Company Information</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body>
<%@ include file="header.jsp" %>
<div class="container">
    ${CompanyInfoAddSuccess}
    ${CompanyInfoAddNotSuccess}
    ${emptyCaseMessage}
    <section class="main">
        <div class="container">
            <div class="chartAcc">
                        
                        <h2 class="heading-light">Company Information</h2>
                        <div class="inputContainer">
                            <div class="staticRow">
                                <div class="col-sm-6">
                            <form:form method="POST" action="./staticinfo?${_csrf.parameterName}=${_csrf.token}"
                                     modelAttribute="staticForm" enctype="multipart/form-data">
                                <spring:bind path="companyName">
                                    <form:label path="companyName">Company Name</form:label>
                                    <form:input type="text" path="companyName" placeholder="Company Name"
                                                autofocus="true"></form:input>
                                    <form:errors path="companyName"></form:errors>

                                </spring:bind>
                                <spring:bind path="companyAddress">
                                    <form:label path="companyAddress">Company Address</form:label>
                                    <form:input type="text" path="companyAddress" placeholder="Company Address"
                                                autofocus="true"></form:input>
                                    <form:errors path="companyAddress"></form:errors>

                                </spring:bind>
                                <spring:bind path="addressSecondary">
                                    <form:label path="addressSecondary">Address line 2</form:label>
                                    <form:input type="text" path="addressSecondary" placeholder="Secondary Address"
                                                autofocus="true"></form:input>
                                    <form:errors path="addressSecondary"></form:errors>

                                </spring:bind>
                                <spring:bind path="tagLine">
                                    <form:label path="tagLine">Tag Line</form:label>
                                    <form:input type="text" path="tagLine" placeholder="Tagline"
                                                autofocus="true"></form:input>
                                    <form:errors path="tagLine"></form:errors>

                                </spring:bind>
                                <spring:bind path="phone">
                                    <form:label path="phone">Phone Number</form:label>
                                    <form:input type="text" path="phone" placeholder="Phone"
                                                autofocus="true"></form:input>
                                    <form:errors path="phone"></form:errors>

                                </spring:bind>
                                <spring:bind path="mobile">
                                    <form:label path="mobile">Mobile Number</form:label>
                                    <form:input type="text" path="mobile" placeholder="Mobile Number"
                                                autofocus="true"></form:input>
                                    <form:errors path="mobile"></form:errors>

                                </spring:bind>
                                <spring:bind path="expenseHead">
                                    <form:label path="expenseHead">Expense Account </form:label>
                                    <form:select path="expenseHead"  >
                                        <c:forEach items="${accounts}" var="account" >
                                            <form:option value="${account.accountCode}">${account.title}
                                                <c:choose>
                                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                    </c:when>
                                                </c:choose>
                                            </form:option>
                                        </c:forEach>
                                    </form:select>
                                </spring:bind>
                                <spring:bind path="postDateCheckAccount">
                                    <form:label path="postDateCheckAccount">PDC Account </form:label>
                                    <form:select path="postDateCheckAccount"  >
                                        <c:forEach items="${accounts}" var="account" >
                                            <form:option value="${account.accountCode}">${account.title}
                                                <c:choose>
                                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                    </c:when>
                                                </c:choose>
                                            </form:option>
                                        </c:forEach>
                                    </form:select>
                                </spring:bind>

                                <spring:bind path="riderHead">
                                    <form:label path="riderHead">Rider Head Account </form:label>
                                    <form:select path="riderHead"  >
                                        <c:forEach items="${accounts}" var="account" >
                                            <form:option value="${account.accountCode}">${account.title}
                                                <c:choose>
                                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                    </c:when>
                                                </c:choose>
                                            </form:option>
                                        </c:forEach>
                                    </form:select>
                                </spring:bind>

                                <spring:bind path="distributorHead">
                                    <form:label path="distributorHead">Distributor Head Account </form:label>
                                    <form:select path="distributorHead"  >
                                        <c:forEach items="${accounts}" var="account" >
                                            <form:option value="${account.accountCode}">${account.title}
                                                <c:choose>
                                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                    </c:when>
                                                </c:choose>
                                            </form:option>
                                        </c:forEach>
                                    </form:select>
                                </spring:bind>

                                <spring:bind path="bankHead">
                                    <form:label path="bankHead">Bank Account </form:label>
                                    <form:select path="bankHead"  >
                                        <c:forEach items="${accounts}" var="account" >
                                            <form:option value="${account.accountCode}">${account.title}
                                                <c:choose>
                                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                    </c:when>
                                                </c:choose>
                                            </form:option>
                                        </c:forEach>
                                    </form:select>
                                </spring:bind>
                                <spring:bind path="customerHead">
                                    <form:label path="customerHead">Customer Account </form:label>
                                    <form:select path="customerHead"  >
                                        <c:forEach items="${accounts}" var="account" >
                                            <form:option value="${account.accountCode}">${account.title}
                                                <c:choose>
                                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                    </c:when>
                                                </c:choose>
                                            </form:option>
                                        </c:forEach>
                                    </form:select>
                                </spring:bind>
                                <spring:bind path="vendorHead">
                                    <form:label path="vendorHead">Vendor Account </form:label>
                                    <form:select path="vendorHead"  >
                                        <c:forEach items="${accounts}" var="account" >
                                            <form:option value="${account.accountCode}">${account.title}
                                                <c:choose>
                                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                    </c:when>
                                                </c:choose>
                                            </form:option>
                                        </c:forEach>
                                    </form:select>
                                </spring:bind>
                                <spring:bind path="productHead">
                                    <form:label path="productHead">Stock Account </form:label>
                                    <form:select path="productHead"  >
                                        <c:forEach items="${accounts}" var="account" >
                                            <form:option value="${account.accountCode}">${account.title}
                                                <c:choose>
                                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                    </c:when>
                                                </c:choose>
                                            </form:option>
                                        </c:forEach>
                                    </form:select>
                                </spring:bind>
                                <spring:bind path="cashInHand">
                                    <form:label path="cashInHand">Cash Account </form:label>
                                    <form:select path="cashInHand"  >
                                        <c:forEach items="${accounts}" var="account" >
                                            <form:option value="${account.accountCode}">${account.title}
                                                <c:choose>
                                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                    </c:when>
                                                </c:choose>
                                            </form:option>
                                        </c:forEach>
                                    </form:select>
                                </spring:bind>
                                </div>
                                <div class="col-sm-6">
                                <spring:bind path="prefixBill">
                                    <form:label path="prefixBill">Prefix For Bill </form:label>
                                    <form:input type="text" path="prefixBill" placeholder="Bill Prefix"
                                                autofocus="true"></form:input>
                                    <form:errors path="prefixBill"></form:errors>

                                </spring:bind>
                                <spring:bind path="prefixDc">
                                    <form:label path="prefixDc">Prefix For DC </form:label>
                                    <form:input type="text" path="prefixDc" placeholder="Delivery Challan Prefix"
                                                autofocus="true"></form:input>
                                    <form:errors path="prefixDc"></form:errors>

                                </spring:bind>
                                <spring:bind path="prefixGRN">
                                    <form:label path="prefixGRN">Prefix For GRN </form:label>
                                    <form:input type="text" path="prefixGRN" placeholder="GRN Prefix"
                                                autofocus="true"></form:input>
                                    <form:errors path="prefixGRN"></form:errors>

                                </spring:bind>
                                <spring:bind path="minBillNumber">
                                    <form:label path="minBillNumber">Minimum Bill Number </form:label>
                                    <form:input type="text" path="minBillNumber" placeholder="Minimum Bill Number"
                                                autofocus="true"></form:input>
                                    <form:errors path="minBillNumber"></form:errors>

                                </spring:bind>
                                <spring:bind path="minDcNumber">
                                    <form:label path="minDcNumber">Minimum DC Number </form:label>
                                    <form:input type="text" path="minDcNumber" placeholder="Minimum DC number"
                                                autofocus="true"></form:input>
                                    <form:errors path="minDcNumber"></form:errors>

                                </spring:bind>
                                <spring:bind path="nationalTaxNumber">
                                    <form:label path="nationalTaxNumber">NTN# </form:label>
                                    <form:input type="text" path="nationalTaxNumber" placeholder="National Tax Number"
                                                autofocus="true"></form:input>
                                    <form:errors path="nationalTaxNumber"></form:errors>

                                </spring:bind>
                                <spring:bind path="salesTaxNumber">
                                    <form:label path="salesTaxNumber">STRN # </form:label>
                                    <form:input type="text" path="salesTaxNumber" placeholder="Sales Tax Number"
                                                autofocus="true"></form:input>
                                    <form:errors path="salesTaxNumber"></form:errors>

                                </spring:bind>
                                <spring:bind path="maxFormRows">
                                    <form:label path="maxFormRows">Form Rows Limit </form:label>
                                    <form:input type="text" path="maxFormRows" placeholder="Form Rows Limit"
                                                autofocus="true"></form:input>
                                    <form:errors path="maxFormRows"></form:errors>

                                </spring:bind>

                                <spring:bind path="logoFile">
                                    <form:label path="logoFile">Select File to upload Logo</form:label>
                                    <form:input type="file" path="logoFile" placeholder="file"
                                                autofocus="true" value="${staticForm.imageUrl}"></form:input>
                                </spring:bind>

                                <spring:bind path="footerFile">
                                    <form:label path="logoFile">Select File to upload Bottom image</form:label>
                                    <form:input type="file" path="footerFile" placeholder="file"
                                                autofocus="true" value="${staticForm.footerImageUrl}"></form:input>
                                </spring:bind>

                                    <form:input type="hidden" path="imagePath"  value="${contextPath}/resources/img" ></form:input>

                                <button type="submit">Submit</button>


                            </form:form>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
</div>

</div>
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>

</html>
