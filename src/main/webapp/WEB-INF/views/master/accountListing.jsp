<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <title>Chart of Accounts</title>
</head>
<body>
<%@ include file = "../header.jsp" %>

<section class="main">
    <div class="container">
        ${AccountEditSuccess}
        ${accountRemoveSuccess}

        <h2 class="heading-main">Account Listing <a href="${contextPath}/Account/addAccount"><span class="addIcon" data-toggle="modal" data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></a></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Account Number </th>
                            <th>Opening Date </th>
                            <th>Title</th>
                            <th>Opening Balance</th>
                            <th>Parent Account</th>
                            <th>Action</th>

                        </tr>
                        </thead>


                    </table>
                </div>
                <div class="customPagi">


                </div>
            </div>
        </div>
    </div>
</section>



<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>
    function remove(id) {
        if(confirm("Are you sure, you want to delete the Account?")) {
            $('#accountRemove' + id).submit();
        }
    }

    function edit(id) {

        $('#accountEdit' + id).submit();
    }
    $(document).ready(function () {
        $('#dataTable').DataTable({
            searchDelay: 900,
            "processing": true,
            "serverSide": true,
            "order": [[2, "desc"]],
            "pageLength": 10,
            "ajax": "${contextPath}/Account/json"

        });
    });
    function refreshPage() {
        window.location.reload();
    }


</script>
</body>
</html>