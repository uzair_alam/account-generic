<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <title>Bank Details</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">
        ${bankRemoveSuccess}
        ${AccountAddSuccess}
        <h2 class="heading-main">Bank Details <span class="addIcon" data-toggle="modal" data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Update</span>
            <span class="addIcon" data-toggle="modal"
                  data-target="#myModal2"><i><img
                    src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Bank Name </br> Branch Name</th>
                            <th>Bank Account Number</br> Branch Code</th>
                            <th>Contact Person</br> Mobile Number</th>
                            <th>Authorized Person </br> Landline#</th>
                            <th>Bank URL</br> Email Address</th>
                            <th>Bank NTN</br> opening Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${bankDetailList}" var="bank" varStatus="status">
                            <tr>
                                <td>${bank.account.title} </br>${bank.bankBranch}</td>
                                <td>${bank.bankAccount} </br>${bank.branchCode}</td>
                                <td>${bank.bankContactPerson} </br>${bank.mobileNumber}</td>
                                <td><c:forEach items="${bank.authorizedPersonList}"
                                               var="auth">${auth.name},</c:forEach> </br>${bank.bankLandline}</td>
                                <td>${bank.url} </br>${bank.emailAddress}</td>
                                <td>${bank.bankNtn} </br>${bank.accountOpeningDate}</td>
                                <td>
                                    <form method="post" id="bankRemove${expenseStatus.index}"
                                          action="${contextPath}/Account/removeBank?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="accountCode"
                                               value="${expense.expenseAccount.accountCode}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a title="Edit" href="#" data-toggle="modal" data-target="#myModal"
                                               onclick="bankFormView(${bank.account.accountCode})"><img
                                                src="${contextPath}/resources/img/editIcon.png" alt=""></a></li>
                                        <li><a title="Delete" href="#" onclick="remove(${status.index})"><img src="${contextPath}/resources/img/deleteIcon.png" alt=""></a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Client Detail</h2>
            </div>
            <form:form method="POST" modelAttribute="bankDetailForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="accountCode">
                                <form:label path="accountCode">Account</form:label>
                                <form:select id="accountDrop" path="accountCode">
                                    <c:forEach items="${accounts}" var="account">
                                        <form:option value="${account.accountCode}">${account.title}
                                            <c:choose>
                                                <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                </c:when>
                                            </c:choose></form:option>
                                    </c:forEach>
                                </form:select>
                            </spring:bind>

                            <spring:bind path="bankAccount">

                                <form:label path="bankAccount">Bank Account</form:label>
                                <form:input id="bankAccount" type="text" path="bankAccount" placeholder="Bank Account"
                                            autofocus="true"></form:input>
                                <form:errors path="bankAccount"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankBranch">

                                <form:label path="bankBranch">Branch Name</form:label>
                                <form:input id="bankBranch" type="text" path="bankBranch" placeholder="Branch Name"
                                            autofocus="true"></form:input>
                                <form:errors path="bankBranch"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankContactPerson">

                                <form:label path="bankContactPerson">Contact Person</form:label>
                                <form:input id="bankContactPerson" type="text" path="bankContactPerson"
                                            placeholder="Contact Person"
                                            autofocus="true"></form:input>
                                <form:errors path="bankContactPerson"></form:errors>

                            </spring:bind>
                            <spring:bind path="mobileNumber">

                                <form:label path="mobileNumber">Mobile Number</form:label>
                                <form:input id="mobileNumber" type="text" path="mobileNumber"
                                            placeholder="Mobile Number"
                                            autofocus="true"></form:input>
                                <form:errors path="mobileNumber"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankLandline">

                                <form:label path="bankLandline">Landline#</form:label>
                                <form:input id="bankLandline" type="text" path="bankLandline"
                                            placeholder="Landline number"
                                            autofocus="true"></form:input>
                                <form:errors path="bankLandline"></form:errors>

                            </spring:bind>
                            <spring:bind path="emailAddress">

                                <form:label path="emailAddress">Email Address</form:label>
                                <form:input id="emailAddress" type="text" path="emailAddress"
                                            placeholder="Email Address"
                                            autofocus="true"></form:input>
                                <form:errors path="emailAddress"></form:errors>

                            </spring:bind>
                        </div>
                        <div class="col-sm-6">
                            <spring:bind path="authorizedPersons">

                                <form:label path="authorizedPersons">Authorized Persons (Comma separated)</form:label>
                                <form:input id="authorizedPersons" type="text" path="authorizedPersons"
                                            placeholder="Email Address"
                                            autofocus="true"></form:input>
                                <form:errors path="authorizedPersons"></form:errors>

                            </spring:bind>
                            <spring:bind path="scanForm">

                                <form:label path="scanForm">Scan Copy</form:label>
                                <form:input id="scanForm" type="text" path="scanForm" placeholder="Scan Copy"
                                            autofocus="true"></form:input>
                                <form:errors path="scanForm"></form:errors>

                            </spring:bind>

                            <spring:bind path="accountOpeningDate">

                                <form:label path="accountOpeningDate">Opening Date</form:label>
                                <form:input id="accountOpeningDate" type="date" path="accountOpeningDate" value="${currentDate}"
                                            autofocus="true"></form:input>
                                <form:errors path="accountOpeningDate"></form:errors>

                            </spring:bind>

                            <spring:bind path="branchCode">

                                <form:label path="branchCode">Branch Code</form:label>
                                <form:input id="branchCode" type="text" path="branchCode" placeholder="Branch Code"
                                            autofocus="true"></form:input>
                                <form:errors path="branchCode"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankNtn">

                                <form:label path="bankNtn">Bank NTN</form:label>
                                <form:input id="bankNtn" type="text" path="bankNtn" placeholder="Bank NTN"
                                            autofocus="true"></form:input>
                                <form:errors path="bankNtn"></form:errors>

                            </spring:bind>

                            <spring:bind path="url">

                                <form:label path="url">Bank URL</form:label>
                                <form:input id="url" type="text" path="url" placeholder="Bank URL"
                                            autofocus="true"></form:input>
                                <form:errors path="url"></form:errors>

                            </spring:bind>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel2">Bank Details</h2>
            </div>
            <form:form method="POST" modelAttribute="accountForm" action="${contextPath}/AccountSupport/addBankAccount">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="title">
                                <form:label path="title">Account Title</form:label>
                                <form:input type="text" path="title" placeholder="Account Title"
                                            autofocus="true"></form:input>
                                <form:errors path="title"></form:errors>

                            </spring:bind>
                            <spring:bind path="parentCode">

                                <form:input type="hidden" path="parentCode" value="${bankHead}"
                                            autofocus="true"></form:input>
                                <form:errors path="parentCode"></form:errors>

                            </spring:bind>

                            <spring:bind path="openingBalance">
                                <form:label path="openingBalance">Opening Balance </form:label>
                                <form:input id="openingBalance" onblur="toggleAccountType()" type="text"
                                            path="openingBalance" placeholder="Opening Balance"></form:input>
                                <form:errors path="openingBalance"></form:errors>

                            </spring:bind>


                            <label>Select Account Type </label>
                            <select id="accountType">
                                <option id="credit" value="credit">Credit</option>
                                <option id="debit" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingBalanceLast">

                                <form:label path="openingBalanceLast">Opening Balance Last </form:label>
                                <form:input id="openingBalanceLast" onblur="toggleAccountTypeLast()" type="text"
                                            path="openingBalanceLast"
                                            placeholder="Last Opening Balance"></form:input>
                                <form:errors path="openingBalanceLast"></form:errors>

                            </spring:bind>
                            <label>Select Account Type Last </label>
                            <select id="accountTypeLast">
                                <option id="creditLast" value="credit">Credit</option>
                                <option id="debitLast" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingCredit">
                                <form:input type="hidden" path="openingCredit" id="openingCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="openingDebit">

                                <form:input type="hidden" path="openingDebit" id="openingDebit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningCredit">
                                <form:input type="hidden" path="lastOpeningCredit" id="lastOpeningCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningDebit">

                                <form:input type="hidden" path="lastOpeningDebit" id="lastOpeningDebit"
                                            value="0.0"></form:input>

                            </spring:bind>

                            <spring:bind path="openingDate">
                                <form:label path="openingDate">Opening Date</form:label>
                                <form:input type="Date" path="openingDate" value="${currentDate}"
                                            required="required"></form:input>
                                <form:errors path="openingDate"></form:errors>

                            </spring:bind>


                            <spring:bind path="bankDetails.bankAccount">

                                <form:label path="bankDetails.bankAccount">Bank Account</form:label>
                                <form:input id="bankAccount" type="text" path="bankDetails.bankAccount" placeholder="Bank Account"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.bankAccount"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankDetails.bankBranch">

                                <form:label path="bankDetails.bankBranch">Branch Name</form:label>
                                <form:input id="bankBranch" type="text" path="bankDetails.bankBranch" placeholder="Branch Name"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.bankBranch"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankDetails.bankContactPerson">

                                <form:label path="bankDetails.bankContactPerson">Contact Person</form:label>
                                <form:input id="bankContactPerson" type="text" path="bankDetails.bankContactPerson"
                                            placeholder="Contact Person"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.bankContactPerson"></form:errors>

                            </spring:bind>
                        </div>

                        <div class="col-sm-6">
                            <spring:bind path="bankDetails.mobileNumber">

                                <form:label path="bankDetails.mobileNumber">Mobile Number</form:label>
                                <form:input id="mobileNumber" type="text" path="bankDetails.mobileNumber"
                                            placeholder="Mobile Number"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.mobileNumber"></form:errors>

                            </spring:bind>

                            <spring:bind path="bankDetails.bankLandline">

                                <form:label path="bankDetails.bankLandline">Landline#</form:label>
                                <form:input id="bankLandline" type="text" path="bankDetails.bankLandline"
                                            placeholder="Landline number"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.bankLandline"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankDetails.emailAddress">

                                <form:label path="bankDetails.emailAddress">Email Address</form:label>
                                <form:input id="emailAddress" type="text" path="bankDetails.emailAddress"
                                            placeholder="Email Address"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.emailAddress"></form:errors>

                            </spring:bind>
                        </div>
                        <div class="col-sm-6">
                            <spring:bind path="bankDetails.authorizedPersons">

                                <form:label path="bankDetails.authorizedPersons">Authorized Persons (Comma separated)</form:label>
                                <form:input id="authorizedPersons" type="text" path="bankDetails.authorizedPersons"
                                            placeholder="Email Address"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.authorizedPersons"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankDetails.scanForm">

                                <form:label path="bankDetails.scanForm">Scan Copy</form:label>
                                <form:input id="scanForm" type="text" path="bankDetails.scanForm" placeholder="Scan Copy"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.scanForm"></form:errors>

                            </spring:bind>

                            <spring:bind path="bankDetails.accountOpeningDate">

                                <form:label path="bankDetails.accountOpeningDate">Opening Date</form:label>
                                <form:input id="accountOpeningDate" type="date" path="bankDetails.accountOpeningDate" value="${currentDate}"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.accountOpeningDate"></form:errors>

                            </spring:bind>

                            <spring:bind path="bankDetails.branchCode">

                                <form:label path="bankDetails.branchCode">Branch Code</form:label>
                                <form:input id="branchCode" type="text" path="bankDetails.branchCode" placeholder="Branch Code"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.branchCode"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankDetails.bankNtn">

                                <form:label path="bankDetails.bankNtn">Bank NTN</form:label>
                                <form:input id="bankNtn" type="text" path="bankDetails.bankNtn" placeholder="Bank NTN"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.bankNtn"></form:errors>

                            </spring:bind>

                            <spring:bind path="bankDetails.url">

                                <form:label path="bankDetails.url">Bank URL</form:label>
                                <form:input id="url" type="text" path="bankDetails.url" placeholder="Bank URL"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.url"></form:errors>

                            </spring:bind>

                        </div>

                    </div>
                </div>
                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</div>

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[1, "desc"]],
            "pageLength": 7
        });
        $('#accountDrop').change(function () {
            bankAjaxCall();
        });

    });

    function remove(id) {
        if (confirm("Are you sure, you want to delete?")) {
            $('#bankRemove' + id).submit();
        }


    }

    function bankAjaxCall() {
        $.ajax({
            url: '${contextPath}/User/addBankDetailsAjax',
            contentType: 'application/json',

            data: {
                accountId: $('#accountDrop').val()
            },
            success: function (response) {
                if (response[0] != "NO-DATA") {
                    $('#bankAccount').val(response[0]);
                    $('#bankBranch').val(response[1]);
                    $('#bankContactPerson').val(response[2]);
                    $('#mobileNumber').val(response[3]);
                    $('#bankLandline').val(response[4]);
                    $('#emailAddress').val(response[5]);
                    $('#authorizedPersons').val(response[6]);
                    $('#scanForm').val(response[7]);
                    $('#accountOpeningDate').val(response[8]);
                    $('#branchCode').val(response[9]);
                    $('#bankNtn').val(response[10]);
                    $('#url').val(response[11]);
                } else {
                    $('#bankAccount').val('');
                    $('#bankBranch').val('');
                    $('#bankContactPerson').val('');
                    $('#mobileNumber').val('');
                    $('#bankLandline').val('');
                    $('#emailAddress').val('');
                    $('#authorizedPersons').val('');
                    $('#scanForm').val('');
                    $('#accountOpeningDate').val("${currentDate}");
                    $('#branchCode').val('');
                    $('#bankNtn').val('');
                    $('#url').val('');
                }

            }
        });
    }

    function bankFormView(id) {
        var accounts = document.getElementById('accountDrop'), i;
        for (i = 0; i < accounts.length; i++) {
            if (accounts[i].value == id) {
                $('#accountDrop').prop('selectedIndex', i);
                bankAjaxCall();
            }
        }
    }

</script>
</body>
</html>