<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Account</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>


<section class="main">
    <div class="container">
        ${taxDetailAddSuccess}
        ${taxDetailRemoveSuccess}
        <div class="chartAcc">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="heading-light">Add Taxation Details</h2>
                    <div class="inputContainer">
                        <form:form method="POST" modelAttribute="taxDetailForm" >
                        <div class="">
                            <spring:bind path="accountCode">

                                    <form:label path="accountCode">Account</form:label>
                                    <form:select path="accountCode"  >
                                        <c:forEach items="${accounts}" var="account" >
                                            <form:option value="${account.accountCode}">${account.title}</form:option>
                                        </c:forEach>
                                    </form:select>

                            </spring:bind>

                            <spring:bind path="filerAmount">

                                    <form:label path="filerAmount">Filer Rate</form:label>
                                    <form:input type="text" path="filerAmount"  placeholder="Percentage for Filer"></form:input>
                                    <form:errors path="filerAmount"></form:errors>

                            </spring:bind>
                            <spring:bind path="nonFilerAmount">
                                    <form:label path="nonFilerAmount">Non-Filer Rate</form:label>
                                    <form:input type="text" path="nonFilerAmount"  placeholder="Percentage for Non-Filer"></form:input>
                                    <form:errors path="nonFilerAmount"></form:errors>

                            </spring:bind>

                            <spring:bind path="effectiveTill">

                                    <form:label path="effectiveTill">Effective Till</form:label>
                                    <form:input type="Date" path="effectiveTill" required="required"></form:input>
                                    <form:errors path="effectiveTill"></form:errors>
                            </spring:bind>

                            <button type="submit">submit</button>
                            </form:form>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <h2 class="heading-light">Tax Accounts</h2>
                    <div class="">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Account</th>
                                <th>Filer Rate</th>
                                <th>Non-Filer Rate</th>
                                <th>Effective Till</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${taxDetails}" var="detail" varStatus="status">

                                <tr>
                                    <td>${detail.baseAccount.title}</td>
                                    <td>${detail.filerAmount}</td>
                                    <td>${detail.nonFilerAmount}</td>
                                    <td>${detail.effectiveTill}</td>
                                    <td><form method="post" action="${contextPath}/Account/removeDetail?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-right">
                                        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                                        <input  type="hidden" name="accountCode" value="${detail.baseAccount.accountCode}"/>
                                        <input class="btnSmall red" type="submit" value="Remove"/>
                                    </form></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>


</body>
</html>
