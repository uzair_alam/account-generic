<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Title - Stage 1</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>

<div class="container">
    ${titleSaveSuccess}
    ${reportRemoveSuccess}


    <div class="row">
        <div class="col-md-12">
            <form:form method="POST" modelAttribute="reportForm" class="form-signin">
                <h2 class="form-signin-heading"> BS/PL Report</h2>
                <h3 class="form-signin-heading"> Generate Report Title</h3>
                <spring:bind path="title">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:label path="title">Report Title</form:label>
                        <form:input type="text" path="title" class="form-control" placeholder="Report Title"
                                    autofocus="true"></form:input>
                        <form:errors path="title"></form:errors>
                    </div>
                </spring:bind>

                <spring:bind path="printSeq">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:label path="printSeq">Printing Sequence</form:label>
                        <form:input type="text" path="printSeq" class="form-control" placeholder="Printing Sequence"
                                    autofocus="true"></form:input>
                        <form:errors path="printSeq"></form:errors>
                    </div>
                </spring:bind>
                <spring:bind path="reportType">
                    <div class="form-group">
                        <form:label path="reportType">Report Type</form:label>
                        <form:select path="reportType" class="form-control" >

                            <form:option value="ProfitAndLoss">Profit And Loss</form:option>
                            <form:option value="BalanceSheet">Balance Sheet</form:option>

                        </form:select>

                    </div>
                </spring:bind>
                <spring:bind path="date">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:label path="date">Creation Date</form:label>
                        <form:input type="Date" path="date" class="form-control" value = "${currentDate}" required = "required"></form:input>
                        <form:errors path="date"></form:errors>
                    </div>
                </spring:bind>

                <spring:bind path="subTotal">
                 <form:label path="subTotal">Is Sub-Total</form:label>
                <form:checkbox path="subTotal"  />
                </spring:bind>

                <spring:bind path="total">
                    <form:label path="total">Is Total</form:label>
                    <form:checkbox path="total"  />
                </spring:bind>

                <spring:bind path="working">
                    <form:label path="working">Is Working</form:label>
                    <form:checkbox path="working"  />
                </spring:bind>

                <button class="btn btn-lg btn-primary btn-block" type="submit">Initiate</button>
            </form:form>
        </div>



    </div>
</div>
<div class="container">
    <h3>Reports</h3>

    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Heading</th>
            <th>Date</th>
            <th>Printing Sequence</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${reports}" var="report" varStatus="status">

            <tr>
                <td>${report.title}</td>
                <td>${report.date}</td>
                <td>${report.printSeq}</td>

                <td ><a class="btn btn-xs btn-primary" href="${contextPath}/Account/accountReport?reportId=${report.stageId}" >Add Notes</a>
                    <form method="post" action="${contextPath}/User/removeReport?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-left">
                        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                        <input  type="hidden" name="reportId" value="${report.stageId}"/>
                        <input class="btn btn-xs btn-danger" type="submit" value="Remove"/>
                    </form></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

</div>


<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function() {

    });



</script>
</body>
</html>
