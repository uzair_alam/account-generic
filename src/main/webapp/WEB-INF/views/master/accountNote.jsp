<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Manage Note</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>

<div class="container">
    ${AccountNoteAddSuccess}
    ${accountNoteRemoveSuccess}

    <div class="row">
        <div class="col-md-6">
            <form:form method="POST" modelAttribute="AccountNoteForm" class="form-signin">
                <h3 class="form-signin-heading">Account Note ${AccountNoteForm.heading}</h3>
                <spring:bind path="heading">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:label path="heading">Note Heading</form:label>
                        <form:input type="text" path="heading" class="form-control" placeholder="Note Heading"
                                    autofocus="true"></form:input>
                        <form:errors path="heading"></form:errors>
                    </div>
                </spring:bind>


                <spring:bind path="reportType">
                    <div class="form-group">
                        <form:label path="reportType">Report Type</form:label>
                        <form:select path="reportType" class="form-control" >

                            <form:option value="ProfitAndLoss">Profit And Loss</form:option>
                            <form:option value="BalanceSheet">Balance Sheet</form:option>

                        </form:select>

                    </div>
                </spring:bind>

        </div>
        <div class="col-md-6 form-signin">
            <spring:bind path="printSeq">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <form:label path="printSeq">Printing Sequence</form:label>
                    <form:input type="text" path="printSeq" class="form-control" placeholder="Printing Sequence"
                                autofocus="true"></form:input>
                    <form:errors path="printSeq"></form:errors>
                </div>
            </spring:bind>

            <spring:bind path="date">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <form:label path="date">Creation Date</form:label>
                    <form:input type="Date" path="date" class="form-control" value = "${currentDate}" required = "required"></form:input>
                    <form:errors path="date"></form:errors>
                </div>
            </spring:bind>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Commit</button>
            </form:form>
                </div>


    </div>
</div>
<div class="container">
    <h3>Account SubHeadings</h3>

    <table border="1px black" class="table table-condensed" >
        <thead>
        <tr>
            <th width="25%">Heading<span class="pull-right">Level Action</span></th>
            <th width="15%">Add Accounts</th>
            <th>Sub-heading Accounts</th>


        </tr>
        </thead>
        <tbody>
        <c:forEach items="${AccountNoteForm.noteSubHeadings}" var="subHead" varStatus="status">

            <tr>
                <td><form:input type="text" path="AccountNoteForm.noteSubHeadings[${status.index}].heading"   autofocus="true"></form:input>
                <form:select path="AccountNoteForm.noteSubHeadings[${status.index}].level" disabled="true">
                    <form:option value="1">1</form:option>
                    <form:option value="2">2</form:option>
                    <form:option value="3">3</form:option>
                    <form:option value="4">4</form:option>
                </form:select>

                    <form:select path="AccountNoteForm.noteSubHeadings[${status.index}].operation" disabled="true">
                    <form:option value="Credit">Credit</form:option>
                    <form:option value="Debit">Debit</form:option>
                    <form:option value="Net">Net</form:option>
                    </form:select>

                    <form method="post" action="${contextPath}/Account/removeNoteSubHead?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-right">
                        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                        <input  type="hidden" name="subId" value="${subHead.subId}"/>
                        <input  type="hidden" name="noteId" value="${noteId}"/>
                    <input class="btn btn-xs btn-danger " type="submit" value="Remove"/>
                    </form></td>
                <td >
                    <form method="post" action="${contextPath}/Account/addSubHeadAccount?${_csrf.parameterName}=${_csrf.token}" >
                        <select name="accountCode" style="width: 65%;">
                            <c:forEach items="${levelAccounts.get(subHead.subId)}" var="account" >
                                <option value="${account.accountCode}">${account.title}</option>
                            </c:forEach>
                        </select>
                        <input  type="hidden" name="subId" value="${subHead.subId}"/>
                        <input  type="hidden" name="noteId" value="${AccountNoteForm.noteId}"/>
                        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                        <button class="btn btn-xs btn-primary" type="submit">Add</button>
                    </form></br>
                </td>

                <td ><c:forEach items="${subHead.accounts}" var="account">
                    <form method="post" action="${contextPath}/Account/removeSubHeadAccount?${_csrf.parameterName}=${_csrf.token}" >
                            ${account.title}<input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                    <input  type="hidden" name="subId" value="${subHead.subId}"/>
                    <input  type="hidden" name="noteId" value="${AccountNoteForm.noteId}"/>
                    <input  type="hidden" name="accountCode" value="${account.accountCode}"/>
                    <input class="btn btn-xs btn-danger pull-right" type="submit" value="Remove"/>
                </form></br>
                </c:forEach>
                </td>



            </tr>
        </c:forEach>


        <tr>

            <td>
                <form method="POST" action="${contextPath}/Account/addSubheading?${_csrf.parameterName}=${_csrf.token}">

                    <input type="text" name="heading"  placeholder="Subheading" />
                    <select name="level"  >
                        <option value="1">1<option>
                        <option value="2">2<option>
                        <option value="3">3<option>
                        <option value="4">4<option>
                    </select>
                    <select name="operation"  >
                        <option value="Credit">Credit<option>
                        <option value="Debit">Debit<option>
                        <option value="Net">Net<option>
                    </select>
                    <input  type="hidden" name="noteId" value="${AccountNoteForm.noteId}"/>
                    <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                    <input class="btn btn-xs btn-primary pull-right" type="submit" value="Add" />
                </form>
            </td>
            <td></td>
            <td></td>

        </tr>
        </tbody>
    </table>

</div>


<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function() {

    });



</script>
</body>
</html>
