<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Account</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>

<div class="container">
    ${AccountAddSuccess}



    <section class="main">
        <div class="container">
            <div class="chartAcc">
                <div class="row">
                    <div class="col-sm-6">
                        <h2 class="heading-light">Chart Of Account</h2>
                        <div class="inputContainer">
                            <form:form method="POST" modelAttribute="accountEditForm" action="${contextPath}/Account/doEditAccount">
                            <div class="">
                                <spring:bind path="title">

                                    <form:input type="text" path="title"  placeholder="Account Title"
                                                autofocus="true"></form:input>
                                    <form:errors path="title"></form:errors>

                                </spring:bind>



                                <spring:bind path="openingBalance">
                                    <form:label path="openingBalance">Opening Balance </form:label>
                                    <form:input  id="openingBalance" onblur="toggleAccountType()" type="text" path="openingBalance"  value="${accountEditForm.openingDebit + accountEditForm.openingCredit}"></form:input>
                                    <form:errors path="openingBalance"></form:errors>

                                </spring:bind>


                                <label>Select Account Type </label>
                                <select id = "accountType" >
                                    <c:choose>
                                        <c:when test="${accountEditForm.openingCredit > accountEditForm.openingDebit}">
                                            <option id = "credit" value="credit" selected>Credit</option>
                                            <option id= "debit" value="debit">Debit</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option id = "credit" value="credit">Credit</option>
                                            <option id= "debit" value="debit" selected>Debit</option>
                                        </c:otherwise>
                                    </c:choose>

                                </select>

                                <spring:bind path="openingBalanceLast">

                                    <form:label path="openingBalanceLast">Opening Balance Last </form:label>
                                    <form:input  id="openingBalanceLast" onblur="toggleAccountTypeLast()" type="text" path="openingBalanceLast"  placeholder="Last Opening Balance"></form:input>
                                    <form:errors path="openingBalanceLast"></form:errors>

                                </spring:bind>
                                <label>Select Account Type Last </label>
                                <select id = "accountTypeLast"  >
                                    <option id = "creditLast" value="credit">Credit</option>
                                    <option id= "debitLast" value="debit">Debit</option>

                                </select>

                                <spring:bind path="openingCredit">
                                    <form:input type="hidden" path="openingCredit"  id="openingCredit"  value="0.0"></form:input>

                                </spring:bind>
                                <spring:bind path="openingDebit">

                                    <form:input type="hidden" path="openingDebit"  id="openingDebit" value="0.0" ></form:input>

                                </spring:bind>
                                <spring:bind path="lastOpeningCredit">
                                    <form:input type="hidden" path="lastOpeningCredit"  id="lastOpeningCredit"  value="0.0"></form:input>

                                </spring:bind>
                                <spring:bind path="lastOpeningDebit">

                                    <form:input type="hidden" path="lastOpeningDebit" id="lastOpeningDebit" value="0.0" ></form:input>

                                </spring:bind>

                                <spring:bind path="openingDate">

                                    <form:input type="Date" path="openingDate"  required = "required"></form:input>
                                    <form:errors path="openingDate"></form:errors>

                                </spring:bind>

                                <button type="submit">submit</button>
                                </form:form>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>



</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function() {

        toggleAccountType();
        toggleAccountTypeLast();


        $('#accountType').change(function() {

            toggleAccountType();
        });
        $('#accountTypeLast').change(function() {

            toggleAccountTypeLast();
        });
    });

    function toggleAccountType() {

        if($('#accountType').val() == "credit"){

            $('#openingCredit').val($('#openingBalance').val());
            $('#openingDebit').val('0.0');
        }else if($('#accountType').val() == "debit"){

            $('#openingDebit').val($('#openingBalance').val());
            $('#openingCredit').val('0.0');
        }
    }

    function toggleAccountTypeLast() {

        if($('#accountTypeLast').val() == "credit"){

            $('#lastOpeningCredit').val($('#openingBalanceLast').val());
            $('#lastOpeningDebit').val('0.0');
        }else if($('#accountTypeLast').val() == "debit"){

            $('#lastOpeningDebit').val($('#openingBalanceLast').val());
            $('#lastOpeningCredit').val('0.0');
        }
    }


</script>
</body>
</html>
