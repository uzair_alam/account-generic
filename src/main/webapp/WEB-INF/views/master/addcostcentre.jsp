<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Account</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${CostCentreAddSuccess}

        <div class="chartAcc">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="heading-light">Cost Centres</h2>
                    <div class="inputContainer">
                        <form:form method="POST" modelAttribute="costCentreForm">
                        <div class="">
                            <spring:bind path="title">

                                <form:label path="title">Cost Center Account</form:label>
                                <form:input type="text" path="title" placeholder="Cost Center Account"
                                            autofocus="true"></form:input>
                                <form:errors path="title"></form:errors>

                            </spring:bind>

                            <spring:bind path="parentAccountCode">

                                <form:label path="parentAccountCode">Account Name</form:label>
                                <form:select path="parentAccountCode">
                                    <c:forEach items="${accounts}" var="account">

                                        <form:option value="${account.accountCode}">${account.title}
                                            <c:choose>
                                                <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                </c:when>
                                            </c:choose></form:option>
                                    </c:forEach>
                                </form:select>

                            </spring:bind>

                            <button type="submit">submit</button>
                            </form:form>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hierarchyContainer">
                        <div class="Viewhierarchy">
                            <ol>
                                <c:forEach items="${baseAccounts}" var="baseAccount">

                                    <li><strong>${baseAccount.title}</strong></li>
                                    <ul style=" list-style:square inside;">
                                        <c:forEach items="${baseAccount.childList}" var="subControl">
                                            <li>${subControl.title}
                                                <ul>
                                                    <c:forEach items="${subControl.childList}" var="subSubControl">
                                                        <li>${subSubControl.title}
                                                            <ul style=" list-style:circle inside;">
                                                                <c:forEach items="${subSubControl.childList}"
                                                                           var="childAccount">
                                                                    <li>${childAccount.title}</li>
                                                                </c:forEach>
                                                            </ul>
                                                        </li>
                                                    </c:forEach>
                                                </ul>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                    </li>

                                </c:forEach>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
