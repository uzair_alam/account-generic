<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Rider Customer Balance</title>


    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>

<div class="container">
    <form method="post" action="${contextPath}/Rider/customerBalance"
          class="form-inline">

        <div class="ledgerReportHeaderFilterSec">
            <h3>Customer Balance</h3>

            <div class="ledgerReportHeaderFilterBlockSp">
                <div class="ledgerReportHeaderFilterBlock">
                    <label> Rider :</label>
                    <select name="riderId" id="rider_name" class="form-control" onchange="this.form.submit()">
                        <option value="0" selected>-- NIL --</option>
                        <c:forEach items="${riders}" var="rider">
                            <c:choose>
                                <c:when test="${rider.riderId == currentRider}">
                                    <option value="${rider.riderId}" selected>${rider.riderAccount.title}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${rider.riderId}">${rider.riderAccount.title}</option>

                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <label> To Date :</label>
                    <input type="date" name="dateTo" class="form-control " onchange="this.form.submit()"
                           value="${toDate}"/>
                </div>
            </div>
            <div class="ledgerReportHalfSec" id="ledgerReportDataInfo">
                <div class="ledgerReportUserInfo">
                    <p>Rider <span id ="riderName"></span></p>
                </div>
                <div class="ledgerReportDateSec">
                    <p>Date To : ${toDate}</p>
                </div>
            </div>


        </div>
    </form>
    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn"
                    onclick="printDiv()">Print
            </button>
            <p colspan="2" align="right" valign=bottom><font color="#000000">Print on : ${currentDate}</font></p>
        </div>

    </div>

</div>
<section class="main">
    <div class="container" id="printDiv">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">

                    <table class="table trialreport">
                        <thead>
                        <th>Account Title</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        </thead>
                        <tbody>
                        <c:set var="totalDebit" value="${0}"/>
                        <c:set var="totalCredit" value="${0}"/>
                        <c:forEach items="${trialModel}" var="trail">
                            <c:set var="totalCredit" value="${totalCredit + trail.credit}"/>
                            <c:set var="totalDebit" value="${totalDebit + trail.debit}"/>

                            <tr>
                                <td>${trail.accountTitle}</td>
                                <c:choose>
                                    <c:when test="${trail.debit != 0.0}">
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${trail.debit}"/></td>
                                    </c:when>

                                    <c:otherwise>
                                        <td></td>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${trail.credit != 0.0 }">
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${-1 * trail.credit}"/></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td></td>
                                    </c:otherwise>
                                </c:choose>

                            </tr>
                        </c:forEach>

                        </tbody>
                        <tr>
                            <td>Total:</td>

                            <td>
                                <fmt:formatNumber type="number" maxFractionDigits="2"
                                                  value="${totalDebit}"/>
                            <td>
                                <fmt:formatNumber type="number" maxFractionDigits="2"
                                                  value="${-1*totalCredit}"/>

                        </tr>
                        </tbody>
                    </table>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function () {

        updateName();
    });

    function printDiv() {

        var divToPrint = document.getElementById('printDiv');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<img src="${imageUrl}" alt="img" width="200" height="80">' + ledgerReportDataInfo.innerHTML + divToPrint.innerHTML +
            '</body></html>');

        newWin.document.close();

        setTimeout(function () {
            newWin.close();
        }, 50000);

    }

    function updateName() {
        $("#riderName").html($("#rider_name option:selected").text());
    }
</script>
</body>
</html>