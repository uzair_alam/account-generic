<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <title>Dashboard</title>
    <style>
        .container {
            position: relative;
            text-align: center;
            color: white;
        }

        .bottom-left {
            position: absolute;
            bottom: 8px;
            left: 16px;
        }

        .top-left {
            position: absolute;
            top: 8px;
            left: 16px;
        }

        .top-right {
            position: absolute;
            top: 8px;
            right: 16px;
        }

        .bottom-right {
            position: absolute;
            bottom: 8px;
            right: 16px;
        }

        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="container">

        <c:choose>
            <c:when test="${imageUrl==''}">
                <div class="top-centered"><img style="width: 250px;height: 100px"
                                               src="${contextPath}/resources/img/logo.png"></div>
            </c:when>
            <c:otherwise>
                <div class="top-centered"><img style="width: 250px;height: 100px" src="${imageUrl}"></div>
            </c:otherwise>
        </c:choose>

        <div class="container-dash">
            <div class="logoDash-container" style="    margin: 3em 0 0 0;padding-bottom: 3em;position: relative">

                <img style="    position: absolute;width: 100%;top: 0;left: 0;"
                     src="${contextPath}/resources/img/logo-dash.jpg" class="center-block img-responsive" alt="">
                <div style="position: relative;padding: 0 205px 0 270px;font-size: 22px;line-height: 68px; font-weight: bolder;font-size: xx-large">${companyName}</div>

            </div>
            <div class="padding-close-container">
                <div class="box-square">
                    <div class="box-square-content lightB">
                        <a href="${contextPath}/Stock/addStock">
                            <img src="${contextPath}/resources/img/d1.png" alt="">
                            <span>PRODUCTS</span>
                        </a>
                    </div>
                </div>
                <div class="box-square ">
                    <div class="box-square-content pGreen">
                        <a href="${contextPath}/Stock/addVendor">
                            <img src="${contextPath}/resources/img/d2.png" alt="">
                            <span>VENDORS</span>
                        </a>
                    </div>
                </div>
                <div class="box-square ">
                    <div class="box-square-content Dgrey">
                        <a href="${contextPath}/Stock/addCustomer">
                            <img src="${contextPath}/resources/img/d3.png" alt="">
                            <span>CUSTOMER</span>
                        </a>
                    </div>
                </div>
                <div class="box-square ">
                    <div class="box-square-content red">
                        <a href="${contextPath}/Account/addExpense">
                            <img src="${contextPath}/resources/img/d4.png" alt="">
                            <span>EXPENSES</span>
                        </a>
                    </div>
                </div>
                <div class="box-square ">
                    <div class="box-square-content orange">
                        <a href="${contextPath}/User/addBankDetails">
                            <img src="${contextPath}/resources/img/d10.png" alt="">
                            <span>BANK DETAIL</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content mergenda">
                        <a href="${contextPath}/settings">
                            <img src="${contextPath}/resources/img/d5.png" alt="">
                            <span>SETTING</span>
                        </a>
                    </div>
                </div>

            </div>

            <div class="m_row">
                <div class="box-square">
                    <div class="box-square-content pGreen">
                        <a href="${contextPath}/Sales/viewSalesVoucher">
                            <img src="${contextPath}/resources/img/d11.png" alt="">
                            <span>SALE</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content lightB">
                        <a href="${contextPath}/Purchase/viewPurchaseVoucher">
                            <img src="${contextPath}/resources/img/d8.png" alt="">
                            <span>PURCHASE</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content Dgrey">
                        <a href="${contextPath}/Account/AccountListing">
                            <img src="${contextPath}/resources/img/d9.png" alt="">
                            <span>ACCOUNTS</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content pGreen">
                        <a href="${contextPath}/Reports/ledgerReportBank">
                            <img src="${contextPath}/resources/img/d10.png" alt="">
                            <span>Bank/Cash Book</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content mergenda">
                        <a href="${contextPath}/Account/createPdcJournalVoucher">
                            <img src="${contextPath}/resources/img/d11.png" alt="">
                            <span>PDC/BANK</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content mergenda">
                        <a href="#">
                            <img src="${contextPath}/resources/img/d12.png" alt="">
                            <span>RIDER REPORT</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content orange">
                        <a href="${contextPath}/Account/viewJournalVoucher">
                            <img src="${contextPath}/resources/img/d12.png" alt="">
                            <span>JOURNAL VOUCHERS</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content red">
                        <a href="${contextPath}/Reports/productLedger">
                            <img src="${contextPath}/resources/img/d13.png" alt="">
                            <span>PRODUCT LEDGER</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content lightB">
                        <a href="${contextPath}/Reports/ledgerReport">
                            <img src="${contextPath}/resources/img/d13.png" alt="">
                            <span>GENERAL LEDGER</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content orange">
                        <a href="${contextPath}/Account/createBankChequeReturn">
                            <img src="${contextPath}/resources/img/d14.png" alt="">
                            <span>CHQ RETURN</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="padding-close-container">
                <div class="box-square">
                    <div class="box-square-content lightB">
                        <a href="${contextPath}/Account/createPaymentVoucher">
                            <img src="${contextPath}/resources/img/d15.png" alt="">
                            <span>PAYMENTS</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content pGreen">
                        <a href="${contextPath}/Account/createReceiptVoucher">
                            <img src="${contextPath}/resources/img/d16.png" alt="">
                            <span>RECEIPTS</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content Dgrey">
                        <a href="${contextPath}/Reports/trialBalanceBasic">
                            <img src="${contextPath}/resources/img/d17.png" alt="">
                            <span>TRIAL BALANCE</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content red">
                        <a href="${contextPath}/Reports/sheetReport">
                            <img src="${contextPath}/resources/img/d18.png" alt="">
                            <span>P&L</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content mergenda">
                        <a href="${contextPath}/Reports/noteSummary">
                            <img src="${contextPath}/resources/img/d19.png" alt="">
                            <span>BALANCE SHEET</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content orange">
                        <a href="${contextPath}/Reports/chartOfAccount">
                            <img src="" alt="">
                            <span>CHART OF ACCOUNTS</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>