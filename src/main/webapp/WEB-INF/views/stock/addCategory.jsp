<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Category</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${stockCategoryAddSuccess}
        <h2 class="heading-main">Stock Categories <span class="addIcon" data-toggle="modal"
                                                        data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Category Title</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${categories}" var="category" varStatus="status">

                            <tr>
                                <td>${category.title}</td>

                                <td>
                                    <form method="post" id="stockRemove"
                                          action="${contextPath}/Stock/removeStock?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="accountCode"
                                               value="${item.stockAccount.accountCode}"/>
                                    </form>
                                    <form method="post" id="stockEdit"
                                          action="${contextPath}/Stock/editStock?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="stockId" value="${item.stockId}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a href="#" onclick="edit();"><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                        <li><a href="#" onclick="remove();"><img
                                                src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Add Stock</h2>
            </div>

            <form:form method="POST" modelAttribute="categoryForm">
                <div class="modal-body">
                    <spring:bind path="title">

                        <form:label path="title">Title</form:label>
                        <form:input type="text" path="title" placeholder="Category Title"
                                    autofocus="true"></form:input>
                        <form:errors path="title"></form:errors>

                    </spring:bind>
                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    $(document).ready(function () {


    });
    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });

</script>
</body>
</html>
