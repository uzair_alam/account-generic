<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Stock</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${stockAddSuccess}
        ${stockEditSuccess}
        ${stockRemoveSuccess}
        ${AccountAddSuccess}

        <h2 class="heading-main">Stock Items <span class="addIcon" data-toggle="modal" data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Update</span>
            <span class="addIcon" data-toggle="modal"
                  data-target="#myModal2"><i><img
                    src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Selling Price</th>
                            <th>Measuring Unit</th>
                            <th>Opening Quantity</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${stockItems}" var="item" varStatus="voucherStatus">

                            <tr>
                                <td>${item.name}</td>
                                <td>${item.description}</td>
                                <td>${item.stockCategory.title}</td>
                                <td>${item.sellingPrice}</td>
                                <td>${item.unitMeasure}</td>
                                <td>${item.openingQuantity}</td>
                                <td>
                                    <form method="post" id="stockRemove${voucherStatus.index}" action="${contextPath}/Stock/removeStock" />
                                        <input type="hidden" name="accountCode"
                                               value="${item.stockAccount.accountCode}"/>
                                    </form>
                                    <ul class="list-inline">

                                        <li><a href="#" onclick="remove(${voucherStatus.index});"><img
                                                src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Update</h2>
            </div>

            <form:form method="POST" modelAttribute="stockForm">
                <div class="modal-body">
                    <spring:bind path="accountCode">

                        <form:label path="accountCode">Select Stock</form:label>
                        <form:select path="accountCode">
                            <c:forEach items="${accounts}" var="account">
                                <form:option value="${account.accountCode}">${account.title}
                                    <c:choose>
                                        <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                        </c:when>
                                    </c:choose>
                                </form:option>
                            </c:forEach>
                        </form:select>


                    </spring:bind>


                    <spring:bind path="categoryId">

                        <form:label path="categoryId">Stock Category</form:label>
                        <form:select path="categoryId">
                            <c:forEach items="${categories}" var="category">
                                <form:option value="${category.categoryId}">${category.title}</form:option>
                            </c:forEach>
                        </form:select>


                    </spring:bind>

                    <spring:bind path="description">


                        <form:label path="description">Description</form:label>
                        <form:textarea type="text" path="description" placeholder="Description"></form:textarea>
                        <form:errors path="description"></form:errors>

                    </spring:bind>

                    <spring:bind path="quantity">

                        <form:label path="quantity">Current Quantity</form:label>
                        <form:input type="text" path="quantity" placeholder="Quantity"
                                    autofocus="true" value="0.0"></form:input>
                        <form:errors path="quantity"></form:errors>

                    </spring:bind>
                    <spring:bind path="openingQuantity">

                        <form:label path="openingQuantity">Opening Quantity</form:label>
                        <form:input type="text" path="openingQuantity" placeholder="Opening Quantity"
                                    autofocus="true" value="0.0"></form:input>
                        <form:errors path="openingQuantity"></form:errors>

                    </spring:bind>
                    <spring:bind path="cost">

                        <form:label path="cost">Cost/Unit</form:label>
                        <form:input type="text" path="cost" placeholder="Cost"
                                    autofocus="true" value="0.0"></form:input>
                        <form:errors path="cost"></form:errors>

                    </spring:bind>
                    <spring:bind path="sellingPrice">

                        <form:label path="sellingPrice">Selling Price/Unit</form:label>
                        <form:input type="text" path="sellingPrice" placeholder="Selling Price"
                                    autofocus="true" value="0.0"></form:input>
                        <form:errors path="sellingPrice"></form:errors>

                    </spring:bind>
                    <spring:bind path="unitMeasure">

                        <form:label path="unitMeasure">Measuring Unit</form:label>
                        <form:input type="text" path="unitMeasure" placeholder="Measuring Unit"
                                    autofocus="true"></form:input>
                        <form:errors path="unitMeasure"></form:errors>

                    </spring:bind>
                    <spring:bind path="itemDate">

                        <form:label path="itemDate">Item Date</form:label>
                        <form:input type="Date" path="itemDate" value="${currentDate}" required="required"></form:input>
                        <form:errors path="itemDate"></form:errors>

                    </spring:bind>
                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel2">Account Details</h2>
            </div>
            <form:form method="POST" modelAttribute="accountForm" action="${contextPath}/AccountSupport/addStockAccount">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="title">
                                <form:label path="title">Account Title</form:label>
                                <form:input type="text" path="title" placeholder="Account Title"
                                            autofocus="true"></form:input>
                                <form:errors path="title"></form:errors>

                            </spring:bind>
                            <spring:bind path="parentCode">

                                <form:input type="hidden" path="parentCode" value="${stockHead}"
                                            autofocus="true"></form:input>
                                <form:errors path="parentCode"></form:errors>

                            </spring:bind>

                            <spring:bind path="openingBalance">
                                <form:label path="openingBalance">Opening Balance </form:label>
                                <form:input id="openingBalance" onblur="toggleAccountType()" type="text"
                                            path="openingBalance" placeholder="Opening Balance"></form:input>
                                <form:errors path="openingBalance"></form:errors>

                            </spring:bind>


                            <label>Select Account Type </label>
                            <select id="accountType">
                                <option id="credit" value="credit">Credit</option>
                                <option id="debit" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingBalanceLast">

                                <form:label path="openingBalanceLast">Opening Balance Last </form:label>
                                <form:input id="openingBalanceLast" onblur="toggleAccountTypeLast()" type="text"
                                            path="openingBalanceLast"
                                            placeholder="Last Opening Balance"></form:input>
                                <form:errors path="openingBalanceLast"></form:errors>

                            </spring:bind>
                            <label>Select Account Type Last </label>
                            <select id="accountTypeLast">
                                <option id="creditLast" value="credit">Credit</option>
                                <option id="debitLast" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingCredit">
                                <form:input type="hidden" path="openingCredit" id="openingCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="openingDebit">

                                <form:input type="hidden" path="openingDebit" id="openingDebit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningCredit">
                                <form:input type="hidden" path="lastOpeningCredit" id="lastOpeningCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningDebit">

                                <form:input type="hidden" path="lastOpeningDebit" id="lastOpeningDebit"
                                            value="0.0"></form:input>

                            </spring:bind>

                            <spring:bind path="openingDate">
                                <form:label path="openingDate">Opening Date</form:label>
                                <form:input type="Date" path="openingDate" value="${currentDate}"
                                            required="required"></form:input>
                                <form:errors path="openingDate"></form:errors>

                            </spring:bind>

                            <spring:bind path="accountStock.description">


                                <form:label path="accountStock.description">Description</form:label>
                                <form:input type="text" path="accountStock.description" placeholder="Description"></form:input>
                                <form:errors path="accountStock.description"></form:errors>

                            </spring:bind>

                        </div>

                        <div class="col-sm-6">
                            <spring:bind path="accountStock.categoryId">

                                <form:label path="accountStock.categoryId">Stock Category</form:label>
                                <form:select path="accountStock.categoryId">
                                    <c:forEach items="${categories}" var="category">
                                        <form:option value="${category.categoryId}">${category.title}</form:option>
                                    </c:forEach>
                                </form:select>


                            </spring:bind>



                            <spring:bind path="accountStock.quantity">

                                <form:label path="accountStock.quantity">Current Quantity</form:label>
                                <form:input type="text" path="accountStock.quantity" placeholder="Quantity"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.quantity"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.openingQuantity">

                                <form:label path="accountStock.openingQuantity">Opening Quantity</form:label>
                                <form:input type="text" path="accountStock.openingQuantity" placeholder="Opening Quantity"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.openingQuantity"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.cost">

                                <form:label path="accountStock.cost">Cost/Unit</form:label>
                                <form:input type="text" path="accountStock.cost" placeholder="Cost"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.cost"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.sellingPrice">

                                <form:label path="accountStock.sellingPrice">Selling Price/Unit</form:label>
                                <form:input type="text" path="accountStock.sellingPrice" placeholder="Selling Price"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.sellingPrice"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.unitMeasure">

                                <form:label path="accountStock.unitMeasure">Measuring Unit</form:label>
                                <form:input type="text" path="accountStock.unitMeasure" placeholder="Measuring Unit"
                                            autofocus="true"></form:input>
                                <form:errors path="accountStock.unitMeasure"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.itemDate">

                                <form:label path="accountStock.itemDate">Item Date</form:label>
                                <form:input type="Date" path="accountStock.itemDate" value="${currentDate}" required="required"></form:input>
                                <form:errors path="accountStock.itemDate"></form:errors>

                            </spring:bind>


                        </div>

                    </div>
                    <div align="center">

                    </div>
                </div>
                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</div>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>

    $(document).ready(function () {

        toggleAccountType();
        toggleAccountTypeLast();


        $('#accountType').change(function () {

            toggleAccountType();
        });
        $('#accountTypeLast').change(function () {

            toggleAccountTypeLast();
        });
    });

    function toggleAccountType() {

        if ($('#accountType').val() == "credit") {

            $('#openingCredit').val($('#openingBalance').val());
            $('#openingDebit').val('0.0');
        } else if ($('#accountType').val() == "debit") {

            $('#openingDebit').val($('#openingBalance').val());
            $('#openingCredit').val('0.0');
        }
    }

    function toggleAccountTypeLast() {

        if ($('#accountTypeLast').val() == "credit") {

            $('#lastOpeningCredit').val($('#openingBalanceLast').val());
            $('#lastOpeningDebit').val('0.0');
        } else if ($('#accountTypeLast').val() == "debit") {

            $('#lastOpeningDebit').val($('#openingBalanceLast').val());
            $('#lastOpeningCredit').val('0.0');
        }
    }


    function remove(id) {
        if(confirm("Are you sure, you want to delete the Stock Item?")) {
            $('#stockRemove' + id).submit();
        }

    }



    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });
</script>
</body>
</html>
