<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Edit Stock</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>


<section class="main">
    <div class="container">

        <div class="chartAcc">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="heading-light">Edit Stock</h2>
                    <div class="inputContainer">
                        <form:form method="POST" modelAttribute="stockFormUpdate" action="${contextPath}/Stock/doEditStock">
                        <div class="">
                            <spring:bind path="name">

                                <form:label path="name">Name</form:label>
                                <form:input type="text" path="name"  placeholder="Name"
                                            autofocus="true"></form:input>
                                <form:errors path="name"></form:errors>

                            </spring:bind>

                            <spring:bind path="accountCode">

                                <form:label path="accountCode">Stock Account</form:label>
                                <form:select  path="accountCode"  >
                                    <c:forEach items="${accounts}" var="account" >
                                        <c:choose>
                                        <c:when test="${account.accountCode == accountCode}">
                                            <form:option value="${account.accountCode}" selected="true" >${account.title}
                                                <c:choose>
                                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                    </c:when>
                                                </c:choose>
                                            </form:option>
                                        </c:when><c:otherwise>
                                            <form:option value="${account.accountCode}">${account.title}
                                                <c:choose>
                                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                    </c:when>
                                                </c:choose>
                                            </form:option>
                                        </c:otherwise>
                                        </c:choose>



                                    </c:forEach>
                                </form:select>


                            </spring:bind>


                            <spring:bind path="categoryId">

                                <form:label path="categoryId">Stock Category</form:label>
                                <form:select  path="categoryId"  >
                                    <c:forEach items="${categories}" var="category" >

                                        <c:choose>
                                        <c:when test="${category.categoryId == categoryId}">
                                        <form:option value="${category.categoryId}" selected ="true" >${category.title}</form:option>
                                        </c:when>
                                            <c:otherwise>
                                                <form:option value="${category.categoryId}">${category.title}</form:option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </form:select>


                            </spring:bind>

                            <spring:bind path="description">


                                <form:label path="description">Description</form:label>
                                <form:textarea   type="text" path="description" placeholder="Description"></form:textarea>
                                <form:errors path="description"></form:errors>

                            </spring:bind>

                            <spring:bind path="quantity">

                                <form:label path="quantity">Current Quantity</form:label>
                                <form:input type="text" path="quantity"  placeholder="Quantity"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="quantity"></form:errors>

                            </spring:bind>
                            <spring:bind path="openingQuantity">

                                <form:label path="openingQuantity">Opening Quantity</form:label>
                                <form:input type="text" path="openingQuantity"  placeholder="Opening Quantity"
                                            autofocus="true" ></form:input>
                                <form:errors path="openingQuantity"></form:errors>

                            </spring:bind>
                            <spring:bind path="cost">

                                <form:label path="cost">Cost/Unit</form:label>
                                <form:input type="text" path="cost" placeholder="Cost"
                                            autofocus="true" ></form:input>
                                <form:errors path="cost"></form:errors>

                            </spring:bind>
                            <spring:bind path="sellingPrice">

                                <form:label path="sellingPrice">Selling Price/Unit</form:label>
                                <form:input type="text" path="sellingPrice" placeholder="Selling Price"
                                            autofocus="true" ></form:input>
                                <form:errors path="sellingPrice"></form:errors>

                            </spring:bind>
                            <spring:bind path="unitMeasure">

                                <form:label path="unitMeasure">Measuring Unit</form:label>
                                <form:input type="text" path="unitMeasure"  placeholder="Measuring Unit"
                                            autofocus="true"></form:input>
                                <form:errors path="unitMeasure"></form:errors>

                            </spring:bind>
                            <spring:bind path="itemDate">

                                <form:label path="itemDate">Item Date</form:label>
                                <form:input type="Date" path="itemDate" value = "${currentDate}" required = "required"></form:input>
                                <form:errors path="itemDate"></form:errors>

                            </spring:bind>
                            <button type="submit">submit</button>
                            </form:form>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>


</body>
</html>
