<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Customer</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>

<div class="container">
    ${AccountAddSuccess}


    <section class="main">
        <div class="container">
            <div class="chartAcc">
                <div class="row">
                    <div class="col-sm-6">
                        <h2 class="heading-light">Add Customer Details</h2>
                        <div class="inputContainer">
                            <form:form method="POST" modelAttribute="accountForm">
                            <div class="inputContainer">
                                <spring:bind path="title">

                                    <form:input type="text" path="title" placeholder="Account Title"
                                                autofocus="true"></form:input>
                                    <form:errors path="title"></form:errors>

                                </spring:bind>
                                <spring:bind path="parentCode">

                                    <form:input type="hidden" path="parentCode" value="${customerHead}"
                                                autofocus="true"></form:input>
                                    <form:errors path="parentCode"></form:errors>

                                </spring:bind>

                                <spring:bind path="openingBalance">
                                    <form:label path="openingBalance">Opening Balance </form:label>
                                    <form:input id="openingBalance" onblur="toggleAccountType()" type="text"
                                                path="openingBalance" placeholder="Opening Balance"></form:input>
                                    <form:errors path="openingBalance"></form:errors>

                                </spring:bind>


                                <label>Select Account Type </label>
                                <select id="accountType">
                                    <option id="credit" value="credit">Credit</option>
                                    <option id="debit" value="debit">Debit</option>

                                </select>

                                <spring:bind path="openingBalanceLast">

                                    <form:label path="openingBalanceLast">Opening Balance Last </form:label>
                                    <form:input id="openingBalanceLast" onblur="toggleAccountTypeLast()" type="text"
                                                path="openingBalanceLast"
                                                placeholder="Last Opening Balance"></form:input>
                                    <form:errors path="openingBalanceLast"></form:errors>

                                </spring:bind>
                                <label>Select Account Type Last </label>
                                <select id="accountTypeLast">
                                    <option id="creditLast" value="credit">Credit</option>
                                    <option id="debitLast" value="debit">Debit</option>

                                </select>

                                <spring:bind path="openingCredit">
                                    <form:input type="hidden" path="openingCredit" id="openingCredit"
                                                value="0.0"></form:input>

                                </spring:bind>
                                <spring:bind path="openingDebit">

                                    <form:input type="hidden" path="openingDebit" id="openingDebit"
                                                value="0.0"></form:input>

                                </spring:bind>
                                <spring:bind path="lastOpeningCredit">
                                    <form:input type="hidden" path="lastOpeningCredit" id="lastOpeningCredit"
                                                value="0.0"></form:input>

                                </spring:bind>
                                <spring:bind path="lastOpeningDebit">

                                    <form:input type="hidden" path="lastOpeningDebit" id="lastOpeningDebit"
                                                value="0.0"></form:input>

                                </spring:bind>

                                <spring:bind path="openingDate">

                                    <form:input type="Date" path="openingDate" value="${currentDate}"
                                                required="required"></form:input>
                                    <form:errors path="openingDate"></form:errors>

                                </spring:bind>

                                <button type="submit">submit</button>

                            </div>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputContainer">


                            <spring:bind path="stockCustomer.mobile">


                                <form:label path="stockCustomer.mobile">Mobile Number</form:label>
                                <form:input type="text" path="stockCustomer.mobile" placeholder="Mobile #"></form:input>
                                <form:errors path="stockCustomer.mobile"></form:errors>

                            </spring:bind>

                            <spring:bind path="stockCustomer.landline">

                                <form:label path="stockCustomer.landline">Landline #</form:label>
                                <form:input type="text" path="stockCustomer.landline" placeholder="LandLine#"
                                            autofocus="true"></form:input>
                                <form:errors path="stockCustomer.landline"></form:errors>

                            </spring:bind>

                            <spring:bind path="stockCustomer.cnic">

                                <form:label path="stockCustomer.cnic">CNIC/NTN</form:label>
                                <form:input type="text" path="stockCustomer.cnic" placeholder="CNIC"
                                            autofocus="true"></form:input>
                                <form:errors path="stockCustomer.cnic"></form:errors>

                            </spring:bind>
                            <spring:bind path="stockCustomer.taxStatus">

                                <form:label path="stockCustomer.taxStatus">Tax Status</form:label>
                                <form:select path="stockCustomer.taxStatus">

                                    <form:option value="FILER">Filer</form:option>
                                    <form:option value="NONFILER">Non-Filer</form:option>

                                </form:select>


                            </spring:bind>
                            <spring:bind path="stockCustomer.saletaxNumber">

                                <form:label path="stockCustomer.saletaxNumber">Sales Tax #</form:label>
                                <form:input type="text" path="stockCustomer.saletaxNumber" placeholder="Sales Tax #"
                                            autofocus="true"></form:input>
                                <form:errors path="stockCustomer.saletaxNumber"></form:errors>

                            </spring:bind>
                            <spring:bind path="stockCustomer.nationalTaxNumber">

                                <form:label path="stockCustomer.nationalTaxNumber">NTN #</form:label>
                                <form:input type="text" path="stockCustomer.nationalTaxNumber" placeholder="NTN"
                                            autofocus="true"></form:input>
                                <form:errors path="stockCustomer.nationalTaxNumber"></form:errors>

                            </spring:bind>
                            <spring:bind path="stockCustomer.address">

                                <form:label path="stockCustomer.address">Address</form:label>
                                <form:textarea type="text" path="stockCustomer.address" placeholder="Address"
                                               autofocus="true"></form:textarea>
                                <form:errors path="stockCustomer.address"></form:errors>

                            </spring:bind>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function () {

        toggleAccountType();
        toggleAccountTypeLast();


        $('#accountType').change(function () {

            toggleAccountType();
        });
        $('#accountTypeLast').change(function () {

            toggleAccountTypeLast();
        });
    });

    function toggleAccountType() {

        if ($('#accountType').val() == "credit") {

            $('#openingCredit').val($('#openingBalance').val());
            $('#openingDebit').val('0.0');
        } else if ($('#accountType').val() == "debit") {

            $('#openingDebit').val($('#openingBalance').val());
            $('#openingCredit').val('0.0');
        }
    }

    function toggleAccountTypeLast() {

        if ($('#accountTypeLast').val() == "credit") {

            $('#lastOpeningCredit').val($('#openingBalanceLast').val());
            $('#lastOpeningDebit').val('0.0');
        } else if ($('#accountTypeLast').val() == "debit") {

            $('#lastOpeningDebit').val($('#openingBalanceLast').val());
            $('#lastOpeningCredit').val('0.0');
        }
    }


</script>
</body>
</html>
