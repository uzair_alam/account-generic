<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Vendor</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${vendorAddSuccess}
        ${vendorRemoveSuccess}
            ${AccountAddSuccess}
        <h2 class="heading-main">Vendors <span class="addIcon" data-toggle="modal" data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Update</span>
            <span class="addIcon" data-toggle="modal"
                  data-target="#myModal2"><i><img
                    src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Mobile</th>
                            <th>CNIC</th>
                            <th>LandLine</th>
                            <th>SaleTax #</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${vendors}" var="vendor" varStatus="voucherStatus">

                            <tr>
                                <td>${vendor.vendorAccount.title}</td>
                                <td>${vendor.address}</td>
                                <td>${vendor.mobile}</td>
                                <td>${vendor.cnic}</td>
                                <td>${vendor.landline}</td>
                                <td>${vendor.saletaxNumber}</td>
                                <td>
                                    <form method="post" id="vendorRemove${voucherStatus.index}"
                                          action="${contextPath}/Stock/removeVendor?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="accountCode"
                                               value="${vendor.vendorAccount.accountCode}"/>
                                    </form>
                                    <form method="post" id="vendorEdit${voucherStatus.index}"
                                          action="${contextPath}/Stock/editStock?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="stockId" value="${item.stockId}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a href="#" onclick="edit(${voucherStatus.index});"><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                        <li><a href="#" onclick="remove(${voucherStatus.index});"><img
                                                src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Update</h2>
            </div>

            <form:form method="POST" modelAttribute="vendorForm">
                <div class="modal-body">
                    <spring:bind path="accountCode">
                        <form:label path="accountCode">Select Vendor</form:label>
                        <form:select path="accountCode">
                            <c:forEach items="${accounts}" var="account">
                                <form:option value="${account.accountCode}">${account.title}
                                    <c:choose>
                                        <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                        </c:when>
                                    </c:choose>
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </spring:bind>

                    <spring:bind path="mobile">


                        <form:label path="mobile">Mobile Number</form:label>
                        <form:input type="text" path="mobile" placeholder="Mobile #"></form:input>
                        <form:errors path="mobile"></form:errors>

                    </spring:bind>

                    <spring:bind path="landline">

                        <form:label path="landline">Landline #</form:label>
                        <form:input type="text" path="landline" placeholder="LandLine#"
                                    autofocus="true"></form:input>
                        <form:errors path="landline"></form:errors>

                    </spring:bind>
                    <spring:bind path="contactPersons">

                        <form:label path="contactPersons">Contact Persons(Comma separated)</form:label>
                        <form:input type="text" path="contactPersons" placeholder="Contact Person"
                                    autofocus="true"></form:input>
                        <form:errors path="contactPersons"></form:errors>

                    </spring:bind>
                    <spring:bind path="cnic">

                        <form:label path="cnic">CNIC/NTN</form:label>
                        <form:input type="text" path="cnic" placeholder="CNIC"
                                    autofocus="true"></form:input>
                        <form:errors path="cnic"></form:errors>

                    </spring:bind>
                    <spring:bind path="taxStatus">

                        <form:label path="taxStatus">Tax Status</form:label>
                        <form:select path="taxStatus">

                            <form:option value="FILER">Filer</form:option>
                            <form:option value="NONFILER">Non-Filer</form:option>

                        </form:select>


                    </spring:bind>
                    <spring:bind path="saletaxNumber">

                        <form:label path="saletaxNumber">Sales Tax #</form:label>
                        <form:input type="text" path="saletaxNumber" placeholder="Sales Tax #"
                                    autofocus="true"></form:input>
                        <form:errors path="saletaxNumber"></form:errors>

                    </spring:bind>
                    <spring:bind path="nationalTaxNumber">

                        <form:label path="nationalTaxNumber">NTN #</form:label>
                        <form:input type="text" path="nationalTaxNumber" placeholder="NTN"
                                    autofocus="true"></form:input>
                        <form:errors path="nationalTaxNumber"></form:errors>

                    </spring:bind>
                    <spring:bind path="address">

                        <form:label path="address">Address</form:label>
                        <form:textarea type="text" path="address" placeholder="Address"
                                       autofocus="true"></form:textarea>
                        <form:errors path="address"></form:errors>

                    </spring:bind>
                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel2">Account Details</h2>
            </div>
            <form:form method="POST" modelAttribute="accountForm" action="${contextPath}/AccountSupport/addVendorAccount">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="title">
                                <form:label path="title">Account Title</form:label>
                                <form:input type="text" path="title" placeholder="Account Title"
                                            autofocus="true"></form:input>
                                <form:errors path="title"></form:errors>

                            </spring:bind>
                            <spring:bind path="parentCode">

                                <form:input type="hidden" path="parentCode" value="${vendorHead}"
                                            autofocus="true"></form:input>
                                <form:errors path="parentCode"></form:errors>

                            </spring:bind>

                            <spring:bind path="openingBalance">
                                <form:label path="openingBalance">Opening Balance </form:label>
                                <form:input id="openingBalance" onblur="toggleAccountType()" type="text"
                                            path="openingBalance" placeholder="Opening Balance"></form:input>
                                <form:errors path="openingBalance"></form:errors>

                            </spring:bind>


                            <label>Select Account Type </label>
                            <select id="accountType">
                                <option id="credit" value="credit">Credit</option>
                                <option id="debit" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingBalanceLast">

                                <form:label path="openingBalanceLast">Opening Balance Last </form:label>
                                <form:input id="openingBalanceLast" onblur="toggleAccountTypeLast()" type="text"
                                            path="openingBalanceLast"
                                            placeholder="Last Opening Balance"></form:input>
                                <form:errors path="openingBalanceLast"></form:errors>

                            </spring:bind>
                            <label>Select Account Type Last </label>
                            <select id="accountTypeLast">
                                <option id="creditLast" value="credit">Credit</option>
                                <option id="debitLast" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingCredit">
                                <form:input type="hidden" path="openingCredit" id="openingCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="openingDebit">

                                <form:input type="hidden" path="openingDebit" id="openingDebit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningCredit">
                                <form:input type="hidden" path="lastOpeningCredit" id="lastOpeningCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningDebit">

                                <form:input type="hidden" path="lastOpeningDebit" id="lastOpeningDebit"
                                            value="0.0"></form:input>

                            </spring:bind>

                            <spring:bind path="openingDate">
                                <form:label path="openingDate">Opening Date</form:label>
                                <form:input type="Date" path="openingDate" value="${currentDate}"
                                            required="required"></form:input>
                                <form:errors path="openingDate"></form:errors>

                            </spring:bind>

                        </div>

                        <div class="col-sm-6">

                            <spring:bind path="stockVendor.mobile">


                                <form:label path="stockVendor.mobile">Mobile Number</form:label>
                                <form:input type="text" path="stockVendor.mobile" placeholder="Mobile #"></form:input>
                                <form:errors path="stockVendor.mobile"></form:errors>

                            </spring:bind>

                            <spring:bind path="stockVendor.landline">

                                <form:label path="stockVendor.landline">Landline #</form:label>
                                <form:input type="text" path="stockVendor.landline" placeholder="LandLine#"
                                            autofocus="true"></form:input>
                                <form:errors path="stockVendor.landline"></form:errors>

                            </spring:bind>

                            <spring:bind path="stockVendor.cnic">

                                <form:label path="stockVendor.cnic">CNIC/NTN</form:label>
                                <form:input type="text" path="stockVendor.cnic" placeholder="CNIC"
                                            autofocus="true"></form:input>
                                <form:errors path="stockVendor.cnic"></form:errors>

                            </spring:bind>
                            <spring:bind path="stockVendor.taxStatus">

                                <form:label path="stockVendor.taxStatus">Tax Status</form:label>
                                <form:select path="stockVendor.taxStatus">

                                    <form:option value="FILER">Filer</form:option>
                                    <form:option value="NONFILER">Non-Filer</form:option>

                                </form:select>


                            </spring:bind>
                            <spring:bind path="stockVendor.saletaxNumber">

                                <form:label path="stockVendor.saletaxNumber">Sales Tax #</form:label>
                                <form:input type="text" path="stockVendor.saletaxNumber" placeholder="Sales Tax #"
                                            autofocus="true"></form:input>
                                <form:errors path="stockVendor.saletaxNumber"></form:errors>

                            </spring:bind>
                            <spring:bind path="stockVendor.nationalTaxNumber">

                                <form:label path="stockVendor.nationalTaxNumber">NTN #</form:label>
                                <form:input type="text" path="stockVendor.nationalTaxNumber" placeholder="NTN"
                                            autofocus="true"></form:input>
                                <form:errors path="stockVendor.nationalTaxNumber"></form:errors>

                            </spring:bind>


                        </div>

                    </div>
                    <div align="center">
                        <spring:bind path="stockVendor.address">

                            <form:label path="stockVendor.address">Address</form:label>
                            <form:textarea type="text" path="stockVendor.address" placeholder="Address"
                                           autofocus="true"></form:textarea>
                            <form:errors path="stockVendor.address"></form:errors>

                        </spring:bind>

                    </div>
                </div>
                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>

    $(document).ready(function () {

        toggleAccountType();
        toggleAccountTypeLast();


        $('#accountType').change(function () {

            toggleAccountType();
        });
        $('#accountTypeLast').change(function () {

            toggleAccountTypeLast();
        });
    });

    function toggleAccountType() {

        if ($('#accountType').val() == "credit") {

            $('#openingCredit').val($('#openingBalance').val());
            $('#openingDebit').val('0.0');
        } else if ($('#accountType').val() == "debit") {

            $('#openingDebit').val($('#openingBalance').val());
            $('#openingCredit').val('0.0');
        }
    }

    function toggleAccountTypeLast() {

        if ($('#accountTypeLast').val() == "credit") {

            $('#lastOpeningCredit').val($('#openingBalanceLast').val());
            $('#lastOpeningDebit').val('0.0');
        } else if ($('#accountTypeLast').val() == "debit") {

            $('#lastOpeningDebit').val($('#openingBalanceLast').val());
            $('#lastOpeningCredit').val('0.0');
        }
    }

    function remove(id) {
        if (confirm("Are you sure, you want to delete?")) {
            $('#vendorRemove' + id).submit();
        }
    }

    function edit(id) {
        //$('#vendorEdit'+ id).submit();
    }

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });


</script>
</body>
</html>
