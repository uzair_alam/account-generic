<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <title>Purchase Vouchers</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">
        ${voucherUpdateSuccess}
        <h2 class="heading-main">Purchase Vouchers<a href="${contextPath}/Purchase/createPurchaseVoucher"><span
                class="addIcon" data-toggle="modal" data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></a></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Voucher Number</th>
                            <th onclick="refreshPage()">Voucher Date</th>
                            <th style="display: none">Date</th>
                            <th>Account</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Rate</th>
                            <th>Amount</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>

                        <c:forEach items="${vouchers}" var="voucher" varStatus="voucherStatus">
                            <c:choose>

                                <c:when test="${voucher.debit != 0.0}">
                                    <tr>
                                        <td id="${voucher.voucherNumber}">${voucher.voucherNumber}</td>

                                        <td><fmt:formatDate pattern="dd/MM/yyyy" value="${voucher.voucherDate}"/></td>
                                        <td style="display: none">${voucher.voucherDate}</td>
                                        <td>${accountMap.get(voucher.accountCode)}</td>
                                        <td>${accountMap.get(voucher.itemAccount)}</td>
                                        <td>${voucher.itemQuantity}</td>
                                        <td>${voucher.itemRate}</td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${voucher.debit}"/></td>
                                        <td>
                                            <form method="post" id="voucherRemove${voucherStatus.index}"
                                                  action="${contextPath}/Account/deleteVoucher?${_csrf.parameterName}=${_csrf.token}">
                                                <input type="hidden" name="${_csrf.parameterName}"
                                                       value="${_csrf.token}"/>
                                                <input type="hidden" name="voucherNumber"
                                                       value="${voucher.voucherNumber}"/>
                                                <input type="hidden" name="redirectUrl"
                                                       value="Purchase/viewPurchaseVoucher"/>
                                            </form>
                                            <ul class="list-inline">
                                                <li><a title="Edit"
                                                       href="${contextPath}/Purchase/updatePurchaseVoucher?voucherNumber=${voucher.voucherNumber}"><img
                                                        src="${contextPath}/resources/img/editIcon.png" alt=""></a></li>
                                                <li><a title="Delete" href="#"
                                                       onclick="remove(${voucherStatus.index});"><img
                                                        src="${contextPath}/resources/img/deleteIcon.png" alt=""></a>
                                                </li>
                                            </ul>
                                        </td>

                                    </tr>
                                </c:when>
                            </c:choose>

                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>
    function remove(id) {
        if (confirm("Are you sure, you want to delete the voucher?")) {
            $('#voucherRemove' + id).submit();
        }
    }

    function getParameterByName(name, url) {
        if (!url)
            url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results)
            return null;
        if (!results[2])
            return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $(document).ready(function () {
        console.log('test')
        if (getParameterByName('id')) {
            $('html,body').animate({
                    scrollTop: $("#" + getParameterByName('id')).offset().top
                },
                'slow');
        }


    })
    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[2, "desc"]],
            "pageLength": 7
        });
    });

    function refreshPage() {
        window.location.reload();
    }


</script>
</body>
</html>