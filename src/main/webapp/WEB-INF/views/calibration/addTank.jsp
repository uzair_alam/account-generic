<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Tank</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">


    <h2 class="form-signin-heading">Tank Registration</h2>
    <form:form class="form-inline formSaleVoucher  " method="POST" modelAttribute="stockTankModel" id="formid">
        <div class="container">

            <div class="pull-left">
                <div class="form-Row">
                    <spring:bind path="name">
                        <form:label path="name">Tank Title</form:label>
                        <form:input type="text" path="name" cssClass="form-control"
                                    autofocus="true"></form:input>
                    </spring:bind>
                </div>
                <div class="form-Row">
                    <spring:bind path="stockId">
                        <form:label path="stockId">Tank Stock</form:label>
                        <form:select path="stockId" id="supplierAccount" cssClass="form-control">
                            <c:forEach items="${stockAccounts}" var="account">
                                <form:option value="${account.accountStock.stockId}">${account.title}</form:option>
                            </c:forEach>
                        </form:select>
                    </spring:bind>
                </div>
                <div class="form-Row">
                    <spring:bind path="description">
                        <form:label path="description">Description</form:label>
                        <form:input type="text" path="description" cssClass="form-control"
                                    autofocus="true"></form:input>
                    </spring:bind>
                </div>
            </div>
            <div class="customerInfoSec">
                <div class="form-Row">
                    <spring:bind path="capacity">
                        <form:label path="capacity">Capacity </form:label>
                        <form:input type="text" path="capacity"  cssClass="form-control"
                                    autofocus="true"></form:input>
                    </spring:bind>
                </div>
                <div  class="form-Row">
                    <spring:bind path="radius">
                        <form:label path="radius">Radius </form:label>
                        <form:input type="text" path="radius"  cssClass="form-control"
                                    autofocus="true"></form:input>
                    </spring:bind>
                </div>
                <div  class="form-Row">
                    <spring:bind path="length">
                        <form:label path="length">Length </form:label>
                        <form:input type="text" path="length"  cssClass="form-control"
                                    autofocus="true"></form:input>
                    </spring:bind>
                </div>
                <div  class="form-Row">
                    <spring:bind path="clearance">
                        <form:label path="clearance">Clearance </form:label>
                        <form:input type="text" path="clearance"  cssClass="form-control"
                                    autofocus="true"></form:input>
                    </spring:bind>
                </div>

            </div>


        </div>

        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">

                <thead>
                <tr>
                    <th>Dip (mm)</th>
                    <th>Volume</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${stockTankModel.tankInfoList}" var="tankInfo" varStatus="infoStatus">

                    <tr>
                        <td>
                            <form:input type="text"
                                        path="tankInfoList[${infoStatus.index}].dip"
                                        class="form-control quantity"
                                        autofocus="true" ></form:input>
                        </td>
                        <td>
                            <form:input type="text"
                                        path="tankInfoList[${infoStatus.index}].volume"
                                        class="form-control quantity"
                                        autofocus="true" ></form:input>
                        </td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">

        <div class="totalBillSec">

        </div>
    </form:form>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

</script>
</body>
</html>
