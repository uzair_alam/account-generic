<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <title>Fuel Tanks</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">
        ${tankDeleteSuccess}


        <h2 class="heading-main">Tanks Listing <a href="${contextPath}/Tanks/addTank"><span class="addIcon"
                                                                                                          data-toggle="modal"
                                                                                                          data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></a></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Product</th>
                            <th>Description</th>
                            <th>Capacity</th>
                            <th>radius</th>
                            <th>Length</th>
                            <th>Clearance</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${tankList}" var="listTank" varStatus="accountStatus">
                            <tr>
                                <td>${listTank.name}</td>
                                <td>${listTank.tankStock.stockAccount.title}</td>
                                <td>${listTank.description}</td>
                                <td>${listTank.capacity}</td>
                                <td>${listTank.radius}</td>
                                <td>${listTank.length}</td>
                                <td>${listTank.clearance}</td>
                                <td>
                                    <form method="post" id="tankRemove${accountStatus.index}"
                                          action="${contextPath}/Tanks/remove?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="tankId" value="${listTank.tankId}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a href="${contextPath}/Tanks/addTank?tankId=${listTank.tankId}" ><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                        <li><a href="#" onclick="remove(${accountStatus.index})"><img
                                                src="${contextPath}/resources/img/remove.png" alt=""></a></li>

                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>
    function remove(id) {
        if (confirm("Are you sure, you want to delete the Tank?")) {
            $('#tankRemove' + id).submit();
        }
    }


    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[1, "desc"]],
            "pageLength": 7
        });
    });


</script>
</body>
</html>