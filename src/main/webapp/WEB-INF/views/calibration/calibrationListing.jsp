<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <title>Calibrations</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">
        ${infoRemoveSuccess}
        ${calibrationSuccess}

        <h2 class="heading-main">Calibration Listing <a href="${contextPath}/Calibration/calibrate"><span class="addIcon"
                                                                                                   data-toggle="modal"
                                                                                                   data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></a></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th onclick="refreshPage()">Date</th>
                            <th style="display: none">Date</th>
                            <th>Actual Amount</th>
                            <th>Ledger Amount</th>
                            <th>Deficiency</th>
                            <th>Excess</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${calibrationList}" var="listCalibrate" varStatus="accountStatus">
                            <tr>
                                <td>${listCalibrate.calibrationStock.stockAccount.title}</td>
                                <td><fmt:formatDate pattern="dd/MM/yyyy" value="${listCalibrate.date}"/></td>
                                <td style="display: none">${listCalibrate.date}</td>
                                <td>${listCalibrate.actualQuantity}</td>
                                <td>${listCalibrate.tanksQuantity}</td>
                                <c:choose>
                                    <c:when test="${listCalibrate.actualQuantity > listCalibrate.tanksQuantity}">
                                        <td>
                                            <fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${listCalibrate.actualQuantity - listCalibrate.tanksQuantity}"/>
                                            (<fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${(listCalibrate.actualQuantity - listCalibrate.tanksQuantity) /listCalibrate.actualQuantity * 100}"/>%)
                                        </td>
                                        <td>0.0 (0.0%)</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>0.0 (0.0%)</td>
                                        <td>
                                            <fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${listCalibrate.tanksQuantity - listCalibrate.actualQuantity}"/>
                                            (<fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${(listCalibrate.tanksQuantity - listCalibrate.actualQuantity) /listCalibrate.actualQuantity * 100}"/>%)
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <td>
                                    <form method="post" id="calibrateStock${accountStatus.index}"
                                          action="${contextPath}/Calibration/generateVouchers?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="infoId" value="${listCalibrate.infoId}"/>
                                    </form>

                                    <form method="post" id="removeCalibration${accountStatus.index}"
                                          action="${contextPath}/Calibration/remove?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="infoId" value="${listCalibrate.infoId}"/>
                                    </form>
                                    <c:choose>
                                        <c:when test="${!listCalibrate.calibrated}">
                                            <ul class="list-inline">
                                                <li><a href="#" onclick="calibrate(${accountStatus.index})"><img
                                                        src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                                <li><a href="#" onclick="remove(${accountStatus.index})"><img
                                                        src="${contextPath}/resources/img/remove.png" alt=""></a></li>

                                            </ul>
                                        </c:when>
                                    </c:choose>

                                </td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>
    function remove(id) {
        if (confirm("Are you sure, you want to delete the Calibration?")) {
            $('#removeCalibration' + id).submit();
        }
    }

    function calibrate(id) {

        if (confirm("Are you sure, you want to create calibration vouchers?")) {
            $('#calibrateStock' + id).submit();
        }
    }

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[2, "desc"]],
            "pageLength": 7
        });
    });
    function refreshPage() {
        window.location.reload();
    }


</script>
</body>
</html>