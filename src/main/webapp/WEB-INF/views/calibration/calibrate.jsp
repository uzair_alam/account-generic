<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Calibrate Product</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">


    <h2 class="form-signin-heading">Calibration</h2>
    <form:form class="form-inline formSaleVoucher  " method="POST" modelAttribute="calibrationModel" id="formid">
        <div class="container">

            <div class="pull-left">

                <div class="form-Row">
                    <spring:bind path="stock.stockId">
                        <form:label path="stock.stockId">Product </form:label>
                        <form:select path="stock.stockId" onchange="changeProduct()" id="stockID" cssClass="form-control">
                            <c:forEach items="${stockList}" var="stock">
                                <form:option value="${stock.stockId}">${stock.stockAccount.title}</form:option>
                            </c:forEach>
                        </form:select>
                    </spring:bind>
                    <spring:bind path="date">
                        <form:label path="date">Date </form:label>
                        <form:input type="date" path="date" readonly="true" cssClass="form-control"
                                    autofocus="true" value = "${currentDate}" ></form:input>
                    </spring:bind>
                    <spring:bind path="currentQuantity">
                        <form:label path="currentQuantity">Current Quantity </form:label>
                        <form:input id="currentQuantity" type="text" path="currentQuantity" readonly="true"  cssClass="form-control"
                                    autofocus="true"></form:input>
                    </spring:bind>
                    <spring:bind path="remarks">
                        <form:label path="remarks">Remarks </form:label>
                        <form:input type="text" path="remarks"   cssClass="form-control"
                                    autofocus="true"></form:input>
                    </spring:bind>
                    <spring:bind path="tankQuantity">
                        <form:input type="hidden" path="tankQuantity"  id="tankResponseQuantity" value="0.0" ></form:input>
                    </spring:bind>

                </div>

            </div>
            <div class="">

                <p>Quantity in Tank(s):<span style="margin-right: 8%" id="totalSpan">0.0</span>
                    Excess:<span style="margin-right: 8%" id="excessSpan">0.0</span>
                    Excess Percentage:<span style="margin-right: 8%" id="excessPerSpan">0.0</span>
                    Deficiency:<span style="margin-right: 8%" id="deficiencySpan">0.0</span>
                    Deficiency Percentage:<span style="margin-right: 8%" id="deficiencyPerSpan">0.0</span>

                </p>
            </div>



        </div>

        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">

                <thead>
                <tr>
                    <th>Tank Title</th>
                    <th>Dip (mm)</th>
                    <th>Volume</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${calibrationModel.stockTankList}" var="tank" varStatus="tankStatus">

                    <tr>
                        <td>
                                ${tank.name}
                        </td>
                        <td>

                            <select  id="infoDrop${tankStatus.index}" class="form-control" onchange="getVolume(this.value,${tankStatus.index});">
                                <option value="0">0</option>
                                <c:forEach items="${tank.tankInfoList}" var="info" >
                                        <option value="${info.infoId}">${info.dip}</option>
                                </c:forEach>
                                </select>
                        </td>
                        <td>
                            <input id="volumeValue${tankStatus.index}" class="responseVolume form-control" readonly value="0.0"/>
                        </td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Save">

        <div class="totalBillSec">

        </div>
    </form:form>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function () {

    });

    function changeProduct() {
        // Instant Redirection. Can be changed to a POST call in future to avoid DDOS Attacks.
        window.location = "${contextPath}/Calibration/calibrate?stockId=" + $('#stockID').val();
    }

    function getVolume(infoId, rowId) {

            $.ajax({
                url : '${contextPath}/Calibration/getVolume',
                contentType: 'application/json',

                data : {
                    infoId : infoId
                },
                success : function(response) {
                    document.getElementById("volumeValue" + rowId).value = response;
                    calculateVolumeTotal();

                }
            });

    }

    function calculateVolumeTotal() {
        var netTotal = 0;
        $(".responseVolume").each(function (index, ele) {
            netTotal += parseFloat($(ele).val());
        });
        console.log("total", netTotal);
        document.getElementById("totalSpan").textContent = netTotal;
        // Check weather it is Excess or Deficient
        var currentQuantity = $('#currentQuantity').val();
        if (currentQuantity > netTotal){
            document.getElementById("deficiencySpan").textContent = (currentQuantity - netTotal).toFixed(2);
            document.getElementById("deficiencyPerSpan").textContent = (((currentQuantity - netTotal)/currentQuantity) * 100).toFixed(2) +'%';
            document.getElementById("excessSpan").textContent = 0.0;
            document.getElementById("excessPerSpan").textContent = 0.0;
        }else{
            document.getElementById("excessSpan").textContent = (netTotal - currentQuantity).toFixed(2);
            document.getElementById("excessPerSpan").textContent = (((netTotal - currentQuantity)/currentQuantity) * 100).toFixed(2) +'%';
            document.getElementById("deficiencySpan").textContent = 0.0;
            document.getElementById("deficiencyPerSpan").textContent = 0.0;
        }
        $('#tankResponseQuantity').val(netTotal);
    }

</script>
</body>
</html>
