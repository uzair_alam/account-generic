<header>

    <div class="container">
        <div class="header-main">
            <div class="leftNav">
                <nav>
                    <ul>
                        <li><a  href="${contextPath}/home">Home</a></li>
                    </ul>
                </nav>
            </div>
            <div class="rightNav">
                <form action="${contextPath}/logout" method="post">
                    <input   type="submit" value="Logout"/>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </form>
            </div>
        </div>
    </div>
</header>