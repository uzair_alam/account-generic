<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Balance Sheet/PL Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>

<div class="container">


    <form method="post" action="${contextPath}/Reports/sheetReport?${_csrf.parameterName}=${_csrf.token}" class="form-inline">
        <div class="ledgerReportHeaderFilterSec">
            <h3>Balance Sheet / PL Report </h3>

            <div class="ledgerReportHeaderFilterBlockSp">
                <div class="ledgerReportHeaderFilterBlock">
                    <label>Report Type:</label>
                    <select name="reportType" class="form-control" onchange="this.form.submit()">

                        <c:choose>
                            <c:when test="${reportType == 'ProfitAndLoss'}">
                                <option value="ProfitAndLoss" selected>Profit And Loss</option>
                                <option value="BalanceSheet">Balance Sheet</option>
                            </c:when>
                            <c:otherwise>
                                <option value="ProfitAndLoss">Profit And Loss</option>
                                <option value="BalanceSheet" selected>Balance Sheet</option>

                            </c:otherwise>
                        </c:choose>

                    </select>
                    <label> To Date :</label>
                    <input type="date" name="dateTo" class="form-control dateField" value="${toDate}"
                           onchange="this.form.submit()"/>

                </div>
            </div>

        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

    </form>
    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn"
                    onclick="printDiv()">Print
            </button>
            <p colspan="2" align="right" valign=bottom><font color="#000000">Print on : ${currentDate}</font></p>
        </div>

    </div>

</div>

<div class="container" id="printDiv">

    <h2 align="center">${reportType} Report</h2>
    <p align="center">To Date : ${toDate}</p>

    <table cellspacing="0" border="0">
        <colgroup span="13" width="200"></colgroup>
        <tr>

            <td style="border-bottom: 1px solid #000000 ;font-size: large ; font-weight: bold" colspan=7 align="left" valign=bottom><font color="#000000">Account Title</font></td>
            <td style="border-bottom: 1px solid #000000 ;font-size: large ; font-weight: bold" colspan=4 align="right" valign=bottom><font color="#000000">Current
                Amount</font></td>
        </tr>


        <c:set var="totalDebit" value="${0}"/>
        <c:set var="totalCredit" value="${0}"/>
        <c:forEach items="${reportSheet.reportStagingList}" var="reportStage">
            <c:set var="totalCredit" value="${totalCredit + trail.credit}"/>
            <c:set var="totalDebit" value="${totalDebit + trail.debit}"/>
            <tr>

                <td colspan="7" align="left" valign=bottom><font color="#000000">${reportStage.title}</font></td>


                <c:choose>
                    <c:when test="${reportStage.subTotal == true}">

                        <c:choose>
                            <c:when test="${reportStage.currentAmount >= 0.0}">
                                <td style="border-top: 0px solid #000000" align="right" colspan="4" valign=bottom><font color="#000000"><fmt:formatNumber type="number" maxFractionDigits="2" value="${reportStage.currentAmount}"/></font>
                                </td>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${reportType == 'ProfitAndLoss'}">
                                        <td style="border-top: 0px solid #000000" align="right" colspan="4" valign=bottom><font color="#000000">(<fmt:formatNumber type="number"
                                                                                                   maxFractionDigits="2"
                                                                                                   value="${-1*reportStage.currentAmount}"/>)</font>
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td style="border-top: 0px solid #000000" align="right" colspan="4" valign=bottom><font color="#000000"><fmt:formatNumber type="number"
                                                                                                  maxFractionDigits="2"
                                                                                                  value="${-1*reportStage.currentAmount}"/></font>
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>

                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${reportStage.total == true}">

                                <c:choose>
                                    <c:when test="${reportStage.currentAmount >= 0.0}">
                                        <td style="border-top: 0px solid #000000;border-bottom: 0px solid #000000"
                                            align="right" colspan="4" valign=bottom><font
                                                color="#000000"><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                                  value="${reportStage.currentAmount}"/></font>
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${reportType == 'ProfitAndLoss'}">
                                                <td style="border-top: 0px solid #000000;border-bottom: 0px solid #000000"
                                                    align="right" colspan="4" valign=bottom><font
                                                        color="#000000">(<fmt:formatNumber type="number"
                                                                                           maxFractionDigits="2"
                                                                                           value="${-1*reportStage.currentAmount}"/>)</font>
                                                </td>
                                            </c:when>
                                            <c:otherwise>
                                                <td style="border-top: 0px solid #000000;border-bottom: 0px solid #000000"
                                                    align="right" colspan="4" valign=bottom><font
                                                        color="#000000"><fmt:formatNumber type="number"
                                                                                          maxFractionDigits="2"
                                                                                          value="${-1*reportStage.currentAmount}"/></font>
                                                </td>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>


                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${reportStage.currentAmount >= 0.0}">
                                        <td align="right" colspan="4" valign=bottom><font
                                                color="#000000"><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                                  value="${reportStage.currentAmount}"/></font>
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${reportType == 'ProfitAndLoss'}">
                                                <td align="right" colspan="4" valign=bottom><font
                                                        color="#000000">(<fmt:formatNumber type="number"
                                                                                           maxFractionDigits="2"
                                                                                           value="${-1*reportStage.currentAmount}"/>)</font>
                                                </td>
                                            </c:when>
                                            <c:otherwise>
                                                <td align="right" colspan="4" valign=bottom><font
                                                        color="#000000"><fmt:formatNumber type="number"
                                                                                          maxFractionDigits="2"
                                                                                          value="${-1*reportStage.currentAmount}"/></font>
                                                </td>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>

                    </c:otherwise>
                </c:choose>
            </tr>

            <c:choose>
                <c:when test="${reportStage.subTotal || reportStage.total}">
                    <tr>
                        <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
                        <td align="left" valign=bottom><font color="#000000"><br></font></td>
                        <td align="left" valign=bottom><font color="#000000"><br></font></td>
                        <td align="left" valign=bottom><font color="#000000"><br></font></td>
                        <td align="left" valign=bottom><font color="#000000"><br></font></td>
                        <td align="left" valign=bottom><font color="#000000"><br></font></td>
                        <td align="left" valign=bottom><font color="#000000"><br></font></td>
                        <td align="left" valign=bottom><font color="#000000"><br></font></td>
                        <td align="left" valign=bottom><font color="#000000"><br></font></td>
                        <td align="left" valign=bottom><font color="#000000"><br></font></td>
                        <td align="left" valign=bottom><font color="#000000"><br></font></td>

                    </tr>
                </c:when>
            </c:choose>
        </c:forEach>
        <tr>
            <td style="border-bottom: 1px solid #000000" height="20" align="left" valign=bottom><font
                    color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font>
            </td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font>
            </td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font>
            </td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font>
            </td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font>
            </td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font>
            </td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font>
            </td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font>
            </td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font>
            </td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font>
            </td>
        </tr>


    </table>

</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function () {

    });

    function printDiv() {

        var divToPrint = document.getElementById('printDiv');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<img src="${imageUrl}" alt="img" width="200" height="80">' + divToPrint.innerHTML +
            '</body></html>');
        newWin.document.close();

        setTimeout(function () {
            newWin.close();
        }, 50000);

    }
</script>
</body>
</html>