<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Chart Of Account Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<%@ include file = "../header.jsp" %>

<div class="container">
    <h2>Chart Of Account Reports</h2>

    <form method="post" action="${contextPath}/Reports/chartOfAccount?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-right">

        <label> Level :</label>
        <select name="level" class="form-control" onchange="this.form.submit()">

            <c:forEach items="${levels}" var="level">
                <c:choose>
                    <c:when test="${level == currentLevel}">
                        <option value="${level}" selected>Level ${level}</option>

                    </c:when>
                    <c:otherwise>
                        <option value="${level}" >Level ${level}</option>

                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <label> Level Option :</label>
        <select name="levelOption" class="form-control" onchange="this.form.submit()">
            <c:choose>
                <c:when test="${currentLevelOption == 0}">
                    <option value="0" selected>Head Of Accounts</option>
                    <option value="1" >Details</option>
                </c:when>
                <c:otherwise>
                    <option value="0" >Head Of Accounts</option>
                    <option value="1" selected>Details</option>

                </c:otherwise>
            </c:choose>

        </select>
        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
        <button class="btn btn-success" id = "printBtn" style="background-color: #292e5a"  onclick="printDiv()">Print</button>
    </form>

</div>
<div class="container" id = "printDiv">
    <h3>Chart Of Account Level ${currentLevel}</h3>
    <ol>
        <c:forEach items="${baseAccounts}" var="baseAccount">

            <li><strong>${baseAccount.title}</strong></li><ul style=" list-style:square inside;" >
            <c:forEach items="${baseAccount.childList}" var="subControl">
                <li>${subControl.title}<ul >
                    <c:forEach items="${subControl.childList}" var="subSubControl">
                        <li>${subSubControl.title}<ul style=" list-style:circle inside;">
                            <c:forEach items="${subSubControl.childList}" var="childAccount">
                                <li>${childAccount.title}</li>
                            </c:forEach>
                        </ul></li>
                    </c:forEach>
                </ul></li>
            </c:forEach>
        </ul></li>



        </c:forEach>
    </ol>

</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    function printDiv()
    {

        var divToPrint=document.getElementById('printDiv');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

        newWin.document.close();

        setTimeout(function(){newWin.close();},50000);

    }
</script>
</body>
</html>