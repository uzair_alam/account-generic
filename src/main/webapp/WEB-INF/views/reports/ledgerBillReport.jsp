<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Bill Detail</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body>
<%@ include file="../header.jsp" %>


<div class="container">


    <div class="ledgerReportHeaderFilterSec">
        <h3>Bill</h3>
        <div class="ledgerReportHeaderFilterBlockSp">
            <div class="ledgerReportHeaderFilterBlock">
                <form method="post"
                      action="${contextPath}/Reports/ledgerBillReport?${_csrf.parameterName}=${_csrf.token}"
                      class="form-inline pull-right">
                    <label> From Date :</label>
                    <input type="date" name="dateFrom" class="form-control " onchange="this.form.submit()"
                           value="${fromDate}"/>
                    <label> To Date :</label>
                    <input type="date" name="dateTo" class="form-control " onchange="this.form.submit()"
                           value="${toDate}"/>
                    <label> Account :</label>
                    <select name="accountCode" id="accountCode" class="form-control " onchange="this.form.submit()">

                        <option value="">NONE</option>
                        <c:forEach items="${accounts}" var="account">
                            <c:choose>
                                <c:when test="${account.accountCode == currentAccount}">
                                    <option value="${account.accountCode}" selected>${account.title}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${account.accountCode}">${account.title}</option>


                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <label style="display: none"> Cost Centre :</label>
                    <select style="display: none" name="costCentreCode" class="form-control "
                            onchange="this.form.submit()">

                        <option value="">NONE</option>
                        <c:forEach items="${costCentres}" var="costCentre">
                            <c:choose>
                                <c:when test="${costCentre.accountCode == currentCostCentre}">
                                    <option value="${costCentre.accountCode}" selected>${costCentre.title}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${costCentre.accountCode}">${costCentre.title}</option>

                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <label style="display: none">Select Month :</label>
                    <select style="display: none" name="month" class="form-control " onchange="this.form.submit()">
                        <option selected='selected'>${currentMonth}</option>
                        <c:forEach items="${monthList}" var="month">
                            <option value="${month}">${month}</option>

                        </c:forEach>
                    </select>
                    <label style="display: none">Year :</label>
                    <select style="display: none" name="year" class="form-control" onchange="this.form.submit()">
                        <option selected='selected'>${currentYear}</option>
                        <c:forEach items="${yearList}" var="year">
                            <option value="${year}">${year}</option>

                        </c:forEach>
                    </select>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                </form>
                <form method="post"
                      action="${contextPath}/Account/ledgerBillReport?${_csrf.parameterName}=${_csrf.token}"
                      class="form-inline pull-right">
                    <label>Month :</label>
                    <select name="month" class="form-control " onchange="this.form.submit()">
                        <option selected='selected'>${currentMonth}</option>
                        <c:forEach items="${monthList}" var="month">
                            <option value="${month}">${month}</option>

                        </c:forEach>
                    </select>
                    <label>Year :</label>
                    <select name="year" class="form-control" onchange="this.form.submit()">
                        <option selected='selected'>${currentYear}</option>
                        <c:forEach items="${yearList}" var="year">
                            <option value="${year}">${year}</option>

                        </c:forEach>
                    </select>
                    <label style="display: none"> Cost Centre :</label>
                    <select style="display: none" name="costCentreCode" class="form-control "
                            onchange="this.form.submit()">

                        <option value="">NONE</option>
                        <c:forEach items="${costCentres}" var="costCentre">
                            <c:choose>
                                <c:when test="${costCentre.accountCode == currentCostCentre}">
                                    <option value="${costCentre.accountCode}" selected>${costCentre.title}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${costCentre.accountCode}">${costCentre.title}</option>

                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <input style="display: none" name="currentAccount" value="${currentAccount}">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </form>

            </div>
        </div>

        <div class="ledgerReportHalfSec" id="ledgerReportDataInfo" style="align-items: center">
            <div class="ledgerReportUserInfo">
                <p style="font-weight: bold; align-content: center">Complete Customized Bill</p>
                <p>Account Name : ${accountMap.get(currentAccount)}</p>
            </div>
            <div class="ledgerReportDateSec">
                <p>Date From : ${fromDate} To : ${toDate}</p>
            </div>
        </div>
    </div>


    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn"
                    onclick="printDiv()">Print
            </button>
            <p colspan="2" align="right" valign=bottom><font color="#000000">Print on : ${currentDate}</font></p>
        </div>

    </div>


</div>

<section class="main">
    <div class="container" id="printDiv">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">


                    <table class="table tableLedger">

                        <thead>
                        <tr>

                            <th>Item Name</th>
                            <th>Description</th>
                            <th>Quantity</th>
                            <th>Rate</th>
                            <th>Amount</th>

                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="total" value="0"></c:set>
                        <c:forEach items="${vouchers.internalVoucherList}" var="internalVoucher" varStatus="status">

                            <c:choose>
                                <c:when test="${internalVoucher.itemQuantity!=0.0}">
                                    <tr>
                                        <td>${accountMap.get(internalVoucher.accountCode)}</td>
                                        <td></td>
                                        <td><fmt:formatNumber maxFractionDigits="2"
                                                              value="${internalVoucher.itemQuantity}"/></td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${internalVoucher.itemRate}"/></td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${internalVoucher.credit}"/></td>
                                        <c:set var="total" value="${total+internalVoucher.credit}"></c:set>
                                    </tr>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                        <tr>
                            <td></td>
                            <td>Total</td>
                            <td></td>
                            <td></td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${total}"/></td>
                        </tr>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    // $(document).ready(function () {
    //     accountCodeEqual();
    //     $('#accountCode').change(function () {
    //         accountCodeEqual();
    //     });
    //
    // });
    // function accountCodeEqual() {
    //     $('#accountMonthCode').val($('#accountCode').val());
    // }
    function printDiv() {

        var divToPrint = document.getElementById('printDiv');
        var ledgerReportDataInfo = document.getElementById('ledgerReportDataInfo');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            '  <link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<img src="${imageUrl}" alt="img" width="200" height="80">' + ledgerReportDataInfo.innerHTML + divToPrint.innerHTML +
            '</body></html>');

        newWin.document.close();

        setTimeout(function () {
            newWin.close();
        }, 50000);

    }
</script>
</body>
</html>
