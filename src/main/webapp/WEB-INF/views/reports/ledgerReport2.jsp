<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ledger Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>

<div class="container">
    <h2>Ledger Reports</h2>

    <form method="post" action="${contextPath}/Reports/ledgerReport?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-right">


        <label> From Date :</label>
        <input type="date"  name="dateFrom"   class="form-control " onchange="this.form.submit()" value="${fromDate}" />
        <label> To Date :</label>
        <input type="date"  name="dateTo"   class="form-control "  onchange="this.form.submit()" value="${toDate}"   />
        </br>
        <label> Account :</label>
        <select name="accountCode" class="form-control " onchange="this.form.submit()">

            <option value="" >NONE</option>
            <c:forEach items="${accounts}" var="account">
                <c:choose>
                    <c:when test="${account.accountCode == currentAccount}">
                        <option value="${account.accountCode}" selected>${account.title}</option>

                    </c:when>
                    <c:otherwise>
                        <option value="${account.accountCode}" >${account.title}</option>

                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <label> Cost Centre :</label>
        <select name="costCentreCode" class="form-control " onchange="this.form.submit()">

            <option value="" >NONE</option>
            <c:forEach items="${costCentres}" var="costCentre">
                <c:choose>
                    <c:when test="${costCentre.accountCode == currentCostCentre}">
                        <option value="${costCentre.accountCode}" selected>${costCentre.title}</option>

                    </c:when>
                    <c:otherwise>
                        <option value="${costCentre.accountCode}" >${costCentre.title}</option>

                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>

    </form>
    <button class="btn btn-success" id = "printBtn" onclick="printDiv()">Print</button>

</div>
<div class="container" id = "printDiv">
    <h3>Account Ledger</h3>
    <table cellspacing="0" border="0">
        <colgroup span="15" width="64"></colgroup>
        <tr>
            <td colspan="15" height="20" align="center" valign=bottom><font color="#000000">Audit Trial - Datewise or Audit Trial - Voucherwise</font></td>
        </tr>
        <tr>
            <td colspan="15" height="20" align="center" valign=bottom><font color="#000000">
                Date From : ${fromDate}  To : ${toDate}
            </font>
            </td>
        </tr>
        <tr>
            <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
        </tr>
        <tr>
            <td height="20" align="left" valign=bottom><font color="#000000">Print on :</font></td>
            <td colspan="2" align="left" valign=bottom><font color="#000000">${currentDate}</font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000">Page : a/zz</font></td>
        </tr>
        <tr>
            <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid #000000" height="20" align="center" valign=bottom><font color="#000000">V.No</font></td>
            <td align="center" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="center" valign=bottom><font color="#000000">V.Date</font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" colspan=5 align="left" valign=bottom><font color="#000000">Description</font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="center" valign=bottom><font color="#000000">Cheque No</font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="center" valign=bottom><font color="#000000">Credit</font></td>
            <td style="border-bottom: 1px solid #000000" align="center" valign=bottom><font color="#000000">Debit</font></td>
            <td style="border-bottom: 1px solid #000000" align="center" valign=bottom><font color="#000000">Balance</font></td>
        </tr>
        <c:set var="totalDebit" value="${0}"/>
        <c:set var="totalCredit" value="${0}"/>
        <c:forEach items="${vouchers}" var="voucher" varStatus="status">


            <tr>
                <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
            </tr>
            <tr>
                <td colspan="2" height="20" align="left" valign=bottom><font color="#000000">${voucher.voucherNumber}</font></td>
                <td colspan="2"align="left" valign=bottom ><font color="#000000">${voucher.voucherDate}</font></td>
                <c:choose>
                    <c:when test="${status.index == 0}">

                        <td colspan="4" align="left" valign=bottom><font color="#000000">Opening Balance</font></td>
                    </c:when>
                    <c:otherwise>
                        <td colspan="4" align="left" valign=bottom><font color="#000000"></font></td>
                    </c:otherwise>
                </c:choose>

                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <c:choose>
                    <c:when test="${status.index == 0}">
                        <c:set var="ledgerBalance" value="${voucher.balance + voucher.credit - voucher.debit}" />
                        <c:choose>
                            <c:when test="${ledgerBalance > 0}">

                                <td align="right" valign=bottom><font color="#000000">${ledgerBalance} DR</font></td>
                            </c:when>
                            <c:otherwise>
                                <td align="right" valign=bottom><font color="#000000">${ledgerBalance} CR</font></td>
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <td align="right" valign=bottom><font color="#000000"></font></td>
                    </c:otherwise>
                </c:choose>

            </tr>
            <tr border="2">
                <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td colspan="4" align="left" valign=bottom><font color="#000000">${accountMap.get(voucher.bankAccount)}</font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="right" valign=bottom ><font color="#000000">${voucher.chequeNumber}</font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <c:choose>
                    <c:when test="${voucher.voucherType == 'J'}">

                        <td align="left" valign=bottom><font color="#000000"><br></font>${voucher.debit}</td>
                        <td align="right" valign=bottom><font color="#000000"><br></font>${voucher.credit }</td>
                        <c:set var="totalCredit" value="${totalCredit + voucher.debit}" />
                        <c:set var="totalDebit" value="${totalDebit + voucher.credit}" />
                    </c:when>
                    <c:otherwise>
                        <td align="left" valign=bottom><font color="#000000"><br></font>${voucher.credit}</td>
                        <td align="right" valign=bottom><font color="#000000"><br></font>${voucher.debit }</td>
                        <c:set var="totalCredit" value="${totalCredit + voucher.credit}" />
                        <c:set var="totalDebit" value="${totalDebit + voucher.debit}" />
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${voucher.balance > 0}">
                        <td align="right" valign=bottom><font color="#000000">${voucher.balance} DR</font></td>
                    </c:when>
                    <c:otherwise>
                        <td align="right" valign=bottom><font color="#000000">${voucher.balance} CR</font></td>
                    </c:otherwise>
                </c:choose>
            </tr>
            <tr>
                <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td colspan="4" align="left" valign=bottom><font color="#000000">${costCentreMap.get(voucher.costCentre)}</font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
            </tr>
            <tr>
                <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td colspan="4" align="left" valign=bottom><font color="#000000"></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="right" valign=bottom><font color="#000000"></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
            </tr>
            <tr>
                <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
            </tr>
            <tr>
                <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td  colspan="6" align="left" valign=bottom><font color="#000000">${voucher.remarks}</font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000000" height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            </tr>
        </c:forEach>
        <tr>
            <td style="border-bottom: 1px solid #000000" height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000">Total: </font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000">
                <fmt:formatNumber type="number" maxFractionDigits="2" value="${totalCredit}"/></font></td>
            <td style="border-bottom: 1px solid #000000" align="right" valign=bottom><font color="#000000">
                <fmt:formatNumber type="number" maxFractionDigits="2" value="${totalDebit}"/></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
        </tr>
    </table>

</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>




    function printDiv()
    {

        var divToPrint=document.getElementById('printDiv');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '\t\n' +
            '</head>\n<body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

        newWin.document.close();

        setTimeout(function(){newWin.close();},10);

    }
</script>
</body>
</html><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ledger Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>

<div class="container">
    <h2>Ledger Reports</h2>

    <form method="post" action="${contextPath}/Reports/ledgerReport?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-right">


        <label> From Date :</label>
        <input type="date"  name="dateFrom"   class="form-control " onchange="this.form.submit()" value="${fromDate}" />
        <label> To Date :</label>
        <input type="date"  name="dateTo"   class="form-control "  onchange="this.form.submit()" value="${toDate}"   />
        </br>
        <label> Account :</label>
        <select name="accountCode" class="form-control " onchange="this.form.submit()">

            <option value="" >NONE</option>
            <c:forEach items="${accounts}" var="account">
                <c:choose>
                    <c:when test="${account.accountCode == currentAccount}">
                        <option value="${account.accountCode}" selected>${account.title}</option>

                    </c:when>
                    <c:otherwise>
                        <option value="${account.accountCode}" >${account.title}</option>

                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <label> Cost Centre :</label>
        <select name="costCentreCode" class="form-control " onchange="this.form.submit()">

            <option value="" >NONE</option>
            <c:forEach items="${costCentres}" var="costCentre">
                <c:choose>
                    <c:when test="${costCentre.accountCode == currentCostCentre}">
                        <option value="${costCentre.accountCode}" selected>${costCentre.title}</option>

                    </c:when>
                    <c:otherwise>
                        <option value="${costCentre.accountCode}" >${costCentre.title}</option>

                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>

    </form>
    <button class="btn btn-success"  onclick="printDiv()">Print</button>

</div>
<div class="container" id = "printDiv">
    <header>
        <div class="container">
            <div class="ledgerReportHeaderSec">
                <div class="ledgerReportHeaderFilterSec">
                    <h3>Ledger Reports</h3>

                    <div class="ledgerReportHeaderFilterBlockSp">
                        <div class="ledgerReportHeaderFilterBlock">
                            <label>From Date  :</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="ledgerReportUserInfo">
                    <p>Account Ledger : Zerkamin</p>
                </div>
                <div class="ledgerReportDateSec">
                    <p>Date From : 2017-11-01 To : 2017-11-21</p>
                </div>
            </div>
        </div>
    </header>
    <section class="main">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-table">
                        <table class="table tableLedger">
                            <thead>
                            <tr>
                                <th>V.No</th>
                                <th>V.Date</th>
                                <th>Description</th>
                                <th>Cheque No</th>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th>Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${vouchers}" var="voucher" varStatus="status">
                            <tr>
                                <td>27/10/2017</td>
                                <td>Name</td>
                                <td>Client</td>
                                <td>php</td>
                                <td>
                                    <p>09:00:00
                                        <span>09:00:00</span>
                                    </p>
                                </td>
                                <td>
                                    <p>09:00:00
                                        <span>09:00:00</span>
                                    </p></td>
                                <td>
                                    <p>09:00:00
                                        <span>09:00:00</span>
                                    </p>
                                </td>
                            </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>




    function printDiv()
    {

        var divToPrint=document.getElementById('printDiv');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '\t\n' +
            '</head>\n<body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

        newWin.document.close();

        setTimeout(function(){newWin.close();},50000);

    }
</script>
</body>
</html>