<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Note Summary Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>

<div class="container">

    <form method="post" action="${contextPath}/Reports/noteSummary?${_csrf.parameterName}=${_csrf.token}"
          class="form-inline">

        <div class="ledgerReportHeaderFilterSec">
            <h3>Note Summary</h3>

            <div class="ledgerReportHeaderFilterBlockSp">
                <div class="ledgerReportHeaderFilterBlock">
                    <label>Report Type:</label>
                    <select name="reportType" class="form-control" onchange="this.form.submit()">

                        <c:choose>
                            <c:when test="${reportType == 'ProfitAndLoss'}">
                                <option value="ProfitAndLoss" selected>Profit And Loss</option>
                                <option value="BalanceSheet">BalanceSheet</option>
                            </c:when>
                            <c:otherwise>
                                <option value="ProfitAndLoss">Profit And Loss</option>
                                <option value="BalanceSheet" selected>BalanceSheet</option>

                            </c:otherwise>
                        </c:choose>

                    </select>
                    <label> To Date :</label>
                    <input type="date" name="dateTo" class="form-control dateField" value="${toDate}"
                           onchange="this.form.submit()"/>

                </div>
            </div>
            <div class="ledgerReportHalfSec" id="ledgerReportDataInfo">
                <div class="ledgerReportUserInfo">
                    <p>${reportType} Report </p>
                    <p>Trial Balance - Datewise</p>
                </div>
                <div class="ledgerReportDateSec">
                    <p>To Date : ${toDate}</p>
                </div>
            </div>
            <div class="left">
                <div class="pdfLinkSec">
                    <button class="btn btn-success" style="background-color: #292e5a" id="printBtn"
                            onclick="printDiv()">Print
                    </button>
                    <p colspan="2" align="left" valign=bottom><font color="#000000">Print on : ${currentDate}</font></p>
                </div>

            </div>

        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

    </form>

</div>
<section class="main">
    <div class="container" id="printDiv">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">


                    <table class="table tableLedger">
                        <thead>
                        <tr>
                            <th>Note Heading</th>
                            <th>Sub-Heading</th>
                            <th>Account</th>
                            <th>Quantity</th>
                            <th>Rate</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="totalDebit" value="${0}"/>
                        <c:set var="totalCredit" value="${0}"/>
                        <c:forEach items="${accountNotes}" var="accountNote">
                            <tr>

                                <td style="font-weight: bold;font-size: large">${accountNote.heading}</td>
                                <td><br></td>
                                <td><br></td>
                                <td><br></td>
                                <td><br></td>
                                <td><br></td>
                            </tr>

                            <c:forEach items="${accountNote.noteSubHeadings}" var="subHeading">
                                <tr style="background-color: #66512c ">
                                    <td><br></td>
                                    <td style="font-weight: bold;font-size: medium ;color:white">${subHeading.heading}</td>
                                    <td><br></td>
                                    <td><br></td>
                                    <td><br></td>
                                    <td><br></td>
                                </tr>
                                <c:set var="stageTotalOpening" value="${0.0}"/>
                                <c:forEach items="${subHeading.trialBalanceSetListOpening}" var="trail2">
                                    <tr>
                                        <td><br></td>
                                        <td><br></td>
                                        <td>${trail2.accountTitle}</td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${trail2.saleQuantity}"/></td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${trail2.itemRate}"/></td>
                                        <td>${trail2.credit + trail2.debit}</td>
                                    </tr>
                                    <c:set var="stageTotalOpening"
                                           value="${stageTotalOpening + trail2.credit + trail2.debit}"/>
                                </c:forEach>
                                <tr>
                                    <td><br></td>
                                    <td><br></td>
                                    <td><br></td>
                                    <td><br></td>
                                    <td><br></td>
                                    <c:choose>
                                        <c:when test="${stageTotalOpening > 0}">
                                            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${stageTotalOpening}"/></td>
                                        </c:when>
                                        <c:otherwise>
                                            <td><br></td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>


                                <c:set var="stageTotal" value="${0.0}"/>
                                <c:forEach items="${subHeading.trialBalanceSetList}" var="trail">
                                    <c:set var="totalCredit" value="${totalCredit + trail.credit}"/>
                                    <c:set var="totalDebit" value="${totalDebit + trail.debit}"/>
                                    <tr>

                                        <td><br></td>
                                        <td><br></td>
                                        <td>${trail.accountTitle}</td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${trail.saleQuantity}"/></td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${trail.itemRate}"/></td>
                                        <td>${trail.credit + trail.debit}</td>
                                    </tr>

                                    <c:set var="stageTotal" value="${stageTotal + trail.credit + trail.debit}"/>
                                </c:forEach>
                                <tr>
                                    <td><br></td>
                                    <td><br></td>
                                    <td><br></td>
                                    <td><br></td>
                                    <td><br></td>
                                    <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                          value="${stageTotal}"/></td>

                                </tr>
                                <c:set var="stageTotalClosing" value="${0.0}"/>
                                <c:forEach items="${subHeading.trialBalanceSetListClosing}" var="trail3">

                                    <tr>
                                        <td><br></td>
                                        <td><br></td>
                                        <td>${trail3.accountTitle}</td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${trail3.saleQuantity}"/></td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${trail3.itemRate}"/></td>
                                        <td>${trail3.credit + trail3.debit}</td>
                                    </tr>

                                    <c:set var="stageTotalClosing"
                                           value="${stageTotalClosing + trail3.credit + trail3.debit}"/>
                                </c:forEach>
                                <td><br></td>
                                <td><br></td>
                                <td><br></td>
                                <td><br></td>
                                <td><br></td>
                                <c:choose>
                                    <c:when test="${stageTotalClosing > 0}">
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${stageTotalClosing}"/></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><br></td>
                                    </c:otherwise>
                                </c:choose>
                                </tr>
                            </c:forEach>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function () {

    });

    function printDiv() {

        var divToPrint = document.getElementById('printDiv');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<img src="${imageUrl}" alt="img" width="200" height="80">' + ledgerReportDataInfo.innerHTML + divToPrint.innerHTML +
            '</body></html>');
        newWin.document.close();

        setTimeout(function () {
            newWin.close();
        }, 50000);

    }
</script>
</body>
</html>