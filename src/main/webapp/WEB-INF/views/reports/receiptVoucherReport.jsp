<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Receipt Voucher Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>

<div class="container">


    <form method="post" action="${contextPath}/Reports/receiptVoucher?${_csrf.parameterName}=${_csrf.token}"
          class="form-inline pull-right">
        <div class="ledgerReportHeaderFilterSec">
            <h3 style="font-size: large">Receipt Voucher Report</h3>

            <div class="ledgerReportHeaderFilterBlockSp">
                <div class="ledgerReportHeaderFilterBlock">
                    <label> From Date :</label>
                    <input type="date" name="dateFrom" class="form-control " onchange="this.form.submit()"
                           value="${fromDate}"/>
                    <label> To Date :</label>
                    <input type="date" name="dateTo" class="form-control " onchange="this.form.submit()"
                           value="${toDate}"/>
                    <label> From Voucher :</label>
                    <select name="voucherNumberFrom" class="form-control" onchange="this.form.submit()">

                        <option value="none" selected>NONE</option>
                        <c:forEach items="${voucherNumbers}" var="voucherNumber">
                            <c:choose>
                                <c:when test="${voucherNumber == fromVoucher}">
                                    <option value="${voucherNumber}" selected>${voucherNumber}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${voucherNumber}">${voucherNumber}</option>

                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <label> To Voucher :</label>
                    <select name="voucherNumberTo" class="form-control voucherField" onchange="this.form.submit()">

                        <option value="none" selected>NONE</option>
                        <c:forEach items="${voucherNumbers}" var="voucherNumber">
                            <c:choose>
                                <c:when test="${voucherNumber == toVoucher}">
                                    <option value="${voucherNumber}" selected>${voucherNumber}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${voucherNumber}">${voucherNumber}</option>

                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>

                </div>
            </div>

            <div class="ledgerReportHalfSec" id="ledgerReportDataInfo">
                <div class="ledgerReportUserInfo">
                    <p>Receipt Vouchers</p>
                </div>
                <div class="ledgerReportDateSec">
                    <p>Date From : ${fromDate} To : ${toDate}</p>
                </div>
            </div>

        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>


    </form>
    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn"
                    onclick="printDiv()">Print
            </button>
            <p colspan="2" align="right" valign=bottom><font color="#000000">Print on : ${currentDate}</font></p>
        </div>

    </div>
</div>

<section class="main">
    <div class="container" id="printDiv">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">


                    <table class="table tableLedger">

                        <thead>
                        <tr>
                            <th>V.No</th>
                            <th>V.Date</th>
                            <th>Description</th>
                            <th>Cheque No</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>


                        <c:forEach items="${vouchers}" var="voucher">
                            <c:choose>
                                <c:when test="${voucher.credit != 0.0}">
                                    <tr>
                                        <td>${voucher.voucherNumber}</td>
                                        <td>${voucher.voucherDate}</td>
                                        <td>${accountMap.get(voucher.bankAccount)}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>${accountMap.get(voucher.accountCode)}</td>
                                        <td>${voucher.chequeNumber}</td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${voucher.credit + voucher.debit}"/></td>
                                    </tr>
                                </c:when>
                            </c:choose>

                            <%--<tr>--%>
                            <%--<td></td>--%>
                            <%--<td></td>--%>
                            <%--<td>${costCentreMap.get(voucher.costCentre)}</td>--%>
                            <%--<td></td>--%>
                            <%--<td></td>--%>
                            <%--</tr>--%>
                            <%--<tr>--%>
                            <%--<td></td>--%>
                            <%--<td></td>--%>
                            <%--<td>${accountMap.get(voucher.taxAccount)}</td>--%>
                            <%--<td></td>--%>
                            <%--<td>${voucher.taxAmount}</td>--%>
                            <%--</tr>--%>
                            <%--<tr>--%>
                            <%--<td></td>--%>
                            <%--<td></td>--%>
                            <%--<td>${voucher.remarks}</td>--%>
                            <%--<td></td>--%>
                            <%--<td></td>--%>
                            <%--</tr>--%>
                        </c:forEach>

                        </tbody>


                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap-datepicker.js"></script>
<script src="${contextPath}/resources/js/jquery-3.2.1.min.js"></script>
<script>

    $(document).ready(function () {
        $('.dateField').change(function () {
            $('.voucherField').val('none');
            this.form.submit();

        });
    });

    function printDiv() {

        var divToPrint = document.getElementById('printDiv');
        var ledgerReportDataInfo = document.getElementById('ledgerReportDataInfo');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            '  <link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<img src="${imageUrl}" alt="img" width="200" height="80">' + ledgerReportDataInfo.innerHTML + divToPrint.innerHTML +
            '</body></html>');


        newWin.document.close();

        setTimeout(function () {
            newWin.close();
        }, 50000);

    }
</script>
</body>
</html>