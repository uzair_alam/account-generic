<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Uni Pharma</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            padding-top: 10px;
        }

        .container {
            margin: 0 auto;
            max-width: 1000px;
        }

        table {
            width: 100%
        }

        .logo {
            width: 80px;
        }

        img {
            width: 80px;
            height: 60%;
        }

        .company {
            text-align: left
        }

        .fullwidth thead {
            width: 100%;
        }

        .border {
            border-spacing: 0;
        }

        .border thead th,
        .border tbody td {
            height: 28px;
            border: 1px solid black
        }

        .bordernone tbody td {
            border: none;
        }

        table.half tbody tr td:first-child {
            width: 45.75%;
            text-align: right;
            border-right: 2px solid;
            position: relative;
            top: -3px;
            padding-right: 5px;
        }
        .footer{position: fixed;bottom: 0;}
        .footer table td{font-size: 14px;padding-right: 15px;}
        table.half tbody tr td:last-child {
            width: 54%;
            text-align: right;
            padding-right: 15px;
        }.footer h3 { display: inline;}

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
            .gap {
                padding: 10px;
            }
            .estimate {
                width: 250px;
            }
        }
    </style>
</head>

<body>
    <div class="container">
        <table>
            <thead>
                <tr>
                    <th class="logo">
                        <img src="${contextPath}/resources/img/Uni-logo.png" alt="logo">
                    </th>
                    <td>
                        <table class="gap">
                            <thead>
                                <tr>
                                    <th colspan="2" class="company">UNI PHARMA</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="3">
                                        Suite # CA 25-FG/02, Chandni Chowk, Main University Road, Gulshan Town, East,
                                    </td>
                                </tr>
                                <tr>
                                    <td>Karachi, Pakistan</td>
                                </tr>
                                <tr>
                                    <td>Email: unipharma@gmail.com</td>
                                </tr>
                                <tr>
                                    <td>Phone:+92-213-485-5002 +92-213-485-5003</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <table class="estimate">
                            <th>Estimate</th>
                            <tr>
                                <td colspan="4">Date: ${currentDate}</td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <b>Invoice No.: ${voucherNumber}</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    Sales Person:
                                </td>
                            </tr>
                            <tr></tr>
                            <tr></tr>
                        </table>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr class="line">
                    <td colspan="5" style="border-bottom:1px solid"></td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align:center">
                        <h3>
                            <u>DELIVERY CHALLAN</u>
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td>To:</td>
                </tr>
            </tbody>
        </table>
        <table class="border">
            <thead>
                <tr>
                    <th>Qty
                        <br> Sale/Bonus</th>
                    <th>Item
                        <br> Description</th>
                    <th>Batch
                        <br> No</th>
                    <th>Packing
                        <br> Size</th>
                    <th>Unit
                        <br> Price</th>
                    <th>Total
                        <br> Value</th>
                    <th>Disc
                        <br> % </th>
                    <th>Net
                        <br> Amount</th>
                </tr>
            </thead>
            <tbody>
    <c:set var="total" value="0"/>
    <c:forEach items="${voucherList}" var="voucher">
                <tr>
                    <td>${voucher.itemQuantity}</td>
                    <td>${accountMap.get(voucher.accountCode)}</td>
                    <td></td>
                    <td></td>
                    <td>${voucher.credit}</td>
                    <td></td>
                    <td></td>
                    <td>${voucher.credit}</td>
                     <c:set var="total" value="${total+voucher.credit}"/>

                </tr>
                    </c:forEach>
                <tr class="bordernone">
                    <td colspan="4" style="border: none;position: relative;top: -46px">
                        <b>Amount In words</b>
                    </td>
                    <td colspan="4">
                        <table class="half" cellspacing="0" ;>
                            <tbody>
                                <tr>
                                    <td colspan="2">Transportation:</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Sub Total:</td>
                                    <td>${total}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Previous Balance:</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>Net Total:</b>
                                    </td>
                                    <td>${total}</td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <table>
            <tr style="float: right; margin-top:50px;">
                <td>Signature: ______________________________</td>
            </tr>
        </table>
    </div>
</body>

</html>