package com.hellokoding.account.validator;

        import com.hellokoding.account.model.Account;
        import com.hellokoding.account.model.CostCentre;
        import org.springframework.stereotype.Component;
        import org.springframework.validation.Errors;
        import org.springframework.validation.ValidationUtils;
        import org.springframework.validation.Validator;

@Component
public class CostCentreValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return CostCentre.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        CostCentre costCentre = (CostCentre) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "NotEmpty");
        if (costCentre.getTitle().length() < 3 || costCentre.getTitle().length() > 32) {
            errors.rejectValue("title", "Size.accountForm.title");
        }


    }
}
