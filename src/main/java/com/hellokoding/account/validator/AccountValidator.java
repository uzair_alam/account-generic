package com.hellokoding.account.validator;

import com.hellokoding.account.model.Account;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class AccountValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return Account.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Account account = (Account) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "NotEmpty");
        if (account.getTitle().length() < 0 || account.getTitle().length() > 250) {
            errors.rejectValue("title", "Size.accountForm.title");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "openingBalance", "NotEmpty");
        if (account.getOpeningBalance() < 0 ) {
            errors.rejectValue("openingBalance", "Size.accountForm.balance");
        }

    }
}
