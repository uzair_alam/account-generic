package com.hellokoding.account.validator;


import com.hellokoding.account.model.TaxDetail;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Calendar;

@Component
public class TaxDetailValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return TaxDetail.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        TaxDetail taxDetail = (TaxDetail) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "filerAmount", "NotEmpty");
        if (taxDetail.getFilerAmount() < 0 || taxDetail.getFilerAmount() > 100) {
            errors.rejectValue("filerAmount", "Size.taxForm.amount");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nonFilerAmount", "NotEmpty");
        if (taxDetail.getNonFilerAmount() < 0 || taxDetail.getNonFilerAmount() > 100) {
            errors.rejectValue("nonFilerAmount", "Size.taxForm.amount");
        }

        if (!(taxDetail.getEffectiveTill().compareTo(new java.sql.Date(Calendar.getInstance().getTime().getTime())) > 0) ) {
            errors.rejectValue("effectiveTill", "Size.taxForm.date");
        }


    }
}
