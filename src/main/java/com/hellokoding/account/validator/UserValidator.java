package com.hellokoding.account.validator;


import com.hellokoding.account.model.UserAuth;
import com.hellokoding.account.service.user.UserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
    @Autowired
    private UserAuthService userAuthService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public boolean supports(Class<?> aClass) {
        return UserAuth.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserAuth user = (UserAuth) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "NotEmpty");
        if (user.getUserName().length() < 6 || user.getUserName().length() > 32) {
            errors.rejectValue("userName", "Size.userForm.username");
        }
        if (userAuthService.findByUserName(user.getUserName()).getPassword().equals(bCryptPasswordEncoder.encode(user.getOldPassword()))) {
            errors.rejectValue("oldPassword", "UserForm.change.Password");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailId", "NotEmpty");
        if (user.getEmailId().length() < 8 || user.getEmailId().length() > 32) {
            errors.rejectValue("emailId", "Size.userForm.emailId");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password");
        }

        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
        }
    }
}
