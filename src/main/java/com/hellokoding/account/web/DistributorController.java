package com.hellokoding.account.web;


import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Distributor;
import com.hellokoding.account.model.StaticInfo;
import com.hellokoding.account.model.StockCustomer;
import com.hellokoding.account.service.distributor.DistributorService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.stock.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;

@Controller
public class DistributorController {

    @Autowired
    private DistributorService distributorService;


    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private CustomerService customerService;


    @RequestMapping(value = "/Distributor/addDistributor", method = RequestMethod.GET)
    public String addDistributor(Model model, RedirectAttributes attributes) {

        model.addAttribute("distributors" , distributorService.findAll());

        Account account = new Account();
        account.setDistributor(new Distributor());
        model.addAttribute("accountForm",account);
        model.addAttribute("currentDate",new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if(staticInfo.getDistributorHead() > 0){
            model.addAttribute("distributorHead",staticInfo.getDistributorHead());
            return "distributor/addDistributor";
        }
        attributes.addFlashAttribute("emptyCaseMessage", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Please enter a valid Distributor Head Account before entering a Distributor.</div>");
        return "redirect:/staticinfo";
    }

    /**
     * Distributor Remove
     * @param attributes Redirect Messages
     * @param distributorId To fetch Distributor
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/removeDistributor", method = RequestMethod.POST)
    public String removeDistributor( RedirectAttributes attributes, @RequestParam(name = "distributorId", defaultValue = "0") Integer distributorId) {

        distributorService.remove(distributorId);
        attributes.addFlashAttribute("distributorRemoveSuccess", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Distributor Removed Successfully.</div>");
        return "redirect:/Distributor/addDistributor";

    }

    /**
     * Listing Of Distributor's Registered Customers
     * @param model
     * @param distributorId Get request Id
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/addCustomer", method = RequestMethod.GET)
    public String addDistributorCustomer(Model model, @RequestParam(name = "distributorId", defaultValue = "0") Integer distributorId) {

        Distributor distributor = distributorService.findByDistributorId(distributorId);
        if(distributor == null ){
            return "error";
        }
        model.addAttribute("distributorCustomers",customerService.findCurrentCustomers(null,distributor,false) );
        model.addAttribute("availableCustomers", customerService.findAvailableCustomers(false));
        model.addAttribute("distributor", distributor);
        return "distributor/distributorCustomers";

    }

    /**
     * Distributor Customer Add POST
     * @param attributes For Success Message
     * @param distributorId Distributor Identification
     * @param customerId Customer Identification
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/addCustomer", method = RequestMethod.POST)
    public String addDistributorCustomer_post(RedirectAttributes attributes, @RequestParam(name = "distributorId", defaultValue = "0") Integer distributorId
            ,@RequestParam(name = "customerId", defaultValue = "0") Integer customerId) {

        Distributor distributor =  distributorService.findByDistributorId(distributorId);
        StockCustomer stockCustomer =  customerService.findByCustomerId(customerId);
        if(distributor == null || stockCustomer == null){
            return "error";
        }
        stockCustomer.setDistributor(distributor);
        customerService.save(stockCustomer);
        attributes.addFlashAttribute("distributorAddSuccess", "<div style=\"background-color: #d6ffbc;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #bbb5b5;border-radius: 5px;font-size: initial;\">Customer For Distributor "+ distributor.getName() +" added Successfully.</div>");
        return "redirect:/Distributor/addCustomer?distributorId=" + distributorId;

    }


    /**
     * Remove Customer from Distributor List
     * @param attributes
     * @param distributorId Distributor ID For Redirection
     * @param customerId CustomerID for Removal
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/removeCustomer", method = RequestMethod.POST)
    public String removeCustomer_post(RedirectAttributes attributes, @RequestParam(name = "distributorId", defaultValue = "0") Integer distributorId
            ,@RequestParam(name = "customerId", defaultValue = "0") Integer customerId) {

        StockCustomer stockCustomer =  customerService.findByCustomerId(customerId);
        if(stockCustomer == null){
            return "error";
        }
        stockCustomer.setDistributor(null);
        customerService.save(stockCustomer);
        attributes.addFlashAttribute("customerRemoveSuccess", "<div style=\"background-color: #d6ffbc;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #bbb5b5;border-radius: 5px;font-size: initial;\">Customer Removed Successfully.</div>");
        return "redirect:/Distributor/addCustomer?distributorId=" + distributorId;

    }
}
