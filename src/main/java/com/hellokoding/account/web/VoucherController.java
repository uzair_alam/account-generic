package com.hellokoding.account.web;

import com.hellokoding.account.model.*;

import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.account.BankDetailService;
import com.hellokoding.account.service.account.CostCentreService;
import com.hellokoding.account.service.automation.UpdateBalanceService;
import com.hellokoding.account.service.datalisting.JSONPopulateService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.taxation.TaxDetailService;
import com.hellokoding.account.validator.VoucherValidator;
import org.bouncycastle.voms.VOMSAttribute;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.log4j.Logger;

@Controller
public class VoucherController {


    @Autowired
    private VoucherService voucherService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CostCentreService costCentreService;

    @Autowired
    private TaxDetailService taxDetailService;

    @Autowired
    private VoucherValidator voucherValidator;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private UpdateBalanceService updateBalanceService;
    @Autowired
    private JSONPopulateService jsonPopulateService;
    @Autowired
    private BankDetailService bankDetailService;


    /////////////////////////////////////// Payment Voucher ///////////////////////////////////
    static Logger logger = Logger.getLogger(VoucherController.class.getName());

    @RequestMapping(value = "/Account/createPaymentVoucher", method = RequestMethod.GET)
    public String addPaymentVoucher(Model model) {
        logger.info("Creating Vouchers");
        model.addAttribute("voucherForm", new Voucher());
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        model.addAttribute("bankDetailAccounts", accountService.getDetailAccounts());
        List<Account> taxationAccounts = accountService.getTaxAccounts();
        List<TaxDetail> taxDetailList = new ArrayList<>();
        for (Account account : taxationAccounts) {
            taxDetailList.add(taxDetailService.findDetail(account));
        }
        model.addAttribute("taxDetail", taxDetailList);
        model.addAttribute("taxationAccounts", taxationAccounts);
        model.addAttribute("costCentres", costCentreService.findAll());
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("PV"));

        model.addAttribute("taxDetail", taxDetailService.findAll());
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAccountsByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("currentDate", formatter.format(date));

        return "transaction/addPaymentVoucher";
    }

    @RequestMapping(value = "/Account/createPaymentVoucher", method = RequestMethod.POST)
    public String addPaymentVoucher_post(@ModelAttribute("voucherForm") Voucher voucher, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        voucherValidator.validate(voucher, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAccountsByLevel(4));
            model.addAttribute("bankDetailAccounts", accountService.getDetailAccounts());
            model.addAttribute("taxationAccounts", accountService.getTaxAccounts());
            model.addAttribute("costCentres", costCentreService.findAll());
            model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("PV"));
            model.addAttribute("paymentVouchers", voucherService.findByVoucherType("PV%"));
            Map accountMap = new HashMap();
            Map costCentreMap = new HashMap();
            for (Account account : accountService.findAccountsByLevel(4)) {
                accountMap.put(account.getAccountCode(), account.getTitle());
            }
            for (CostCentre costCentre : costCentreService.findAll()) {
                costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
            }
            model.addAttribute("accountMap", accountMap);
            model.addAttribute("costCentreMap", costCentreMap);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            model.addAttribute("currentDate", formatter.format(date));

            return "transaction/addPaymentVoucher";
        }
        String voucherNumber = voucherService.savePayment(voucher).getVoucherNumber();
        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Voucher <strong>" + voucherNumber + " </strong>for Account with code: <strong>" + voucher.getAccountCode() + " </strong>added successfully.</div>");
        return "redirect:/Account/createPaymentVoucher";
    }


    /////////////////////////////////////// Receipt Voucher ///////////////////////////////////

    @RequestMapping(value = "/Account/createReceiptVoucher", method = RequestMethod.GET)
    public String addReceiptVoucher(Model model) {
        model.addAttribute("voucherForm", new Voucher());
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        model.addAttribute("bankDetailAccounts", accountService.getDetailAccounts());
        model.addAttribute("taxationAccounts", accountService.findAll());
        List<Account> taxationAccounts = accountService.getTaxAccounts();
        List<TaxDetail> taxDetailList = new ArrayList<>();
        for (Account account : taxationAccounts) {
            taxDetailList.add(taxDetailService.findDetail(account));
        }
        model.addAttribute("staticInfo", staticInfoService.findStaticInfo());
        model.addAttribute("taxDetail", taxDetailList);
        model.addAttribute("taxationAccounts", taxationAccounts);
        model.addAttribute("costCentres", costCentreService.findAll());
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("RV"));

        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAccountsByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("currentDate", formatter.format(date));

        return "transaction/addReceiptVoucher";
    }

    @RequestMapping(value = "/Account/createReceiptVoucher", method = RequestMethod.POST)
    public String addReceiptVoucher_post(@ModelAttribute("voucherForm") Voucher voucher, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        voucherValidator.validate(voucher, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAccountsByLevel(4));
            model.addAttribute("bankDetailAccounts", accountService.getDetailAccounts());
            model.addAttribute("taxationAccounts", accountService.getTaxAccounts());
            model.addAttribute("costCentres", costCentreService.findAll());
            model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("RV"));
            model.addAttribute("receiptVouchers", voucherService.findByVoucherType("RV%"));
            Map accountMap = new HashMap();
            Map costCentreMap = new HashMap();
            for (Account account : accountService.findAccountsByLevel(4)) {
                accountMap.put(account.getAccountCode(), account.getTitle());
            }
            for (CostCentre costCentre : costCentreService.findAll()) {
                costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
            }
            model.addAttribute("accountMap", accountMap);
            model.addAttribute("costCentreMap", costCentreMap);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            model.addAttribute("currentDate", formatter.format(date));

            return "transaction/addReceiptVoucher";
        }
        String voucherNumber = voucherService.saveReceipt(voucher).getVoucherNumber();
        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Voucher <strong>" + voucherNumber + " </strong>for Account with code: <strong>" + voucher.getAccountCode() + " </strong>added successfully.</div>");
        return "redirect:/Account/createReceiptVoucher";
    }


/////////////////////////////////////// Tax Values ///////////////////////////////////

    @RequestMapping(value = "/Account/ajax", method = RequestMethod.GET)

    public @ResponseBody
    List<Double> ajaxAccount(@RequestParam(value = "taxAccountId", defaultValue = "0") int taxAccountId) {

        if (taxAccountId > 0) {

            List<Double> taxResponseList = new ArrayList<>();
            TaxDetail taxDetail = taxDetailService.findDetail(accountService.findAccount(taxAccountId));
            taxResponseList.add(taxDetail.getFilerAmount());
            taxResponseList.add(taxDetail.getNonFilerAmount());
            return taxResponseList;
        } else {
            List<Double> taxResponseList = new ArrayList<>();
            return taxResponseList;
        }
    }


    /////////////////////////////////////// Journal Voucher ///////////////////////////////////

    @RequestMapping(value = "/Account/createJournalVoucher", method = RequestMethod.GET)
    public String addJournalVoucher(Model model) {
        VoucherList voucherList = new VoucherList();
        for (int i = 0; i < 20; i++) {
            voucherList.add(new Voucher());
        }

        model.addAttribute("voucherList", voucherList);
        model.addAttribute("accounts", accountService.getAccounts());
        model.addAttribute("costcentres", costCentreService.findAll());
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("JV"));
        model.addAttribute("journalVouchers", voucherService.findByVoucherType("JV%"));
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        model.addAttribute("accountMap", accountMap);
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("currentDate", formatter.format(date));

        return "transaction/addJournalVoucher";
    }

    @RequestMapping(value = "/Account/createJournalVoucher", method = RequestMethod.POST)
    public String addJournalVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        String voucherNumber = "";
        for (Voucher voucher : voucherList.getVoucherList()) {
            if (voucher.getAccountCode() > 0) {
                voucherService.saveJournal(voucher);
                voucherNumber = voucher.getVoucherNumber();
            }

        }

        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Journal Vouchers with number <strong>" + voucherNumber + " </strong>added successfully.</div>");
        return "redirect:/Account/createJournalVoucher";
    }


    /////////////////////////////////////// Bank Reconciliation ///////////////////////////////////

    @RequestMapping(value = "/Account/bankReconciliation", method = RequestMethod.GET)
    public String bankReconciliation(Model model) {

        model.addAttribute("accounts", accountService.getDetailAccounts());

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("currentDate", formatter.format(date));

        return "transaction/bankReconciliation";
    }

    @RequestMapping(value = "/Account/reconciliation_post", method = RequestMethod.POST)
    public String bankReconciliation_post(Model model, @RequestParam("account") int accountCode
            , @RequestParam("voucherStatus") String status
            , @RequestParam("dateTill") java.sql.Date date) {

        Map oMap = new HashMap();

        if (status.equals("Pending")) {
            List<Voucher> voucherList = voucherService.getVouchersByBankAccount(accountCode, date, true);
            model.addAttribute("vouchers", voucherList);
            List<Integer> accountCodes = new ArrayList<>();
            for (Voucher voucher : voucherList) {
                accountCodes.add(voucher.getAccountCode());
            }
            if (accountCodes.size() > 0) {
                for (Account account : accountService.getAccountNames(accountCodes)) {
                    oMap.put(account.getAccountCode(), account.getTitle());
                }
            }

        } else {
            List<Voucher> voucherList = voucherService.getVouchersByBankAccount(accountCode, date, false);
            model.addAttribute("vouchers", voucherList);
            List<Integer> accountCodes = new ArrayList<>();
            for (Voucher voucher : voucherList) {
                accountCodes.add(voucher.getAccountCode());
            }
            if (accountCodes.size() > 0) {
                for (Account account : accountService.getAccountNames(accountCodes)) {
                    oMap.put(account.getAccountCode(), account.getTitle());
                }
            }
        }
        model.addAttribute("accounts", accountService.getDetailAccounts());
        model.addAttribute("currentDate", date);
        model.addAttribute("postAccount", accountCode);
        model.addAttribute("status", status);
        model.addAttribute("oMap", oMap);

        return "transaction/bankReconciliationView";
    }

    @RequestMapping(value = "/Account/voucherAction", method = RequestMethod.POST)
    public String voucherAction(Model model, @RequestParam("account") int accountCode
            , @RequestParam("voucherStatus") String status
            , @RequestParam("voucherId") int voucherId
            , @RequestParam("clearDate") java.sql.Date date) {

        Map oMap = new HashMap();
        if (status.equals("Pending")) {
            Voucher voucher = voucherService.getVoucher(voucherId);
            voucher.setClearingDate(date);
            voucherService.save(voucher);
            List<Voucher> voucherList = voucherService.getVouchersByBankAccount(accountCode, date, true);
            model.addAttribute("vouchers", voucherList);
            List<Integer> accountCodes = new ArrayList<>();
            for (Voucher voucher2 : voucherList) {
                accountCodes.add(voucher2.getAccountCode());
            }
            if (accountCodes.size() > 0) {
                for (Account account : accountService.getAccountNames(accountCodes)) {
                    oMap.put(account.getAccountCode(), account.getTitle());
                }
            }
        } else {
            Voucher voucher = voucherService.getVoucher(voucherId);
            voucher.setClearingDate(null);
            voucherService.save(voucher);
            List<Voucher> voucherList = voucherService.getVouchersByBankAccount(accountCode, date, false);
            model.addAttribute("vouchers", voucherList);
            List<Integer> accountCodes = new ArrayList<>();
            for (Voucher voucher2 : voucherList) {
                accountCodes.add(voucher2.getAccountCode());
            }
            if (accountCodes.size() > 0) {
                for (Account account : accountService.getAccountNames(accountCodes)) {
                    oMap.put(account.getAccountCode(), account.getTitle());
                }
            }
        }
        model.addAttribute("accounts", accountService.getDetailAccounts());
        model.addAttribute("currentDate", date);
        model.addAttribute("postAccount", accountCode);
        model.addAttribute("status", status);
        model.addAttribute("oMap", oMap);

        return "transaction/bankReconciliationView";
    }


    /////////////////////////////////////////// Voucher Removal ///////////////////

    @RequestMapping(value = "/Account/deleteVoucher", method = RequestMethod.POST)
    public String removeVoucher(@RequestParam("voucherNumber") String voucherNumber, @RequestParam("redirectUrl") String redirectUrl) {


        voucherService.deleteVouchers(voucherNumber);
        return "redirect:/" + redirectUrl;
    }

/////////////////////////////////////////// Get customer info /////////////////////////////////

    @RequestMapping(value = "Account/ajax/getCustomerInfo", method = RequestMethod.GET)

    public @ResponseBody
    List<String> getCustomerInfo(@RequestParam(value = "accountCode", defaultValue = "0") int accountCode, @RequestParam("isPurchase") boolean check) {

        Account account = updateBalanceService.calculateLedgerBalance(accountService.findAccount(accountCode));
        return accountService.getInfo(account, check);
    }

    //////////////////////////////// Add Voucher ///////////////////////////////////////////////

    @RequestMapping(value = "Account/ajax/addVoucher", method = RequestMethod.POST)
    public ResponseEntity<?> addVoucher(@ModelAttribute("voucherData") Voucher voucher, @RequestParam("prefix") String prefix) {

        if (prefix.equals("RV")) {
            if(voucher.getBankAccount()==staticInfoService.findStaticInfo().getPostDateCheckAccount()){
                voucher.setStatus("PDC-PENDING");
            }
            voucherService.saveReceipt(voucher);
        } else if (prefix.equals("PV")) {
            voucherService.savePayment(voucher);
        }
        return ResponseEntity.ok(voucherService.generateVoucherNumber(prefix));
    }


    ///////////////////////////// Edit Voucher /////////////////////////////////////////

    @RequestMapping(value = "Account/ajax/editVoucher", method = RequestMethod.GET)
    public @ResponseBody
    Voucher editVoucher(@ModelAttribute("voucherId") int voucherId) {
        Voucher voucherDetail = voucherService.getVoucher(voucherId);
        return voucherDetail;
    }


    ///////////////////////// View Journal Voucher ////////////////////////////////////////

    @RequestMapping(value = "/Account/viewJournalVoucher", method = RequestMethod.GET)
    public String viewJournalVoucher(Model model) {
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        List<Voucher> voucherList = voucherService.findByVoucherType("JV%");
        int creditAccount = 0;
        for (Voucher voucher : voucherList) {

            if (voucher.getDebit() != 0) {
                creditAccount = voucher.getAccountCode();
            } else {
                voucher.setCreditAccount(creditAccount);
            }
        }

        model.addAttribute("journalVouchers", voucherList);
        Map accountMap = new HashMap();
        for (Account account : accountService.findAccountsByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        return "transaction/viewJournalVoucher";
    }

    ///////////////////////////////// Edit Journal Voucher //////////////////////////////////

    @RequestMapping(value = "/Account/editJournalVoucher", method = RequestMethod.GET)
    public String updateJournalVoucher(Model model, @RequestParam(value = "voucherNumber") String voucherNumber) {
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        List<Voucher> voucherItemList = voucherService.findByVoucherNumber(voucherNumber);
        VoucherList voucherList = new VoucherList();
        for (Voucher voucher : voucherItemList) {
            voucherList.add(voucher);
        }
        int size = voucherItemList.size();
        for (int i = 0; i < staticInfo.getMaxFormRows() - size; i++) {
            voucherList.add(new Voucher());
        }

        model.addAttribute("voucherList", voucherList);
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        model.addAttribute("costcentres", costCentreService.findAll());
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("journalVouchers", voucherService.findByVoucherType("JV%"));
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        model.addAttribute("accountMap", accountMap);
        if (voucherItemList.size() > 1) {
            model.addAttribute("currentDate", voucherItemList.get(1).getVoucherDate());
        }

        return "transaction/updateJournalVoucher";
    }

    @RequestMapping(value = "/Account/editJournalVoucher", method = RequestMethod.POST)
    public String updateJournalVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList,
                                            @RequestParam("voucherNumber") String voucherNumber,
                                            RedirectAttributes attributes) {
        List<Voucher> voucherNumberList = voucherService.findByVoucherNumber(voucherNumber);
        if (voucherNumberList.size() > 0) {
            voucherService.remove(voucherNumberList);
        }
        for (Voucher voucher : voucherList.getVoucherList()) {
            if (voucher.getAccountCode() > 0) {
                voucherService.saveJournal(voucher);
                voucherNumber = voucher.getVoucherNumber();
            }

        }

        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Journal Vouchers with number <strong>" + voucherNumber + " </strong>updated successfully.</div>");
        return "redirect:/Account/viewJournalVoucher";
    }
    /////////////////////////////Get ledger balance for customer and venders ////////////////////////////

    @RequestMapping(value = "Account/ajax/getAmountBalance", method = RequestMethod.GET)
    public @ResponseBody
    List<Double> getAmountBalance(@ModelAttribute("accountCode") int accountCode, @RequestParam("prefix") String prefix) {
        List<Double> amountInfoList = new ArrayList<>();
        Account account = accountService.findAccount(accountCode);
        if (prefix.equals("RV")) {
            if (account.getStockCustomer() != null) {
                account = updateBalanceService.calculateLedgerBalance(accountService.findAccount(accountCode));
                amountInfoList.add(account.getTotalBalanceAmount());
                amountInfoList.add(account.getAmountLimit());

            }
        } else if (prefix.equals("PV")) {
            if (account.getStockVendor() != null) {
                account = updateBalanceService.calculateLedgerBalance(accountService.findAccount(accountCode));
                amountInfoList.add(account.getTotalBalanceAmount());
                amountInfoList.add(account.getAmountLimit());

            }
        }

        return amountInfoList;
    }


    /////////////////////////// JSON Generator On DataTable Ajax Call For Payment Vouchers//////////////////

    @RequestMapping(value = "/Payments/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Voucher>> paymentVoucherListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populatePaymentVoucherList(voucherService.findAllPaged(pageIndex, length, "voucherDate", false, searchString, "PV"), draw), HttpStatus.OK);


    }

    /////////////////////////// JSON Generator On DataTable Ajax Call For Receipt Vouchers//////////////////

    @RequestMapping(value = "/Receipt/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Voucher>> receiptVoucherListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populateReceiptVoucherList(voucherService.findAllPaged(pageIndex, length, "voucherDate", false, searchString, "RV"), draw), HttpStatus.OK);


    }

    /////////////////////////////////////////////// pdc journal vouchers ////////////////////////////////////////
    @RequestMapping(value = "/Account/createPdcJournalVoucher", method = RequestMethod.GET)
    public String addPdcJournalVoucher(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("dateFrom", date);
        model.addAttribute("dateTo", date);
        model.addAttribute("bankAccounts", bankDetailService.findAll());
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("PD%"));
        VoucherList voucherList = new VoucherList();
        for (Voucher voucher : voucherService.findJournalVouchersByPdcBankAccount(date, date, staticInfoService.findStaticInfo().getPostDateCheckAccount())) {
            if (voucher.getStatus().equals("PDC-PENDING")) {
                voucherList.add(voucher);
            }
        }
        model.addAttribute("accounts", accountService.getAccounts());
        model.addAttribute("costcentres", costCentreService.findAll());
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("PD"));
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        model.addAttribute("accountMap", accountMap);
        model.addAttribute("currentDate", date);
        return "transaction/pdcJournalVoucher";
    }

    @RequestMapping(value = "/Account/createPdcJournalVoucher", method = RequestMethod.POST)
    public String addPdcJournalVoucher_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("dateFrom", fromDate);
        model.addAttribute("dateTo", toDate);
        model.addAttribute("bankAccounts", bankDetailService.findAll());
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("PD%"));
        VoucherList voucherList = new VoucherList();
        for (Voucher voucher : voucherService.findJournalVouchersByPdcBankAccount(fromDate, toDate, staticInfoService.findStaticInfo().getPostDateCheckAccount())) {
            if  (voucher.getStatus().equals("PDC-PENDING")) {
                voucherList.add(voucher);
            }
        }
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("accounts", accountService.getAccounts());
        model.addAttribute("costcentres", costCentreService.findAll());
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("PD"));

        return "transaction/pdcJournalVoucher";
    }

    @RequestMapping(value = "/Account/savePdcJournalVoucher", method = RequestMethod.POST)
    public String savePdcJournalVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        String voucherNumber = "";
        for (Voucher voucher : voucherList.getVoucherList()) {

            if (voucher.getAccountCode() > 0 && voucher.isCheck()) {
                for(Voucher receiptVoucher:   voucherService.findByVoucherNumber(voucher.getVoucherNumberForPdc())){
                    receiptVoucher.setStatus("PDC-CREATED");
                    voucherService.save(receiptVoucher);
                }
                voucher.setDebit(voucher.getCredit());
                voucher.setAccountCode(staticInfoService.findStaticInfo().getPostDateCheckAccount());
                voucher.setStatus("PARTIAL");
                voucher.setCredit(null);
                voucherService.saveJournal(voucher);
                Voucher bankVoucher = new Voucher();
                bankVoucher.setVoucherNumber(voucher.getVoucherNumber());
                bankVoucher.setAccountCode(voucher.getBankAccount());
                bankVoucher.setBankAccount(0);
                bankVoucher.setVoucherDate(voucher.getVoucherDate());
                bankVoucher.setStatus("PARTIAL");
                bankVoucher.setCredit(voucher.getDebit());
                bankVoucher.setDebit(null);
                voucherService.saveJournal(bankVoucher);
                voucherNumber = bankVoucher.getVoucherNumber();
            }

        }

        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Post Dated Vouchers added successfully.</div>");
        return "redirect:/Account/createPdcJournalVoucher";
    }
    /////////////////////////////////////////////////// Bank Check Return Voucher //////////////////////////////////////////////////////////
    @RequestMapping(value = "/Account/createBankChequeReturn", method = RequestMethod.GET)
    public String addBankCheckReturn(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("dateFrom", date);
        model.addAttribute("dateTo", date);
        model.addAttribute("bankAccounts", bankDetailService.findAll());
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("CR%"));
        VoucherList voucherList = new VoucherList();
        for (Voucher voucher : voucherService.findBankAccountByDate(date, date)) {
            voucherList.add(voucher);
        }
        model.addAttribute("pdcAccount", staticInfoService.findStaticInfo().getPostDateCheckAccount());
        model.addAttribute("accounts", accountService.getAccounts());
        model.addAttribute("costcentres", costCentreService.findAll());
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("CR"));
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        model.addAttribute("accountMap", accountMap);
        model.addAttribute("currentDate", date);
        return "transaction/chequeReturnVoucher";
    }

    @RequestMapping(value = "/Account/createBankChequeReturn", method = RequestMethod.POST)
    public String addBankCheckReturn_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("dateFrom", fromDate);
        model.addAttribute("dateTo", toDate);
        model.addAttribute("bankAccounts", bankDetailService.findAll());
        model.addAttribute("pdcAccount", staticInfoService.findStaticInfo().getPostDateCheckAccount());
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("CR%"));
        VoucherList voucherList = new VoucherList();
        for (Voucher voucher : voucherService.findBankAccountByDate(fromDate, toDate)) {
            voucherList.add(voucher);
        }
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("accounts", accountService.getAccounts());
        model.addAttribute("costcentres", costCentreService.findAll());
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("CR"));
        return "transaction/chequeReturnVoucher";
    }
    @RequestMapping(value = "/Account/saveBankChequeReturn", method = RequestMethod.POST)
    public String saveBankChequeReturn_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        String voucherNumber = "";
        for (Voucher voucher : voucherList.getVoucherList()) {

            if (voucher.getAccountCode() > 0 && voucher.isCheck()) {
                for(Voucher receiptVoucher:   voucherService.findByVoucherNumber(voucher.getVoucherNumberForPdc())){
                    receiptVoucher.setStatus("RETURN");
                    voucherService.save(receiptVoucher);
                }
                voucher.setAccountCode(staticInfoService.findStaticInfo().getPostDateCheckAccount());
                voucher.setStatus("RETURN");
                voucherService.saveJournal(voucher);
                Voucher bankVoucher = new Voucher();
                bankVoucher.setVoucherNumber(voucher.getVoucherNumber());
                bankVoucher.setAccountCode(voucher.getBankAccount());
                bankVoucher.setBankAccount(0);
                bankVoucher.setVoucherDate(voucher.getVoucherDate());
                bankVoucher.setStatus("RETURN");
                bankVoucher.setDebit(voucher.getCredit());
                bankVoucher.setCredit(null);
                voucherService.saveJournal(bankVoucher);
                voucherNumber = bankVoucher.getVoucherNumber();
            }

        }

        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Bank Check Return Vouchers added successfully.</div>");
        return "redirect:/Account/createBankChequeReturn";
    }
}
