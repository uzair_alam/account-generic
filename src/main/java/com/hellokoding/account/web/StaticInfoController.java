package com.hellokoding.account.web;

import com.hellokoding.account.model.StaticInfo;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class StaticInfoController {


    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/staticinfo", method = RequestMethod.GET)
    public String addStaticInfo(Model model) {

        if (staticInfoService.findStaticInfo() != null) {
            StaticInfo staticInfo = staticInfoService.findByInfoId(1);
            model.addAttribute("staticForm", staticInfo);
        } else {
            model.addAttribute("staticForm", new StaticInfo());
        }

        model.addAttribute("accounts", accountService.findAll());
        return "staticinfo";
    }

    @RequestMapping(value = "staticinfo", method = RequestMethod.POST)
    public String addStaticInfo_post(@ModelAttribute(value = "staticForm") StaticInfo staticInfo, RedirectAttributes attributes) throws IOException {

        String imageUrl = staticInfo.getImagePath();
        MultipartFile logoFile = staticInfo.getLogoFile();
        MultipartFile bottomImageFile = staticInfo.getFooterFile();
        staticInfoService.uploadImage(logoFile);
        staticInfoService.uploadImage(bottomImageFile);
        if (staticInfoService.findStaticInfo() != null) {
            staticInfo.setInfoId(1);
            StaticInfo staticInfoImage = staticInfoService.findByInfoId(1);
            if (staticInfo.getFooterFile().getSize() == 0) {

                staticInfo.setFooterImageUrl(staticInfoImage.getFooterImageUrl());
            } else {

                staticInfo.setFooterImageUrl(imageUrl + "/" + bottomImageFile.getOriginalFilename());
            }
            if (staticInfo.getLogoFile().getSize() == 0) {
                staticInfo.setImageUrl(staticInfoImage.getImageUrl());
            } else {
                staticInfo.setImageUrl(imageUrl + "/" + logoFile.getOriginalFilename());
            }
        }

        staticInfoService.save(staticInfo);

        attributes.addFlashAttribute("CompanyInfoAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\"><strong></strong>Company information added successfully</div>");
        return "redirect:/staticinfo";
    }


}
