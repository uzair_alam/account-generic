package com.hellokoding.account.web;

import com.hellokoding.account.model.*;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.account.BankDetailService;
import com.hellokoding.account.service.account.ExpenseDetailService;
import com.hellokoding.account.service.distributor.DistributorService;
import com.hellokoding.account.service.rider.RiderService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.stock.StockService;
import com.hellokoding.account.validator.AccountValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;

@Controller
public class AccountSupportController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountValidator accountValidator;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private StockService stockService;

    @Autowired
    private ExpenseDetailService expenseDetailService;

    @Autowired
    BankDetailService bankDetailService;

    @Autowired
    private RiderService riderService;

    @Autowired
    private DistributorService distributorService;

    /// Customer Scenario
    @RequestMapping(value = "/AccountSupport/addCustomerAccount", method = RequestMethod.GET)
    public String addCustomerAccount(Model model) {
        Account account = new Account();
        account.setStockCustomer(new StockCustomer());
        model.addAttribute("accountForm",account);
        model.addAttribute("accounts",accountService.findAllFiltered());
        model.addAttribute("currentDate",new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if(staticInfo.getCustomerHead() > 0){
            model.addAttribute("customerHead",staticInfo.getCustomerHead());
            return "stock/addCustomerAccount";
        }
        return "redirect:/staticinfo";
    }

    @RequestMapping(value = "/AccountSupport/addCustomerAccount", method = RequestMethod.POST)
    public String addCustomerAccount_post(@ModelAttribute("accountForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts",accountService.findAllFiltered());
            return "stock/addCustomer";
        }

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if(staticInfo.getCustomerHead() > 0){

            StockCustomer stockCustomer = accountForm.getStockCustomer();
            accountForm.setStockCustomer(null);
            Account account = accountService.save(accountForm);
            stockCustomer.setCustomerAccount(account);
            stockService.saveCustomer(stockCustomer);

            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Customer Account with Code: <strong>"+account.getAccountCode()+" </strong>added successfully.</div>");
        }else{
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Customer Account.</div>");
        }

        return "redirect:/Stock/addCustomer";
    }

    /// Vendor Scenario
    @RequestMapping(value = "/AccountSupport/addVendorAccount", method = RequestMethod.GET)
    public String addvendorAccount(Model model) {
        Account account = new Account();
        account.setStockVendor(new StockVendor());
        model.addAttribute("accountForm",account);
        model.addAttribute("accounts",accountService.findAllFiltered());
        model.addAttribute("currentDate",new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if(staticInfo.getVendorHead() > 0){
            model.addAttribute("vendorHead",staticInfo.getVendorHead());
            return "stock/addVendorAccount";
        }
        return "redirect:/staticinfo";
    }

    @RequestMapping(value = "/AccountSupport/addVendorAccount", method = RequestMethod.POST)
    public String addVendorAccount_post(@ModelAttribute("accountForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts",accountService.findAllFiltered());
            return "stock/addVendor";
        }

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if(staticInfo.getVendorHead() > 0){

            StockVendor stockVendor = accountForm.getStockVendor();
            accountForm.setStockVendor(null);
            Account account = accountService.save(accountForm);
            stockVendor.setVendorAccount(account);
            stockService.saveVendor(stockVendor);

            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Vendor Account with Code: <strong>"+account.getAccountCode()+" </strong>added successfully.</div>");
        }else{
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Vendor Account.</div>");
        }

        return "redirect:/Stock/addVendor";
    }


    ///////// Stock Scenario

    @RequestMapping(value = "/AccountSupport/addStockAccount", method = RequestMethod.GET)
    public String addStockAccount(Model model) {
        Account account = new Account();
        account.setAccountStock(new Stock());
        model.addAttribute("accountForm",account);
        model.addAttribute("accounts",accountService.findAllFiltered());
        model.addAttribute("categories",stockService.getStockCategories());
        model.addAttribute("currentDate",new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if(staticInfo.getProductHead() > 0){
            model.addAttribute("stockHead",staticInfo.getProductHead());
            return "stock/addStockAccount";
        }
        return "redirect:/staticinfo";
    }

    @RequestMapping(value = "/AccountSupport/addStockAccount", method = RequestMethod.POST)
    public String addStockAccount_post(@ModelAttribute("accountForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts",accountService.findAllFiltered());
            return "stock/addStock";
        }

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if(staticInfo.getProductHead() > 0){

            Stock stock = accountForm.getAccountStock();
            accountForm.setAccountStock(null);
            Account account = accountService.save(accountForm);
            stock.setStockAccount(account);
            stock.setName(account.getTitle());
            stock.setStockCategory(stockService.findByCategoryId(stock.getCategoryId()));
            stockService.saveStock(stock);

            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Stock Account with Code: <strong>"+account.getAccountCode()+" </strong>added successfully.</div>");
        }else{
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Product Account.</div>");
        }

        return "redirect:/Stock/addStock";
    }
    ///////// Expense Scenario
    @RequestMapping(value = "/AccountSupport/addExpenseAccount", method = RequestMethod.POST)
    public String addExpenseAccount_post(@ModelAttribute("accountForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAllFiltered());
            return "master/addExpenseDetails";
        }

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getExpenseHead() > 0) {

            ExpenseDetail expenseDetail = accountForm.getExpenseDetail();
            accountForm.setExpenseDetail(null);
            Account account = accountService.save(accountForm);
            expenseDetail.setExpenseAccount(account);
            expenseDetailService.save(expenseDetail);

            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Expense Account with Code: <strong>" + account.getAccountCode() + " </strong>added successfully.</div>");
        } else {
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Expense Account.</div>");
        }

        return "redirect:/Account/addExpense";
    }


    // Bank Scenario

    @RequestMapping(value = "/AccountSupport/addBankAccount", method = RequestMethod.POST)
    public String addBankAccount_post(@ModelAttribute("accountForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAllFiltered());
            return "master/addBankDetails";
        }

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getBankHead() > 0) {

            BankDetails bankDetails = accountForm.getBankDetails();
            accountForm.setBankDetails(null);
            Account account = accountService.save(accountForm);
            bankDetails.setAccount(account);
            bankDetailService.save(bankDetails);

            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Bank Account with Code: <strong>" + account.getAccountCode() + " </strong>added successfully.</div>");
        } else {
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Bank Account.</div>");
        }

        return "redirect:/User/addBankDetails";
    }

    /**
     * Rider Account Direct Add Support. Get Request From RiderController
     * @param accountForm
     * @param bindingResult
     * @param attributes
     * @return Response and Rider Listing Page
     */
    @RequestMapping(value = "/AccountSupport/addRiderAccount", method = RequestMethod.POST)
    public String addRiderAccount_post(@ModelAttribute("accountForm") Account accountForm,  BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "redirect:/Rider/addRider";
        }

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getRiderHead() > 0) {

            Rider rider = accountForm.getRider();
            accountForm.setRider(null);
            Account account = accountService.save(accountForm);
            rider.setRiderAccount(account);
            rider.setActive(true);
            riderService.save(rider);

            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Rider Account with Code: <strong>" + account.getAccountCode() + " </strong>added successfully.</div>");
        } else {
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Rider Account.</div>");
        }

        return "redirect:/Rider/addRider";
    }


    /**
     * Distributor Account Direct Add Support. Get Request From DistributorController
     * @param accountForm
     * @param bindingResult
     * @param attributes
     * @return Response and Rider Listing Page
     */
    @RequestMapping(value = "/AccountSupport/addDistributorAccount", method = RequestMethod.POST)
    public String addDistributorAccount_post(@ModelAttribute("accountForm") Account accountForm,  BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "redirect:/Distributor/addDistributor";
        }

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getDistributorHead() > 0) {

            Distributor distributor = accountForm.getDistributor();
            accountForm.setDistributor(null);
            Account account = accountService.save(accountForm);
            distributor.setDistributorAccount(account);
            distributor.setActive(true);
            distributorService.save(distributor);

            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Distributor Account with Code: <strong>" + account.getAccountCode() + " </strong>added successfully.</div>");
        } else {
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Distributor Account.</div>");
        }

        return "redirect:/Distributor/addDistributor";
    }

}
