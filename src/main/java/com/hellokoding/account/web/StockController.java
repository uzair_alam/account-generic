package com.hellokoding.account.web;


import com.hellokoding.account.model.*;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.datalisting.JSONPopulateService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.stock.CustomerService;
import com.hellokoding.account.service.stock.StockService;
import com.hellokoding.account.service.stock.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;

@Controller
@SessionAttributes({"stockFormUpdate"})
public class StockController {

    @Autowired
    private StockService stockService;

    @Autowired
    private CustomerService customerService;
    @Autowired
    private VendorService vendorService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private JSONPopulateService jsonPopulateService;

    //////////////////////////////////Stock ///////////////////////////////////////
    @RequestMapping(value = "/Stock/addStock", method = RequestMethod.GET)
    public String addStock(Model model, RedirectAttributes attributes) {
        model.addAttribute("stockForm", new Stock());
        model.addAttribute("categories", stockService.getStockCategories());
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        model.addAttribute("stockItems", stockService.findAllStock());

        Account account = new Account();
        account.setAccountStock(new Stock());
        model.addAttribute("accountForm", account);
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getProductHead() > 0) {
            model.addAttribute("stockHead", staticInfo.getProductHead());
            return "stock/addStock";
        }
        attributes.addFlashAttribute("emptyCaseMessage", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Please enter a valid stock Head Account before entering a stock.</div>");
        return "redirect:/staticinfo";


    }

    @RequestMapping(value = "/Stock/addStock", method = RequestMethod.POST)
    public String addStock_post(@ModelAttribute("stockForm") Stock stock, RedirectAttributes attributes) {

        Account account = accountService.findAccount(stock.getAccountCode());
        stockService.removeStock(account);
        stock.setStockCategory(stockService.findByCategoryId(stock.getCategoryId()));
        stock.setStockAccount(account);
        stockService.saveStock(stock);
        attributes.addFlashAttribute("stockAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Stock  <strong>" + stock.getName() + " </strong>added successfully.</div>");
        return "redirect:/Stock/addStock";
    }

    @RequestMapping(value = "/Stock/removeStock", method = RequestMethod.POST)
    public String removeStock(RedirectAttributes attributes, @RequestParam("accountCode") Integer accountCode) {

        stockService.removeStock(accountService.findAccount(accountCode));

        attributes.addFlashAttribute("stockRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Stock removed successfully.</div>");
        return "redirect:/Stock/addStock";
    }

    @RequestMapping(value = "/Stock/editStock", method = RequestMethod.GET)
    public String editStock(@RequestParam("stockId") Integer stockId, Model model) {

        Stock stock = stockService.findByStockId(stockId);
        model.addAttribute("accountCode", stock.getStockAccount().getAccountCode());
        model.addAttribute("categoryId", stock.getStockCategory().getCategoryId());
        model.addAttribute("stockFormUpdate", stock);
        model.addAttribute("categories", stockService.getStockCategories());
        model.addAttribute("accounts", accountService.findAll());
        return "stock/editStock";
    }

    @RequestMapping(value = "/Stock/doEditStock", method = RequestMethod.POST)
    public String doEditStock_post(@ModelAttribute("stockFormUpdate") Stock stockUpdate, RedirectAttributes attributes) {

        //Account stockAccount =accountService.findAccount(stockUpdate.getAccountCode());
        //stockService.removeStock(stockAccount);
        //stockUpdate.setStockAccount(stockAccount);
        //stockUpdate.setStockCategory(stockService.findByCategoryId(stockUpdate.getCategoryId()));
        stockService.saveStock(stockUpdate);
        attributes.addFlashAttribute("stockEditSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Stock  <strong>" + stockUpdate.getName() + " </strong> edited successfully.</div>");
        return "redirect:/Stock/addStock";
    }

    //////////////////////////////////Vendor ///////////////////////////////////////
    @RequestMapping(value = "/Stock/addVendor", method = RequestMethod.GET)
    public String addVendor(Model model, RedirectAttributes attributes) {
        model.addAttribute("vendorForm", new StockVendor());
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        model.addAttribute("vendors", vendorService.findAll());

        Account account = new Account();
        account.setStockVendor(new StockVendor());
        model.addAttribute("accountForm", account);
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getVendorHead() > 0) {
            model.addAttribute("vendorHead", staticInfo.getVendorHead());
            return "stock/addVendor";
        }
        attributes.addFlashAttribute("emptyCaseMessage", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Please enter a valid Vendor Head Account before entering a Vendor.</div>");
        return "redirect:/staticinfo";
    }

    @RequestMapping(value = "/Stock/addVendor", method = RequestMethod.POST)
    public String addVendor_post(@ModelAttribute("vendorForm") StockVendor stockVendor, RedirectAttributes attributes) {

        Account account = accountService.findAccount(stockVendor.getAccountCode());
        vendorService.removeVendor(account);
        stockVendor.setVendorAccount(account);
        stockService.saveVendor(stockVendor);
        attributes.addFlashAttribute("stockVendorAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Stock Vendor <strong>" + stockVendor.getVendorAccount().getTitle() + " </strong>added successfully.</div>");
        return "redirect:/Stock/addVendor";
    }

    @RequestMapping(value = "/Stock/removeVendor", method = RequestMethod.POST)
    public String removeVendor(RedirectAttributes attributes, @RequestParam("accountCode") Integer accountCode) {

        stockService.removeVendor(accountService.findAccount(accountCode));

        attributes.addFlashAttribute("vendorRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Vendor removed successfully.</div>");
        return "redirect:/Stock/addVendor";
    }

    ////////////////////////////////// Customer ///////////////////////////////////////
    @RequestMapping(value = "/Stock/addCustomer", method = RequestMethod.GET)
    public String addCustomer(Model model, RedirectAttributes attributes) {
        model.addAttribute("customerForm", new StockCustomer());
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        model.addAttribute("customers", customerService.findAll());

        Account account = new Account();
        account.setStockCustomer(new StockCustomer());
        model.addAttribute("accountForm", account);
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getCustomerHead() > 0) {
            model.addAttribute("customerHead", staticInfo.getCustomerHead());
            return "stock/addCustomer";
        }
        attributes.addFlashAttribute("emptyCaseMessage", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Please enter a valid Customer Head Account before entering a Customer.</div>");
        return "redirect:/staticinfo";
    }

    @RequestMapping(value = "/Stock/addCustomer", method = RequestMethod.POST)
    public String addCustomer_post(@ModelAttribute("customerForm") StockCustomer stockCustomer, RedirectAttributes attributes) {

        Account account = accountService.findAccount(stockCustomer.getAccountCode());
        customerService.removeCustomer(account);
        stockCustomer.setCustomerAccount(account);
        stockService.saveCustomer(stockCustomer);
        attributes.addFlashAttribute("stockCustomerAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Customer <strong>" + stockCustomer.getCustomerAccount().getTitle() + " </strong>added successfully.</div>");
        return "redirect:/Stock/addCustomer";
    }

    @RequestMapping(value = "/Stock/removeCustomer", method = RequestMethod.POST)
    public String removeCustomer(RedirectAttributes attributes, @RequestParam("accountCode") Integer accountCode) {

        stockService.removeCustomer(accountService.findAccount(accountCode));

        attributes.addFlashAttribute("customerRemoveSuccess", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Customer removed successfully.</div>");
        return "redirect:/Stock/addCustomer";
    }

    ////////////////////////////////////////// Stock Category /////////////////////////////////////

    @RequestMapping(value = "/Stock/addStockCategory", method = RequestMethod.GET)
    public String addCategory(Model model) {
        model.addAttribute("categoryForm", new StockCategory());
        model.addAttribute("categories", stockService.getStockCategories());
        return "stock/addCategory";
    }

    @RequestMapping(value = "/Stock/addStockCategory", method = RequestMethod.POST)
    public String addCategory_post(@ModelAttribute("categoryForm") StockCategory stockCategory, RedirectAttributes attributes) {

        stockService.saveCategory(stockCategory);
        attributes.addFlashAttribute("stockCategoryAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Category <strong>" + stockCategory.getTitle() + " </strong>added successfully.</div>");
        return "redirect:/Stock/addStockCategory";
    }


    /////////////////////////// JSON Generator On DataTable Ajax Call For Stock Listing//////////////////

    @RequestMapping(value = "/Stock/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Stock>> stockListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populateStockList(stockService.findAllPaged(pageIndex, length, "", false, searchString), draw), HttpStatus.OK);
        //accountService.findAccount(1001000000);

    }


}
