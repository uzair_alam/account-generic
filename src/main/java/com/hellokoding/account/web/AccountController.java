package com.hellokoding.account.web;

import com.hellokoding.account.model.*;
import com.hellokoding.account.repository.AuthorizedPersonRepository;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.account.BankDetailService;
import com.hellokoding.account.service.account.CostCentreService;
import com.hellokoding.account.service.account.ExpenseDetailService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.datalisting.JSONPopulateService;
import com.hellokoding.account.validator.AccountValidator;
import com.hellokoding.account.validator.CostCentreValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
@SessionAttributes({"accountEditForm"})
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private CostCentreService costCentreService;

    @Autowired
    private BankDetailService bankDetailService;

    @Autowired
    private AccountValidator accountValidator;

    @Autowired
    private CostCentreValidator costCentreValidator;

    @Autowired
    private AuthorizedPersonRepository authorizedPersonRepository;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private ExpenseDetailService expenseDetailService;

   @Autowired
    private JSONPopulateService jsonPopulateService;

    ////////////////////////////Account ////////////////////////////////////////

    @RequestMapping(value = "/Account/addAccount", method = RequestMethod.GET)
    public String addAccount(Model model) {
        model.addAttribute("accountForm", new Account());
        model.addAttribute("accounts", accountService.findAllFiltered());
        model.addAttribute("baseAccounts", accountService.findAccountsByLevel(1));
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");
        model.addAttribute("currentDate", formatter.format(date));

        return "master/addaccount";
    }

    @RequestMapping(value = "/Account/addAccount", method = RequestMethod.POST)
    public String addAccount_post(@ModelAttribute("accountForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAllFiltered());
            return "master/addaccount";
        }

        Account account = accountService.save(accountForm);

        attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Account with Code: <strong>" + account.getAccountCode() + " </strong>added successfully.</div>");
        return "redirect:/Account/addAccount";
    }

    @RequestMapping(value = "/Account/AccountListing", method = RequestMethod.GET)
    public String AccountListing() {

        return "master/accountListing";
    }


    @RequestMapping(value = "/Account/editAccount", method = RequestMethod.POST)
    public String editAccount(Model model, @RequestParam("accountCode") Integer accountCode) {
        Account account = accountService.findAccount(accountCode);
        if (account.getParentAccount() != null) {
            account.setParentCode(account.getParentAccount().getAccountCode());
        }

        model.addAttribute("accountEditForm", account);
        model.addAttribute("accounts", accountService.findAllFiltered());
        model.addAttribute("baseAccounts", accountService.findAccountsByLevel(1));
        return "master/editAccount";
    }

    @RequestMapping(value = "/Account/doEditAccount", method = RequestMethod.POST)
    public String editAccount_post(@ModelAttribute("accountEditForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAllFiltered());
            return "master/addaccount";
        }

        accountService.update(accountForm);

        attributes.addFlashAttribute("AccountEditSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Account with Code: <strong>" + accountForm.getAccountCode() + " </strong>Edited successfully.</div>");
        return "redirect:/Account/AccountListing";
    }

    @RequestMapping(value = "/Account/removeAccount", method = RequestMethod.POST)
    public String removeAccount(RedirectAttributes attributes, @RequestParam("accountCode") Integer accountCode) {

        Account account = accountService.findAccount(accountCode);

        if (accountService.accountDeletable(account)) {
            accountService.delete(account.getAccountCode());
            attributes.addFlashAttribute("accountRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Account removed successfully.</div>");

        } else {
            attributes.addFlashAttribute("accountRemoveSuccess", "<div style=\"background-color: #f3bbec;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #0c480c;border-radius: 5px;font-size: initial;\">Account <strong>" + account.getTitle() + " </strong>cannot be removed. It may contain child data which has to be removed first.</div>");
        }
        return "redirect:/Account/AccountListing";
    }

    ///////////////////////////// Cost centre //////////////////////////////////
    @RequestMapping(value = "/Account/addCostCentre", method = RequestMethod.GET)
    public String addCostCentre(Model model) {
        model.addAttribute("costCentreForm", new CostCentre());
        model.addAttribute("accounts", accountService.findAll());
        model.addAttribute("baseAccounts", accountService.findAccountsByLevel(1));
        return "master/addcostcentre";
    }

    @RequestMapping(value = "/Account/addCostCentre", method = RequestMethod.POST)
    public String addCostCentre_post(@ModelAttribute("costCentreForm") CostCentre costCentre, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        costCentreValidator.validate(costCentre, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAll());
            return "master/addcostcentre";
        }
        CostCentre costCentre1 = costCentreService.save(costCentre);
        attributes.addFlashAttribute("CostCentreAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Cost Centre with Code: <strong>" + costCentre1.getAccountCode() + " </strong>added successfully.</div>");
        return "redirect:/Account/addCostCentre";
    }


    @RequestMapping(value = "/User/addBankDetails", method = RequestMethod.GET)
    public String addBankDetails(Model model, RedirectAttributes attributes) {
        model.addAttribute("bankDetailForm", new BankDetails());
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("bankDetailList", bankDetailService.findAll());
        Account account = new Account();
        account.setBankDetails(new BankDetails());
        model.addAttribute("accountForm", account);
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getBankHead() > 0) {
            model.addAttribute("bankHead", staticInfo.getBankHead());
            return "master/addBankDetails";
        }
        attributes.addFlashAttribute("emptyCaseMessage", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Please enter a valid Bank Head Account before entering a Bank Account.</div>");
        return "redirect:/staticinfo";

    }

    @RequestMapping(value = "/User/addBankDetails", method = RequestMethod.POST)
    public String addBankDetails_post(@ModelAttribute("bankDetailForm") BankDetails bankDetails, RedirectAttributes attributes) {

        bankDetailService.save(bankDetails);
        attributes.addFlashAttribute("accountDetailAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Details form account Code: <strong>" + bankDetails.getAccountCode() + " </strong>added successfully.</div>");
        return "redirect:/User/addBankDetails";
    }

    @RequestMapping(value = "/Account/removeBank", method = RequestMethod.POST)
    public String removeBank(RedirectAttributes attributes, @RequestParam("accountCode") Integer accountCode) {

        bankDetailService.delete(accountService.findAccount(accountCode));

        attributes.addFlashAttribute("bankRemoveSuccess", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Bank Account removed successfully.</div>");
        return "redirect:/User/addBankDetails";
    }


    @RequestMapping(value = "/User/addBankDetailsAjax", method = RequestMethod.GET)
    public @ResponseBody
    List<String> bankDetailAjax(@RequestParam(value = "accountId", defaultValue = "0") int accountId, Model model) {

        BankDetails bankDetail = bankDetailService.findBankDetail(accountService.findAccount(accountId));
        if (bankDetail != null) {
            String[] stringList = {bankDetail.getBankAccount(), bankDetail.getBankBranch(), bankDetail.getBankContactPerson(), bankDetail.getMobileNumber(),
                    bankDetail.getBankLandline(), bankDetail.getEmailAddress(), String.join(",", authorizedPersonRepository.getAuthPersons(bankDetail)), bankDetail.getScanForm(), bankDetail.getAccountOpeningDate().toString(), bankDetail.getBranchCode()
                    , bankDetail.getBankNtn(), bankDetail.getUrl()};
            return Arrays.asList(stringList);
        }
        String[] stringList = {"NO-DATA"};
        return Arrays.asList(stringList);
    }

    ////////////////////////////////// ExpenseDetail ///////////////////////////////////////
    @RequestMapping(value = "/Account/addExpense", method = RequestMethod.GET)
    public String addExpense(Model model, RedirectAttributes attributes) {
        model.addAttribute("expenseForm", new ExpenseDetail());
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        model.addAttribute("expenses", expenseDetailService.findAll());
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getCustomerHead() > 0) {
            model.addAttribute("expenseHead", staticInfo.getExpenseHead());
            return "master/addExpenseDetails";
        }
        attributes.addFlashAttribute("emptyCaseMessage", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Please enter a valid Expense Head Account before entering a Expense.</div>");
        return "redirect:/staticinfo";
    }

    @RequestMapping(value = "/Account/addExpense", method = RequestMethod.POST)
    public String addExpense_post(@ModelAttribute("customerForm") ExpenseDetail expenseDetail, RedirectAttributes attributes) {

        Account account = accountService.findAccount(expenseDetail.getAccountCode());
        expenseDetailService.delete(account);
        expenseDetail.setExpenseAccount(account);
        expenseDetailService.save(expenseDetail);
        attributes.addFlashAttribute("expenseAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Expense <strong>" + expenseDetail.getExpenseAccount().getTitle() + " </strong>added successfully.</div>");
        return "redirect:/Account/addExpense";
    }

    @RequestMapping(value = "/Account/removeExpense", method = RequestMethod.POST)
    public String removeCustomer(RedirectAttributes attributes, @RequestParam("accountCode") Integer accountCode) {

        expenseDetailService.delete(accountService.findAccount(accountCode));

        attributes.addFlashAttribute("expenseRemoveSuccess", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Expense removed successfully.</div>");
        return "redirect:/Account/addExpense";
    }

    /////////////////////////// JSON Call For Account Listing//////////////////
        @RequestMapping(value = "/Account/json", method = RequestMethod.GET)
       @ResponseBody
        public HttpEntity<Page<Account>> AccountListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {

        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populateAccountList(accountService.findAllPaged(pageIndex, length, "", false, searchString), draw), HttpStatus.OK);


    }


}
