package com.hellokoding.account.web;

import com.hellokoding.account.model.*;
import com.hellokoding.account.reportModel.JournalVoucherSet;
import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.account.CostCentreService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import com.hellokoding.account.service.ledger.TrialBalanceService;
import com.hellokoding.account.service.note.NoteCalculationService;
import com.hellokoding.account.service.note.NoteService;
import com.hellokoding.account.service.statement.BalanceCalculationService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class ReportController {


    @Autowired
    private AccountService accountService;

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private CostCentreService costCentreService;

    @Autowired
    private LedgerCalculationService ledgerCalculationService;

    @Autowired
    private TrialBalanceService trialBalanceService;

    @Autowired
    private NoteCalculationService noteCalculationService;

    @Autowired
    private BalanceCalculationService balanceCalculationService;

    @Autowired
    private NoteService noteService;

    @Autowired
    private StaticInfoService staticInfoService;

    //////////////////////// Cost Centre Report /////////////////////////////

    @RequestMapping(value = "/Reports/costCentres", method = RequestMethod.GET)
    public String costCentreReport(Model model) {

        model.addAttribute("accounts", accountService.findWithCostCentres());
        model.addAttribute("allAccounts", accountService.findWithCostCentres());

        return "reports/costCentreReport";
    }


    @RequestMapping(value = "/Reports/costCentres", method = RequestMethod.POST)
    public String costCentreReport_post(Model model, @RequestParam("account") Integer accountCode) {

        List<Account> accountList = new ArrayList<>();
        if (accountCode == null) {
            accountList = accountService.findWithCostCentres();

        } else {
            accountList.add(accountService.findAccount(accountCode));
        }
        model.addAttribute("accounts", accountList);
        model.addAttribute("allAccounts", accountService.findWithCostCentres());
        model.addAttribute("postAccount", accountCode);


        return "reports/costCentreReport";
    }

    //////////////////////// Payment Voucher Report /////////////////////////////


    @RequestMapping(value = "/Reports/paymentVoucher", method = RequestMethod.GET)
    public String paymentVoucherReport(Model model) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("PV%"));
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("vouchers", voucherService.findByPrefix(date, date, "PV%"));


        return "reports/paymentVoucherReport";
    }

    @RequestMapping(value = "/Reports/paymentVoucher", method = RequestMethod.POST)
    public String paymentVoucherReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("voucherNumberFrom") String fromVoucher
            , @RequestParam("voucherNumberTo") String toVoucher) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("fromVoucher", fromVoucher);
        model.addAttribute("toVoucher", toVoucher);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("PV%"));
        if (fromVoucher.equals("none") || toVoucher.equals("none")) {
            model.addAttribute("vouchers", voucherService.findByPrefix(fromDate, toDate, "PV%"));
        } else {
            model.addAttribute("vouchers", voucherService.findByPrefixAndVoucher(fromVoucher, toVoucher, "PV%"));
        }


        return "reports/paymentVoucherReport";
    }

/////////////////////////////////// Receipt Voucher Reports//////////////////////////////

    @RequestMapping(value = "/Reports/receiptVoucher", method = RequestMethod.GET)
    public String receiptVoucherReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("RV%"));
        model.addAttribute("vouchers", voucherService.findByPrefix(date, date, "RV%"));


        return "reports/receiptVoucherReport";
    }

    @RequestMapping(value = "/Reports/receiptVoucher", method = RequestMethod.POST)
    public String receiptVoucherReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("voucherNumberFrom") String fromVoucher
            , @RequestParam("voucherNumberTo") String toVoucher) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("fromVoucher", fromVoucher);
        model.addAttribute("toVoucher", toVoucher);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("RV%"));
        if (fromVoucher.equals("none") || toVoucher.equals("none")) {
            model.addAttribute("vouchers", voucherService.findByPrefix(fromDate, toDate, "RV%"));
        } else {
            model.addAttribute("vouchers", voucherService.findByPrefixAndVoucher(fromVoucher, toVoucher, "RV%"));
        }


        return "reports/receiptVoucherReport";
    }


    /////////////////////////////////// Journal Voucher Reports//////////////////////////////

    @RequestMapping(value = "/Reports/journalVoucher", method = RequestMethod.GET)
    public String journalVoucherReport(Model model) {
        JournalVoucherSet journalVoucherSet = new JournalVoucherSet();
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("JV%"));
        for (String voucherNumber : voucherService.findJournalByPrefix(date, date, "JV%")) {
            VoucherList voucherList = new VoucherList();
            voucherList.setVoucherList(voucherService.findByVoucherNumber(voucherNumber));
            Map accountMap = new HashMap();
            for (Account account : accountService.findAll()) {
                accountMap.put(account.getAccountCode(), account.getTitle());
            }
            model.addAttribute("accountMap", accountMap);
            journalVoucherSet.add(voucherList);
        }

        model.addAttribute("journalVoucherSet", journalVoucherSet);
        return "reports/journalVoucherReport";
    }

    @RequestMapping(value = "/Reports/journalVoucher", method = RequestMethod.POST)
    public String journalVoucherReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("voucherNumberFrom") String fromVoucher
            , @RequestParam("voucherNumberTo") String toVoucher) {

        JournalVoucherSet journalVoucherSet = new JournalVoucherSet();
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("fromVoucher", fromVoucher);
        model.addAttribute("toVoucher", toVoucher);
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("JV%"));

        if (fromVoucher.equals("none") || toVoucher.equals("none")) {
            for (String voucherNumber : voucherService.findJournalByPrefix(fromDate, toDate, "JV%")) {

                VoucherList voucherList = new VoucherList();
                voucherList.setVoucherList(voucherService.findByVoucherNumber(voucherNumber));
                Map accountMap = new HashMap();
                for (Account account : accountService.findAll()) {
                    accountMap.put(account.getAccountCode(), account.getTitle());
                }
                model.addAttribute("accountMap", accountMap);
                journalVoucherSet.add(voucherList);
            }
        } else {
            for (String voucherNumber : voucherService.findJournalVouchersByPrefixAndNumber(fromVoucher, toVoucher, "JV%")) {

                VoucherList voucherList = new VoucherList();
                voucherList.setVoucherList(voucherService.findByVoucherNumber(voucherNumber));
                Map accountMap = new HashMap();
                for (Account account : accountService.findAll()) {
                    accountMap.put(account.getAccountCode(), account.getTitle());
                }
                model.addAttribute("accountMap", accountMap);
                journalVoucherSet.add(voucherList);
            }
        }

        model.addAttribute("journalVoucherSet", journalVoucherSet);
        return "reports/journalVoucherReport";
    }


    /////////////////////////////////// Chart Of Account Reports//////////////////////////////
    @RequestMapping(value = "/Reports/chartOfAccount", method = RequestMethod.GET)
    public String chartOfAccount(Model model) {

        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        model.addAttribute("baseAccounts", accountService.findAccountsByLevel(1));
        model.addAttribute("levels", levelList);
        model.addAttribute("currentLevel", 1);
        model.addAttribute("currentLevelOption", 0);


        return "reports/chartOfAccountReport";
    }

    @RequestMapping(value = "/Reports/chartOfAccount", method = RequestMethod.POST)
    public String chartOfAccount_post(Model model, @RequestParam("level") Integer level, @RequestParam("levelOption") Integer levelOption) {

        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        model.addAttribute("baseAccounts", accountService.findAccountsByLevel(level));
        model.addAttribute("levels", levelList);
        model.addAttribute("currentLevel", level);
        model.addAttribute("currentLevelOption", levelOption);

        if (levelOption == 0) {
            return "reports/chartOfAccountReport";
        }
        return "reports/chartOfAccountOnlyLevel";
    }


    /////////////////////////////////// ledger Reports//////////////////////////////


    @RequestMapping(value = "/Reports/ledgerReport", method = RequestMethod.GET)
    public String ledgerReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        model.addAttribute("costCentres", null);


        return "reports/ledgerReport";
    }


    @RequestMapping(value = "/Reports/ledgerReport", method = RequestMethod.POST)
    public String ledgerReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("accountCode") Integer accountCode
            , @RequestParam("costCentreCode") Long costCentreCode) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentAccount", accountCode);
        model.addAttribute("currentCostCentre", costCentreCode);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAccountsByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        if (accountCode == null) {
            model.addAttribute("vouchers", null);
            model.addAttribute("costCentres", null);
        } else {
            model.addAttribute("vouchers", ledgerCalculationService.getSortedVouchers(accountCode, costCentreCode, fromDate, toDate, false,false));
            model.addAttribute("costCentres", costCentreService.getCostCentres(accountCode));
        }

        return "reports/ledgerReport";
    }

    /////////////////////////////////// Bank ledger Reports //////////////////////////////

    @RequestMapping(value = "/Reports/ledgerReportBank", method = RequestMethod.GET)
    public String ledgerReportBank(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("accounts", accountService.getDetailAccounts());


        return "reports/ledgerReportBank";
    }


    @RequestMapping(value = "/Reports/ledgerReportBank", method = RequestMethod.POST)
    public String ledgerReportBank_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("accountCode") Integer accountCode
    ) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentAccount", accountCode);

        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("accounts", accountService.getDetailAccounts());
        if (accountCode == null) {
            model.addAttribute("vouchers", null);

        } else {
            model.addAttribute("vouchers", ledgerCalculationService.getSortedVouchers(accountCode, null, fromDate, toDate, true,false));

        }


        return "reports/ledgerReportBank";
    }


    /////////////////////////////////// trial Balance Basic Report//////////////////////////////

    @RequestMapping(value = "/Reports/trialBalanceBasic", method = RequestMethod.GET)
    public String TrailBalanceBasicReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));

        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("levels", levelList);
        model.addAttribute("trailModel", trialBalanceService.getTrailBalanceBasic(1, date,null,null));
        return "reports/trialBasic";
    }

    @RequestMapping(value = "/Reports/trialBalanceBasic", method = RequestMethod.POST)
    public String TrailBalanceBasicReport_post(Model model
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("level") int level) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));

        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", toDate);
        model.addAttribute("levels", levelList);
        model.addAttribute("trailModel", trialBalanceService.getTrailBalanceBasic(level, toDate,null,null));
        model.addAttribute("currentLevel", level);
        return "reports/trialBasic";
    }

    /////////////////////////////////// trail Balance Detailed Report//////////////////////////////

    @RequestMapping(value = "/Reports/trialBalanceDetail", method = RequestMethod.GET)
    public String TrailBalanceDetailReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));

        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("levels", levelList);
        model.addAttribute("trailModel", trialBalanceService.getTrailBalanceDetailed(1, date, date));
        return "reports/trialDetailed";
    }

    @RequestMapping(value = "/Reports/trialBalanceDetail", method = RequestMethod.POST)
    public String TrailBalanceDetailReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("level") int level) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));

        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", toDate);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("levels", levelList);
        model.addAttribute("currentLevel", level);
        model.addAttribute("trailModel", trialBalanceService.getTrailBalanceDetailed(level, fromDate, toDate));
        return "reports/trialDetailed";
    }


    /////////////////////////////////// Note Summary Report//////////////////////////////


    @RequestMapping(value = "/Reports/noteSummary", method = RequestMethod.GET)
    public String noteSummary(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<AccountNote> accountNotes = noteCalculationService.getNoteSummary("ProfitAndLoss", date, noteService.findByNoteType("ProfitAndLoss", 1), false);
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("toDate", date);
        model.addAttribute("accountNotes", accountNotes);
        model.addAttribute("reportType", "ProfitAndLoss");
        return "reports/noteSummaryReport";
    }

    @RequestMapping(value = "/Reports/noteSummary", method = RequestMethod.POST)
    public String noteSummary_post(Model model
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("reportType") String reportType) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<AccountNote> accountNotes = noteCalculationService.getNoteSummary(reportType, toDate, noteService.findByNoteType(reportType, 1), false);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", toDate);
        model.addAttribute("accountNotes", accountNotes);
        model.addAttribute("reportType", reportType);
        return "reports/noteSummaryReport";
    }

    ////////////////////////////////////////////////////Balance / PL Sheet //////////////////////////////


    @RequestMapping(value = "/Reports/sheetReport", method = RequestMethod.GET)
    public String sheetReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("toDate", date);
        model.addAttribute("reportType", "ProfitAndLoss");
        model.addAttribute("reportSheet", balanceCalculationService.getReportSheet("ProfitAndLoss", date));

        return "reports/balanceSheetReport";
    }

    @RequestMapping(value = "/Reports/sheetReport", method = RequestMethod.POST)
    public String sheetReport_post(Model model
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("reportType") String reportType) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", toDate);
        model.addAttribute("reportType", reportType);
        model.addAttribute("reportSheet", balanceCalculationService.getReportSheet(reportType, toDate));

        return "reports/balanceSheetReport";
    }

    ///////////////////////////////////////////////// Product Ledger //////////////////////////////////////////////////////

    @RequestMapping(value = "/Reports/productLedger", method = RequestMethod.GET)
    public String productLedger(Model model){
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("accounts", accountService.getStockAccounts());
        model.addAttribute("costCentres", null);

        return "reports/productLedger";
    }

    @RequestMapping(value = "/Reports/productLedger", method = RequestMethod.POST)
    public String productLedger_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("accountCode") Integer accountCode
            , @RequestParam("costCentreCode") Long costCentreCode) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentAccount", accountCode);
        model.addAttribute("currentCostCentre", costCentreCode);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("accounts", accountService.getStockAccounts());
        if (accountCode == null) {
            model.addAttribute("vouchers", null);
            model.addAttribute("costCentres", null);
        } else {
            model.addAttribute("vouchers", ledgerCalculationService.getSortedVouchers(accountCode, costCentreCode, fromDate, toDate, false,true));
            model.addAttribute("costCentres", costCentreService.getCostCentres(accountCode));
        }

        return "reports/productLedger";
    }

}
