package com.hellokoding.account.web;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.CostCentre;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.account.CostCentreService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import com.hellokoding.account.service.ledgerBill.LedgerBillService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class LedgerBillController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private LedgerCalculationService ledgerCalculationService;

    @Autowired
    private CostCentreService costCentreService;

    @Autowired
    private LedgerBillService ledgerBillService;

    @Autowired
    private StaticInfoService staticInfoService;

    @RequestMapping(value = "/Reports/ledgerBillReport", method = RequestMethod.GET)
    public String billDetail(Model model) {
        Calendar calendar = Calendar.getInstance();
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<String> months = Arrays.asList(new DateFormatSymbols().getMonths());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("yearList", ledgerBillService.getYearList());
        model.addAttribute("currentYear", Calendar.getInstance().get(Calendar.YEAR));
        model.addAttribute("monthList", months);
        model.addAttribute("currentMonth", calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        return "/reports/ledgerBillReport";
    }

    @RequestMapping(value = "/Reports/ledgerBillReport", method = RequestMethod.POST)
    public String billDetail_Post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("accountCode") Integer accountCode
            , @RequestParam("costCentreCode") Long costCentreCode
            , @RequestParam("year") Integer year
            , @RequestParam("month") String month) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        List<String> months = Arrays.asList(new DateFormatSymbols().getMonths());
        model.addAttribute("yearList", ledgerBillService.getYearList());
        model.addAttribute("currentYear", year);
        model.addAttribute("monthList", months);
        model.addAttribute("currentMonth", month);
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentAccount", accountCode);
        model.addAttribute("currentCostCentre", costCentreCode);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAccountsByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        if (accountCode == null) {
            model.addAttribute("vouchers", null);
            model.addAttribute("costCentres", null);
        } else {
            List<Voucher> voucherList = ledgerCalculationService.getSortedVouchers(accountCode, costCentreCode, fromDate, toDate, false,false)
                    .stream()
                    .filter(e -> e.getVoucherNumber().substring(0, 2).equals("SL"))
                    .collect(Collectors.toList());
            Voucher voucher = ledgerBillService.getLedgerBill(voucherList);

            model.addAttribute("vouchers", voucher);
            model.addAttribute("costCentres", costCentreService.getCostCentres(accountCode));
        }

        return "/reports/ledgerBillReport";
    }

    @RequestMapping(value = "/Account/ledgerBillReport", method = RequestMethod.GET)
    public String getBillMonth(){
        return "redirect:/Reports/ledgerBillReport";
    }

    @RequestMapping(value = "/Account/ledgerBillReport", method = RequestMethod.POST)
    public String getBillMonth_Post(Model model, @RequestParam("year") Integer year,
                                    @RequestParam("month") String month,
                                    @RequestParam("currentAccount") Integer accountCode,
                                    @RequestParam("costCentreCode") Long costCentreCode) {
        java.sql.Date firstDay = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        java.sql.Date lastDay = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<String> months = Arrays.asList(new DateFormatSymbols().getMonths());
        Map<String, Date> dateMap;
        model.addAttribute("yearList", ledgerBillService.getYearList());
        model.addAttribute("currentYear", year);
        model.addAttribute("monthList", months);
        model.addAttribute("currentMonth", month);
        try {
            dateMap = ledgerBillService.getMonthDate(month, year);
            firstDay = new java.sql.Date(dateMap.get("firstDay").getTime());
            lastDay = new java.sql.Date(dateMap.get("lastDay").getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return billDetail_Post(model, firstDay, lastDay, accountCode, costCentreCode, year, month);
    }

}

