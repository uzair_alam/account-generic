package com.hellokoding.account.web;


import com.hellokoding.account.model.StaticInfo;
import com.hellokoding.account.model.UserAuth;
import com.hellokoding.account.service.automation.UpdateBalanceService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.user.UserAuthService;
import com.hellokoding.account.validator.CredentialValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Calendar;

@Controller
public class LoginController {


    @Autowired
    private CredentialValidator credentialValidator;

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private UpdateBalanceService updateBalanceService;


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        model.addAttribute("loginActive", "active");
        model.addAttribute("credintialForm", new UserAuth());
        return "login/login";
    }

    //Logout mapping
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String showLoggedout() {
        return "login/logout";
    }


    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registration(@ModelAttribute("credintialForm") UserAuth userForm, BindingResult bindingResult, Model model) {
        credentialValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("signupActive", "active");
            return "login/login";
        }

        userAuthService.save(userForm);

        //   securityService.autologin(userForm.getUserName(), userForm.getPasswordConfirm());

        return "redirect:/login";
    }


    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo != null) {
            if(staticInfo.getUpdatedDate()!=null) {
                if (!(staticInfo.getUpdatedDate().toString()).equals(date.toString())) {
                    staticInfo.setUpdatedDate(date);
                    staticInfoService.save(staticInfo);
                    updateBalanceService.calculateDailyBalance();
                }
            }
            else{
                staticInfo.setUpdatedDate(date);
                staticInfoService.save(staticInfo);
                updateBalanceService.calculateDailyBalance();
            }
            if (staticInfo.getCompanyName() == null || staticInfo.getCompanyName().isEmpty()) {
                model.addAttribute("companyName", "Your Company Name");
            } else {
                model.addAttribute("companyName", staticInfo.getCompanyName());
            }
            if (staticInfo.getImageUrl() == null || staticInfo.getImageUrl().isEmpty()) {
                model.addAttribute("imageUrl", "");
            } else {
                model.addAttribute("imageUrl", staticInfo.getImageUrl());
            }
        } else {

            model.addAttribute("imageUrl", "");
            model.addAttribute("companyName", "Your Company Name");
        }
        return "dashboard";

    }


    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String settings() {
        return "settings";
    }

}
