package com.hellokoding.account.web;


import com.hellokoding.account.model.*;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.calibration.CalibrationInfoService;
import com.hellokoding.account.service.calibration.QuantityCalculator;
import com.hellokoding.account.service.calibration.StockTankService;
import com.hellokoding.account.service.calibration.TankInfoService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.stock.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@SessionAttributes({"stockTankModel","calibrationModel"})
public class CalibrationController {

    @Autowired
    private CalibrationInfoService calibrationInfoService;

    @Autowired
    private TankInfoService tankInfoService;

    @Autowired
    private StockTankService stockTankService;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private StockService stockService;

    @Autowired
    private QuantityCalculator quantityCalculator;


    ///// Tank Registration Add/Update

    @RequestMapping(value = "/Tanks/addTank", method = RequestMethod.GET)
    public String addStockTank(Model model,@RequestParam(value = "tankId", defaultValue = "0") int tankId) {

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        StockTank stockTank = null;
        if(tankId > 0){
            // Update Case
            stockTank = stockTankService.findByTankId(tankId);
            stockTank.setStockId(stockTank.getTankStock().getStockId());
            for (int i = 0; i < staticInfo.getMaxFormRows() - stockTank.getTankInfoList().size(); i++) {
                TankInfo tankInfo = new TankInfo();
                tankInfo.setDip(0.0);
                tankInfo.setVolume(0.0);
                tankInfo.setStockTank(stockTank);
                stockTank.getTankInfoList().add(tankInfo);
            }
        }else{
            // New Addition
            List<TankInfo> tankInfoList = new ArrayList<>();
            stockTank = new StockTank();
            for (int i = 0; i < staticInfo.getMaxFormRows(); i++) {
                TankInfo tankInfo = new TankInfo();
                tankInfo.setDip(0.0);
                tankInfo.setVolume(0.0);
                tankInfo.setStockTank(stockTank);
                tankInfoList.add(tankInfo);
            }
            stockTank.setTankInfoList(tankInfoList);
        }


        model.addAttribute("stockTankModel", stockTank);
        model.addAttribute("stockAccounts", accountService.getStockAccounts());

        return "calibration/addTank";
    }

    @RequestMapping(value = "/Tanks/addTank", method = RequestMethod.POST)
    public String addStockTank_post(@ModelAttribute("stockTankModel") StockTank stockTank) {

        List<TankInfo> tankInfoList = stockTank.getTankInfoList()
                .stream()
                .filter(e -> e.getDip() != 0.0)
                .collect(Collectors.toList());
        stockTank.setTankInfoList(tankInfoList);
        stockTank.setTankStock(stockService.findByStockId(stockTank.getStockId()));
        stockTankService.save(stockTank);

        return "redirect:/Calibration/tankList";
    }


    //////////// Tank Removal
    @RequestMapping(value = "/Tanks/remove", method = RequestMethod.POST)
    public String removeTank(@RequestParam(value = "tankId", defaultValue = "0") int tankId,RedirectAttributes attributes) {

        StockTank stockTank = stockTankService.findByTankId(tankId);
        stockTankService.remove(stockTank);
        attributes.addFlashAttribute("tankDeleteSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Product Tank <strong>" + stockTank.getName() + " </strong>removed successfully.</div>");
        return "redirect:/Calibration/tankList";
    }





    @RequestMapping(value = "/Calibration/calibrate", method = RequestMethod.GET)
    public String createCalibration(Model model,@RequestParam(value = "stockId", defaultValue = "0") int stockId) {


        CalibrationModel calibrationModel =  new CalibrationModel();
        Stock stock = null;
        if(stockId == 0){
            stock = stockService.findByStockId(stockService.findMaxId());
        }else{
            stock = stockService.findByStockId(stockId);
        }

        if(stock == null){
            return "error";
        }
        calibrationModel.setStock(stock);
        calibrationModel.setStockTankList(stock.getStockTankList());
        calibrationModel.setCurrentQuantity(quantityCalculator.getCalculatedQuantity(stock));
        model.addAttribute("calibrationModel", calibrationModel);
        model.addAttribute("stockList", stockService.findAllStock()
                                        .stream()
                                        .filter(e -> !e.getStockCategory().getTitle().equals("Lubricants"))
                                        .collect(Collectors.toList()));
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        return "calibration/calibrate";
    }

    @RequestMapping(value = "/Calibration/calibrate", method = RequestMethod.POST)
    public String createCalibration_post(@ModelAttribute("calibrationModel") CalibrationModel calibrationModel) {

        CalibrationInfo calibrationInfo = new CalibrationInfo();
        calibrationInfo.setActualQuantity(calibrationModel.getCurrentQuantity());
        calibrationInfo.setTanksQuantity(calibrationModel.getTankQuantity());
        calibrationInfo.setDate(calibrationModel.getDate());
        calibrationInfo.setCalibrationStock(calibrationModel.getStock());
        calibrationInfo.setCalibrated(false);
        calibrationInfoService.save(calibrationInfo);

        return "redirect:/Calibration/calibrationList";
    }


    // Ajax Call for Volume
    @RequestMapping(value = "/Calibration/getVolume", method = RequestMethod.GET)
    public @ResponseBody String getVolume(@RequestParam(value="infoId", defaultValue="0") int infoId) {

        TankInfo tankInfo = tankInfoService.findByInfoId(infoId);
        if(tankInfo != null) {

            return Double.toString(tankInfo.getVolume());
        }
        return Double.toString(0.0);
    }


    ////// LIsting for Tanks and Calibration

    @RequestMapping(value = "/Calibration/calibrationList", method = RequestMethod.GET)
    public String calibrationList(Model model) {

        model.addAttribute("calibrationList", calibrationInfoService.findAll());
        return "calibration/calibrationListing";
    }

    @RequestMapping(value = "/Calibration/tankList", method = RequestMethod.GET)
    public String tankList(Model model) {

        model.addAttribute("tankList", stockTankService.findAll());
        return "calibration/tankListing";
    }



    /////// Calibration Finalize or remove Voucher Creation

    //////////// Calibration Removal
    @RequestMapping(value = "/Calibration/remove", method = RequestMethod.POST)
    public String removeCalibration(@RequestParam(value = "infoId", defaultValue = "0") int infoId,RedirectAttributes attributes) {

        CalibrationInfo calibrationInfo = calibrationInfoService.findByInfoId(infoId);
        calibrationInfoService.remove(calibrationInfo);
        attributes.addFlashAttribute("infoRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Calibration info removed successfully.</div>");
        return "redirect:/Calibration/calibrationList";
    }

    //////////// Calibration Finalize
    @RequestMapping(value = "/Calibration/generateVouchers", method = RequestMethod.POST)
    public String finalizeCalibration(@RequestParam(value = "infoId", defaultValue = "0") int infoId,RedirectAttributes attributes) {

        CalibrationInfo calibrationInfo = calibrationInfoService.findByInfoId(infoId);
        calibrationInfoService.finalizeCalibration(calibrationInfo);
        attributes.addFlashAttribute("calibrationSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Calibration and vouchers generated successfully.</div>");
        return "redirect:/Calibration/calibrationList";
    }


}
