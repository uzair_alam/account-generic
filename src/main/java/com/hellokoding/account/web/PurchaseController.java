package com.hellokoding.account.web;

import com.hellokoding.account.model.*;
import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.datalisting.JSONPopulateService;
import com.hellokoding.account.service.nozzle.DailyReadingService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class PurchaseController {

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private DailyReadingService dailyReadingService;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private JSONPopulateService jsonPopulateService;

    @RequestMapping(value = "/Purchase/viewPurchaseVoucher", method = RequestMethod.GET)
    public String viewPurchaseVoucher() {

        return "purchase/viewPurchaseVoucher";
    }

    @RequestMapping(value = "/Purchase/createPurchaseVoucher", method = RequestMethod.GET)
    public String addPurchaseVoucher(Model model) {
        VoucherList voucherList = new VoucherList();
        for (int i = 0; i < 20; i++) {
            voucherList.add(new Voucher());
        }

        model.addAttribute("voucherList", voucherList);
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("LP"));
        model.addAttribute("stockAccounts", accountService.getStockAccounts());
        model.addAttribute("accounts", accountService.getVendorAccounts());

        return "purchase/addPurchaseVoucher";
    }

    @RequestMapping(value = "/Purchase/createPurchaseVoucher", method = RequestMethod.POST)
    public String addPurchaseVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        String voucherNumber = "";
        Double supplierCreditAmount = 0.0;
        Voucher supplierVoucher = new Voucher();
        for (Voucher voucher : voucherList.getVoucherList()) {

            if (voucher.getItemAccount() > 0) {
                supplierCreditAmount += voucher.getDebit();
                supplierVoucher.setAccountCode(voucher.getAccountCode());
                supplierVoucher.setVoucherNumber(voucher.getVoucherNumber());
                supplierVoucher.setBillDate(voucher.getBillDate());
                supplierVoucher.setBillNumber(voucher.getBillNumber());
                supplierVoucher.setVoucherDate(voucher.getVoucherDate());

                voucher.setAccountCode(voucher.getItemAccount());
                dailyReadingService.updateDailyReading(voucher);
                voucherService.saveJournal(voucher);

            }

        }

        if (supplierVoucher.getVoucherNumber() != null) {
            voucherNumber = supplierVoucher.getVoucherNumber();
            supplierVoucher.setCredit(supplierCreditAmount);
            voucherService.saveJournal(supplierVoucher);
        }

        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Purchase Vouchers with number <strong>" + voucherNumber + " </strong>added successfully.</div>");
        return "redirect:/Purchase/createPurchaseVoucher";
    }

    /////////////////////////////////////////// Edit Purchase Vouchers //////////////////////////////////

    @RequestMapping(value = "/Purchase/updatePurchaseVoucher", method = RequestMethod.GET)
    public String updatePurchaseVoucher(Model model, @RequestParam(value = "voucherNumber") String voucherNumber) {
        List<Voucher> voucherItemList = voucherService.findByVoucherNumber(voucherNumber);
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        VoucherList voucherList = new VoucherList();
        Account currentVendorAccount = new Account();
        java.sql.Date currentDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        for (Voucher voucher : voucherItemList) {
            if (voucher.getItemAccount() > 0) {
                voucher.setItemQuantity(voucher.getItemQuantity());
                voucher.setItemRate(voucher.getItemRate());
                voucher.setBillAmount(voucher.getBillAmount());
                voucherList.add(voucher);
            } else {
                currentVendorAccount = accountService.findAccount(voucher.getAccountCode());
                currentDate = voucher.getVoucherDate();
            }
        }
        int size = voucherItemList.size();
        for (int i = 0; i < staticInfoService.findStaticInfo().getMaxFormRows() - size; i++) {
            Voucher voucher = new Voucher();
            voucher.setItemQuantity(0.0);
            voucher.setBillAmount(0.0);
            voucher.setItemRate(0.0);
            voucherList.add(voucher);
        }
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("currentDate", currentDate);
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("currentVendorAccount", currentVendorAccount);
        model.addAttribute("stockAccounts", accountService.getStockAccounts());
        model.addAttribute("accounts", accountService.getVendorAccounts());
        model.addAttribute("cashInHand", staticInfo.getCashInHand());

        return "purchase/updatePurchaseVoucher";
    }
    /////////////////////////////////// print Purchase Invoice///////////////////////////////////

    @RequestMapping(value = "/Purchase/printPurchaseInvoice", method = RequestMethod.GET)
    public String printDeliveryChallan(Model model, @RequestParam("voucherNumber") String voucherNumber) {
        List<Voucher> voucherList = voucherService.findByVoucherNumber(voucherNumber);
        Map accountMap = new HashMap();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo != null) {
            model.addAttribute("bottomImageUrl", staticInfo.getFooterImageUrl());
            if (staticInfo.getImageUrl() == null || staticInfo.getImageUrl().isEmpty()) {
                model.addAttribute("imageUrl", "");
            } else {
                model.addAttribute("imageUrl", staticInfo.getImageUrl());
            }
            if (staticInfo.getCompanyName() == null || staticInfo.getCompanyName().isEmpty()) {
                model.addAttribute("companyName", "Your Company Name");
            } else {
                model.addAttribute("companyName", staticInfo.getCompanyName());
            }
            model.addAttribute("companyAddress", staticInfo.getCompanyAddress());
            model.addAttribute("phone", staticInfo.getPhone());
        } else {

            model.addAttribute("imageUrl", "");
            model.addAttribute("companyName", "Your Company Name");
        }
        model.addAttribute("currentDate", dateFormat.format(date));
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("accountCode", voucherList.stream().filter(e -> e.getCredit() < 1).findFirst().get().getAccountCode());
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("voucherNumber", voucherNumber);
        return "purchase/printPurchaseInvoice";
    }


    @RequestMapping(value = "/Purchase/updatePurchaseVoucher", method = RequestMethod.POST)
    public String updatePurchaseVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, @RequestParam("voucherNumber") String voucherNumber, RedirectAttributes attributes) {
        Double supplierCreditAmount = 0.0;
        Voucher supplierVoucher = new Voucher();
        List<Voucher> voucherNumberList = voucherService.findByVoucherNumber(voucherNumber);
        if (voucherNumberList.size() > 0) {
            voucherService.remove(voucherNumberList);
        }
        for (Voucher voucher : voucherList.getVoucherList()) {
            if (voucher.getItemAccount() > 0) {
                supplierCreditAmount += voucher.getDebit();
                supplierVoucher.setAccountCode(voucher.getAccountCode());
                supplierVoucher.setVoucherNumber(voucher.getVoucherNumber());
                supplierVoucher.setBillDate(voucher.getBillDate());
                supplierVoucher.setBillNumber(voucher.getBillNumber());
                supplierVoucher.setVoucherDate(voucher.getVoucherDate());

                voucher.setAccountCode(voucher.getItemAccount());
                voucherService.saveJournal(voucher);

            }

        }

        if (supplierVoucher.getVoucherNumber() != null) {
            supplierVoucher.setCredit(supplierCreditAmount);
            voucherService.saveJournal(supplierVoucher);
        }

        attributes.addFlashAttribute("voucherUpdateSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Purchase Vouchers with number <strong>" + voucherNumber + " </strong>updated successfully.</div>");
        return "redirect:/Purchase/viewPurchaseVoucher";
    }

    /////////////////////////// JSON Generator On DataTable Ajax Call //////////////////

    @RequestMapping(value = "/Purchase/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Voucher>> saleVoucherListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populatePurchaseVoucherList(voucherService.findAllPaged(pageIndex, length, "voucherDate", false, searchString,"LP"), draw), HttpStatus.OK);
        //accountService.findAccount(1001000000);

    }


}
