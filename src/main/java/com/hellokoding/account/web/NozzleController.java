package com.hellokoding.account.web;


import com.hellokoding.account.model.*;
import com.hellokoding.account.model.nozzle.CashCreditRecord;
import com.hellokoding.account.model.nozzle.NozzleDayEndList;
import com.hellokoding.account.model.nozzle.VerificationModel;
import com.hellokoding.account.repository.DailyReadingRepository;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import com.hellokoding.account.service.nozzle.DayEndService;
import com.hellokoding.account.service.nozzle.NozzleService;
import com.hellokoding.account.service.stock.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.Date;
import java.util.*;

@Controller
@SessionAttributes({"dayEndList"})
public class NozzleController {

    @Autowired
    private NozzleService nozzleService;

    @Autowired
    private  AccountService accountService;

    @Autowired
    private StockService stockService;

    @Autowired
    private DayEndService dayEndService;





    //////////////////////////////////Master Nozzle ///////////////////////////////////////
    @RequestMapping(value = "/Nozzle/addNozzleMaster", method = RequestMethod.GET)
    public String addNozzleMaster(Model model) {
        model.addAttribute("nozzleForm",new NozzleMaster());
        model.addAttribute("nozzles", nozzleService.findAllNozzles());
        model.addAttribute("stockItems",stockService.findAllStock() );
        model.addAttribute("maxOrder",nozzleService.getMaxSortOrder() + 1);
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);

        return "nozzle/addNozzle";
    }

    @RequestMapping(value = "/Nozzle/addNozzleMaster", method = RequestMethod.POST)
    public String addNozzleMaster_post(@ModelAttribute("nozzleForm") NozzleMaster nozzleMaster, RedirectAttributes attributes) {

        nozzleMaster.setNozzleItem(stockService.findByStockId(nozzleMaster.getStockId()));
        if(!nozzleService.orderExists(nozzleMaster.getSortOrder())){
            nozzleService.saveNozzle(nozzleMaster);
            attributes.addFlashAttribute("nozzleAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Nozzle  <strong>"+nozzleMaster.getNozzleName()+" </strong>added successfully.</div>");
        } else{
            attributes.addFlashAttribute("nozzleAddSuccess", "<div style=\"background-color: #deb3bf;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #8a3f54;border-radius: 5px;font-size: initial;\">Nozzle with sort Order <strong>"+nozzleMaster.getSortOrder()+" </strong>already exists.</div>");
        }
        return "redirect:/Nozzle/addNozzleMaster";
    }


    @RequestMapping(value = "/Nozzle/removeNozzle", method = RequestMethod.POST)
    public String removeNozzle(RedirectAttributes attributes,@RequestParam("nozzleCode") Integer nozzleCode) {

        nozzleService.removeNozzleByCode(nozzleCode);

        attributes.addFlashAttribute("nozzleRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Nozzle removed successfully.</div>");
        return "redirect:/Nozzle/addNozzleMaster";
    }


    @RequestMapping(value = "/Nozzle/editNozzle", method = RequestMethod.POST)
    public String editNozzle(RedirectAttributes attributes,@RequestParam("nozzleCode") Integer nozzleCode,Model model) {
        model.addAttribute("nozzleForm",nozzleService.findByNozzleCode(nozzleCode));
        return "nozzle/editNozzle";
    }

    @RequestMapping(value = "/Nozzle/doEditNozzle", method = RequestMethod.POST)
    public String doEditNozzle_post(@ModelAttribute("nozzleForm") NozzleMaster nozzleMaster, RedirectAttributes attributes) {

        nozzleService.saveNozzle(nozzleMaster);
        attributes.addFlashAttribute("nozzleEditSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Nozzle  <strong>"+nozzleMaster.getNozzleName()+" </strong>added successfully.</div>");
        return "redirect:/Nozzle/addNozzleMaster";
    }




    /////////////////////////////////////////// Add Reading ////////////////////////////
    @RequestMapping(value = "/Nozzle/addReading", method = RequestMethod.GET)
    public String addReading(Model model) {
        model.addAttribute("readingForm",new DailyReading());
        model.addAttribute("nozzles", nozzleService.findAllNozzles());
        model.addAttribute("stockAccounts",accountService.getStockAccounts());
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);

        return "nozzle/addReading";
    }



    @RequestMapping(value = "/Nozzle/addReading", method = RequestMethod.POST)
    public String addReading_post(@ModelAttribute("readingForm") DailyReading dailyReading, RedirectAttributes attributes) {


        dailyReading.setItemAccount(accountService.findAccount(dailyReading.getAccountCode()));
        dailyReading.setNozzle(nozzleService.findByNozzleCode(dailyReading.getNozzleCode()));
        // saveReading creates an internal SC voucher
        nozzleService.saveReading(dailyReading);
        attributes.addFlashAttribute("readingAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Reading for <strong>"+dailyReading.getItemAccount().getTitle()+" </strong>added successfully.</div>");
        return "redirect:/Nozzle/addDailyReading";
    }


    /////////////////////////////////////////// Daily Reading ////////////////////////////
    @RequestMapping(value = "/Nozzle/addDayEnd", method = RequestMethod.GET)
    public String addDayEnd(Model model, Date date,String message) {

        if(date == null){
            Date maxDate = dayEndService.getMaxDate();
            if(maxDate == null){
                date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            }else{
                Calendar c = Calendar.getInstance();
                c.setTime(maxDate);
                c.add(Calendar.DATE, 1);
                date =  new java.sql.Date( maxDate.getTime() + 24*60*60*1000);

            }

        }

        Map<Integer,Double> nozzleReadingMap = nozzleService.getNozzleMap(date);
        Map<Integer,Double> itemReadingMap = nozzleService.getItemMap(date);

        NozzleDayEndList nozzleDayEndList =  new NozzleDayEndList();
        for(NozzleMaster nozzleMaster : nozzleService.findAllNozzles()){
            DailyReading dailyReading = new DailyReading();
            dailyReading.setNozzle(nozzleMaster);
            dailyReading.setItemAccount(nozzleMaster.getNozzleItem().getStockAccount());
            if(nozzleReadingMap.containsKey(nozzleMaster.getNozzleCode())){
                dailyReading.setOpeningReading(nozzleReadingMap.get(nozzleMaster.getNozzleCode()));
            }else{
                dailyReading.setOpeningReading(nozzleMaster.getOpeningReading());
            }
            nozzleDayEndList.addDailyNozzle(dailyReading);
        }


        List<Stock> stockList = stockService.findAllStock();
        for(Stock stock : stockList){
            if(stock.getStockCategory().getTitle().equals("Lubricants")){
                DailyReading dailyReading = new DailyReading();
                dailyReading.setItemAccount(stock.getStockAccount());
                if(itemReadingMap.containsKey(stock.getStockAccount().getAccountCode())){
                    dailyReading.setOpeningReading(itemReadingMap.get(stock.getStockAccount().getAccountCode()));
                }else{
                    dailyReading.setOpeningReading(stock.getOpeningQuantity());
                }
                nozzleDayEndList.addDailyItem(dailyReading);
            }

        }
        model.addAttribute("dayEndList", nozzleDayEndList);
        model.addAttribute("currentDate", date);
        model.addAttribute("errorMessage", message);

        return "nozzle/addDayEnd";
    }

    @RequestMapping(value = "/Nozzle/addDayEnd", method = RequestMethod.POST)
    public String addDayEnd_post(@ModelAttribute("dayEndList") NozzleDayEndList nozzleDayEndList, Model model) {

        VerificationModel verificationModel = dayEndService.verifyDayEnd(nozzleDayEndList);
        if (verificationModel.isValid()) {


            List<Stock> stockList = new ArrayList<>();
            ///////<<<<< for deletion of old records >>>//////////////////////////
            List<DailyReading> deletionList = new ArrayList<>();
            deletionList.addAll(nozzleDayEndList.getDailyNozzleReadingList());
            deletionList.addAll(nozzleDayEndList.getDailyItemReadingList());
            dayEndService.removeDayEndDetails(deletionList, nozzleDayEndList.getDate());
            /////////////////////////////////////////////////////////////>>>>
            for (DailyReading dailyReading : nozzleDayEndList.getDailyNozzleReadingList()) {
                stockList.add(dailyReading.getItemAccount().getAccountStock());
                nozzleService.saveReading(dailyReading);
            }

            for (DailyReading dailyReading : nozzleDayEndList.getDailyItemReadingList()) {
                stockList.add(dailyReading.getItemAccount().getAccountStock());
                nozzleService.saveReading(dailyReading);
            }

            Set<Stock> hs = new HashSet<>();
            hs.addAll(stockList);
            stockList.clear();
            stockList.addAll(hs);
            for (Stock stock : stockList) {
                dayEndService.createDayEndVoucher(stock, nozzleDayEndList.getDate());
            }
            return dayEndFinal(model, nozzleDayEndList.getDate(),null);

        }

        return addDayEnd(model, nozzleDayEndList.getDate(),verificationModel.getErrorMessage());

    }

    @RequestMapping(value = "/Nozzle/addDayEndDatePost", method = RequestMethod.POST)
    public String addDayEndDatePost(Model model,@RequestParam("reportDate") Date date) {

        Date maxDate = dayEndService.getMaxDate();
        if(maxDate != null) {
            if (!date.before(maxDate)) {
                return addDayEnd(model, date, null);
            }else{
                return dayEndFinal(model,date, null);
            }
        }
        return addDayEnd(model,date, null);
    }

    //////////////////////////////Day End Report //////////////////

    @RequestMapping(value = "/Nozzle/dayEndFinal", method = RequestMethod.GET)
    public String dayEndFinal(Model model, Date date, String message) {

        if(date == null){
            date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        }


        List <CashCreditRecord> cashCreditRecordList = new ArrayList<>();
        List<Stock> stockList = stockService.findAllStock();
        Double meterSale = 0.0;
        Double cashSale = 0.0;
        for(Stock stock : stockList){
            CashCreditRecord cashCreditRecord = new CashCreditRecord();
            cashCreditRecord.setItem(stock.getName());
            cashCreditRecord.setCredit(LedgerCalculationService.round(dayEndService.calculateStockCredit(stock,date),2));
            cashCreditRecord.setCash(LedgerCalculationService.round(dayEndService.calculateStockDebit(stock,date),2));
            cashCreditRecord.setTotal(LedgerCalculationService.round(cashCreditRecord.getCash() + cashCreditRecord.getCredit(),2));
            cashCreditRecord = dayEndService.populateQuantity(cashCreditRecord,stock.getStockAccount(),date);
            cashSale += cashCreditRecord.getCash();
            if(!stock.getStockCategory().getTitle().equals("Lubricants")){
                meterSale += cashCreditRecord.getSaleQuantity();
            }
            cashCreditRecordList.add(cashCreditRecord);
        }
        model.addAttribute("meterSale", LedgerCalculationService.round(meterSale,2));
        model.addAttribute("cashSale", LedgerCalculationService.round(cashSale,2));
        model.addAttribute("cashModel" , dayEndService.populateCashModel(date));
        model.addAttribute("cashCreditList", cashCreditRecordList);
        model.addAttribute("currentDate", date);
        model.addAttribute("showUpdate" ,!(date.before(dayEndService.getMaxDate())));
        model.addAttribute("errorMessage", message);
        return "nozzle/dayEndFinal";
    }


    @RequestMapping(value = "/Nozzle/dayEndFinalPost", method = RequestMethod.POST)
    public String dayEndFinal_post(Model model,@RequestParam("reportDate") Date date) {

        return dayEndFinal(model,date,null);

    }

    /////////////////////// For post of update ////////////////////////////////

    @RequestMapping(value = "/Nozzle/updateNozzleEntry", method = RequestMethod.POST)
    public String updateNozzleEntry(Model model,@RequestParam("reportDate") Date date) {

        return addDayEndDatePost(model,date);

    }


}
