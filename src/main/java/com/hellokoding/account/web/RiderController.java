package com.hellokoding.account.web;


import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Rider;
import com.hellokoding.account.model.StaticInfo;
import com.hellokoding.account.model.StockCustomer;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.ledger.TrialBalanceService;
import com.hellokoding.account.service.rider.RiderService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.stock.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

@Controller
public class RiderController {


    @Autowired
    private RiderService riderService;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private TrialBalanceService trialBalanceService;

    /**
     * Rider Listing And Addition
     * @param model
     * @param attributes
     * @return Response Page
     */
    @RequestMapping(value = "/Rider/addRider", method = RequestMethod.GET)
    public String addRider(Model model, RedirectAttributes attributes) {

        model.addAttribute("riders" , riderService.findAll());

        Account account = new Account();
        account.setRider(new Rider());
        model.addAttribute("accountForm",account);
        model.addAttribute("currentDate",new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if(staticInfo.getRiderHead() > 0){
            model.addAttribute("riderHead",staticInfo.getRiderHead());
            return "rider/addRider";
        }
        attributes.addFlashAttribute("emptyCaseMessage", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Please enter a valid Rider Head Account before entering a Rider.</div>");
        return "redirect:/staticinfo";
    }

    /**
     * Rider Remove
     * @param attributes Redirect Messages
     * @param riderId To fetch Rider
     * @return Response Page
     */
    @RequestMapping(value = "/Rider/removeRider", method = RequestMethod.POST)
    public String removeRider( RedirectAttributes attributes, @RequestParam(name = "riderId", defaultValue = "0") Integer riderId) {

        riderService.remove(riderId);
        attributes.addFlashAttribute("riderRemoveSuccess", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Rider Removed Successfully.</div>");
        return "redirect:/Rider/addRider";

    }


    /**
     * Listing Of Rider's Registered Customers
     * @param model
     * @param riderId Get request Id
     * @return Response Page
     */
    @RequestMapping(value = "/Rider/addCustomer", method = RequestMethod.GET)
    public String addRiderCustomer(Model model, @RequestParam(name = "riderId", defaultValue = "0") Integer riderId) {

        Rider rider = riderService.findByRiderId(riderId);
        if(rider == null ){
            return "error";
        }
        model.addAttribute("riderCustomers",customerService.findCurrentCustomers(rider,null,true) );
        model.addAttribute("availableCustomers", customerService.findAvailableCustomers(true));
        model.addAttribute("rider", rider);
        return "rider/riderCustomers";

    }

    /**
     * Rider Customer Add POST
     * @param attributes For Success Message
     * @param riderId Rider Identification
     * @param customerId Customer Identification
     * @return Response Page
     */
    @RequestMapping(value = "/Rider/addCustomer", method = RequestMethod.POST)
    public String addRiderCustomer_post(RedirectAttributes attributes, @RequestParam(name = "riderId", defaultValue = "0") Integer riderId
                                ,@RequestParam(name = "customerId", defaultValue = "0") Integer customerId) {

        Rider rider = riderService.findByRiderId(riderId);
        StockCustomer stockCustomer =  customerService.findByCustomerId(customerId);
        if(rider == null || stockCustomer == null){
            return "error";
        }
        stockCustomer.setRider(rider);
        customerService.save(stockCustomer);
        attributes.addFlashAttribute("customerAddSuccess", "<div style=\"background-color: #d6ffbc;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #bbb5b5;border-radius: 5px;font-size: initial;\">Customer For Rider "+rider.getName()+" added Successfully.</div>");
        return "redirect:/Rider/addCustomer?riderId=" + riderId;

    }


    /**
     * Remove Customer from Rider List
     * @param attributes
     * @param riderId Rider ID For Redirection
     * @param customerId CustomerID for Removal
     * @return Response Page
     */
    @RequestMapping(value = "/Rider/removeCustomer", method = RequestMethod.POST)
    public String removeCustomer_post(RedirectAttributes attributes, @RequestParam(name = "riderId", defaultValue = "0") Integer riderId
            ,@RequestParam(name = "customerId", defaultValue = "0") Integer customerId) {

        StockCustomer stockCustomer =  customerService.findByCustomerId(customerId);
        if(stockCustomer == null){
            return "error";
        }
        stockCustomer.setRider(null);
        customerService.save(stockCustomer);
        attributes.addFlashAttribute("customerRemoveSuccess", "<div style=\"background-color: #d6ffbc;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #bbb5b5;border-radius: 5px;font-size: initial;\">Customer Removed Successfully.</div>");
        return "redirect:/Rider/addCustomer?riderId=" + riderId;

    }

    /**
     * To check Rider's Customer Balances . GET Request
     * @param model
     * @return Response Page
     */
    @RequestMapping(value = "/Rider/customerBalance", method = RequestMethod.GET)
    public String customerBalance(Model model) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("riders", riderService.findAll());
        model.addAttribute("currentRider", 0);
        model.addAttribute("trialModel",null);
        return "rider/customerBalance";
    }

    /**
     * POST call for Rider's Customer Balance
     * @param model
     * @param toDate Balance Till date
     * @param riderId Rider Identification
     * @return Response Page
     */
    @RequestMapping(value = "/Rider/customerBalance", method = RequestMethod.POST)
    public String customerBalance_post(Model model
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("riderId") int riderId) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", toDate);
        model.addAttribute("riders", riderService.findAll());
        model.addAttribute("currentRider", riderId);
        model.addAttribute("trialModel", trialBalanceService.getTrailBalanceBasic(1, toDate,null,accountService.findAccountForRider(riderId)));
        return "rider/customerBalance";
    }

}
