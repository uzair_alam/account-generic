package com.hellokoding.account.constants;

import com.hellokoding.account.model.TaxDetail;

public interface Constants {


    static String ACCOUNT_TYPE_DEBIT = "debit";
    static String ACCOUNT_TYPE_CREDIT = "credit";
    static String PREFIX_FOR_PAYMENT = "PV";
    static String PREFIX_FOR_RECEIPT = "RV";
    static String PREFIX_FOR_JOURNAL = "JV";
    static String PREFIX_FOR_SALE = "SL";
    static String PREFIX_FOR_PURCHASE = "LP";
    static String NOTE_OPTION_DEBIT = "Debit";
    static String NOTE_OPTION_CREDIT = "Credit";
    static String NOTE_OPTION_NET = "Net";
    static Integer CASH_ACCOUNT = 1104010001;
    static Integer ADJUSTMENT_ACCOUNT = 1104010005;






}
