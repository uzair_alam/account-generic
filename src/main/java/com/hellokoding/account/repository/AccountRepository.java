package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.BankDetails;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AccountRepository extends PagingAndSortingRepository<Account, Long>, QueryByExampleExecutor<Account> {

    @Query("select a from Account a where  a.parentAccount is null ")
    public List<Account> findControlAccounts();

    @Query("select coalesce(max(a.accountCode), 0) from Account a where  a.parentAccount is null ")
    public int findMaxControlAccount();

    @Query("select coalesce(max(a.accountCode), 0) from Account a where  a.parentAccount =:parent ")
    public int findMaxInnerAccount(@Param("parent") Account parentCode);

    @Query("select a from Account a join fetch a.stockCustomer s join fetch s.rider r where r.riderId =:riderId ")
    public List<Account> findAccountForRider(@Param("riderId") int riderId);


    public List<Account> findByIsActive(int isActive);

    public Account save(Account account);

    public Account findByAccountCode(int accountCode);

    public List<Account> findByBankDetailsNotNull();

    public List<Account> findByAccountStockNotNull();

    public List<Account> findByStockCustomerNotNull();

    public List<Account> findByStockVendorNotNull();

    public List<Account> findDistinctByCostCentresNotNull();

    public List<Account> findByBankDetails(BankDetails bankDetails);

    public List<Account> findDistinctByTaxDetailListNotNull();

    public List<Account> findAccountByLevel(int level);

    @Query("select a from Account a where a.accountCode in :accountCodeList ")
    public List<Account> getAccountNames(@Param("accountCodeList") List<Integer> accountCodeList);


    @Modifying
    @Transactional
    @Query("DELETE FROM Account a where a.accountCode = :accountCode")
    void deleteAccount(@Param("accountCode") Integer accountCode);

}
