package com.hellokoding.account.repository;

import com.hellokoding.account.model.Rider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface RiderRepository extends JpaRepository<Rider, Long> {

    @Modifying
    @Transactional
    @Query("DELETE FROM Rider r WHERE r.riderId = :riderId")
    void deleteByRiderId(@Param("riderId") Integer id);

    Rider findByRiderId(int riderId);
}
