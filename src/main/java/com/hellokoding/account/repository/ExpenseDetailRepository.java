package com.hellokoding.account.repository;


import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.ExpenseDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ExpenseDetailRepository extends JpaRepository<ExpenseDetail,Long> {
    @Modifying
    @Transactional
    @Query("DELETE FROM ExpenseDetail e WHERE e.expenseAccount = :account")
    void deleteByAccountCode(@Param("account") Account account);
}
