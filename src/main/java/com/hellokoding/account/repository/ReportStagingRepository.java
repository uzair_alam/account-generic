package com.hellokoding.account.repository;

import com.hellokoding.account.model.ReportStaging;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReportStagingRepository extends JpaRepository<ReportStaging ,Long> {

  ReportStaging findByStageId(Integer stageId);
  List<ReportStaging> findByReportType(String reportType);
}
