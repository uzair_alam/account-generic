package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.NozzleMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface NozzleMasterRepository extends JpaRepository <NozzleMaster , Long> {

    NozzleMaster findByNozzleCode(Integer nozzleCode);

    @Query("SELECT coalesce(max (n.sortOrder), 0) FROM NozzleMaster n")
    int getMaxOrder();

    List<NozzleMaster> findBySortOrder(Integer sortOrder);

    @Modifying
    @Transactional
    @Query("DELETE FROM NozzleMaster t WHERE t.nozzleCode = :nozzleCode")
    void removeNozzle(@Param("nozzleCode") Integer nozzleCode);
}
