package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.StockVendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface StockVendorRepository extends JpaRepository<StockVendor, Long> {

    @Modifying
    @Transactional
    @Query("DELETE FROM StockVendor s WHERE s.vendorAccount = :account")
    void deleteByAccountCode(@Param("account") Account account);
}


