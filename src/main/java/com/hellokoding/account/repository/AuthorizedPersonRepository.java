package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.AuthorizedPerson;
import com.hellokoding.account.model.BankDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;



public interface AuthorizedPersonRepository extends JpaRepository<AuthorizedPerson , Long> {

    @Query("Select a.name FROM com.hellokoding.account.model.AuthorizedPerson a WHERE a.bankDetails = :bank")
    public List<String> getAuthPersons(@Param("bank") BankDetails bankDetails);
}
