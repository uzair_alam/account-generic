package com.hellokoding.account.repository;

import com.hellokoding.account.model.StockContactPerson;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockContactPersonRepository extends JpaRepository <StockContactPerson, Long> {
}
