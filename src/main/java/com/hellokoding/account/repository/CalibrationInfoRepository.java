package com.hellokoding.account.repository;

import com.hellokoding.account.model.CalibrationInfo;
import com.hellokoding.account.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CalibrationInfoRepository extends JpaRepository<CalibrationInfo,Long> {

    CalibrationInfo findByInfoId(int infoId);

    List<CalibrationInfo> findByCalibrationStockAndIsCalibratedAndInfoIdLessThanEqual(Stock stock,boolean calibrated, int infoId);
}
