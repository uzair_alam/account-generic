package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.TaxDetail;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;

public interface TaxDetailRepository extends JpaRepository <TaxDetail, Long> {


    @Modifying
    @Transactional
    @Query("DELETE FROM TaxDetail t WHERE t.baseAccount = :account")
    public void removeDetails(@Param("account") Account account);



    public TaxDetail findFirstByBaseAccountAndEffectiveTillGreaterThanEqual(Account baseAccount,Date dateTill);

}
