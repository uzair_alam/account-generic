package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Voucher;
import org.hibernate.criterion.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Pageable;

import java.sql.Date;
import java.util.List;

public interface VoucherRepository extends PagingAndSortingRepository<Voucher, Long>, QueryByExampleExecutor<Voucher> {

    public Voucher findByVoucherId(int voucherId);

    public List<Voucher> findByVoucherNumber(String voucherNumber);

    public List<Voucher> findByBankAccount(int bankAccountCode);

    public List<Voucher> findByAccountCodeAndVoucherDateLessThanEqual(int bankAccountCode, Date date);

    public List<Voucher> findByVoucherNumberIn(List<String> voucherList);

    @Query("select v from Voucher v where v.voucherDate >= :fromDate and v.voucherDate <= :toDate and v.accountCode=:accountCode")
    public List<Voucher> findVouchersByCurrentDate(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("accountCode") int accountCode);

    public List<Voucher> findByAccountCodeOrderByVoucherDateAsc(int accountCode);

    @Query("select max(v.voucherNumber) from Voucher v where v.voucherNumber like :prefixString")
    public String findMaxVoucherNumber(@Param("prefixString") String prefixString);

    @Query("select v.voucherNumber from Voucher v where v.voucherId = (select max(v.voucherId) from v where v.referenceVoucher is not null)")
    public String findMaxIdForChild();

    @Query("select v from Voucher v where v.voucherNumber like :prefixString")
    public List<Voucher> findByVoucherType(@Param("prefixString") String prefixString);

    @Query("select v from Voucher v where v.bankAccount = :bank and v.voucherDate <= :date and v.clearingDate is null")
    public List<Voucher> findPendingVouchers(@Param("bank") int bankAccount, @Param("date") Date date);

    @Query("select v from Voucher v where v.bankAccount = :bank and v.voucherDate <= :date and v.clearingDate is not null")
    public List<Voucher> findClearedVouchers(@Param("bank") int bankAccount, @Param("date") Date date);

    @Query("select v from Voucher v where v.voucherDate >= :fromDate and v.voucherDate <= :toDate and v.voucherNumber like :prefixString")
    public List<Voucher> findVouchersByPrefix(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("prefixString") String prefixString);

    @Query("select distinct v.voucherNumber from Voucher v where v.voucherDate >= :fromDate and v.voucherDate <= :toDate and v.voucherNumber like :prefixString")
    public List<String> findJournalVouchersByPrefix(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("prefixString") String prefixString);

    @Query("select v from Voucher v where v.clearingDate >= :fromDate and v.clearingDate <= :toDate and v.bankAccount =:bankAccount")
    public List<Voucher> findJournalVouchersByBankAccount(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("bankAccount") int bankAccount);


    @Query("select distinct v.voucherNumber from Voucher v where v.voucherNumber between :fromVoucher and :toVoucher and v.voucherNumber like :prefixString")
    public List<String> findJournalVouchersByPrefixAndNumber(@Param("fromVoucher") String fromVoucher, @Param("toVoucher") String toVoucher, @Param("prefixString") String prefixString);

    @Query("select v from Voucher v where v.voucherNumber between :fromVoucher and :toVoucher and v.voucherNumber like :prefixString")
    public List<Voucher> findVouchersByPrefixAndNumber(@Param("fromVoucher") String fromVoucher, @Param("toVoucher") String toVoucher, @Param("prefixString") String prefixString);

    @Query("select distinct v.voucherNumber from Voucher v where v.voucherNumber like :prefixString")
    public List<String> findDistinctVoucherNumbers(@Param("prefixString") String prefixString);

    @Query("select v from Voucher v where v.voucherDate >= :fromDate and v.voucherDate <= :toDate and v.bankAccount > 0 and v.status = 'PARTIAL'")
    public List<Voucher> findBankAccountByDateOrder(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

    @Query("select v from Voucher v where v.voucherDate = :toDate and v.itemAccount =:itemAccount and v.voucherNumber like :prefixString")
    public List<Voucher> findVouchersByItemAccountAndPrefix(@Param("toDate") Date toDate, @Param("itemAccount") Integer account, @Param("prefixString") String prefixString);

    @Query(value = "SELECT * FROM voucher WHERE voucher.account_code = ?1 AND voucher.voucher_id < ?2 AND voucher.voucher_date <= ?3 AND voucher.voucher_number LIKE 'LP%' ORDER BY voucher.voucher_id DESC LIMIT 1", nativeQuery = true)
    Voucher getVoucherForClosing(Integer accountCode, Integer voucherId, Date toDate);
    //////////////////////// Removal for  re insertion in daily reading /////////////////////////

    @Modifying
    @Transactional
    @Query("DELETE FROM Voucher v WHERE v.voucherNumber IN :numberList")
    void removeVouchers(@Param("numberList") List<String> voucherNumberList);

    @Query("SELECT distinct v.voucherNumber from Voucher v WHERE v.accountCode = :account and v.voucherDate = :voucherDate and v.voucherNumber like :prefixString")
    List<String> removalVoucher(@Param("account") Integer accountCode, @Param("voucherDate") Date voucherDate, @Param("prefixString") String prefixString);

    @Modifying
    @Transactional
    @Query("DELETE FROM Voucher v WHERE v.voucherNumber = :voucherNumber ")
    void removeVouchersByNumber(@Param("voucherNumber") String voucherNumber);

    @Modifying
    @Transactional
    @Query("DELETE FROM Voucher v WHERE v.referenceVoucher = :voucherNumber ")
    void removeVouchersByReference(@Param("voucherNumber") String voucherNumber);


    //////// Testing Voucher ExampleMatcher Scenario
    // For Purchase
    Page<Voucher> findByVoucherNumberLikeAndDebitGreaterThan(String prefix, double debit, Pageable pageable);

    // For Sale
    Page<Voucher> findByVoucherNumberLikeAndCreditGreaterThan(String prefix, double credit, Pageable pageable);

    // For Payment
    Page<Voucher> findByVoucherNumberLikeAndDebitGreaterThanAndBankAccountGreaterThan(String prefix, double debit, int bankAccount, Pageable pageable);

    // For Receipt
    Page<Voucher> findByVoucherNumberLikeAndCreditGreaterThanAndBankAccountGreaterThan(String prefix, double credit, int bankAccount, Pageable pageable);

    // For dayEnd Verification
    @Query("SELECT coalesce(sum (v.itemQuantity), 0.0) FROM Voucher v where v.itemAccount =:itemCode and v.voucherDate >:fromDate and v.voucherDate <=:toDate and (v.remarks <>:notEqualTo or v.remarks is null) and v.voucherNumber like 'SL%'")
    Double getTotalSale(@Param("itemCode") Integer itemCode, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("notEqualTo") String notEqualTo);

}
