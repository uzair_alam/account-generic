package com.hellokoding.account.repository;

import com.hellokoding.account.model.TankInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TankInfoRepository extends JpaRepository<TankInfo, Long> {

    TankInfo findByInfoId(int infoId);
}
