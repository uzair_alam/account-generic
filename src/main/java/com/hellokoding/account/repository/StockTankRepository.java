package com.hellokoding.account.repository;

import com.hellokoding.account.model.StockTank;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockTankRepository extends JpaRepository <StockTank,Long> {

    StockTank findByTankId(int tankId);
}
