package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.CostCentre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CostCentreRepository extends JpaRepository<CostCentre, Long> {


    public CostCentre save(CostCentre costCentre);

    public List<CostCentre> findByParentAccount(Account parentAccount);

    @Query("select coalesce(max(a.accountCode), 0) from CostCentre a where  a.parentAccount =:parent ")
    public Long findMaxInnerAccount(@Param("parent") Account parent);
}
