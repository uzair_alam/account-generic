package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.transaction.annotation.Transactional;

public interface StockRepository extends PagingAndSortingRepository<Stock ,Long>, QueryByExampleExecutor<Stock> {

    @Modifying
    @Transactional
    @Query("DELETE FROM Stock s WHERE s.stockAccount = :account")
    void deleteByAccountCode(@Param("account") Account account);

    Stock findByStockId(Integer stockId);

    @Query("select coalesce(min(s.stockId), 0) from Stock s")
     int findMaxId();

}
