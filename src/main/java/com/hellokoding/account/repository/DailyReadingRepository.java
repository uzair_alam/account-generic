package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.DailyReading;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

public interface DailyReadingRepository extends JpaRepository<DailyReading, Long> {


    @Query(value = "SELECT * FROM daily_reading r1 WHERE DATE = (SELECT DATE FROM daily_reading r3 WHERE r1.item_code = r3.item_code AND r3.date < ?1 ORDER BY r3.date DESC LIMIT 1) GROUP BY item_code", nativeQuery = true)
    public List<DailyReading> getItemMap(Date date);

    @Query(value = "SELECT * FROM daily_reading r1 WHERE DATE = (SELECT DATE FROM daily_reading r3 WHERE r1.nozzle_code = r3.nozzle_code AND r3.date < ?1 ORDER BY r3.date DESC LIMIT 1) GROUP BY nozzle_code", nativeQuery = true)
    public List<DailyReading> getNozzleMap(Date date);

    List<DailyReading> findByItemAccountAndDate(Account account, Date date);

    @Query("SELECT max(d.date) FROM DailyReading d")
    Date getMaxDate();

    @Query("SELECT max(d.date) FROM DailyReading d where d.itemAccount=:itemAccount ")
    Date getMaxDateByAccount(@Param("itemAccount") Account itemAccount);


    //////////////////////// Removal for  re insertion in daily reading /////////////////////////

    @Modifying
    @Transactional
    @Query("DELETE FROM DailyReading d WHERE d.itemAccount = :account and d.date = :date ")
    public void removeDailyReading(@Param("account") Account account, @Param("date") Date date);

    @Query("SELECT d FROM DailyReading d WHERE d.itemAccount = :itemAccount and d.date  =:date")
    public DailyReading getLatestReading(@Param("itemAccount") Account itemAccount, @Param("date") Date date);
    @Query("SELECT distinct max(d.date) FROM DailyReading d WHERE d.itemAccount =:itemAccount")
    public Date getMaxDate(@Param("itemAccount") Account itemAccount);

}
