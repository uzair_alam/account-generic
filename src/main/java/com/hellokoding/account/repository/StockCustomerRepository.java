package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Distributor;
import com.hellokoding.account.model.Rider;
import com.hellokoding.account.model.StockCustomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface StockCustomerRepository extends JpaRepository<StockCustomer , Long> {

    @Modifying
    @Transactional
    @Query("DELETE FROM StockCustomer s WHERE s.customerAccount = :account")
    void deleteByAccountCode(@Param("account") Account account);


    StockCustomer findByCustomerId(int id);

    List<StockCustomer> findByDistributorIsNull();

    List<StockCustomer> findByRiderIsNull();

    List<StockCustomer> findByRider(Rider rider);

    List<StockCustomer> findByDistributor(Distributor distributor);
}
