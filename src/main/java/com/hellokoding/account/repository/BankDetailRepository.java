package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.BankDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface  BankDetailRepository extends JpaRepository<BankDetails, Long> {

    @Modifying
    @Transactional
    @Query("DELETE FROM BankDetails b WHERE b.account = :account")
    public void removeDetails(@Param("account") Account account);

    public BankDetails findByAccount(Account account);
    void deleteByAccount(@Param("account") Account account);


    @Query(value = "SELECT EXISTS (SELECT * FROM bank_details WHERE account = ?1 ) ", nativeQuery = true)
    public Integer isBank(Integer account);


}

