package com.hellokoding.account.repository;

import com.hellokoding.account.model.StockCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockCategoryRepository  extends JpaRepository <StockCategory ,Long>{

    StockCategory findByCategoryId(Integer categoryId);
}
