package com.hellokoding.account.reportModel;

import com.hellokoding.account.model.ReportStaging;

import java.util.ArrayList;
import java.util.List;

public class ReportStagingSet {

    private  List<ReportStaging> reportStagingList;

    public List<ReportStaging> getReportStagingList() {
        return reportStagingList;
    }

    public void setReportStagingList(List<ReportStaging> reportStagingList) {
        this.reportStagingList = reportStagingList;
    }


    public ReportStagingSet() {
        this.reportStagingList = new ArrayList<>();
    }



    public void add(ReportStaging reportStaging) {
        this.reportStagingList.add(reportStaging);
    }

}
