package com.hellokoding.account.reportModel;

import com.hellokoding.account.model.Account;

public class TrialBalanceSet {

    private String accountTitle;
    private double debit;
    private double credit;
    private double debitDuringPeriod;
    private double creditDuringPeriod;
    private double debitCalculated;
    private Double saleQuantity;
    private Double itemRate;
    private Account account;


    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Double getSaleQuantity() {
        return saleQuantity;
    }

    public void setSaleQuantity(Double saleQuantity) {
        this.saleQuantity = saleQuantity;
    }

    public Double getItemRate() {
        return itemRate;
    }

    public void setItemRate(Double itemRate) {
        this.itemRate = itemRate;
    }

    public double getDebitCalculated() {
        return debitCalculated;
    }

    public void setDebitCalculated(double debitCalculated) {
        this.debitCalculated = debitCalculated;
    }

    public double getCreditCalculated() {
        return creditCalculated;
    }

    public void setCreditCalculated(double creditCalculated) {
        this.creditCalculated = creditCalculated;
    }

    private double creditCalculated;

    public String getAccountTitle() {
        return accountTitle;
    }

    public void setAccountTitle(String accountTitle) {
        this.accountTitle = accountTitle;
    }

    public double getDebit() {
        return debit;
    }

    public void setDebit(double debit) {
        this.debit = debit;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public double getDebitDuringPeriod() {
        return debitDuringPeriod;
    }

    public void setDebitDuringPeriod(double debitDuringPeriod) {
        this.debitDuringPeriod = debitDuringPeriod;
    }

    public double getCreditDuringPeriod() {
        return creditDuringPeriod;
    }

    public void setCreditDuringPeriod(double creditDuringPeriod) {
        this.creditDuringPeriod = creditDuringPeriod;
    }




}
