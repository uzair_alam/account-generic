package com.hellokoding.account.service.ledgerBill;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.service.account.AccountService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LedgerBillService {

    @Autowired
    private AccountService accountService;
    // Return the voucher with total quantity and amount

    public Voucher getLedgerBill(List<Voucher> voucherList) {
        Voucher finalVoucher = new Voucher();
        List<Voucher> voucherFinalList = new ArrayList<>();
        for (Account account : accountService.getStockAccounts()) {
            Voucher voucher = new Voucher();
            voucher.setItemAccount(account.getAccountCode());
            List<Voucher> voucherFinalItemList = setVoucherDetail(voucher, voucherList);
            for (Voucher voucher2 : voucherFinalItemList) {
                voucherFinalList.add(voucher2);
            }
        }
        finalVoucher.setInternalVoucherList(voucherFinalList);

        return finalVoucher;
    }

    //Return the list for the same item with different rates
    public List<Voucher> setVoucherDetail(Voucher voucher, List<Voucher> voucherList) {
        double rate = 0;

        List<Voucher> sameItemList = new ArrayList<>();
        List<Voucher> finalVoucherList = new ArrayList<>();
        for (Voucher internalVoucher : voucherList) {
            for (Voucher voucherItemAccount : internalVoucher.getInternalVoucherList()) {
                if (voucher.getItemAccount().equals(voucherItemAccount.getItemAccount())) {

                    sameItemList.add(voucherItemAccount);
                }

            }
        }
        List<Double> itemRateList = new ArrayList<>();
        for (Voucher sameItemVoucher : sameItemList) {

            Voucher finalVoucher = new Voucher();
            int count = 0;
            double amount = 0;
            double quantity = 0;
            if (itemRateList.contains(sameItemVoucher.getItemRate())) {
                continue;
            }
            for (Voucher sameItemRate : sameItemList) {

                if (sameItemRate.getItemRate() != null && sameItemVoucher.getItemRate() != null) {
                    if (sameItemRate.getItemRate().equals(sameItemVoucher.getItemRate())) {
                        quantity += sameItemRate.getItemQuantity();
                        amount += sameItemRate.getCredit();
                        count++;
                    }
                }
            }
            finalVoucher.setAccountCode(sameItemVoucher.getItemAccount());
            finalVoucher.setItemRate(sameItemVoucher.getItemRate());
            finalVoucher.setCredit(amount);
            finalVoucher.setItemQuantity(quantity);
            finalVoucherList.add(finalVoucher);
            itemRateList.add(sameItemVoucher.getItemRate());
            if (sameItemList.size() == count) {
                break;
            }

        }

        return finalVoucherList;
    }

    //Return the Montn Number
    public int getMonthInt(String month) {

        int monthNumber = 0;
        switch (month) {
            case "January":
                monthNumber = 1;
                break;
            case "February":
                monthNumber = 2;
                break;
            case "March":
                monthNumber = 3;
                break;
            case "April":
                monthNumber = 4;
                break;
            case "May":
                monthNumber = 5;
                break;
            case "June":
                monthNumber = 6;
                break;
            case "July":
                monthNumber = 7;
                break;
            case "August":
                monthNumber = 8;
                break;
            case "September":
                monthNumber = 9;
                break;
            case "October":
                monthNumber = 10;
                break;
            case "November":
                monthNumber = 11;
                break;
            case "December":
                monthNumber = 12;
                break;
        }
        return monthNumber;
    }

    // Return the Month's Start and End Date
    public Map<String, Date> getMonthDate(String month, int year) throws ParseException {
        String date = "";
        Map<String, Date> dateMap = new HashMap<>();
        int monthNumber = getMonthInt(month);
        date = "1/" + monthNumber + "/" + year;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date firstDay = dateFormat.parse(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(firstDay);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DATE, -1);
        Date lastDay = calendar.getTime();

        dateMap.put("firstDay", firstDay);
        dateMap.put("lastDay", lastDay);
        return dateMap;
    }

    // Return the yearList
    public List<Integer> getYearList() {
        List<Integer> yearList = new ArrayList<>();
        for (int i = 2010; i <= 2050; i++) {
            yearList.add(i);
        }
        return yearList;
    }

}
