package com.hellokoding.account.service.distributor;

import com.hellokoding.account.model.Distributor;
import com.hellokoding.account.repository.DistributorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DistributorServiceImpl implements DistributorService {

    @Autowired
    private DistributorRepository distributorRepository;


    @Override
    public List<Distributor> findAll() {
        return distributorRepository.findAll();
    }

    @Override
    public Distributor findByDistributorId(int id) {
        return distributorRepository.findByDistributorId(id);
    }

    @Override
    public Distributor save(Distributor distributor) {
        return distributorRepository.save(distributor);
    }

    @Override
    public void remove(int distributorId) {
        distributorRepository.deleteByDistributorId(distributorId);
    }
}
