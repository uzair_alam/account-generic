package com.hellokoding.account.service.distributor;

import com.hellokoding.account.model.Distributor;

import java.util.List;

public interface DistributorService {

    List<Distributor> findAll();
    Distributor findByDistributorId(int id);
    Distributor save(Distributor distributor);
    void remove(int distributorId);
}
