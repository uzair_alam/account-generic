package com.hellokoding.account.service.stock;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.StockVendor;
import com.hellokoding.account.repository.StockVendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VendorServiceImpl implements VendorService {


    @Autowired
    private StockVendorRepository stockVendorRepository;

     public  void removeVendor(Account account){
        stockVendorRepository.deleteByAccountCode(account);
    }

     public List<StockVendor> findAll(){
         return stockVendorRepository.findAll();
    }

}
