package com.hellokoding.account.service.stock;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Distributor;
import com.hellokoding.account.model.Rider;
import com.hellokoding.account.model.StockCustomer;
import com.hellokoding.account.repository.StockCustomerRepository;
import com.hellokoding.account.service.calibration.QuantityCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private StockCustomerRepository stockCustomerRepository;


    public void removeCustomer(Account account){
        stockCustomerRepository.deleteByAccountCode(account);
    }

    public List<StockCustomer> findAll(){
        return stockCustomerRepository.findAll();
    }
    public void save(StockCustomer stockCustomer){
        stockCustomerRepository.save(stockCustomer);
    }

    @Override
    public StockCustomer findByCustomerId(int id) {
        return stockCustomerRepository.findByCustomerId(id);
    }

    @Override
    public List<StockCustomer> findAvailableCustomers(boolean isRider) {

        if(isRider){
            return stockCustomerRepository.findByRiderIsNull();
        }
        return stockCustomerRepository.findByDistributorIsNull();
    }

    @Override
    public List<StockCustomer> findCurrentCustomers(Rider rider, Distributor distributor, boolean isRider) {
        if(isRider){
            return stockCustomerRepository.findByRider(rider);
        }
        return stockCustomerRepository.findByDistributor(distributor);
    }
}
