package com.hellokoding.account.service.stock;

import com.hellokoding.account.model.*;
import org.springframework.data.domain.Page;

import java.util.List;

public interface StockService {

    List<StockCategory> getStockCategories();
    List<Stock> findAllStock();
    Stock findByStockId( Integer stockId);
    StockCategory findByCategoryId(Integer categoryId);
    void saveStock(Stock stock);
    void removeStock(Account account);
    void removeVendor(Account account);
    void removeCustomer(Account account);
    void saveVendor(StockVendor stockVendor);
    void saveCustomer(StockCustomer stockCustomer);
    void saveCategory(StockCategory stockCategory);
    int findMaxId();
    Page<Stock> findAllPaged(int pageIndex,int size, String orderColumn, boolean isAsc, String searchString);


}
