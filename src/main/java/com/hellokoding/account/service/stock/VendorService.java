package com.hellokoding.account.service.stock;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.StockVendor;

import java.util.List;

public interface VendorService {

    void removeVendor(Account account);
    List<StockVendor> findAll();
}
