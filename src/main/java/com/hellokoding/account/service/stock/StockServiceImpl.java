package com.hellokoding.account.service.stock;


import com.hellokoding.account.model.*;
import com.hellokoding.account.repository.StockCategoryRepository;
import com.hellokoding.account.repository.StockCustomerRepository;
import com.hellokoding.account.repository.StockRepository;
import com.hellokoding.account.repository.StockVendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StockServiceImpl implements StockService{

    @Autowired
    private StockCategoryRepository stockCategoryRepository;

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private StockVendorRepository stockVendorRepository;

    @Autowired
    private StockCustomerRepository stockCustomerRepository;


    public List<StockCategory> getStockCategories(){

        return stockCategoryRepository.findAll();
    }

    public List<Stock> findAllStock(){
        return (List<Stock>) stockRepository.findAll();
    }

    public Stock findByStockId(Integer stockId){
        return stockRepository.findByStockId(stockId);
    }

    public StockCategory findByCategoryId(Integer categoryId){
        return stockCategoryRepository.findByCategoryId(categoryId);
    }

    public void saveStock(Stock stock){
        stock.setName(stock.getStockAccount().getTitle());
        stockRepository.save(stock);
    }

    public void removeStock(Account account){
        stockRepository.deleteByAccountCode(account);
    }

    public void removeVendor(Account account){
        stockVendorRepository.deleteByAccountCode(account);
    }

    public void removeCustomer(Account account){
        stockCustomerRepository.deleteByAccountCode(account);
    }

    public void saveVendor(StockVendor stockVendor){
        if(stockVendor.getContactPersons() != null) {
            List<StockContactPerson> stockContactPersonList = new ArrayList<>();
            for (String name : stockVendor.getContactPersons().split("\\s*,\\s*")) {
                StockContactPerson stockContactPerson = new StockContactPerson();
                stockContactPerson.setName(name);
                stockContactPerson.setParentVendor(stockVendor);
                stockContactPersonList.add(stockContactPerson);
            }

            stockVendor.setStockContactPersonList(stockContactPersonList);
        }
        stockVendorRepository.save(stockVendor);
    }

    public void saveCustomer(StockCustomer stockCustomer){

        if(stockCustomer.getContactPersons() != null) {
            List<StockContactPerson> stockContactPersonList = new ArrayList<>();
            for (String name : stockCustomer.getContactPersons().split("\\s*,\\s*")) {
                StockContactPerson stockContactPerson = new StockContactPerson();
                stockContactPerson.setName(name);
                stockContactPerson.setParentCustomer(stockCustomer);
                stockContactPersonList.add(stockContactPerson);
            }

            stockCustomer.setStockContactPersonList(stockContactPersonList);
        }
        stockCustomerRepository.save(stockCustomer);
    }

    public void saveCategory(StockCategory stockCategory){
        stockCategoryRepository.save(stockCategory);
    }

    @Override
    public int findMaxId() {
        return stockRepository.findMaxId();
    }


    public Page<Stock> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString) {

        Stock exampleStock = new Stock();
        StockCategory stockCategory = new StockCategory();
        stockCategory.setTitle(searchString);
        exampleStock.setName(searchString);
        exampleStock.setStockCategory(stockCategory);

        //exampleAccount.setAccountCode(Integer.parseInt(searchString));
        ExampleMatcher columnMappingMatcher = ExampleMatcher.matchingAny()
                .withIgnorePaths("sellingPrice","openingQuantity","cost","StockCategory.categoryId")
                .withIgnoreNullValues()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
                .withMatcher("StockCategory.title", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase());

        Example<Stock> stockExample = Example.of(exampleStock, columnMappingMatcher);

        return stockRepository.findAll(stockExample, new PageRequest(pageIndex, size));
    }
}
