package com.hellokoding.account.service.stock;

import com.hellokoding.account.model.*;

import java.util.List;

public interface CustomerService {

    void removeCustomer(Account account);
    List<StockCustomer> findAll();
    void save(StockCustomer stockCustomer);

    /**
     * Find Customer By CustomerID
     * @param id Customer Primary Key
     * @return Customer
     */
    StockCustomer findByCustomerId(int id);

    /**
     * Find Customers  available for assignment to Distributor or Rider
     * @param isRider Whether Rider or Customer
     * @return List Of Available Customers
     */
    List<StockCustomer> findAvailableCustomers(boolean isRider);

    /**
     * Find Current Customer Assignments to Distributor Or Rider
     * @param rider
     * @param distributor
     * @param isRider whether for Rider or Distributor
     * @return List Of Assignments
     */
    List<StockCustomer> findCurrentCustomers(Rider rider, Distributor distributor, boolean isRider);

}
