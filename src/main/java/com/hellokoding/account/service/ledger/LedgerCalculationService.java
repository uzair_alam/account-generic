package com.hellokoding.account.service.ledger;

import com.hellokoding.account.constants.Constants;
import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.repository.AccountRepository;
import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.stock.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class LedgerCalculationService {

    @Autowired
    private AccountService accountService;

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private StockService stockService;

    private static Logger LOGGER = Logger.getLogger("InfoLogging");

    public List<Voucher> getSortedVouchers(int accountCode, Long costCentre, Date fromDate, Date toDate, boolean forBank, boolean forProductLedger) {

        List<Voucher> sortedVoucherList = new ArrayList<>();
        Account account = accountService.findAccount(accountCode);

        Double openingAmount = 0.0;

        openingAmount = account.getOpeningDebit() - account.getOpeningCredit();
        List<Voucher> voucherList = null;
        if (forBank) {
            voucherList = voucherService.findByBankAccountCode(accountCode);
        } else {
            voucherList = voucherService.getVouchersByAccount(accountCode);
        }
        List<Voucher> genericVoucherList;
        if (forProductLedger) {
            genericVoucherList = productLedger(voucherList, account.getAccountStock().getOpeningQuantity());
        } else {
            genericVoucherList = generateLedger(voucherList, openingAmount, forBank);
        }
        for (Voucher voucher : genericVoucherList) {
            if (costCentre != null) {
                if (voucher.getCostCentre() != null) {
                    if (voucher.getCostCentre().equals(costCentre) && validateDate(fromDate, toDate, voucher.getVoucherDate())) {
                        voucher.setVoucherType(voucher.getVoucherNumber().substring(0, 2));

                        sortedVoucherList.add(voucher);
                    }
                }
            } else {

                if (validateDate(fromDate, toDate, voucher.getVoucherDate())) {
                    voucher.setVoucherType(voucher.getVoucherNumber().substring(0, 2));
                    sortedVoucherList.add(voucher);

                }

            }
        }

        return getLedgerVoucher(sortedVoucherList);
    }

    public List<Voucher> generateLedger(List<Voucher> allVouchers, Double openingAmount, Boolean isBank) {

        List<Voucher> generatedListForAll = new ArrayList<>();

        for (Voucher voucher : allVouchers) {

            if (isBank) {
                openingAmount += voucher.getCredit();
                openingAmount -= voucher.getDebit();
            } else {
                openingAmount -= voucher.getCredit();
                openingAmount += voucher.getDebit();
            }

            voucher.setBalance(round(openingAmount, 2));
            generatedListForAll.add(voucher);

        }
        return generatedListForAll;
    }

    private boolean validateDate(Date fromDate, Date toDate, Date voucherDate) {

        if (voucherDate.before(fromDate)) {
            return false;
        }
        if (voucherDate.after(toDate)) {
            return false;
        }

        return true;
    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
//////////////////////////// get ledger vouchers list to show detail on customer ledger page /////////////////////////////

    public List<Voucher> getLedgerVoucher(List<Voucher> voucherList) {
        List<Voucher> masterList = voucherService.findByVoucherNumberIn(voucherList.stream()
                .map(Voucher::getVoucherNumber)
                .collect(Collectors.toList()));
        for (Voucher voucher : voucherList) {
            List<Voucher> internalList = new ArrayList<>();
            for (Voucher internalVoucher : masterList) {
                if (voucher.getDebit() > voucher.getCredit()) {
                    if (voucher.getVoucherNumber().equals(internalVoucher.getVoucherNumber()) && internalVoucher.getCredit() > 0) {
                        internalList.add(internalVoucher);
                    }
                } else {
                    if (voucher.getVoucherNumber().equals(internalVoucher.getVoucherNumber()) && internalVoucher.getDebit() > 0) {
                        internalList.add(internalVoucher);
                    }
                }
            }
                voucher.setInternalVoucherList(internalList);
        }
        return voucherList;

    }

    ///////////////////////////////// generate ledger list for product ledger ///////////////////////////

    public List<Voucher> productLedger(List<Voucher> allVouchers, Double openingQuantity) {

        List<Voucher> generatedListForAll = new ArrayList<>();

        for (Voucher voucher : allVouchers) {
            double creditQuantity = 0.0;
            double debitQuantity = 0.0;
            if (voucher.getCredit() > voucher.getDebit()) {
                creditQuantity += voucher.getItemQuantity();
                openingQuantity -= voucher.getItemQuantity();
            } else if (voucher.getDebit() > voucher.getCredit()) {
                debitQuantity += voucher.getItemQuantity();
                openingQuantity += voucher.getItemQuantity();
            }
            voucher.setDebit(round(debitQuantity, 2));
            voucher.setCredit(round(creditQuantity, 2));
            voucher.setBalance(round(openingQuantity, 2));
            generatedListForAll.add(voucher);

        }
        return generatedListForAll;
    }

}
