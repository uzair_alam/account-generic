package com.hellokoding.account.service.ledger;

import com.hellokoding.account.constants.Constants;
import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.reportModel.TrialBalanceSet;
import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static com.hellokoding.account.service.ledger.LedgerCalculationService.round;

@Service
public class TrialBalanceService {


    @Autowired
    private AccountService accountService;

    @Autowired
    private VoucherService voucherService;


    public List<TrialBalanceSet> getTrailBalanceBasic(int accountLevel, Date toDate, Account accountCode, List<Account> accountListParam) {
        List<Account> accountList = new ArrayList<>();
        if (accountCode != null) {
            accountList.add(accountService.findAccount(accountCode.getAccountCode()));
        }else if(accountListParam != null){
            accountList = accountListParam;
        }
        else {
            accountList = accountService.findAccountsByLevel(accountLevel);
        }
        List<TrialBalanceSet> trialBalanceSetList = new ArrayList<>();
        if (accountLevel != 0 || accountCode != null ) {
            for (Account account : accountList) {
                TrialBalanceSet trialBalanceSet = new TrialBalanceSet();
                trialBalanceSet.setAccountTitle(account.getTitle());

                Double trialAmountDebit = 0.0;
                Double trialAmountCredit = 0.0;
                for (Account accountInternal : getInternalAccountList(account, new ArrayList<>())) {

                    Double openingAmount = accountInternal.getOpeningDebit() - accountInternal.getOpeningCredit();
                    double calculatedAmount = round(getAmount(voucherService.getVouchersByAccount(accountInternal.getAccountCode())
                            , Constants.ACCOUNT_TYPE_CREDIT, openingAmount, null, toDate, 1), 2);
                    if (calculatedAmount > 0) {
                        trialAmountDebit += calculatedAmount;
                    } else {
                        trialAmountCredit += calculatedAmount;
                    }

                }
                trialBalanceSet.setCredit(trialAmountCredit);
                trialBalanceSet.setDebit(trialAmountDebit);

                trialBalanceSetList.add(trialBalanceSet);
            }
        }
        return trialBalanceSetList;
    }


    public List<TrialBalanceSet> getTrailBalanceDetailed(int accountLevel, Date fromDate, Date toDate) {

        List<TrialBalanceSet> trialBalanceSetList = new ArrayList<>();
        for (Account account : accountService.findAccountsByLevel(accountLevel)) {
            TrialBalanceSet trailBalanceSet = new TrialBalanceSet();
            trailBalanceSet.setAccountTitle(account.getTitle());

            Double openingAmount = account.getOpeningDebit() - account.getOpeningCredit();
            Double trialAmount = round(getAmount(voucherService.getVouchersByAccount(account.getAccountCode())
                    , Constants.ACCOUNT_TYPE_CREDIT, openingAmount, null, fromDate, 2), 2);



            if (trialAmount < 0) {
                trailBalanceSet.setCredit(trialAmount);
                trailBalanceSet.setCreditCalculated(round(getAmount(voucherService.getVouchersByAccount(account.getAccountCode())
                        , Constants.ACCOUNT_TYPE_CREDIT, trailBalanceSet.getCredit(), fromDate, toDate, 3), 2));

                if (trailBalanceSet.getCreditCalculated() > trailBalanceSet.getCredit()) {
                    trailBalanceSet.setCreditDuringPeriod(round(trailBalanceSet.getCreditCalculated() - trailBalanceSet.getCredit(), 2));
                } else if (trailBalanceSet.getCreditCalculated() < trailBalanceSet.getCredit()) {
                    trailBalanceSet.setDebitDuringPeriod(round(trailBalanceSet.getCredit() - trailBalanceSet.getCreditCalculated(), 2));
                }

            } else {
                trailBalanceSet.setDebit(trialAmount);
                trailBalanceSet.setDebitCalculated(round(getAmount(voucherService.getVouchersByAccount(account.getAccountCode())
                        , Constants.ACCOUNT_TYPE_DEBIT, trailBalanceSet.getDebit(), fromDate, toDate, 3), 2));

                if (trailBalanceSet.getDebitCalculated() > trailBalanceSet.getDebit()) {
                    trailBalanceSet.setDebitDuringPeriod(round(trailBalanceSet.getDebitCalculated() - trailBalanceSet.getDebit(), 2));
                } else if (trailBalanceSet.getDebitCalculated() < trailBalanceSet.getDebit()) {
                    trailBalanceSet.setCreditDuringPeriod(round(trailBalanceSet.getDebit() - trailBalanceSet.getDebitCalculated(), 2));

                }
            }
            trialBalanceSetList.add(trailBalanceSet);
        }
        return trialBalanceSetList;
    }

    public double getAmount(List<Voucher> allVouchers, String accountType, double openingAmount, Date fromDate, Date toDate, int checkType) {

        for (Voucher voucher : allVouchers) {
            if (validateDate(voucher.getVoucherDate(), fromDate, toDate, checkType)) {

                openingAmount += voucher.getDebit();
                openingAmount -= voucher.getCredit();
            }
        }
        return openingAmount;

    }

    public boolean validateDate(Date voucherDate, Date fromDate, Date toDate, int checkType) {
        if (checkType == 1) {
            if (!voucherDate.after(toDate))
                return true;
        } else if (checkType == 2) {
            if (voucherDate.before(toDate))
                return true;
        } else if (checkType == 3) {
            if (voucherDate.after(fromDate) && !voucherDate.after(toDate))
                return true;

        }
        return false;
    }

    private List<Account> getInternalAccountList(Account account,List<Account> finalList) {


        if(account.getLevel() == 4){
            finalList.add(account);
        }else {
            for (Account accountL1 : account.getChildList()) {
                if (accountL1.getLevel() == 4) {
                    finalList.add(accountL1);
                } else {
                    getInternalAccountList(accountL1, finalList);
                }
            }
        }
        return finalList;
    }

}
