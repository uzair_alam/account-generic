package com.hellokoding.account.service.staticInfo;

import com.hellokoding.account.model.StaticInfo;
import com.hellokoding.account.repository.StaticInfoRepository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sun.awt.image.FileImageSource;
import sun.awt.image.JPEGImageDecoder;

@Service
public class StaticInfoServiceImpl implements StaticInfoService {

    @Autowired
    private StaticInfoRepository staticInfoRepository;

    public StaticInfo save(StaticInfo staticInfo) {
        return staticInfoRepository.save(staticInfo);
    }

    public StaticInfo findStaticInfo() {
        List<StaticInfo> staticInfoList = staticInfoRepository.findAll();
        if (!staticInfoList.isEmpty()) {
                return staticInfoList.get(0);
        } else return null;
    }

    public StaticInfo findByInfoId(int infoId) {
        return staticInfoRepository.findByInfoId(infoId);
    }

    /////////////////////////upload file function////////////////////////////////////
    public void uploadImage(MultipartFile multipartFile) {
        URL resource = getClass().getResource("/");
        String path = resource.getPath();
        path = path.replace("WEB-INF/classes/", "resources/img/") + multipartFile.getOriginalFilename();
      //  path = path.replace("target/classes/", "src/main/webapp/resources/img/").replaceAll("%20", " ") + multipartFile.getOriginalFilename();
        File file = new File(path);
        try {
            JPEGImageDecoder decoder = new JPEGImageDecoder(new FileImageSource(file.getAbsolutePath()), new FileInputStream(file.getAbsolutePath()));
            decoder.produceImage();

        } catch (Exception e) {
            try {
                file.exists();
                file.delete();
                OutputStream os = new FileOutputStream(path);
                byte[] imageByteArray = multipartFile.getBytes();
                os.write(imageByteArray);
                os.flush();
                os.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

}
