package com.hellokoding.account.service.staticInfo;

import com.hellokoding.account.model.StaticInfo;

import java.io.File;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface StaticInfoService {

    public StaticInfo save(StaticInfo staticInfo);
    public StaticInfo findByInfoId(int infoId);
    public StaticInfo findStaticInfo();
    public void uploadImage(MultipartFile multipartFile);

}
