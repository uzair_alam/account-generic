package com.hellokoding.account.service.Voucher;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Voucher;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public interface VoucherService {

    Voucher savePayment(Voucher voucher);

    Voucher saveReceipt(Voucher voucher);

    void saveJournal(Voucher voucher);

    Voucher getVoucher(int voucherId);

    List<Voucher> getVouchersByAccount(int accountCode);

    public List<Voucher> findByVoucherNumber(String voucherNumber);

    public List<Voucher> findByBankAccountCode(int bankAccountCode);

    List<Voucher> getVouchersByBankAccount(int bankAccount, Date date, boolean nullStatus);

    void save(Voucher voucher);

    List<Voucher> findByPrefix(Date fromDate, Date toDate, String prefix);

    List<Voucher> findByVoucherType(String prefix);

    Integer getSecondaryAccount(List<Voucher> voucherList, String voucherNumber);

    List<Voucher> findByPrefixAndVoucher(String fromVoucher, String toVoucher, String prefix);

    List<String> findJournalByPrefix(Date fromDate, Date toDate, String prefix);

    List<String> findJournalVouchersByPrefixAndNumber(String fromVoucher, String toVoucher, String prefixString);

    List<Voucher> findJournalVouchersByPdcBankAccount(Date fromDate, Date toDate, int bankAccount);

    List<Voucher> findBankAccountByDate(Date fromDate, Date toDate);

    List<String> getVoucherNumbers(String prefix);

    public String generateVoucherNumber(String prefix);

    void deleteVouchers(String voucherNumber);

    Voucher getVoucherForClosing(Integer accountCode, Integer voucherId, Date toDate);

    List<Voucher> findByVoucherNumberIn(List<String> voucherNumbers);

    void remove(List<Voucher> voucherList);

    List<Voucher> getVoucherByCurrentDate(Date fromDate, Date toDate, int accountCode);

    Page<Voucher> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString, String prefix);

    Double getTotalSale(Integer itemCode, Date fromDate, Date toDate);

}
