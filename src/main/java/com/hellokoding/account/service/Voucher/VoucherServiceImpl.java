package com.hellokoding.account.service.Voucher;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.repository.BankDetailRepository;
import com.hellokoding.account.repository.VoucherRepository;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class VoucherServiceImpl implements VoucherService {

    @Autowired
    private VoucherRepository voucherRepository;

    @Autowired
    private BankDetailRepository bankDetailRepository;

    public Voucher savePayment(Voucher voucher) {

        // insertion of

        voucher.setIsActive(1);
        voucher.setDebit(LedgerCalculationService.round(voucher.getDebit(), 2));

        List<Voucher> voucherList = voucherRepository.findByVoucherNumber(voucher.getVoucherNumber());
        if (voucherList.size() > 0) {
            voucherRepository.delete(voucherList);
        }

        Voucher baseVoucher = voucherRepository.save(voucher);

        //insertion of Bank Credit voucher
        Voucher bankVoucher = new Voucher();
        bankVoucher.setVoucherNumber(voucher.getVoucherNumber());
        bankVoucher.setVoucherDate(voucher.getVoucherDate());
        bankVoucher.setAccountCode(voucher.getBankAccount());
        bankVoucher.setCredit(LedgerCalculationService.round(voucher.getDebit(), 2));
        bankVoucher.setIsActive(1);
        if (bankDetailRepository.isBank(voucher.getAccountCode()) != 0) {
            bankVoucher.setBankAccount(voucher.getAccountCode());
        }
        voucherRepository.save(bankVoucher);

        // if tax exists
        if (voucher.getTaxAmount() > 0) {
            String innerVoucherNumber = "";
            int maxVoucherID = 0;
            if (voucherRepository.findMaxIdForChild() == null) {
                maxVoucherID++;
                innerVoucherNumber = Integer.toString(maxVoucherID);
            } else {
                maxVoucherID = Integer.parseInt(voucherRepository.findMaxIdForChild().replaceAll("[^0-9]+", ""));
                maxVoucherID++;
                innerVoucherNumber = Integer.toString(maxVoucherID);
            }
            if (maxVoucherID <= 9) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 99) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 9999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 99999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 999999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }

            innerVoucherNumber = "JT" + innerVoucherNumber;

            //insertion of debit tax voucher for Vendor
            Voucher debitVoucher = new Voucher();
            Voucher creditVoucher = new Voucher();

            debitVoucher.setVoucherNumber(innerVoucherNumber);
            creditVoucher.setVoucherNumber(innerVoucherNumber);

            debitVoucher.setVoucherDate(voucher.getVoucherDate());
            debitVoucher.setAccountCode(voucher.getAccountCode());
            debitVoucher.setDebit(LedgerCalculationService.round(voucher.getTaxAmount(), 2));
            debitVoucher.setReferenceVoucher(voucher.getVoucherNumber());
            debitVoucher.setIsActive(1);

            //insertion of credit voucher for Tax Account

            creditVoucher.setVoucherDate(voucher.getVoucherDate());
            creditVoucher.setAccountCode(voucher.getTaxAccount());
            creditVoucher.setCredit(LedgerCalculationService.round(voucher.getTaxAmount(), 2));
            creditVoucher.setReferenceVoucher(voucher.getVoucherNumber());
            creditVoucher.setIsActive(1);

            voucherRepository.save(debitVoucher);
            voucherRepository.save(creditVoucher);

        }
        return baseVoucher;
    }

    public Voucher saveReceipt(Voucher voucher) {

        voucher.setIsActive(1);
        voucher.setCredit(LedgerCalculationService.round(voucher.getCredit(), 2));
        List<Voucher> voucherList = voucherRepository.findByVoucherNumber(voucher.getVoucherNumber());
        if (voucherList.size() > 0) {
            voucherRepository.delete(voucherList);
        }
        Voucher baseVoucher = voucherRepository.save(voucher);

        //insertion of Bank voucher
        Voucher bankVoucher = new Voucher();
        bankVoucher.setVoucherNumber(voucher.getVoucherNumber());
        bankVoucher.setVoucherDate(voucher.getVoucherDate());
        bankVoucher.setAccountCode(voucher.getBankAccount());
        bankVoucher.setDebit(LedgerCalculationService.round(voucher.getCredit(), 2));
        bankVoucher.setIsActive(1);
        if (bankDetailRepository.isBank(voucher.getAccountCode()) != 0) {
            bankVoucher.setBankAccount(voucher.getAccountCode());
        }

        voucherRepository.save(bankVoucher);

        // if tax exists
        if (voucher.getTaxAmount() > 0) {
            String innerVoucherNumber = "";
            int maxVoucherID = 0;
            if (voucherRepository.findMaxIdForChild() == null) {
                maxVoucherID++;
                innerVoucherNumber = Integer.toString(maxVoucherID);
            } else {
                maxVoucherID = Integer.parseInt(voucherRepository.findMaxIdForChild().replaceAll("[^0-9]+", ""));
                maxVoucherID++;
                innerVoucherNumber = Integer.toString(maxVoucherID);
            }
            if (maxVoucherID <= 9) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 99) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 9999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 99999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 999999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }

            innerVoucherNumber = "JT" + innerVoucherNumber;
            Voucher debitVoucher = new Voucher();
            Voucher creditVoucher = new Voucher();

            debitVoucher.setVoucherNumber(innerVoucherNumber);
            creditVoucher.setVoucherNumber(innerVoucherNumber);

            //insertion of debit tax  voucher
            debitVoucher.setVoucherDate(voucher.getVoucherDate());
            debitVoucher.setAccountCode(voucher.getAccountCode());
            debitVoucher.setDebit(LedgerCalculationService.round(voucher.getTaxAmount(), 2));
            debitVoucher.setReferenceVoucher(voucher.getVoucherNumber());
            debitVoucher.setIsActive(1);

            //insertion of credit tax voucher
            creditVoucher.setVoucherDate(voucher.getVoucherDate());
            creditVoucher.setAccountCode(voucher.getTaxAccount());
            creditVoucher.setCredit(LedgerCalculationService.round(voucher.getTaxAmount(), 2));
            creditVoucher.setReferenceVoucher(voucher.getVoucherNumber());
            creditVoucher.setIsActive(1);

            voucherRepository.save(debitVoucher);
            voucherRepository.save(creditVoucher);

        }
        return baseVoucher;
    }

    public void saveJournal(Voucher voucher) {
        voucher.setIsActive(1);
        voucherRepository.save(voucher);
    }

    public String generateVoucherNumber(String prefix) {
        String voucherNumber = "";
        int maxVoucherID = 0;
        if (voucherRepository.findMaxVoucherNumber(prefix + "%") == null) {
            maxVoucherID++;
            voucherNumber = Integer.toString(maxVoucherID);
        } else {
            maxVoucherID = Integer.parseInt(voucherRepository.findMaxVoucherNumber(prefix + "%").replaceAll("[^0-9]+", ""));
            maxVoucherID++;
            voucherNumber = Integer.toString(maxVoucherID);
        }
        if (maxVoucherID <= 9) {
            voucherNumber = "0" + voucherNumber;
        }
        if (maxVoucherID <= 99) {
            voucherNumber = "0" + voucherNumber;
        }
        if (maxVoucherID <= 999) {
            voucherNumber = "0" + voucherNumber;
        }
        if (maxVoucherID <= 9999) {
            voucherNumber = "0" + voucherNumber;
        }
        if (maxVoucherID <= 99999) {
            voucherNumber = "0" + voucherNumber;
        }
        if (maxVoucherID <= 999999) {
            voucherNumber = "0" + voucherNumber;
        }

        voucherNumber = prefix + voucherNumber;

        return voucherNumber;
    }

    public List<Voucher> getVouchersByBankAccount(int bankAccount, Date date, boolean nullStatus) {

        if (nullStatus) {
            return voucherRepository.findPendingVouchers(bankAccount, date);
        } else {
            return voucherRepository.findClearedVouchers(bankAccount, date);
        }
    }

    @Override
    public Voucher getVoucher(int voucherId) {
        return voucherRepository.findByVoucherId(voucherId);
    }

    public List<Voucher> findByVoucherNumber(String voucherNumber) {
        return voucherRepository.findByVoucherNumber(voucherNumber);
    }

    @Override
    public void save(Voucher voucher) {
        voucherRepository.save(voucher);
    }

    public List<Voucher> findByPrefix(Date fromDate, Date toDate, String prefix) {
        return voucherRepository.findVouchersByPrefix(fromDate, toDate, prefix);
    }

    public List<String> findJournalByPrefix(Date fromDate, Date toDate, String prefix) {
        return voucherRepository.findJournalVouchersByPrefix(fromDate, toDate, prefix);
    }

    public List<Voucher> findJournalVouchersByPdcBankAccount(Date fromDate, Date toDate,int bankAccount){
        return voucherRepository.findJournalVouchersByBankAccount(fromDate,toDate,bankAccount);
    }


    public List<String> getVoucherNumbers(String prefix) {
        return voucherRepository.findDistinctVoucherNumbers(prefix);
    }
    public List<Voucher> findBankAccountByDate(Date fromDate, Date toDate) {
        return voucherRepository.findBankAccountByDateOrder(fromDate, toDate);
    }


    public List<Voucher> findByPrefixAndVoucher(String fromVoucher, String toVoucher, String prefix) {
        return voucherRepository.findVouchersByPrefixAndNumber(fromVoucher, toVoucher, prefix);
    }

    public List<Voucher> getVouchersByAccount(int accountCode) {
        return voucherRepository.findByAccountCodeOrderByVoucherDateAsc(accountCode);
    }

    public List<String> findJournalVouchersByPrefixAndNumber(String fromVoucher, String toVoucher, String prefixString) {
        return voucherRepository.findJournalVouchersByPrefixAndNumber(fromVoucher, toVoucher, prefixString);
    }

    public List<Voucher> findByBankAccountCode(int bankAccountCode) {
        return voucherRepository.findByBankAccount(bankAccountCode);
    }

    public List<Voucher> findByVoucherType(String prefix) {
        return voucherRepository.findByVoucherType(prefix);
    }

    public void deleteVouchers(String voucherNumber) {
        voucherRepository.removeVouchersByReference(voucherNumber);
        voucherRepository.removeVouchersByNumber(voucherNumber);
    }

    public Voucher getVoucherForClosing(Integer accountCode, Integer voucherId, Date toDate) {
        return voucherRepository.getVoucherForClosing(accountCode, voucherId, toDate);
    }

    public Integer getSecondaryAccount(List<Voucher> voucherList, String voucherNumber) {
        int accountCode = 0;
        for (Voucher voucher : voucherList) {
            if (voucher.getVoucherNumber().substring(0, 2).equalsIgnoreCase("SL")) {
                if (voucher.getDebit() > voucher.getCredit() && voucher.getVoucherNumber().equals(voucherNumber)) {
                    accountCode = voucher.getAccountCode();
                    break;
                }
            } else if (voucher.getVoucherNumber().substring(0, 2).equalsIgnoreCase("LP")) {
                if (voucher.getDebit() < voucher.getCredit() && voucher.getVoucherNumber().equals(voucherNumber)) {
                    accountCode = voucher.getAccountCode();
                    break;
                }
            }

        }
        return accountCode;

    }

    public List<Voucher> findByVoucherNumberIn(List<String> voucherNumbers) {
        return voucherRepository.findByVoucherNumberIn(voucherNumbers);
    }

    public void remove(List<Voucher> voucherList) {
        voucherRepository.delete(voucherList);
    }

    public List<Voucher> getVoucherByCurrentDate(Date fromDate, Date toDate, int accountCode) {
        return voucherRepository.findVouchersByCurrentDate(fromDate, toDate, accountCode);
    }

    public Page<Voucher> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString, String prefix) {



        if (searchString.startsWith(prefix) || searchString.startsWith(prefix.toLowerCase())) {
            prefix = searchString.toUpperCase();
        }else{
            prefix += searchString;
        }
        Sort sort = null;
        if(isAsc){
            sort = new Sort(Sort.Direction.ASC,orderColumn);
        }else{
            sort = new Sort(Sort.Direction.DESC,orderColumn);
        }

        if(prefix.startsWith("SL")){
            return voucherRepository.findByVoucherNumberLikeAndCreditGreaterThan("%"+ prefix + "%", 0.0, new PageRequest(pageIndex, size,sort));
        }else if(prefix.startsWith("LP")){
            return voucherRepository.findByVoucherNumberLikeAndDebitGreaterThan("%"+ prefix + "%", 0.0, new PageRequest(pageIndex, size,sort));
        }else if(prefix.startsWith("RV")){
            return voucherRepository.findByVoucherNumberLikeAndCreditGreaterThanAndBankAccountGreaterThan("%"+ prefix + "%", 0.0,0, new PageRequest(pageIndex, size,sort));
        }

        return voucherRepository.findByVoucherNumberLikeAndDebitGreaterThanAndBankAccountGreaterThan("%"+ prefix + "%", 0.0,0, new PageRequest(pageIndex, size,sort));

    }

    public Double getTotalSale(Integer itemCode, Date fromDate, Date toDate){
        return voucherRepository.getTotalSale(itemCode, fromDate, toDate,"CALIBRATE");
    }

}
