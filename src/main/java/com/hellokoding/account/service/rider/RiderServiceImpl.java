package com.hellokoding.account.service.rider;

import com.hellokoding.account.model.Rider;
import com.hellokoding.account.repository.RiderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RiderServiceImpl implements RiderService {

    @Autowired
    private RiderRepository riderRepository;

    @Override
    public List<Rider> findAll() {
        return riderRepository.findAll();
    }

    @Override
    public Rider findByRiderId(int riderId) {
        return riderRepository.findByRiderId(riderId);
    }

    @Override
    public Rider save(Rider rider) {
        return riderRepository.save(rider);
    }

    @Override
    public void remove(int riderId) {
        riderRepository.deleteByRiderId(riderId);
    }
}


