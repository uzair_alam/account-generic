package com.hellokoding.account.service.rider;

import com.hellokoding.account.model.Rider;

import java.util.List;

public interface RiderService {

    List<Rider> findAll();
    Rider findByRiderId(int riderId);
    Rider save(Rider rider);
    void remove(int rider);
}
