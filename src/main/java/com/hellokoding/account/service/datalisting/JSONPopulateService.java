package com.hellokoding.account.service.datalisting;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Stock;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.model.jsonentity.JSONDataModel;
import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.account.CostCentreService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import static java.lang.Math.toIntExact;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class JSONPopulateService {


    @Autowired
    private ViewActionBuilder viewActionBuilder;

    @Autowired
    private AccountService accountService;


    @Autowired
    private VoucherService voucherService;

    // Account Listing Generator
    public JSONDataModel populateAccountList(Page<Account> accountPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(accountPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(accountPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        for (Account account : accountPage.getContent()) {
            Account parentAccount = account.getParentAccount();
            String parentAccountTitle = "-";
            if (parentAccount != null) {
                parentAccountTitle = parentAccount.getTitle();
            }
            dataString.add(new String[]{Integer.toString(account.getAccountCode())
                    , df.format(account.getOpeningDate())
                    , account.getTitle()
                    , Double.toString(account.getOpeningDebit() + account.getOpeningCredit())
                    , parentAccountTitle,
                    viewActionBuilder.getAccountAction(Integer.toString(account.getAccountCode()))
            });
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }

    // Stock Listing Generator
    public JSONDataModel populateStockList(Page<Stock> stockPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(stockPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(stockPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();

        for (Stock stock : stockPage.getContent()) {

            dataString.add(new String[]{
                    stock.getName()
                    , stock.getDescription()
                    , stock.getStockCategory().getTitle()
                    , Double.toString(stock.getSellingPrice())
                    , stock.getUnitMeasure()
                    , Double.toString(stock.getOpeningQuantity())
                    , viewActionBuilder.getStockAction(Integer.toString(stock.getStockAccount().getAccountCode()), Integer.toString(stock.getStockId()))

            });
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }

    // Sale Voucher Listing Generator
    public JSONDataModel populateSaleVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        for (Voucher voucher : voucherPage.getContent()) {

            String secondaryAccount = "";
            String itemName = "";
            if (voucher.getCredit() > voucher.getDebit()) {
                Integer secondaryCode = voucherService.getSecondaryAccount(voucherService.findByVoucherType("SL%"), voucher.getVoucherNumber());
                if (secondaryCode != 0)
                    secondaryAccount = accountService.findAccount(secondaryCode).getTitle();
            }
            if(voucher.getItemAccount() > 0){
                itemName = accountService.findAccount(voucher.getItemAccount()).getTitle();
            }
            if (voucher.getCredit() > 0) {
                dataString.add(new String[]{
                        voucher.getVoucherNumber()
                        , df.format(voucher.getVoucherDate())
                        , secondaryAccount
                        , itemName
                        , Double.toString(voucher.getItemQuantity())
                        , Double.toString(voucher.getItemRate())
                        , Double.toString(LedgerCalculationService.round(voucher.getCredit(), 2))
                        , viewActionBuilder.getSaleVoucherAction(voucher.getVoucherNumber(),Integer.toString(voucher.getVoucherId()))

                });
            }
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }


    // Purchase
    // Voucher Listing Generator
    public JSONDataModel populatePurchaseVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        for (Voucher voucher : voucherPage.getContent()) {

            String secondaryAccount = "";
            String itemName = "";
            if (voucher.getCredit() < voucher.getDebit()) {
                Integer secondaryCode = voucherService.getSecondaryAccount(voucherService.findByVoucherType("LP%"), voucher.getVoucherNumber());
                if (secondaryCode != 0)
                    secondaryAccount = accountService.findAccount(secondaryCode).getTitle();
            }
            if(voucher.getItemAccount() > 0){
                itemName = accountService.findAccount(voucher.getItemAccount()).getTitle();
            }
            if (voucher.getDebit() > 0) {
                dataString.add(new String[]{
                        voucher.getVoucherNumber()
                        , df.format(voucher.getVoucherDate())
                        , secondaryAccount
                        , itemName
                        , Double.toString(voucher.getItemQuantity())
                        , Double.toString(voucher.getItemRate())
                        , Double.toString(LedgerCalculationService.round(voucher.getDebit(), 2))
                        , viewActionBuilder.getPurchaseVoucherAction(voucher.getVoucherNumber(),Integer.toString(voucher.getVoucherId()))

                });
            }
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }

    // Payment
    // Voucher Listing Generator
    public JSONDataModel populatePaymentVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        for (Voucher voucher : voucherPage.getContent()) {
            String taxAccount = "";
            String costCentre = "";
            if(voucher.getTaxAccount() != null){
                taxAccount = accountService.findAccount(voucher.getTaxAccount()).getTitle();
            }
            if(voucher.getCostCentre() != null){
                costCentre = Long.toString(voucher.getCostCentre());
            }

                dataString.add(new String[]{
                        voucher.getVoucherNumber() + "</br>" + df.format(voucher.getVoucherDate())
                        , accountService.findAccount(voucher.getBankAccount()).getTitle() +"</br>"+ voucher.getChequeNumber()
                        , accountService.findAccount(voucher.getAccountCode()).getTitle() +"</br>"+ costCentre
                        , taxAccount +"</br>"+ voucher.getRemarks()
                        , Double.toString(voucher.getBillAmount()) + "</br>"+ Double.toString(voucher.getTaxAmount())
                        , Double.toString(LedgerCalculationService.round(voucher.getDebit(),2))
                        , viewActionBuilder.getPaymentVoucherAction(voucher.getVoucherNumber(),Integer.toString(voucher.getVoucherId()))

                });
            }


        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }


    // Receipt
    // Voucher Listing Generator
    public JSONDataModel populateReceiptVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        for (Voucher voucher : voucherPage.getContent()) {
            String taxAccount = "";
            String costCentre = "";
            if(voucher.getTaxAccount() != null){
                taxAccount = accountService.findAccount(voucher.getTaxAccount()).getTitle();
            }
            if(voucher.getCostCentre() != null){
                costCentre = Long.toString(voucher.getCostCentre());
            }

            dataString.add(new String[]{
                    voucher.getVoucherNumber() + "</br>" + df.format(voucher.getVoucherDate())
                    , accountService.findAccount(voucher.getBankAccount()).getTitle() +"</br>"+ voucher.getChequeNumber()
                    , accountService.findAccount(voucher.getAccountCode()).getTitle() +"</br>"+ costCentre
                    , taxAccount +"</br>"+ voucher.getRemarks()
                    , Double.toString(voucher.getBillAmount()) + "</br>"+ Double.toString(voucher.getTaxAmount())
                    , Double.toString(LedgerCalculationService.round(voucher.getCredit(),2))
                    , viewActionBuilder.getReceiptVoucherAction(voucher.getVoucherNumber(),Integer.toString(voucher.getVoucherId()))

            });
        }


        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }



}



