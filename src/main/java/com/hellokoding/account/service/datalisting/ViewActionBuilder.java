package com.hellokoding.account.service.datalisting;



import com.hellokoding.account.service.staticInfo.StaticInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URL;


@Service
public class ViewActionBuilder {



    /**
     * Method to get Project Name for resource mapping.
     * @return Project name String
     */



    public String getProjectName(){

        URL resource = getClass().getResource("/");
        String path = resource.getPath();
        path = path.replace("/target/classes/", "");
        String[] pathList = path.split("/");
        return "/"  + pathList[pathList.length - 3];
        //return "/account";
    }

    /**
     * Account Action List Provider
     * @param accountCode
     * @return HTML
     */
    public  String getAccountAction(String accountCode){



        return "<form method=\"post\" id = \"accountRemove"+accountCode+"\" action=\""+getProjectName()+"/Account/removeAccount\">\n" +

                "                                    <input  type=\"hidden\" name=\"accountCode\" value=\""+accountCode+"\"/>\n" +
                "                                </form>\n" +
                "\n" +
                "                                    <form method=\"post\" id = \"accountEdit"+accountCode+"\" action=\""+getProjectName()+"/Account/editAccount\">\n" +

                "                                        <input  type=\"hidden\" name=\"accountCode\" value=\""+accountCode+"\"/>\n" +
                "                                    </form>\n" +
                "                                    <ul class=\"list-inline\">\n" +
                "                                        <li><a href=\"#\" onclick=\"edit("+accountCode+");\"><img src=\""+getProjectName()+"/resources/img/edit.png\" alt=\"\"></a></li>\n" +
                "                                        <li><a href=\"#\" onclick=\"remove("+accountCode+");\"><img src=\""+getProjectName()+"/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "\n" +
                "                                    </ul>";
    }


    /**
     * Stock Action List Provider
     * @param accountCode
     * @return HTML
     */
    public String getStockAction(String accountCode, String stockId){

        return "                                    <form method=\"post\" id=\"stockRemove"+accountCode+"\" action=\""+getProjectName()+"/Stock/removeStock\" />\n" +
                "                                        <input type=\"hidden\" name=\"accountCode\"\n" +
                "                                               value=\""+accountCode+"\"/>\n" +
                "                                    </form>\n" +
                "                                    <ul class=\"list-inline\">\n" +
                "\n" +
                "                                        <li><a href=\""+getProjectName()+"/Stock/editStock?stockId="+stockId+"\" ><img\n" +
                "                                                src=\""+getProjectName()+"/resources/img/edit.png\" alt=\"\"></a></li>\n" +
                "                                        <li><a href=\"#\" onclick=\"remove("+accountCode+");\"><img\n" +
                "                                                src=\""+getProjectName()+"/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                    </ul>";
    }


    /**
     * Sale Voucher Action List Provider
     * @param voucherNumber
     * @param index
     * @return HTML
     */
    public String getSaleVoucherAction(String voucherNumber,String index){

        return "                                            <form method=\"post\" id=\"voucherRemove"+index+"\" action=\""+getProjectName()+"/Account/deleteVoucher\">\n" +

                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\""+voucherNumber+"\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"Sales/viewSalesVoucher\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove("+index+");\"><img src=\""+getProjectName()+"/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Edit\" href=\""+getProjectName()+"/Sales/updateSaleVoucher?voucherNumber="+voucherNumber+"\"><img src=\""+getProjectName()+"/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Sale Invoice\" href=\""+getProjectName()+"/Sales/printSaleVoucher?voucherNumber="+voucherNumber+"\"><img src=\""+getProjectName()+"/resources/img/print.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
                "                                                <li><a title=\"Delivery Challan\" href=\""+getProjectName()+"/Sales/printDeliveryChallan?voucherNumber="+voucherNumber+"\"><img src=\""+getProjectName()+"/resources/img/print.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
                "                                        </ul>";
    }


    /**
     * Purchase Voucher Action List Provider
     * @param voucherNumber
     * @param index
     * @return HTML
     */
    public String getPurchaseVoucherAction(String voucherNumber,String index){

        return "                                            <form method=\"post\" id=\"voucherRemove"+index+"\" action=\""+getProjectName()+"/Account/deleteVoucher\">\n" +

                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\""+voucherNumber+"\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"Purchase/viewPurchaseVoucher\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove("+index+");\"><img src=\""+getProjectName()+"/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Edit\" href=\""+getProjectName()+"/Purchase/updatePurchaseVoucher?voucherNumber="+voucherNumber+"\"><img src=\""+getProjectName()+"/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Purchase Invoice\" href=\""+getProjectName()+"/Purchase/printPurchaseInvoice?voucherNumber="+voucherNumber+"\"><img src=\""+getProjectName()+"/resources/img/print.png\" alt=\"\" width=\\\"20\" height=\"20\"></a></li>\n" +

                "                                            </ul>";
    }


    /**
     * Payment Voucher Action List Provider
     * @param voucherNumber
     * @param index
     * @return HTML
     */
    public String getPaymentVoucherAction(String voucherNumber, String index){

        return "                                            <form method=\"post\" id=\"voucherRemove"+index+"\" action=\""+getProjectName()+"/Account/deleteVoucher\">\n" +
                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\""+voucherNumber+"\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"Account/createPaymentVoucher\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
                "                                                <li><a title=\"Edit\" href=\"#\" onclick=\"edit("+index+")\" class=\"editForm\"><img src=\""+getProjectName()+"/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove("+index+");\"><img src=\""+getProjectName()+"/resources/img/deleteIcon.png\" alt=\"\"></a>\n" +
                "                                                </li>\n" +
                "                                            </ul>";
    }

    /**
     * Receipt Voucher Action List Provider
     * @param voucherNumber
     * @param index
     * @return HTML
     */
    public String getReceiptVoucherAction(String voucherNumber, String index){

        return "                                            <form method=\"post\" id=\"voucherRemove"+index+"\" action=\""+getProjectName()+"/Account/deleteVoucher\">\n" +
                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\""+voucherNumber+"\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"Account/createReceiptVoucher\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
                "                                                <li><a title=\"Edit\" href=\"#\" onclick=\"edit("+index+")\" class=\"editForm\"><img src=\""+getProjectName()+"/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove("+index+");\"><img src=\""+getProjectName()+"/resources/img/deleteIcon.png\" alt=\"\"></a>\n" +
                "                                                </li>\n" +
                "                                            </ul>";
    }


}
