package com.hellokoding.account.service.statement;

import com.hellokoding.account.constants.Constants;
import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.AccountNote;
import com.hellokoding.account.model.NoteSubHeading;
import com.hellokoding.account.model.ReportStaging;
import com.hellokoding.account.reportModel.ReportStagingSet;
import com.hellokoding.account.reportModel.TrialBalanceSet;
import com.hellokoding.account.repository.ReportStagingRepository;
import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.ledger.TrialBalanceService;
import com.hellokoding.account.service.note.NoteCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

import static com.hellokoding.account.service.ledger.LedgerCalculationService.round;

@Service
public class BalanceCalculationService {

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private TrialBalanceService trialBalanceService;

    @Autowired
    private ReportStagingRepository reportStagingRepository;

    @Autowired
    private NoteCalculationService noteCalculationService;

    public ReportStagingSet getReportSheet(String reportType , Date toDate) {

        ReportStagingSet reportStagingSet  = new ReportStagingSet();
        List<ReportStaging> reportStagingList = reportStagingRepository.findByReportType(reportType);
        Double workingAmount = 0.0;

        for (ReportStaging reportStaging : reportStagingList) {
            Double amountCurrent = 0.0;

            for (AccountNote accountNote : noteCalculationService.getNoteSummary(reportType,toDate,reportStaging.getAccountNoteList(),reportStaging.getWorking())){

            for (NoteSubHeading noteSubHeading : accountNote.getNoteSubHeadings()) {

                if(noteSubHeading.getOperation().equals(Constants.NOTE_OPTION_CREDIT)){
                    amountCurrent += noteSubHeading.getSubheadingTotal();
                }else {
                    amountCurrent -= noteSubHeading.getSubheadingTotal();

                }

            }
        }
        if(!reportStaging.getSubTotal() && !reportStaging.getTotal() && reportStaging.getWorking()){
                workingAmount = amountCurrent;
        }
            if((reportStaging.getSubTotal() || reportStaging.getTotal()) && reportStaging.getWorking()){
                amountCurrent += workingAmount;
            }
        reportStaging.setCurrentAmount(round(amountCurrent,2));
    }

        //for sorting in ascending order of printing Sequence
        if(!reportStagingList.isEmpty()) {
            Collections.sort(reportStagingList, new Comparator<ReportStaging>() {
                @Override
                public int compare(ReportStaging p1, ReportStaging p2) {
                    return p1.getPrintSeq() - p2.getPrintSeq();
                }
            });
            reportStagingSet.setReportStagingList(reportStagingList);
        }

        return reportStagingSet;
    }
}
