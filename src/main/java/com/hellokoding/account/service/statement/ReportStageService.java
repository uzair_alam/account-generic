package com.hellokoding.account.service.statement;

import com.hellokoding.account.model.ReportStaging;

import java.util.List;


public interface ReportStageService {

    List<ReportStaging> findAll();
    ReportStaging findByReportId(Integer reportId);
    void saveTitle(ReportStaging reportStaging);
    void removeReport(ReportStaging reportStaging);

}
