package com.hellokoding.account.service.calibration;
import com.hellokoding.account.model.Stock;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuantityCalculator {

    @Autowired
    private VoucherService voucherService;

    public double getCalculatedQuantity (Stock stock){
       double responseQuantity = stock.getOpeningQuantity();

       for(Voucher voucher :voucherService.getVouchersByAccount(stock.getStockAccount().getAccountCode())){
           if(voucher.getVoucherNumber().substring(0,2).equals("LP")) {
           responseQuantity += voucher.getItemQuantity();
           }
           else if(voucher.getVoucherNumber().substring(0,2).equals("SC")
                   || voucher.getVoucherNumber().substring(0,2).equals("SL")){
               responseQuantity -= voucher.getItemQuantity();
           }
       }

       return LedgerCalculationService.round(responseQuantity,2);
    }
}
