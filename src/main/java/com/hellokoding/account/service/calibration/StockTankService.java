package com.hellokoding.account.service.calibration;

import com.hellokoding.account.model.StockTank;

import java.util.List;

public interface StockTankService {

    StockTank save (StockTank stockTank);
    StockTank findByTankId(int tankId);
    List<StockTank> findAll();
    void remove(StockTank stockTank);

}
