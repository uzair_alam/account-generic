package com.hellokoding.account.service.calibration;

import com.hellokoding.account.model.CalibrationInfo;

import java.util.List;

public interface CalibrationInfoService {

    CalibrationInfo save (CalibrationInfo calibrationInfo);
    CalibrationInfo findByInfoId(int infoId);
    List<CalibrationInfo> findAll();
    void remove(CalibrationInfo calibrationInfo);
    boolean finalizeCalibration(CalibrationInfo calibrationInfo);

}
