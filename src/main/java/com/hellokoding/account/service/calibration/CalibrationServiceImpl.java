package com.hellokoding.account.service.calibration;

import com.hellokoding.account.model.CalibrationInfo;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.repository.CalibrationInfoRepository;
import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import org.apache.poi.ss.formula.functions.Vlookup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class CalibrationServiceImpl implements CalibrationInfoService {


    @Autowired
    private CalibrationInfoRepository calibrationInfoRepository;

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private StaticInfoService staticInfoService;

    public CalibrationInfo save (CalibrationInfo calibrationInfo){
        return calibrationInfoRepository.save(calibrationInfo);
    }
    public CalibrationInfo findByInfoId(int infoId){
        return calibrationInfoRepository.findByInfoId(infoId);
    }

    @Override
    public List<CalibrationInfo> findAll() {
        return calibrationInfoRepository.findAll();
    }

    @Override
    public void remove(CalibrationInfo calibrationInfo) {
        calibrationInfoRepository.delete(calibrationInfo);
    }

    @Override
    public boolean finalizeCalibration(CalibrationInfo calibrationInfo) {


        int adjustmentAccount =  staticInfoService.findStaticInfo().getCashInHand();
        int stockAccountCode = calibrationInfo.getCalibrationStock().getStockAccount().getAccountCode();

        if(calibrationInfo.getActualQuantity() > calibrationInfo.getTanksQuantity()){
            // Creation of Sale voucher if the actual Quantity is less than the Dip reading.
            double variation = calibrationInfo.getActualQuantity() - calibrationInfo.getTanksQuantity();
            Voucher saleVoucher = new Voucher();
            saleVoucher.setVoucherNumber(voucherService.generateVoucherNumber("SL"));
            saleVoucher.setVoucherDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            saleVoucher.setCredit(0.01);
            saleVoucher.setItemQuantity(variation);
            saleVoucher.setItemRate(0.0);
            saleVoucher.setItemAccount(stockAccountCode);
            saleVoucher.setAccountCode(calibrationInfo.getCalibrationStock().getStockAccount().getAccountCode());
            saleVoucher.setRemarks("CALIBRATE");
            changeStatus(calibrationInfo);
            voucherService.saveJournal(saleVoucher);

            //Debit Voucher For Process Adjustment
            Voucher adjustmentVoucher =  new Voucher();
            adjustmentVoucher.setVoucherNumber(saleVoucher.getVoucherNumber());
            adjustmentVoucher.setVoucherDate(saleVoucher.getVoucherDate());
            adjustmentVoucher.setDebit(0.01);
            adjustmentVoucher.setItemQuantity(variation);
            adjustmentVoucher.setItemAccount(stockAccountCode);
            adjustmentVoucher.setAccountCode(adjustmentAccount);
            adjustmentVoucher.setRemarks("CALIBRATE");
            voucherService.saveJournal(adjustmentVoucher);
            return true;

        }else if(calibrationInfo.getActualQuantity() < calibrationInfo.getTanksQuantity()) {
            // Creation of Purchase voucher if the actual Quantity is greater than the Dip reading.
            double variation = calibrationInfo.getTanksQuantity() - calibrationInfo.getActualQuantity();
            Voucher purchaseVoucher = new Voucher();
            purchaseVoucher.setVoucherNumber(voucherService.generateVoucherNumber("LP"));
            purchaseVoucher.setVoucherDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            purchaseVoucher.setDebit( 0.01);
            purchaseVoucher.setItemQuantity(variation);
            purchaseVoucher.setItemRate(0.0);
            purchaseVoucher.setItemAccount(stockAccountCode);
            purchaseVoucher.setAccountCode(calibrationInfo.getCalibrationStock().getStockAccount().getAccountCode());
            purchaseVoucher.setRemarks("CALIBRATE");
            changeStatus(calibrationInfo);
            voucherService.saveJournal(purchaseVoucher);

            //Credit Voucher For Process Adjustment
            Voucher adjustmentVoucher =  new Voucher();
            adjustmentVoucher.setVoucherNumber(purchaseVoucher.getVoucherNumber());
            adjustmentVoucher.setVoucherDate(purchaseVoucher.getVoucherDate());
            adjustmentVoucher.setCredit(0.01);
            adjustmentVoucher.setItemQuantity(variation);
            adjustmentVoucher.setItemAccount(stockAccountCode);
            adjustmentVoucher.setAccountCode(adjustmentAccount);
            adjustmentVoucher.setRemarks("CALIBRATE");
            voucherService.saveJournal(adjustmentVoucher);
            return true;
        }
        return false;
    }

    private void changeStatus(CalibrationInfo calibrationInfo){

        for(CalibrationInfo calibrationInfo1 : calibrationInfoRepository.findByCalibrationStockAndIsCalibratedAndInfoIdLessThanEqual(
                calibrationInfo.getCalibrationStock(),false,calibrationInfo.getInfoId())){

            calibrationInfo1.setCalibrated(true);
            calibrationInfoRepository.save(calibrationInfo1);
        }

    }
}
