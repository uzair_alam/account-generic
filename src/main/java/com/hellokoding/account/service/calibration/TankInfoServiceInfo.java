package com.hellokoding.account.service.calibration;


import com.hellokoding.account.model.TankInfo;
import com.hellokoding.account.repository.TankInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TankInfoServiceInfo implements TankInfoService {

    @Autowired
    private TankInfoRepository tankInfoRepository;

    public TankInfo save (TankInfo tankInfo){
        return tankInfoRepository.save(tankInfo);
    }
    public TankInfo findByInfoId(int infoId){
        return tankInfoRepository.findByInfoId(infoId) ;
    }
}
