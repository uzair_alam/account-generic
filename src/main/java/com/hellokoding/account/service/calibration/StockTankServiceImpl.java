package com.hellokoding.account.service.calibration;

import com.hellokoding.account.model.StockTank;
import com.hellokoding.account.repository.StockTankRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockTankServiceImpl implements StockTankService {

    @Autowired
    private StockTankRepository stockTankRepository;

    public StockTank save (StockTank stockTank){
        return stockTankRepository.save(stockTank);
    }
    public StockTank findByTankId(int tankId){
       return stockTankRepository.findByTankId(tankId);
    }
    public List<StockTank> findAll(){
        return stockTankRepository.findAll();
    }

    @Override
    public void remove(StockTank stockTank) {
         stockTankRepository.delete(stockTank);
    }
}
