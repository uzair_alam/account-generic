package com.hellokoding.account.service.calibration;

import com.hellokoding.account.model.TankInfo;

public interface TankInfoService {

    TankInfo save (TankInfo tankInfo);
    TankInfo findByInfoId(int infoId);
}
