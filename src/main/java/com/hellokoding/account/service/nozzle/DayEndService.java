package com.hellokoding.account.service.nozzle;


import com.hellokoding.account.constants.Constants;
import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.DailyReading;
import com.hellokoding.account.model.Stock;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.model.nozzle.CashCreditRecord;
import com.hellokoding.account.model.nozzle.DayEndCashModel;
import com.hellokoding.account.model.nozzle.NozzleDayEndList;
import com.hellokoding.account.model.nozzle.VerificationModel;
import com.hellokoding.account.repository.DailyReadingRepository;
import com.hellokoding.account.repository.VoucherRepository;
import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DayEndService {


    @Autowired
    private DailyReadingRepository dailyReadingRepository;

    @Autowired
    private VoucherRepository voucherRepository;

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private AccountService accountService;


    public void createDayEndVoucher(Stock stock, Date date) {

        Double voucherDebitAmount = 0.0;
        Double voucherItemQuantity = 0.0;
        List<DailyReading> dailyReadingList = dailyReadingRepository.findByItemAccountAndDate(stock.getStockAccount(), date);
        for (DailyReading dailyReading : dailyReadingList) {
            if (stock.getStockCategory().getTitle().equals("Lubricants")) {
                voucherItemQuantity += dailyReading.getOpeningReading() - dailyReading.getClosingReading() - dailyReading.getTestQuantity();
                voucherDebitAmount += (dailyReading.getOpeningReading() - dailyReading.getClosingReading() - dailyReading.getTestQuantity()) * stock.getSellingPrice();
            } else {
                voucherItemQuantity += dailyReading.getClosingReading() - dailyReading.getOpeningReading() - dailyReading.getTestQuantity();
                voucherDebitAmount += (dailyReading.getClosingReading() - dailyReading.getOpeningReading() - dailyReading.getTestQuantity()) * stock.getSellingPrice();
            }
        }
        for (Voucher voucher : voucherRepository.findVouchersByItemAccountAndPrefix(date, stock.getStockAccount().getAccountCode(), "SL%")) {
            voucherItemQuantity -= voucher.getItemQuantity();
            voucherDebitAmount -= voucher.getCredit();
        }

        // for bank debit voucher
        Voucher bankVoucher = new Voucher();
        bankVoucher.setVoucherNumber(voucherService.generateVoucherNumber("SC"));
        bankVoucher.setAccountCode(Constants.CASH_ACCOUNT);
        bankVoucher.setItemAccount(stock.getStockAccount().getAccountCode());
        bankVoucher.setItemQuantity(LedgerCalculationService.round(voucherItemQuantity,2));
        bankVoucher.setItemRate(LedgerCalculationService.round(stock.getSellingPrice(),2));
        bankVoucher.setDebit(LedgerCalculationService.round(voucherDebitAmount,2));
        bankVoucher.setVoucherDate(date);

        // for item credit voucher
        Voucher itemVoucher = new Voucher();
        itemVoucher.setVoucherNumber(bankVoucher.getVoucherNumber());
        itemVoucher.setAccountCode(stock.getStockAccount().getAccountCode());
        itemVoucher.setItemAccount(stock.getStockAccount().getAccountCode());
        itemVoucher.setItemQuantity(LedgerCalculationService.round(voucherItemQuantity,2));
        itemVoucher.setItemRate(LedgerCalculationService.round(stock.getSellingPrice(),2));
        itemVoucher.setCredit(LedgerCalculationService.round(voucherDebitAmount,2));
        itemVoucher.setVoucherDate(date);

        if((itemVoucher.getCredit() + itemVoucher.getDebit()) != 0.0) {
            voucherService.save(itemVoucher);
            voucherService.save(bankVoucher);
        }

    }


    public Double calculateStockCredit(Stock stock, Date date) {

        Double stockCreditAmount = 0.0;
        for (Voucher voucher : voucherRepository.findVouchersByItemAccountAndPrefix(date, stock.getStockAccount().getAccountCode(), "SL%")) {

            stockCreditAmount += voucher.getCredit();
        }

        return stockCreditAmount;
    }

    public Double calculateStockDebit(Stock stock, Date date) {

        Double stockDebitAmount = 0.0;
        for (Voucher voucher : voucherRepository.findVouchersByItemAccountAndPrefix(date, stock.getStockAccount().getAccountCode(), "SC%")) {

            stockDebitAmount += voucher.getDebit();
        }

        return stockDebitAmount;
    }


    public CashCreditRecord populateQuantity(CashCreditRecord cashCreditRecord, Account itemAccount, Date date) {

        Double saleQuantity = 0.0;
        Double testQuantity = 0.0;

        for (DailyReading dailyReading : dailyReadingRepository.findByItemAccountAndDate(itemAccount, date)) {
            if(itemAccount.getAccountStock().getStockCategory().getTitle().equals("Lubricants")){
                saleQuantity += dailyReading.getOpeningReading() - dailyReading.getClosingReading() - dailyReading.getTestQuantity();
                testQuantity += dailyReading.getTestQuantity();
            }else {
                saleQuantity += dailyReading.getClosingReading() - dailyReading.getOpeningReading() - dailyReading.getTestQuantity();
                testQuantity += dailyReading.getTestQuantity();
            }
        }
        cashCreditRecord.setSaleQuantity(saleQuantity);
        cashCreditRecord.setTestQuantity(testQuantity);

        return cashCreditRecord;
    }


    ///////////////////////////////////// Day End removal ////////////////////////////


    public void removeDayEndDetails(List<DailyReading> dailyReadingList, Date date) {


        for (DailyReading dailyReading : dailyReadingList) {
            List<String> voucherNumbers = voucherRepository.removalVoucher(dailyReading.getItemAccount().getAccountCode(), date, "SC%");
            if(voucherNumbers.size() > 0){
                voucherRepository.removeVouchers(voucherNumbers);
            }

            dailyReadingRepository.removeDailyReading(dailyReading.getItemAccount(), date);
        }

    }


    public DayEndCashModel populateCashModel(Date date) {

        Account account = accountService.findAccount(Constants.CASH_ACCOUNT);
        Double openingCash = account.getOpeningDebit() - account.getOpeningCredit();
        List<Voucher> voucherList = voucherRepository.findByAccountCodeAndVoucherDateLessThanEqual(Constants.CASH_ACCOUNT, date);
        DayEndCashModel dayEndCashModel = new DayEndCashModel();
        ////// for opening Amount  ///////
        for (Voucher voucher : voucherList) {
            if (!voucher.getVoucherDate().equals(date)) {
                openingCash -= voucher.getCredit();
                openingCash += voucher.getDebit();
            }
        }
        dayEndCashModel.setOpeningCash(LedgerCalculationService.round(openingCash,2));
        ////////// for Receipt ///////////
        Double totalReceipt = 0.0;
        Double withdrawalAmount = 0.0;
        for (Voucher voucher : voucherList) {
            if (voucher.getVoucherNumber().substring(0, 2).equals("RV")) {
                if (voucher.getVoucherDate().equals(date)) {
                    totalReceipt += voucher.getDebit();
                    if(voucher.getBankAccount() != 0){
                        withdrawalAmount += voucher.getDebit();
                    }

                }


            }

        }

        dayEndCashModel.setReceiptAmount(LedgerCalculationService.round(totalReceipt - withdrawalAmount,2));
        dayEndCashModel.setWithdrawal(LedgerCalculationService.round(withdrawalAmount,2));
        ////////// for Payment ///////////
        Double totalPayment = 0.0;
        Double paymentDeposit = 0.0;
        for (Voucher voucher : voucherList) {
            if (voucher.getVoucherNumber().substring(0, 2).equals("PV")) {
                if (voucher.getVoucherDate().equals(date)) {
                    totalPayment += voucher.getCredit();

                    if(voucher.getBankAccount() != 0){
                        paymentDeposit += voucher.getCredit();
                    }

                }

            }
        }
        dayEndCashModel.setCashPayment(LedgerCalculationService.round(totalPayment - paymentDeposit,2));
        dayEndCashModel.setPaymentDeposit(LedgerCalculationService.round(paymentDeposit,2));


        return dayEndCashModel;
    }


    public Date getMaxDate(){
        return dailyReadingRepository.getMaxDate();
    }


    /**
     * Verification of Quantities of Stock added in Day End Report
     * @param nozzleDayEndList
     * @return statusFlag
     */
    public VerificationModel verifyDayEnd(NozzleDayEndList nozzleDayEndList){
        VerificationModel verificationModel = new VerificationModel();
        verificationModel.setValid(true);
        String message = "Failed to insert Daily Reading. Please note:";

        Map<Integer,Double> nozzleStockMap = new HashMap<>();
        Map<Integer,Double> stockMap = new HashMap<>();
        // For Product with nozzle Registry
        for(DailyReading dailyReading : nozzleDayEndList.getDailyNozzleReadingList()){
            int accountCode = dailyReading.getItemAccount().getAccountCode();
            if(!nozzleStockMap.containsKey(accountCode)){
                nozzleStockMap.put(accountCode, dailyReading.getClosingReading() - dailyReading.getOpeningReading());
            }else{
                nozzleStockMap.put(accountCode, nozzleStockMap.get(accountCode) + (dailyReading.getClosingReading() - dailyReading.getOpeningReading()));
            }
        }
        // For Lubricants
        for(DailyReading dailyReading : nozzleDayEndList.getDailyItemReadingList()){
            int accountCode = dailyReading.getItemAccount().getAccountCode();
            if(!stockMap.containsKey(accountCode)){
                stockMap.put(accountCode, dailyReading.getOpeningReading() - dailyReading.getClosingReading());
            }else{
                stockMap.put(accountCode, stockMap.get(accountCode) + ( dailyReading.getOpeningReading() - dailyReading.getClosingReading() ));
            }
        }

        for(Map.Entry<Integer,Double> entry : nozzleStockMap.entrySet()){
            Account itemAccount = accountService.findAccount(entry.getKey());
            double saleQuantity = voucherService.getTotalSale(entry.getKey(),dailyReadingRepository.getMaxDateByAccount(itemAccount),nozzleDayEndList.getDate());
             if(saleQuantity > entry.getValue()){
                verificationModel.setValid(false);
                message += "</br> Nozzle Sale: <strong>" + entry.getValue() + "</strong> Unit(s) For <strong>"+ itemAccount.getTitle() + "</strong> cannot be less than Actual sale : <strong>"+ saleQuantity +"</strong> Unit(s). ";
            }
        }
        for(Map.Entry<Integer,Double> entry : stockMap.entrySet()){
            Account itemAccount = accountService.findAccount(entry.getKey());
            double saleQuantity = voucherService.getTotalSale(entry.getKey(),dailyReadingRepository.getMaxDateByAccount(itemAccount),nozzleDayEndList.getDate());
            if(saleQuantity > entry.getValue()){
                verificationModel.setValid(false);
                message += "</br> Item Sale: <strong>" + entry.getValue() + "</strong> Unit(s) For <strong>"+ itemAccount.getTitle() + "</strong> does not match actual Sale Quantity : <strong>"+ saleQuantity +"</strong> Unit(s). ";
            }
        }

        verificationModel.setErrorMessage(message);
        return verificationModel;
    }


}
