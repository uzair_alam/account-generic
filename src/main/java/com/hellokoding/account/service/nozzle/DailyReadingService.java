/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.service.nozzle;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.DailyReading;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.repository.DailyReadingRepository;
import com.hellokoding.account.service.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;

/**
 * @author hashim
 */
@Service
public class DailyReadingService {

    @Autowired
    private NozzleService nozzleService;
    @Autowired
    private AccountService accountService;

    @Autowired
    private DailyReadingRepository dailyReadingRepository;

    public void updateDailyReading(Voucher voucher) {

        DailyReading newDailyReading = new DailyReading();
        /*java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());*/
        Account account = accountService.findAccount(voucher.getAccountCode());

        if (account.getAccountStock().getStockCategory().getTitle().equalsIgnoreCase("Lubricants")) {

            DailyReading dailyReading = nozzleService.getLatestReading(account);
            if (dailyReading == null) {
                newDailyReading.setClosingReading(account.getAccountStock().getOpeningQuantity() + voucher.getItemQuantity());
                newDailyReading.setOpeningReading(account.getAccountStock().getOpeningQuantity());
                newDailyReading.setItemAccount(account);
                newDailyReading.setDate(dailyReadingRepository.getMaxDate());
                nozzleService.saveReading(newDailyReading);
            } else {
                dailyReading.setClosingReading(dailyReading.getClosingReading() + voucher.getItemQuantity());
                nozzleService.saveReading(dailyReading);
            }

        }
    }
}
