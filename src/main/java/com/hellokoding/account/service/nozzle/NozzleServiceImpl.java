package com.hellokoding.account.service.nozzle;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.DailyReading;
import com.hellokoding.account.model.NozzleMaster;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.repository.DailyReadingRepository;
import com.hellokoding.account.repository.NozzleMasterRepository;
import com.hellokoding.account.service.Voucher.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NozzleServiceImpl implements NozzleService {

    @Autowired
    private NozzleMasterRepository nozzleMasterRepository;

    @Autowired
    private DailyReadingRepository dailyReadingRepository;


    public void saveNozzle(NozzleMaster nozzleMaster) {
        nozzleMasterRepository.save(nozzleMaster);
    }

    public void saveReading(DailyReading dailyReading) {
        dailyReadingRepository.save(dailyReading);
    }

    public int getMaxSortOrder() {
        return nozzleMasterRepository.getMaxOrder();
    }

    public boolean orderExists(Integer sortOrder) {
        if (nozzleMasterRepository.findBySortOrder(sortOrder).size() > 0) {
            return true;
        }
        return false;
    }

    public List<NozzleMaster> findAllNozzles() {
        Sort sort = new Sort(Sort.Direction.ASC, "sortOrder");
        return nozzleMasterRepository.findAll(sort);
    }

    public NozzleMaster findByNozzleCode(Integer nozzleCode) {
        return nozzleMasterRepository.findByNozzleCode(nozzleCode);
    }

    public void removeNozzleByCode(Integer nozzleCode) {
        nozzleMasterRepository.removeNozzle(nozzleCode);
    }

    public Map<Integer, Double> getNozzleMap(Date date) {
        Map<Integer, Double> nMap = new HashMap<>();
        for (DailyReading dailyReading : dailyReadingRepository.getNozzleMap(date)) {
            nMap.put(dailyReading.getNozzle().getNozzleCode(), dailyReading.getClosingReading());
        }
        return nMap;
    }

    public Map<Integer, Double> getItemMap(Date date) {
        Map<Integer, Double> iMap = new HashMap<>();
        for (DailyReading dailyReading : dailyReadingRepository.getItemMap(date)) {
            iMap.put(dailyReading.getItemAccount().getAccountCode(), dailyReading.getClosingReading());
        }
        return iMap;
    }

    public DailyReading getLatestReading(Account itemCode) {
        Date date = dailyReadingRepository.getMaxDate(itemCode);
        return dailyReadingRepository.getLatestReading(itemCode, date);
    }

}
