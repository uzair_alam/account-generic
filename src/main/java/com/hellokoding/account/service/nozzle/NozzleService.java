package com.hellokoding.account.service.nozzle;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.DailyReading;
import com.hellokoding.account.model.NozzleMaster;

import java.sql.Date;
import java.util.List;
import java.util.Map;

public interface NozzleService {

    void saveNozzle(NozzleMaster nozzleMaster);
    void saveReading(DailyReading dailyReading);
     List<NozzleMaster> findAllNozzles();
     Map<Integer,Double> getItemMap(Date date);
     Map<Integer,Double> getNozzleMap(Date date);
     NozzleMaster findByNozzleCode(Integer nozzleCode);
     void removeNozzleByCode(Integer nozzleCode);
     int getMaxSortOrder();
     boolean orderExists(Integer sortOrder);
     DailyReading getLatestReading( Account itemCode); 
}
