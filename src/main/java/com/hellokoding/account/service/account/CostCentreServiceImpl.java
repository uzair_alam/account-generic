package com.hellokoding.account.service.account;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.CostCentre;
import com.hellokoding.account.repository.AccountRepository;
import com.hellokoding.account.repository.CostCentreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CostCentreServiceImpl implements CostCentreService{

    @Autowired
    private CostCentreRepository costCentreRepository;

    @Autowired
    private AccountRepository accountRepository;

    public CostCentre save(CostCentre costCentre){

        Account parentAccount = accountRepository.findByAccountCode(costCentre.getParentAccountCode());
        long maxAccountCode = costCentreRepository.findMaxInnerAccount(parentAccount);
        if(maxAccountCode > 0){
            maxAccountCode++;
            costCentre.setAccountCode(maxAccountCode);
            costCentre.setParentAccount(parentAccount);
            costCentre.setIsActive(1);
            return costCentreRepository.save(costCentre);

        }else{
            String accountCode = Integer.toString(parentAccount.getAccountCode()) + "0001";
            costCentre.setParentAccount(parentAccount);
            costCentre.setAccountCode(Long.parseLong(accountCode));
            costCentre.setIsActive(1);
            return costCentreRepository.save(costCentre);
        }

    }

    public List<CostCentre> findAll(){

        return costCentreRepository.findAll();
    }

    public List<CostCentre> getCostCentres(Integer accountCode){


        return costCentreRepository.findByParentAccount(accountRepository.findByAccountCode(accountCode));
    }
}
