package com.hellokoding.account.service.account;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.AuthorizedPerson;
import com.hellokoding.account.model.BankDetails;
import com.hellokoding.account.repository.AccountRepository;
import com.hellokoding.account.repository.AuthorizedPersonRepository;
import com.hellokoding.account.repository.BankDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class BankDetailServiceImpl implements BankDetailService {


    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private BankDetailRepository bankDetailRepository;
    @Autowired
    private AuthorizedPersonRepository authorizedPersonRepository;




    public void save(BankDetails bankDetails) {
//        Account account = accountRepository.findByAccountCode(bankDetails.getAccountCode());
//        bankDetailRepository.removeDetails(account);
//
//        bankDetails.setIsActive(1);
//        bankDetails.setAccount(account);
        BankDetails bankDetails1 = bankDetailRepository.save(bankDetails);
        List<String> items = Arrays.asList(bankDetails.getAuthorizedPersons().split("\\s*,\\s*"));
        for (String name : items) {
            AuthorizedPerson authorizedPerson = new AuthorizedPerson();
            authorizedPerson.setName(name);
            authorizedPerson.setIsActive(1);
            authorizedPerson.setBankDetails(bankDetails1);
            authorizedPersonRepository.save(authorizedPerson);

        }

    }

    public BankDetails findBankDetail(Account account){

        return bankDetailRepository.findByAccount(account);
    }

    public List<BankDetails> findAll(){
        return bankDetailRepository.findAll();
    }
    public void delete(Account account) {
        bankDetailRepository.deleteByAccount(account);
    }

}
