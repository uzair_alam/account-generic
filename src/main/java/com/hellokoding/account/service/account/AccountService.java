package com.hellokoding.account.service.account;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.BankDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AccountService {

    Account save(Account account);
    void update(Account account);
    void remove(Account account);
    List<Account> getAccountNames(List<Integer> accountCodeList);
    List<Account> findAll( );
    List<Account> findAccountForRider(int riderId);
    List<Account> findWithCostCentres();
    List<Account> findAllFiltered();
    List<Account> findAccountsByLevel(int level);
    List<Account> getDetailAccounts();
    List<Account> getStockAccounts();
    List<Account> getCustomerAccounts();
    List<Account> getVendorAccounts();
    List<Account> getAccounts();
    List<Account> getTaxAccounts();
    Account findAccount(int accountCode);
    boolean accountDeletable(Account account);
    void delete(Integer accountCode);
    List<String> getInfo(Account account, boolean check);

    Page<Account> findAllPaged(int pageIndex,int size, String orderColumn, boolean isAsc, String searchString);


}
