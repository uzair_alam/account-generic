package com.hellokoding.account.service.account;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.ExpenseDetail;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ExpenseDetailService {
    List<ExpenseDetail> findAll();
    void delete(Account account);
    void save(ExpenseDetail expenseDetail);
}

