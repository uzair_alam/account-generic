package com.hellokoding.account.service.account;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.BankDetails;
import com.itextpdf.text.pdf.parser.LineSegment;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface BankDetailService {

    public void save (BankDetails bankDetails);
    public BankDetails findBankDetail(Account account);
    public List<BankDetails> findAll();
    void delete(Account account);
}
