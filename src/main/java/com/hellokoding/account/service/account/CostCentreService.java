package com.hellokoding.account.service.account;

import com.hellokoding.account.model.CostCentre;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Service
public interface CostCentreService {

    public CostCentre save(CostCentre costCentre);
    List<CostCentre> getCostCentres(Integer accountCode);
    public List<CostCentre> findAll();
}
