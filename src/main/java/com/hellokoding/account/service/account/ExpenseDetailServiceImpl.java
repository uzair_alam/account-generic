package com.hellokoding.account.service.account;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.ExpenseDetail;
import com.hellokoding.account.repository.ExpenseDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpenseDetailServiceImpl implements ExpenseDetailService {
    @Autowired
    private ExpenseDetailRepository expenseDetailRepository;

    public void delete(Account account){
        expenseDetailRepository.deleteByAccountCode(account);
    }
    public List<ExpenseDetail> findAll(){
        return expenseDetailRepository.findAll();
    }
    public void save(ExpenseDetail expenseDetail){
        expenseDetailRepository.save(expenseDetail);
    }
}
