package com.hellokoding.account.service.account;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.StockCustomer;
import com.hellokoding.account.model.StockVendor;
import com.hellokoding.account.repository.AccountRepository;
import com.hellokoding.account.repository.TaxDetailRepository;
import com.hellokoding.account.repository.VoucherRepository;
import com.hellokoding.account.service.stock.CustomerService;
import com.hellokoding.account.service.stock.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TaxDetailRepository taxDetailRepository;

    @Autowired
    private VoucherRepository voucherRepository;

    @Autowired
    private VendorService vendorService;

    private CustomerService customerService;

    public Account save(Account account) {

        if (account.getParentCode() == null) {
            int maxAccountCode = accountRepository.findMaxControlAccount();
            if (maxAccountCode > 0) {
                String currentMax = "";
                currentMax += Integer.toString(maxAccountCode).charAt(0);
                currentMax += Integer.toString(maxAccountCode).charAt(1);
                int max = Integer.parseInt(currentMax);
                if (max <= 99) {
                    max++;
                    currentMax = Integer.toString(max);
                    currentMax += "00000000";
                    account.setAccountCode(Integer.parseInt(currentMax));
                    account.setLevel(1);
                    account.setIsActive(1);
                    return accountRepository.save(account);
                }
            } else {
                account.setAccountCode(1000000000);
                account.setIsActive(1);
                account.setLevel(1);
                return accountRepository.save(account);
            }
        } else {
            if (Integer.toString(account.getParentCode()).charAt(2) == '0' && Integer.toString(account.getParentCode()).charAt(3) == '0') {
                String currentMax = "";
                int maxAccountCode = accountRepository.findMaxInnerAccount(accountRepository.findByAccountCode(account.getParentCode()));
                if (maxAccountCode > 0) {
                    currentMax = Integer.toString(maxAccountCode).substring(2, 4);
                    int max = Integer.parseInt(currentMax);
                    if (max <= 99) {
                        max++;
                        currentMax = "";
                        if (max <= 9) {
                            currentMax += "0";
                        }
                        currentMax += Integer.toString(max);
                        String parentCode = Integer.toString(account.getParentCode());
                        String accountCode = parentCode.substring(0, 2) + currentMax.charAt(0) + currentMax.charAt(1) + parentCode.substring(4);
                        account.setAccountCode(Integer.parseInt(accountCode));
                        account.setIsActive(1);
                        account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                        account.setLevel(2);
                        return accountRepository.save(account);
                    }
                } else {
                    String parentCode = Integer.toString(account.getParentCode());
                    String accountCode = parentCode.substring(0, 2) + "01000000";
                    account.setAccountCode(Integer.parseInt(accountCode));
                    account.setIsActive(1);
                    account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                    account.setLevel(2);
                    return accountRepository.save(account);
                }

            } else if (Integer.toString(account.getParentCode()).charAt(4) == '0' && Integer.toString(account.getParentCode()).charAt(5) == '0') {
                String currentMax = "";
                int maxAccountCode = accountRepository.findMaxInnerAccount(accountRepository.findByAccountCode(account.getParentCode()));
                if (maxAccountCode > 0) {
                    currentMax = Integer.toString(maxAccountCode).substring(4, 6);
                    int max = Integer.parseInt(currentMax);
                    if (max <= 99) {
                        max++;
                        currentMax = "";
                        if (max <= 9) {
                            currentMax += "0";
                        }
                        currentMax += Integer.toString(max);
                        String parentCode = Integer.toString(account.getParentCode());
                        String accountCode = parentCode.substring(0, 4) + currentMax.charAt(0) + currentMax.charAt(1) + parentCode.substring(6);
                        account.setAccountCode(Integer.parseInt(accountCode));
                        account.setIsActive(1);
                        account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                        account.setLevel(3);
                        return accountRepository.save(account);
                    }
                } else {
                    String parentCode = Integer.toString(account.getParentCode());
                    String accountCode = parentCode.substring(0, 4) + "010000";
                    account.setAccountCode(Integer.parseInt(accountCode));
                    account.setIsActive(1);
                    account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                    account.setLevel(3);
                    return accountRepository.save(account);
                }
            } else {
                String currentMax = "";
                int maxAccountCode = accountRepository.findMaxInnerAccount(accountRepository.findByAccountCode(account.getParentCode()));
                if (maxAccountCode > 0) {
                    currentMax = Integer.toString(maxAccountCode).substring(6);
                    int max = Integer.parseInt(currentMax);
                    if (max <= 9999) {
                        max++;
                        currentMax = "";
                        if (max <= 999) {
                            currentMax += "0";
                        }
                        if (max <= 99) {
                            currentMax += "0";
                        }

                        if (max <= 9) {
                            currentMax += "0";
                        }

                        currentMax += Integer.toString(max);
                        String parentCode = Integer.toString(account.getParentCode());
                        String accountCode = parentCode.substring(0, 6) + currentMax;
                        account.setAccountCode(Integer.parseInt(accountCode));
                        account.setIsActive(1);
                        account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                        account.setLevel(4);
                        return accountRepository.save(account);
                    }
                } else {
                    String parentCode = Integer.toString(account.getParentCode());
                    String accountCode = parentCode.substring(0, 6) + "0001";
                    account.setAccountCode(Integer.parseInt(accountCode));
                    account.setIsActive(1);
                    account.setLevel(4);
                    account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                    return accountRepository.save(account);
                }
            }


        }
        return null;

        //accountRepository.save(account);
    }

    public void update(Account account) {
        accountRepository.save(account);
    }

    public void remove(Account account) {
        accountRepository.delete(account);
    }

    public List<Account> findAll() {
        return accountRepository.findByIsActive(1);
    }

    @Override
    public List<Account> findAccountForRider(int riderId) {
        return accountRepository.findAccountForRider(riderId);
    }

    public List<Account> getDetailAccounts() {

        return accountRepository.findByBankDetailsNotNull();
    }

    public List<Account> getStockAccounts() {
        return accountRepository.findByAccountStockNotNull();
    }

    public List<Account> getCustomerAccounts() {
        return accountRepository.findByStockCustomerNotNull();
    }

    public List<Account> getVendorAccounts() {
        return accountRepository.findByStockVendorNotNull();
    }

    public List<Account> getAccounts() {

        return accountRepository.findByBankDetails(null);
    }

    public List<Account> getTaxAccounts() {
        List<Account> accountList = new ArrayList<>();
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        for (Account account : accountRepository.findDistinctByTaxDetailListNotNull()) {
            if (taxDetailRepository.findFirstByBaseAccountAndEffectiveTillGreaterThanEqual(account, date) != null) {
                accountList.add(account);
            }
        }
        return accountList;
    }

    public Account findAccount(int accountCode) {
        return accountRepository.findByAccountCode(accountCode);
    }

    public List<Account> getAccountNames(List<Integer> accountCodeList) {
        return accountRepository.getAccountNames(accountCodeList);
    }

    public List<Account> findWithCostCentres() {
        return accountRepository.findDistinctByCostCentresNotNull();
    }



    public List<Account> findAllFiltered() {
        List<Account> accountList = new ArrayList<>();
        for (Account account : accountRepository.findAll()) {
            String accountCode = Integer.toString(account.getAccountCode());
            if (accountCode.substring(accountCode.length() - 4).equals("0000")) {
                accountList.add(account);
            }
        }
        return accountList;
    }


    public List<Account> findAccountsByLevel(int level) {
        return accountRepository.findAccountByLevel(level);
    }

    public boolean accountDeletable(Account account) {

        return account.getChildList().size() < 1
                && account.getNoteSubHeadings().size() < 1
                && account.getStockCustomer() == null
                && account.getStockVendor() == null
                && account.getAccountStock() == null
                && voucherRepository.findByAccountCodeOrderByVoucherDateAsc(account.getAccountCode()).size() < 1;
    }


    public void delete(Integer accountCode) {
        accountRepository.deleteAccount(accountCode);
    }

    public List<String> getInfo(Account account, boolean check) {
        List<String> infolist = new ArrayList<>();
        if (check) {
            StockVendor stockVendor = account.getStockVendor();
            infolist.add((account.getTitle()));
            infolist.add(stockVendor.getAddress());
            infolist.add(stockVendor.getMobile());
            infolist.add(stockVendor.getCnic());
            infolist.add(stockVendor.getLandline());
            infolist.add(account.getTotalBalanceAmount().toString());
            infolist.add(account.getAmountLimit().toString());
        } else {
            StockCustomer stockCustomer = account.getStockCustomer();
            infolist.add(account.getTitle());
            infolist.add(stockCustomer.getAddress());
            infolist.add(stockCustomer.getMobile());
            infolist.add(stockCustomer.getCnic());
            infolist.add(stockCustomer.getLandline());
            infolist.add(account.getTotalBalanceAmount().toString());
            infolist.add(account.getAmountLimit().toString());
        }
        return infolist;
    }

    public Page<Account> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString) {

        Account exampleAccount = new Account();
        exampleAccount.setTitle(searchString);
        exampleAccount.setAccountCode(null);
        //exampleAccount.setAccountCode(Integer.parseInt(searchString));
        ExampleMatcher columnMappingMatcher = ExampleMatcher.matchingAny()
                .withIgnorePaths("accountCode","isActive","level","openingCredit","lastOpeningCredit","lastOpeningDebit","openingDebit")
                .withIgnoreNullValues()
                .withMatcher("title", ExampleMatcher.GenericPropertyMatchers.startsWith().ignoreCase());

        Example<Account> accountExample = Example.of(exampleAccount, columnMappingMatcher);

        return accountRepository.findAll(accountExample, new PageRequest(pageIndex, size, Sort.Direction.DESC,"OpeningDate"));
    }

}
