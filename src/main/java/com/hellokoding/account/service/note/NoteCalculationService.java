package com.hellokoding.account.service.note;

import com.hellokoding.account.constants.Constants;
import com.hellokoding.account.model.*;
import com.hellokoding.account.reportModel.TrialBalanceSet;
import com.hellokoding.account.repository.StockRepository;
import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.ledger.TrialBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static com.hellokoding.account.service.ledger.LedgerCalculationService.round;

@Service
public class NoteCalculationService {


    @Autowired
    private NoteService noteService;


    @Autowired
    private VoucherService voucherService;

    @Autowired
    private TrialBalanceService trialBalanceService;

    @Autowired
    private StockRepository stockRepository;



    public List<AccountNote> getNoteSummary(String reportType, Date toDate, List<AccountNote> accountNotes, Boolean isWorking) {

        /*List<AccountNote> accountNotes = ;*/
        for (AccountNote accountNote : accountNotes) {
            for (NoteSubHeading noteSubHeading : accountNote.getNoteSubHeadings()) {

                Double subheadingTotal = 0.0;

                if (noteSubHeading.getOperation().equals(Constants.NOTE_OPTION_DEBIT)) {

                    ////////////////////////// Opening Stock ////////////////////////////////
                    List<TrialBalanceSet> trialBalanceSetListOpening = new ArrayList<>();

                    for (Stock stock : stockRepository.findAll()) {
                        if ((stock.getStockAccount().getOpeningDebit() + stock.getStockAccount().getOpeningCredit()) > 0
                                || stock.getOpeningQuantity() > 0.0) {
                            TrialBalanceSet trialBalanceSet = new TrialBalanceSet();
                            trialBalanceSet.setAccountTitle(stock.getStockAccount().getTitle());
                            trialBalanceSet.setDebit(stock.getStockAccount().getOpeningDebit() + stock.getStockAccount().getOpeningCredit());
                            trialBalanceSet.setSaleQuantity(stock.getOpeningQuantity());
                            trialBalanceSet.setItemRate((trialBalanceSet.getCredit() + trialBalanceSet.getDebit()) / trialBalanceSet.getSaleQuantity());
                            trialBalanceSet.setAccount(stock.getStockAccount());
                            if (!isWorking) {
                                subheadingTotal += trialBalanceSet.getDebit();
                            }

                            trialBalanceSetListOpening.add(trialBalanceSet);
                        }
                    }
                    noteSubHeading.setTrialBalanceSetListOpening(trialBalanceSetListOpening);

                    ///////////////////////// During Period Purchase ////////////////////////
                    List<TrialBalanceSet> trialBalanceSetList = new ArrayList<>();

                    for (Account account : getInternalAccountList(noteSubHeading.getAccounts(),new ArrayList<>())) {
                        TrialBalanceSet trialBalanceSet = new TrialBalanceSet();
                        trialBalanceSet.setAccountTitle(account.getTitle());
                        trialBalanceSet.setAccount(account);


                        List<Voucher> voucherList = voucherService.getVouchersByAccount(account.getAccountCode());

                        trialBalanceSet.setSaleQuantity(getSaleQuantity(voucherList, Constants.NOTE_OPTION_DEBIT, null, toDate, 1));
                        trialBalanceSet.setDebit(round(getAmount(voucherList, Constants.NOTE_OPTION_DEBIT, 0.0, null, toDate, 1), 2));
                        trialBalanceSet.setItemRate(trialBalanceSet.getDebit() / trialBalanceSet.getSaleQuantity());
                        if (!isWorking) {
                            subheadingTotal += trialBalanceSet.getDebit();
                        }

                        trialBalanceSetList.add(trialBalanceSet);
                    }
                    noteSubHeading.setTrialBalanceSetList(trialBalanceSetList);

                    /////////////////////// Closing Stock ///////////////////////

                    noteSubHeading.setTrialBalanceSetListClosing(getClosingList(noteSubHeading.getTrialBalanceSetListOpening(),
                            noteSubHeading.getTrialBalanceSetList(), toDate));
                    for (TrialBalanceSet trialBalanceSet : noteSubHeading.getTrialBalanceSetListClosing()) {
                        if (isWorking) {
                            subheadingTotal += trialBalanceSet.getDebit();
                        } else {
                            subheadingTotal -= trialBalanceSet.getDebit();
                        }
                    }

                    ////////////// Case Credit and Net ////////////////////////////
                } else {

                    List<TrialBalanceSet> trialBalanceSetList = new ArrayList<>();
                    for (Account account : getInternalAccountList(noteSubHeading.getAccounts(),new ArrayList<>())) {
                        TrialBalanceSet trialBalanceSet = new TrialBalanceSet();
                        trialBalanceSet.setAccountTitle(account.getTitle());
                        trialBalanceSet.setAccount(account);

                        Double openingAmount = account.getOpeningDebit() - account.getOpeningCredit();

                        List<Voucher> voucherList = voucherService.getVouchersByAccount(account.getAccountCode());

                        if (noteSubHeading.getOperation().equals(Constants.NOTE_OPTION_CREDIT)) {
                            trialBalanceSet.setSaleQuantity(getSaleQuantity(voucherList, Constants.NOTE_OPTION_CREDIT, null, toDate, 1));
                            trialBalanceSet.setCredit(round(getAmount(voucherList, Constants.NOTE_OPTION_CREDIT, 0.0, null, toDate, 1), 2));

                            subheadingTotal += trialBalanceSet.getCredit();
                        } else {
                            trialBalanceSet.setSaleQuantity(0.0);
                            trialBalanceSet.setDebit(round(getAmount(voucherList, Constants.NOTE_OPTION_NET, openingAmount, null, toDate, 1), 2));
                            subheadingTotal += trialBalanceSet.getDebit();
                        }
                        trialBalanceSet.setItemRate((trialBalanceSet.getCredit() + trialBalanceSet.getDebit()) / trialBalanceSet.getSaleQuantity());
                        trialBalanceSetList.add(trialBalanceSet);
                    }
                    noteSubHeading.setTrialBalanceSetList(trialBalanceSetList);
                }
                noteSubHeading.setSubheadingTotal(subheadingTotal);
            }
        }
        return accountNotes;
    }


    public double getAmount(List<Voucher> allVouchers, String accountType, double openingAmount, Date fromDate, Date toDate, int checkType) {

        for (Voucher voucher : allVouchers) {
            if (trialBalanceService.validateDate(voucher.getVoucherDate(), fromDate, toDate, checkType)) {

                if (accountType.equals(Constants.NOTE_OPTION_CREDIT)) {
                    openingAmount += voucher.getCredit();

                } else if (accountType.equals(Constants.NOTE_OPTION_DEBIT)) {
                    openingAmount += voucher.getDebit();

                } else {
                    openingAmount += voucher.getDebit();
                    openingAmount -= voucher.getCredit();
                }
            }

        }
        return openingAmount;

    }


    private Double getSaleQuantity(List<Voucher> voucherList, String operationType, Date fromDate, Date toDate, int checkType) {
        Double saleQuantity = 0.0;

        for (Voucher voucher : voucherList) {
            if (trialBalanceService.validateDate(voucher.getVoucherDate(), fromDate, toDate, checkType)) {
                if (operationType.equals(Constants.NOTE_OPTION_CREDIT)) {
                    if (voucher.getCredit() > 0.0) {
                        saleQuantity += voucher.getItemQuantity();
                    }
                } else if (operationType.equals(Constants.NOTE_OPTION_DEBIT)) {
                    if (voucher.getDebit() > 0.0) {
                        saleQuantity += voucher.getItemQuantity();
                    }
                }
            }
        }
        return saleQuantity;
    }


    private List<TrialBalanceSet> getClosingList(List<TrialBalanceSet> openingList, List<TrialBalanceSet> purchaseList, Date toDate) {

        List<TrialBalanceSet> closingList = new ArrayList<>();
        for (TrialBalanceSet trial : purchaseList) {

            TrialBalanceSet trialBalanceSetOpening = null;
            try {
                 trialBalanceSetOpening = openingList
                        .stream()
                        .filter(f -> f.getAccountTitle().equals(trial.getAccountTitle()))
                        .collect(singletonCollector());
            }catch (Exception e){
                e.printStackTrace();
            }

            TrialBalanceSet clone = new TrialBalanceSet();
            clone.setAccountTitle(trial.getAccountTitle());
            clone.setSaleQuantity(trialBalanceSetOpening.getSaleQuantity() + trial.getSaleQuantity() - getItemSaleQuantity(trial.getAccount(), toDate));
            clone.setDebit(round(getClosingAmount(trial.getAccount(), clone.getSaleQuantity(), toDate), 2));
            clone.setItemRate(clone.getDebit() / clone.getSaleQuantity());
            closingList.add(clone);



        }

        return closingList;
    }

    private Double getItemSaleQuantity(Account account, Date toDate) {

        return getSaleQuantity(voucherService.getVouchersByAccount(account.getAccountCode()), Constants.NOTE_OPTION_CREDIT, null, toDate, 1);
    }


    private Double getClosingAmount(Account account, Double closingAmount, Date toDate) {

        Double deductionAmount = 0.0;
        Double currentAmount = 0.0;
        Integer voucherId = 2147483647;
        /*Voucher voucher = new Voucher();*/

        while (closingAmount > 0.0) {

            Voucher voucher = voucherService.getVoucherForClosing(account.getAccountCode(), voucherId, toDate);
            if (voucher != null) {
                if (closingAmount <= voucher.getItemQuantity()) {
                    deductionAmount = closingAmount;
                } else {
                    deductionAmount = voucher.getItemQuantity();
                }
                currentAmount += deductionAmount * voucher.getItemRate();
                closingAmount -= deductionAmount;
                voucherId = voucher.getVoucherId();
            } else {
                currentAmount += closingAmount * account.getAccountStock().getCost();
                closingAmount = 0.0;
            }
        }
        return currentAmount;

    }


    private List<Account> getInternalAccountList(List<Account> accountList,List<Account> finalList) {

        for (Account accountL1 : accountList) {
            if (accountL1.getLevel() == 4) {
                finalList.add(accountL1);
            } else {
                getInternalAccountList(accountL1.getChildList(),finalList);
            }
        }
        return finalList;
    }


    public static <T> Collector<T, ?, T> singletonCollector()  throws IllegalStateException{
        return Collectors.collectingAndThen(
                Collectors.toList(),
                list -> {
                    if (list.size() != 1) {
                        throw new IllegalStateException();
                    }
                    return list.get(0);
                }
        );
    }
}
