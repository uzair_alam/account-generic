package com.hellokoding.account.service.note;


import com.hellokoding.account.model.AccountNote;
import com.hellokoding.account.model.NoteSubHeading;
import com.hellokoding.account.model.ReportStaging;
import com.hellokoding.account.repository.AccountNoteRepository;
import com.hellokoding.account.repository.NoteSubHeadingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    @Autowired
    private AccountNoteRepository accountNoteRepository;

    @Autowired
    private NoteSubHeadingRepository noteSubHeadingRepository;

    @Override
    public void saveNote(AccountNote accountNote) {
        accountNoteRepository.save(accountNote);
    }

    public AccountNote findByNoteId(int noteId){
        return accountNoteRepository.findByNoteId(noteId);
    }

    public List<AccountNote> findAll(){
        return accountNoteRepository.findByStatus(1);
    }

    public List<AccountNote> findAllByReportStaging(ReportStaging reportStaging){
        return accountNoteRepository.findByStatusAndReportStaging(0,reportStaging);
    }

    public List<AccountNote> findByNoteType(String noteType, Integer status){
        return accountNoteRepository.findByReportTypeAndStatus(noteType,status);
    }

    public void removeNote (AccountNote accountNote){
        accountNoteRepository.delete(accountNote);
    }

    //////////////////////////////////////////////////////

    public void saveNoteSubHead(NoteSubHeading noteSubHeading){
        noteSubHeadingRepository.save(noteSubHeading);
    }

    public NoteSubHeading findBySubId(Integer subId){
        return noteSubHeadingRepository.findBySubId(subId);
    }

    public List<NoteSubHeading> findAllSubHead(){
        return noteSubHeadingRepository.findAll();
    }

    public void removeNoteSubHead (NoteSubHeading noteSubHeading){
        noteSubHeadingRepository.delete(noteSubHeading);
    }


}

