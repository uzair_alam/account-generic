package com.hellokoding.account.service.taxation;


import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.TaxDetail;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TaxDetailService {

    void save(TaxDetail taxDetail);
    void remove(Account account);
    List<TaxDetail> findAll();
    TaxDetail findDetail(Account account);
}
