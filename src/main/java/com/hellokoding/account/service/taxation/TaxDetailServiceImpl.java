package com.hellokoding.account.service.taxation;


import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.TaxDetail;
import com.hellokoding.account.repository.AccountRepository;
import com.hellokoding.account.repository.TaxDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class TaxDetailServiceImpl implements TaxDetailService{

    @Autowired
    private TaxDetailRepository taxDetailRepository;

    @Autowired
    private AccountRepository accountRepository;

    public void save(TaxDetail taxDetail){
        Account account = accountRepository.findByAccountCode(taxDetail.getAccountCode());
        //taxDetailRepository.removeDetails(account);
        taxDetail.setBaseAccount(account);
        taxDetail.setIsActive(1);
        taxDetailRepository.save(taxDetail);
}


    public TaxDetail findDetail(Account account)
    {

     return taxDetailRepository.findFirstByBaseAccountAndEffectiveTillGreaterThanEqual(account,new java.sql.Date(Calendar.getInstance().getTime().getTime()));
    }

    public List<TaxDetail> findAll(){
        return taxDetailRepository.findAll();
    }

    public void remove(Account account){
        taxDetailRepository.removeDetails(account);
    }

}
