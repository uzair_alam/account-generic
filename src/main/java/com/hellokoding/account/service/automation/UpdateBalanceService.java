package com.hellokoding.account.service.automation;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.StockCustomer;
import com.hellokoding.account.model.StockVendor;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.reportModel.TrialBalanceSet;
import com.hellokoding.account.service.Voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import com.hellokoding.account.service.ledger.TrialBalanceService;
import com.hellokoding.account.service.stock.CustomerService;
import com.hellokoding.account.service.stock.StockService;
import com.hellokoding.account.service.stock.VendorService;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class UpdateBalanceService {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private VendorService vendorService;
    @Autowired
    private TrialBalanceService trialBalanceService;
    @Autowired
    private VoucherService voucherService;
    @Autowired
    private AccountService accountService;

    public void calculateDailyBalance() {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        for (Account account : accountService.findAll()) {
            List<TrialBalanceSet> trailBalanceCustomerList = trialBalanceService.getTrailBalanceBasic(0, date, account,null);
            if (trailBalanceCustomerList.size() > 0) {
                if (trailBalanceCustomerList.get(0).getDebit() > 0) {
                    account.setLedgerBalance(trailBalanceCustomerList.get(0).getDebit());
                } else {
                    account.setLedgerBalance(trailBalanceCustomerList.get(0).getCredit());
                }
                accountService.update(account);
            }
        }
    }

    public Account calculateLedgerBalance(Account account) {
        Double totalAmount;
        Double ledgerBalance;
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
                ledgerBalance = account.getLedgerBalance();
                List<Voucher> voucherList = voucherService.getVoucherByCurrentDate(date, date, account.getAccountCode());
                totalAmount = calculateBalanceAmount(voucherList, ledgerBalance);
                account.setTotalBalanceAmount(totalAmount);

        return account;
    }


    public Double calculateBalanceAmount(List<Voucher> allVouchers, Double ledgerBalance) {

        for (Voucher voucher : allVouchers) {
            ledgerBalance -= voucher.getCredit();
            ledgerBalance += voucher.getDebit();

        }
        return ledgerBalance;
    }


}
