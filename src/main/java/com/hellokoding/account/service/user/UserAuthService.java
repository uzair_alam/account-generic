package com.hellokoding.account.service.user;

import com.hellokoding.account.model.UserAuth;

public interface UserAuthService{

   void save(UserAuth user);
    public UserAuth findByUserName(String username);

}
