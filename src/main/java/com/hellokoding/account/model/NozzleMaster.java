package com.hellokoding.account.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;



@Entity
@Table(name = "nozzle_master")
public class NozzleMaster {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "nozzle_code")
    private int nozzleCode;
    @Column(name = "nozzle_name")
    private String nozzleName;
    @Column(name = "opening_reading")
    private double openingReading;
    @Column(name = "measure_unit")
    private String measureUnit;
    @Column(name = "sort_order")
    private Integer sortOrder;
    @Column(name = "date")
    private Date date;

    @OneToMany(mappedBy = "nozzle", cascade = CascadeType.ALL)
    private List<DailyReading> dailyReadingList;


    @ManyToOne (cascade=CascadeType.ALL)
    @JoinColumn(name="nozzle_item", unique= true, nullable=true, insertable=true, updatable=true)
    private Stock nozzleItem;


    @Transient
    private Integer stockId;

    public Stock getNozzleItem() {
        return nozzleItem;
    }

    public void setNozzleItem(Stock nozzleItem) {
        this.nozzleItem = nozzleItem;
    }

    public Integer getStockId() {
        return stockId;
    }

    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    public List<DailyReading> getDailyReadingList() {
        return dailyReadingList;
    }

    public void setDailyReadingList(List<DailyReading> dailyReadingList) {
        this.dailyReadingList = dailyReadingList;
    }

    public int getNozzleCode() {
        return nozzleCode;
    }

    public void setNozzleCode(int nozzleCode) {
        this.nozzleCode = nozzleCode;
    }

    public String getNozzleName() {
        return nozzleName;
    }

    public void setNozzleName(String nozzleName) {
        this.nozzleName = nozzleName;
    }

    public double getOpeningReading() {
        return openingReading;
    }

    public void setOpeningReading(double openingReading) {
        this.openingReading = openingReading;
    }

    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
}
