package com.hellokoding.account.model;

import javax.persistence.*;

import java.sql.Date;

@Entity
@Table(name = "account_centre")
public class CostCentre {

    @Id
    @Column(name = "account_code")
    private Long accountCode;


    private String title;

    @Column(name = "opening_date")
    private Date openingDate;

    @Column(name = "isActive")
    private int isActive;

    @ManyToOne()
    @JoinColumn(name = "parent_code", nullable = true)
    private Account parentAccount;

    @Transient
    private int parentAccountCode;

    public int getParentAccountCode() {
        return parentAccountCode;
    }

    public void setParentAccountCode(int parentAccountCode) {
        this.parentAccountCode = parentAccountCode;
    }



    public Account getParentAccount() {
        return parentAccount;
    }

    public void setParentAccount(Account parentAccount) {
        this.parentAccount = parentAccount;
    }




    public Long getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(Long accountCode) {
        this.accountCode = accountCode;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

}
