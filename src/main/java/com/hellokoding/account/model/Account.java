package com.hellokoding.account.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "account")
public class Account implements Serializable {

    @Id
    @Column(name = "account_code")
    private Integer accountCode;


    private int level;
    private String title;

    @Column(name = "opening_date")
    private Date openingDate;
    @Column(name = "opening_credit")
    private double openingCredit;
    @Column(name = "opening_debit")
    private double openingDebit;
    @Column(name = "last_opening_credit")
    private double lastOpeningCredit;
    @Column(name = "ledger_balance")
    private Double ledgerBalance;
    @Column(name = "amount_limit")
    private Double amountLimit;

    @Column(name = "last_opening_debit")
    private double lastOpeningDebit;
    @Column(name = "isActive")
    private int isActive;
    @OneToMany(mappedBy = "parentAccount", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<CostCentre> costCentres;

    @OneToOne(fetch=FetchType.LAZY, mappedBy="account")
    private BankDetails bankDetails;



    @Transient
    private double openingBalance;

    @Transient
    private double openingBalanceLast;

    @Transient
    private Integer parentCode;

    //amount for ledger balance
    @Transient
    private Double totalBalanceAmount;


    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="parent_code")
    private Account parentAccount;

    @OneToMany(mappedBy="parentAccount")
    private List<Account> childList;

    @ManyToMany(mappedBy = "accounts" , fetch = FetchType.LAZY)
    private List<NoteSubHeading> noteSubHeadings;


    @OneToMany(mappedBy = "itemAccount", cascade = CascadeType.ALL)
    private List<DailyReading> dailyReadingList;

    @OneToOne(mappedBy = "distributorAccount", cascade = CascadeType.ALL)
    private Distributor distributor;

    @OneToOne(mappedBy = "riderAccount", cascade = CascadeType.ALL)
    private Rider rider;

    public List<DailyReading> getDailyReadingList() {
        return dailyReadingList;
    }

    public void setDailyReadingList(List<DailyReading> dailyReadingList) {
        this.dailyReadingList = dailyReadingList;
    }

    public List<NoteSubHeading> getNoteSubHeadings() {
        return noteSubHeadings;
    }

    public void setNoteSubHeadings(List<NoteSubHeading> noteSubHeadings) {
        this.noteSubHeadings = noteSubHeadings;
    }



    public List<TaxDetail> getTaxDetailList() {
        return taxDetailList;
    }

    public void setTaxDetailList(List<TaxDetail> taxDetailList) {
        this.taxDetailList = taxDetailList;
    }

    @OneToMany(fetch=FetchType.LAZY, mappedBy="baseAccount" ,cascade = CascadeType.ALL)
    private List<TaxDetail> taxDetailList;


    public Stock getAccountStock() {
        return accountStock;
    }

    public void setAccountStock(Stock accountStock) {
        this.accountStock = accountStock;
    }

    @OneToOne(mappedBy = "stockAccount", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Stock accountStock;

    @OneToOne(mappedBy = "vendorAccount", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private StockVendor stockVendor;


    @OneToOne(mappedBy = "customerAccount", cascade = CascadeType.ALL ,fetch = FetchType.LAZY)
    private StockCustomer stockCustomer ;

    @OneToOne(mappedBy = "expenseAccount", cascade = CascadeType.ALL)
    private ExpenseDetail expenseDetail;

    @OneToMany(mappedBy = "bankDetails", cascade = CascadeType.ALL ,fetch = FetchType.LAZY)
    private List<AuthorizedPerson> authorizedPersonList;

    public StockVendor getStockVendor() {
        return stockVendor;
    }

    public void setStockVendor(StockVendor stockVendor) {
        this.stockVendor = stockVendor;
    }

    public StockCustomer getStockCustomer() {
        return stockCustomer;
    }

    public void setStockCustomer(StockCustomer stockCustomer) {
        this.stockCustomer = stockCustomer;
    }

    public List<AuthorizedPerson> getAuthorizedPersonList() {
        return authorizedPersonList;
    }

    public ExpenseDetail getExpenseDetail() {
        return expenseDetail;
    }

    public void setExpenseDetail(ExpenseDetail expenseDetail) {
        this.expenseDetail = expenseDetail;
    }

    public void setAuthorizedPersonList(List<AuthorizedPerson> authorizedPersonList) {
        this.authorizedPersonList = authorizedPersonList;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        if(level > 0  && level < 5)
        this.level = level;
    }
    public double getLastOpeningCredit() {
        return lastOpeningCredit;
    }

    public void setLastOpeningCredit(double lastOpeningCredit) {
        this.lastOpeningCredit = lastOpeningCredit;
    }

    public double getLastOpeningDebit() {
        return lastOpeningDebit;
    }

    public void setLastOpeningDebit(double lastOpeningDebit) {
        this.lastOpeningDebit = lastOpeningDebit;
    }

    public double getOpeningBalanceLast() {
        return openingBalanceLast;
    }

    public void setOpeningBalanceLast(double openingBalanceLast) {
        this.openingBalanceLast = openingBalanceLast;
    }

    public double getOpeningCredit() {
        return openingCredit;
    }

    public void setOpeningCredit(double openingCredit) {
        this.openingCredit = openingCredit;
    }

    public double getOpeningDebit() {
        return openingDebit;
    }

    public void setOpeningDebit(double openingDebit) {
        this.openingDebit = openingDebit;
    }
    public Account getParentAccount() {
        return parentAccount;
    }

    public void setParentAccount(Account parentAccount) {
        this.parentAccount = parentAccount;
    }

    public List<Account> getChildList() {
        return childList;
    }

    public void setChildList(List<Account> childList) {
        this.childList = childList;
    }


    public BankDetails getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(BankDetails bankDetails) {
        this.bankDetails = bankDetails;
    }


    public List<CostCentre> getCostCentres() {
        return costCentres;
    }

    public void setCostCentres(List<CostCentre> costCentres) {
        this.costCentres = costCentres;
    }

    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }



    public Integer getParentCode() {
        return parentCode;
    }

    public void setParentCode(Integer parentCode) {
        this.parentCode = parentCode;
    }



    public Integer getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(Integer AccountCode) {
        accountCode = AccountCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public Double getLedgerBalance() {
        return ledgerBalance;
    }

    public void setLedgerBalance(Double ledgerBalance) {
        this.ledgerBalance = ledgerBalance;
    }

    public Double getAmountLimit() {
        return amountLimit;
    }

    public void setAmountLimit(Double amountLimit) {
        this.amountLimit = amountLimit;
    }

    public Double getTotalBalanceAmount() {
        return totalBalanceAmount;
    }

    public void setTotalBalanceAmount(Double totalBalanceAmount) {
        this.totalBalanceAmount = totalBalanceAmount;
    }

    public Distributor getDistributor() {
        return distributor;
    }

    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }

    public Rider getRider() {
        return rider;
    }

    public void setRider(Rider rider) {
        this.rider = rider;
    }
}
