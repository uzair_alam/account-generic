package com.hellokoding.account.model;

import javax.persistence.*;

@Entity(name = "authorized_person")
public class AuthorizedPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "person_id")
    private int personId;
    private String name;
    private int isActive;

    @ManyToOne()
    @JoinColumn(name = "detail_id", nullable = true)
    private BankDetails bankDetails;




    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public BankDetails getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(BankDetails bankDetails) {
        this.bankDetails = bankDetails;
    }




}
