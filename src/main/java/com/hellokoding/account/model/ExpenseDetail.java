package com.hellokoding.account.model;

import javax.persistence.*;

@Entity
@Table(name = "expense_details")
public class ExpenseDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "expense_id")
    private int expenseId;
    @Column(name = "description")
    private String description;
    @Column(name = "expense_ntn")
    private String expenseNtn;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_number", unique = true, nullable = true, insertable = true, updatable = true)
    private Account expenseAccount;

    @Transient
    private Integer accountCode;

    public int getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(int expenseId) {
        this.expenseId = expenseId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpenseNtn() {
        return expenseNtn;
    }

    public void setExpenseNtn(String expenseNtn) {
        this.expenseNtn = expenseNtn;
    }

    public Account getExpenseAccount() {
        return expenseAccount;
    }

    public void setExpenseAccount(Account expenseAccount) {
        this.expenseAccount = expenseAccount;
    }

    public Integer getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(Integer accountCode) {
        this.accountCode = accountCode;
    }
}
