package com.hellokoding.account.model;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "stock_tank")
public class StockTank {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "tank_id")
    private int tankId;
    @Column(name = "name")
    private String name;
    @Column(name = "capacity")
    private String capacity;
    @Column(name = "description")
    private String description;
    @Column(name = "radius")
    private Double radius;
    @Column(name = "length")
    private Double length;
    @Column(name = "clearance")
    private Double clearance;

    @Transient
    private Integer stockId;


    // For Clearance of Stock
    @Transient
    private double volume;
    @Transient
    private double dip;

    @ManyToOne()
    @JoinColumn(name = "stock_id", nullable = true)
    private Stock tankStock;

    @OneToMany(mappedBy = "stockTank", cascade = CascadeType.ALL,orphanRemoval=true)
    private List<TankInfo> tankInfoList;

    public int getTankId() {
        return tankId;
    }

    public void setTankId(int tankId) {
        this.tankId = tankId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getClearance() {
        return clearance;
    }

    public void setClearance(Double clearance) {
        this.clearance = clearance;
    }

    public Stock getTankStock() {
        return tankStock;
    }

    public void setTankStock(Stock tankStock) {
        this.tankStock = tankStock;
    }

    public List<TankInfo> getTankInfoList() {
        return tankInfoList;
    }

    public void setTankInfoList(List<TankInfo> tankInfoList) {
        this.tankInfoList = tankInfoList;
    }

    public Integer getStockId() {
        return stockId;
    }

    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getDip() {
        return dip;
    }

    public void setDip(double dip) {
        this.dip = dip;
    }
}
