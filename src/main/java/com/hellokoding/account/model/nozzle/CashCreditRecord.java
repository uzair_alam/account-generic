package com.hellokoding.account.model.nozzle;

public class CashCreditRecord {

    private Double cash;
    private Double credit;
    private String item;
    private Double total;
    private Double saleQuantity;
    private Double testQuantity;

    public Double getCash() {
        if(cash == null){
            return 0.0;
        }

        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }

    public Double getCredit() {
        if(credit == null){
            return 0.0;
        }
        return credit;
    }

    public Double getSaleQuantity() {
        if(saleQuantity == null)
        return 0.0;

        return saleQuantity;
    }

    public void setSaleQuantity(Double saleQuantity) {
        this.saleQuantity = saleQuantity;
    }

    public Double getTestQuantity() {
        if(testQuantity == null)
            return 0.0;

        return testQuantity;
    }

    public void setTestQuantity(Double testQuantity) {
        this.testQuantity = testQuantity;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
