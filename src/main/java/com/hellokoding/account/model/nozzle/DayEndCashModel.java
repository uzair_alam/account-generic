package com.hellokoding.account.model.nozzle;

public class DayEndCashModel {

    private Double receiptAmount;
    private Double paymentDeposit;
    private Double withdrawal;
    private Double cashPayment;
    private Double openingCash;

    public Double getOpeningCash() {
        return openingCash;
    }

    public void setOpeningCash(Double openingCash) {
        this.openingCash = openingCash;
    }

    public Double getReceiptAmount() {
        return receiptAmount;
    }

    public void setReceiptAmount(Double receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public Double getPaymentDeposit() {
        return paymentDeposit;
    }

    public void setPaymentDeposit(Double paymentDeposit) {
        this.paymentDeposit = paymentDeposit;
    }

    public Double getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(Double withdrawal) {
        this.withdrawal = withdrawal;
    }

    public Double getCashPayment() {
        return cashPayment;
    }

    public void setCashPayment(Double cashPayment) {
        this.cashPayment = cashPayment;
    }
}
