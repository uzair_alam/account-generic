package com.hellokoding.account.model.nozzle;

import com.hellokoding.account.model.DailyReading;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class NozzleDayEndList {


    private List<DailyReading> dailyNozzleReadingList;
    private List<DailyReading> dailyItemReadingList;
    private Date date;


    public NozzleDayEndList(){

        this.dailyNozzleReadingList = new ArrayList<DailyReading>();
        this.dailyItemReadingList = new ArrayList<DailyReading>();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<DailyReading> getDailyNozzleReadingList() {
        return dailyNozzleReadingList;
    }

    public void setDailyNozzleReadingList(List<DailyReading> dailyNozzleReadingList) {
        this.dailyNozzleReadingList = dailyNozzleReadingList;
    }

    public List<DailyReading> getDailyItemReadingList() {
        return dailyItemReadingList;
    }

    public void setDailyItemReadingList(List<DailyReading> dailyItemReadingList) {
        this.dailyItemReadingList = dailyItemReadingList;
    }

    public void addDailyNozzle(DailyReading dailyReading){
        this.dailyNozzleReadingList.add(dailyReading);
    }

    public void addDailyItem(DailyReading dailyReading){
        this.dailyItemReadingList.add(dailyReading);
    }


}
