package com.hellokoding.account.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "daily_reading")
public class DailyReading {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "reading_id")
    private int readingId;
    @Column(name = "opening_reading")
    private Double openingReading;
    @Column(name = "closing_reading")
    private Double closingReading;
    @Column(name = "test_quantity")
    private Double testQuantity;
    @Column(name = "date")
    private Date date;
    @Transient
    private Integer accountCode;
    @Transient
    private Integer nozzleCode;

    @ManyToOne()
    @JoinColumn(name = "item_code", nullable = true)
    private Account itemAccount;

    @ManyToOne()
    @JoinColumn(name = "nozzle_code", nullable = true)
    private NozzleMaster nozzle;

    public Account getItemAccount() {
        return itemAccount;
    }

    public void setItemAccount(Account itemAccount) {
        this.itemAccount = itemAccount;
    }

    public NozzleMaster getNozzle() {
        return nozzle;
    }

    public void setNozzle(NozzleMaster nozzle) {
        this.nozzle = nozzle;
    }

    public Integer getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(Integer accountCode) {
        this.accountCode = accountCode;
    }

    public Integer getNozzleCode() {
        return nozzleCode;
    }

    public void setNozzleCode(Integer nozzleCode) {
        this.nozzleCode = nozzleCode;
    }

    public int getReadingId() {
        return readingId;
    }

    public void setReadingId(int readingId) {
        this.readingId = readingId;
    }

    public Double getOpeningReading() {
        return openingReading;
    }

    public void setOpeningReading(Double openingReading) {
        this.openingReading = openingReading;
    }

    public Double getClosingReading() {
        return closingReading;
    }

    public void setClosingReading(Double closingReading) {
        this.closingReading = closingReading;
    }

    public Double getTestQuantity() {
        if(testQuantity == null)
            return 0.0;

        return testQuantity;
    }

    public void setTestQuantity(Double testQuantity) {
        this.testQuantity = testQuantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


}
