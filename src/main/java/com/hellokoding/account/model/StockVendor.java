package com.hellokoding.account.model;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "stock_vendor")
public class StockVendor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "vendor_id")
    private int vendorId;
    @Column(name = "address")
    private String address;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "landline")
    private String landline;
    @Column(name = "cnic")
    private String cnic;
    @Column(name = "saletax_number")
    private String saletaxNumber;
    @Column(name = "ntn")
    private String nationalTaxNumber;
    @Column(name = "tax_status")
    private String taxStatus;

    @Transient
    private Integer accountCode;

    @Transient
    private String contactPersons;

    @OneToOne (cascade=CascadeType.ALL)
    @JoinColumn(name="account_number", unique= true, nullable=true, insertable=true, updatable=true)
    private Account vendorAccount;

    @OneToMany(mappedBy = "parentVendor", cascade = CascadeType.ALL)
    private List<StockContactPerson> stockContactPersonList;

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getSaletaxNumber() {
        return saletaxNumber;
    }

    public void setSaletaxNumber(String saletaxNumber) {
        this.saletaxNumber = saletaxNumber;
    }

    public String getTaxStatus() {
        return taxStatus;
    }

    public void setTaxStatus(String taxStatus) {
        this.taxStatus = taxStatus;
    }

    public Integer getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(Integer accountCode) {
        this.accountCode = accountCode;
    }

    public String getContactPersons() {
        return contactPersons;
    }

    public void setContactPersons(String contactPersons) {
        this.contactPersons = contactPersons;
    }

    public Account getVendorAccount() {
        return vendorAccount;
    }

    public void setVendorAccount(Account vendorAccount) {
        this.vendorAccount = vendorAccount;
    }

    public List<StockContactPerson> getStockContactPersonList() {
        return stockContactPersonList;
    }

    public void setStockContactPersonList(List<StockContactPerson> stockContactPersonList) {
        this.stockContactPersonList = stockContactPersonList;
    }

    public String getNationalTaxNumber() {
        return nationalTaxNumber;
    }

    public void setNationalTaxNumber(String nationalTaxNumber) {
        this.nationalTaxNumber = nationalTaxNumber;
    }
}

