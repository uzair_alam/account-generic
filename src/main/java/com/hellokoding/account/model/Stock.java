package com.hellokoding.account.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "stock")
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "stock_id")
    private Integer stockId;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "quantity")
    private Double quantity;
    @Column(name = "opening_quantity")
    private Double openingQuantity;
    @Column(name = "cost")
    private double cost;
    @Column(name = "selling_price")
    private Double sellingPrice;
    @Column(name = "unit_measure")
    private String unitMeasure;
    @Column(name = "item_date")
    private Date itemDate;
    @Transient
    private Integer categoryId;

    @Transient
    private Integer accountCode;

    public Integer getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(Integer accountCode) {
        this.accountCode = accountCode;
    }

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "account_code", unique = true, nullable = true, insertable = true, updatable = true)
    private Account stockAccount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", nullable = true)
    private StockCategory stockCategory;

    @OneToMany(mappedBy = "nozzleItem", cascade = CascadeType.ALL)
    private List<NozzleMaster> nozzleList;

    @OneToMany(mappedBy = "calibrationStock", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<CalibrationInfo> calibrationInfoList;

    @OneToMany(mappedBy = "tankStock", cascade = CascadeType.ALL ,fetch = FetchType.LAZY)
    private List<StockTank> stockTankList;

    public List<NozzleMaster> getNozzleList() {
        return nozzleList;
    }

    public void setNozzleList(List<NozzleMaster> nozzleList) {
        this.nozzleList = nozzleList;
    }

    public Integer getStockId() {
        return stockId;
    }

    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    public Date getItemDate() {
        return itemDate;
    }

    public void setItemDate(Date itemDate) {
        this.itemDate = itemDate;
    }

    public Account getStockAccount() {
        return stockAccount;
    }

    public void setStockAccount(Account stockAccount) {
        this.stockAccount = stockAccount;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public StockCategory getStockCategory() {
        return stockCategory;
    }

    public void setStockCategory(StockCategory stockCategory) {
        this.stockCategory = stockCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getOpeningQuantity() {
        return openingQuantity;
    }

    public void setOpeningQuantity(Double openingQuantity) {
        this.openingQuantity = openingQuantity;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public List<CalibrationInfo> getCalibrationInfoList() {
        return calibrationInfoList;
    }

    public void setCalibrationInfoList(List<CalibrationInfo> calibrationInfoList) {
        this.calibrationInfoList = calibrationInfoList;
    }

    public List<StockTank> getStockTankList() {
        return stockTankList;
    }

    public void setStockTankList(List<StockTank> stockTankList) {
        this.stockTankList = stockTankList;
    }
}
