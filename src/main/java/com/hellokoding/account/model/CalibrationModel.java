package com.hellokoding.account.model;

import java.sql.Date;
import java.util.List;

public class CalibrationModel {

    private Stock stock;
    private double currentQuantity;
    private Date date;
    private String remarks;
    private Double tankQuantity;

    private List<StockTank> stockTankList;


    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public double getCurrentQuantity() {
        return currentQuantity;
    }

    public void setCurrentQuantity(double currentQuantity) {
        this.currentQuantity = currentQuantity;
    }

    public List<StockTank> getStockTankList() {
        return stockTankList;
    }

    public void setStockTankList(List<StockTank> stockTankList) {
        this.stockTankList = stockTankList;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Double getTankQuantity() {
        return tankQuantity;
    }

    public void setTankQuantity(Double tankQuantity) {
        this.tankQuantity = tankQuantity;
    }
}
