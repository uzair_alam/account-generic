package com.hellokoding.account.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "account_note")
public class AccountNote {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "note_id")
    private int noteId;
    @Column(name = "heading")
    private String heading;
    @Column(name = "date")
    private Date date;
    @Column(name = "print_seq")
    private Integer printSeq;
    @Column(name = "report_type")
    private String reportType;


    @Column(name = "status")
    private int status;


    @ManyToOne()
    @JoinColumn(name = "report_id", nullable = true)
    private ReportStaging reportStaging;


    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "note_subheading", joinColumns = @JoinColumn(name = "note_id"), inverseJoinColumns = @JoinColumn(name = "sub_id"))
    private List<NoteSubHeading> noteSubHeadings;


    public ReportStaging getReportStaging() {
        return reportStaging;
    }

    public void setReportStaging(ReportStaging reportStaging) {
        this.reportStaging = reportStaging;
    }
    public List<NoteSubHeading> getNoteSubHeadings() {
        return noteSubHeadings;
    }

    public void setNoteSubHeadings(List<NoteSubHeading> noteSubHeadings) {
        this.noteSubHeadings = noteSubHeadings;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getPrintSeq() {
        return printSeq;
    }

    public void setPrintSeq(Integer printSeq) {
        this.printSeq = printSeq;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }


}
