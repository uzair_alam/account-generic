package com.hellokoding.account.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "voucher")
public class Voucher {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "voucher_id")
    private int voucherId;
    @Column(name = "voucher_number")
    private String voucherNumber;
    @Column(name = "voucher_date")
    private Date voucherDate;
    @Column(name = "cheque_number")
    private String chequeNumber;
    @Column(name = "bank_account")
    private int bankAccount;
    @Column(name = "account_code")
    private Integer accountCode;
    @Column(name = "cost_centre")
    private Long costCentre;
    @Column(name = "tax_account")
    private Integer taxAccount;
    @Column(name = "bill_amount")
    private Double billAmount;
    @Column(name = "tax_amount")
    private Double taxAmount;
    @Column(name = "clearing_date")
    private Date clearingDate;
    private String remarks;
    private Integer isActive;
    @Column(name = "reference_voucher")
    private String referenceVoucher;
    private Double credit;
    private Double debit;
    @Column(name = "item_account")
    private Integer itemAccount;
    @Column(name = "bill_number")
    private String billNumber;
    @Column(name = "bill_date")
    private Date billDate;
    @Column(name = "item_quantity")
    private Double itemQuantity;
    @Column(name = "item_rate")
    private Double itemRate;
    @Column(name = "tax_rate")
    private Double taxRate;

    @Column(name = "status")
    private String status;
    @Transient
    private boolean check;
    @Transient
    private double balance;
    @Transient
    private List<Voucher> internalVoucherList;
    @Transient
    private String voucherType;
    // for journal voucher
    @Transient
    private Integer creditAccount;

    @Transient
    private String voucherNumberForPdc;



    public Integer getItemAccount() {
        if (itemAccount == null) {
            return 0;
        }

        return itemAccount;
    }

    public void setItemAccount(Integer itemAccount) {
        this.itemAccount = itemAccount;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public Double getItemQuantity() {
        if (itemQuantity == null) {
            return 0.0;
        }

        return itemQuantity;
    }

    public void setItemQuantity(Double itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public Double getItemRate() {

        if(itemRate ==  null)
            return 0.0;

        return itemRate;
    }

    public String getVoucherNumberForPdc() {
        return voucherNumberForPdc;
    }

    public void setVoucherNumberForPdc(String voucherNumberForPdc) {
        this.voucherNumberForPdc = voucherNumberForPdc;
    }

    public void setItemRate(Double itemRate) {
        this.itemRate = itemRate;
    }

    public Date getClearingDate() {
        return clearingDate;
    }

    public void setClearingDate(Date clearingDate) {
        this.clearingDate = clearingDate;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getReferenceVoucher() {
        return referenceVoucher;
    }

    public void setReferenceVoucher(String referenceVoucher) {
        this.referenceVoucher = referenceVoucher;
    }

    public int getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(int voucherId) {
        this.voucherId = voucherId;
    }

    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public Date getVoucherDate() {
        return voucherDate;
    }

    public void setVoucherDate(Date voucherDate) {
        this.voucherDate = voucherDate;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public int getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(int bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Integer getAccountCode() {
        if (accountCode == null) {
            return 0;
        }

        return accountCode;
    }

    public void setAccountCode(Integer accountCode) {
        this.accountCode = accountCode;
    }

    public Long getCostCentre() {
        return costCentre;
    }

    public void setCostCentre(Long costCentre) {
        this.costCentre = costCentre;
    }

    public Integer getTaxAccount() {
        return taxAccount;
    }

    public void setTaxAccount(Integer taxAccount) {
        this.taxAccount = taxAccount;
    }

    public Double getBillAmount() {
        if(billAmount == null)
            return 0.0;

        return billAmount;
    }

    public void setBillAmount(Double billAmount) {
        this.billAmount = billAmount;
    }

    public Double getTaxAmount() {
        if(taxAmount == null)
            return 0.0;

        return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Double getCredit() {
        if (credit == null) {
            return 0.0;
        }

        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getDebit() {
        if (debit == null) {
            return 0.0;
        }

        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public List<Voucher> getInternalVoucherList() {
        return internalVoucherList;
    }

    public void setInternalVoucherList(List<Voucher> internalVoucherList) {
        this.internalVoucherList = internalVoucherList;
    }
    public Double getTaxRate() { return taxRate; }

    public void setTaxRate(Double taxRate) { this.taxRate = taxRate; }

    public Integer getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(Integer creditAccount) {
        this.creditAccount = creditAccount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
