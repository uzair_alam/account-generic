package com.hellokoding.account.model;
import com.hellokoding.account.reportModel.TrialBalanceSet;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "note_sub_heading")
public class NoteSubHeading {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sub_id")
    private int subId;
    @Column(name = "heading")
    private String heading;
    @Column(name = "level")
    private Integer level;
    @Column(name = "operation")
    private String operation;


    @ManyToMany(mappedBy = "noteSubHeadings" , fetch = FetchType.LAZY)
    private List<AccountNote> accountNoteList;


    public List<AccountNote> getAccountNoteList() {
        if(accountNoteList != null){
        return accountNoteList;}
        return new ArrayList<>();
    }

    public void setAccountNoteList(List<AccountNote> accountNoteList) {
        this.accountNoteList = accountNoteList;
    }



    @ManyToMany
    @JoinTable(name = "subheading_account", joinColumns = @JoinColumn(name = "sub_id"), inverseJoinColumns = @JoinColumn(name = "account_code"))
    private List<Account> accounts;

    //added for the processing of note summary report.
    @Transient
    private List<TrialBalanceSet> trialBalanceSetList;

    @Transient
    private List<TrialBalanceSet> trialBalanceSetListOpening;

    @Transient
    private List<TrialBalanceSet> trialBalanceSetListClosing;

    @Transient
    private boolean action;

    @Transient
    private Integer parentNote;

    @Transient
    private Double subheadingTotal;

    public Double getSubheadingTotal() {
        if(subheadingTotal == null) {
            return 0.0;
        }
        return subheadingTotal;
    }

    public void setSubheadingTotal(Double subheadingTotal) {
        this.subheadingTotal = subheadingTotal;
    }

    public boolean isAction() {
        return action;
    }

    public void setAction(boolean action) {
        this.action = action;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Integer getParentNote() {
        return parentNote;
    }

    public void setParentNote(Integer parentNote) {
        this.parentNote = parentNote;
    }

    public List<TrialBalanceSet> getTrialBalanceSetListOpening() {
        if(trialBalanceSetListOpening == null){
            return new ArrayList<>();
        }

        return trialBalanceSetListOpening;
    }

    public void setTrialBalanceSetListOpening(List<TrialBalanceSet> trialBalanceSetListOpening) {
        this.trialBalanceSetListOpening = trialBalanceSetListOpening;
    }

    public List<TrialBalanceSet> getTrialBalanceSetListClosing() {
        if(trialBalanceSetListClosing == null){
            return new ArrayList<>();
        }

        return trialBalanceSetListClosing;
    }

    public void setTrialBalanceSetListClosing(List<TrialBalanceSet> trialBalanceSetListClosing) {
        this.trialBalanceSetListClosing = trialBalanceSetListClosing;
    }

    public List<TrialBalanceSet> getTrialBalanceSetList() {
        if(trialBalanceSetList != null){
            return trialBalanceSetList;
        }
        return new ArrayList<>();
    }

    public void setTrialBalanceSetList(List<TrialBalanceSet> trialBalanceSetList) {
        this.trialBalanceSetList = trialBalanceSetList;
    }



    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }



    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }


}
