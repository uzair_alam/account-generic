package com.hellokoding.account.model;

import javax.persistence.*;

@Entity
@Table(name = "tank_info")
public class TankInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "info_id")
    private int infoId;
    @Column(name = "dip")
    private Double dip;
    @Column(name = "volume")
    private Double volume;

    @ManyToOne()
    @JoinColumn(name = "tank_id", nullable = true)
    private StockTank stockTank;

    public int getInfoId() {
        return infoId;
    }

    public void setInfoId(int infoId) {
        this.infoId = infoId;
    }

    public Double getDip() {
        return dip;
    }

    public void setDip(Double dip) {
        this.dip = dip;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public StockTank getStockTank() {
        return stockTank;
    }

    public void setStockTank(StockTank stockTank) {
        this.stockTank = stockTank;
    }
}
