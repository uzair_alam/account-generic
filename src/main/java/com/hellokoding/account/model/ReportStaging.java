package com.hellokoding.account.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;


@Entity
@Table(name ="report_staging")
public class ReportStaging{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "stage_id")
    private int stageId;
    @Column(name = "title")
    private String title;
    @Column(name = "date")
    private Date date;
    @Column(name = "print_seq")
    private Integer printSeq;

    @Column(name = "report_type")
    private String reportType;

    @Column(name = "is_sub_total")
    private Boolean isSubTotal;

    @Column(name = "is_total")
    private Boolean isTotal;

    @Column(name = "is_working")
    private Boolean isWorking;

    @Transient
    private Double currentAmount;
    @Transient
    private Integer lastAmount;


    public Double getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(Double currentAmount) {
        this.currentAmount = currentAmount;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public Integer getLastAmount() {
        return lastAmount;
    }

    public void setLastAmount(Integer lastAmount) {
        this.lastAmount = lastAmount;
    }

    public Boolean getSubTotal() {
        return isSubTotal;
    }

    public void setSubTotal(Boolean subTotal) {
        isSubTotal = subTotal;
    }

    public Boolean getTotal() {
        return isTotal;
    }

    public void setTotal(Boolean total) {
        isTotal = total;
    }

    public Boolean getWorking() {
        return isWorking;
    }

    public void setWorking(Boolean working) {
        isWorking = working;
    }

    public List<AccountNote> getAccountNoteList() {
        return accountNoteList;
    }

    public void setAccountNoteList(List<AccountNote> accountNoteList) {
        this.accountNoteList = accountNoteList;
    }

    @OneToMany(mappedBy = "reportStaging", cascade = CascadeType.ALL)
    private List<AccountNote> accountNoteList;


    public int getStageId() {
        return stageId;
    }

    public void setStageId(int stageId) {
        this.stageId = stageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getPrintSeq() {
        return printSeq;
    }

    public void setPrintSeq(Integer printSeq) {
        this.printSeq = printSeq;
    }




}
