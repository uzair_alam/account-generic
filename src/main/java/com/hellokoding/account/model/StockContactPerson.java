package com.hellokoding.account.model;

import javax.persistence.*;


@Entity
@Table(name = "stock_auth_persons")
public class StockContactPerson {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "person_id")
    private int personId;
    @Column(name = "name")
    private String name;

    @ManyToOne()
    @JoinColumn(name = "customer_id", nullable = true)
    private StockCustomer parentCustomer;

    @ManyToOne()
    @JoinColumn(name = "vendor_id", nullable = true)
    private StockVendor parentVendor;

    public StockCustomer getParentCustomer() {
        return parentCustomer;
    }

    public void setParentCustomer(StockCustomer parentCustomer) {
        this.parentCustomer = parentCustomer;
    }

    public StockVendor getParentVendor() {
        return parentVendor;
    }

    public void setParentVendor(StockVendor parentVendor) {
        this.parentVendor = parentVendor;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
