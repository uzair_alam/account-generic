package com.hellokoding.account.model;

import javax.persistence.*;
import java.sql.Date;


@Entity
@Table(name = "tax_details")
public class TaxDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "tax_detail_id")
    private int taxDetailId;
    @Column(name = "filer_amount")
    private double filerAmount;
    @Column(name = "non_filer_amount")
    private double nonFilerAmount;
    @Column(name = "effective_till")
    private Date effectiveTill;
    private Integer isActive;

    @Transient
    private int accountCode;

    public Account getBaseAccount() {
        return baseAccount;
    }

    public void setBaseAccount(Account baseAccount) {
        this.baseAccount = baseAccount;
    }

    @ManyToOne()
    @JoinColumn(name = "account", nullable = true)
    private Account baseAccount;

    public int getTaxDetailId() {
        return taxDetailId;
    }

    public void setTaxDetailId(int taxDetailId) {
        this.taxDetailId = taxDetailId;
    }

    public double getFilerAmount() {
        return filerAmount;
    }

    public void setFilerAmount(double filerAmount) {
        this.filerAmount = filerAmount;
    }

    public double getNonFilerAmount() {
        return nonFilerAmount;
    }

    public void setNonFilerAmount(double nonFilerAmount) {
        this.nonFilerAmount = nonFilerAmount;
    }

    public Date getEffectiveTill() {
        return effectiveTill;
    }

    public void setEffectiveTill(Date effectiveTill) {
        this.effectiveTill = effectiveTill;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }


    public int getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(int accountCode) {
        this.accountCode = accountCode;
    }




}
