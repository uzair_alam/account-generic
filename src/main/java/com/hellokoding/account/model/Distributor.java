package com.hellokoding.account.model;


import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "distributors")
public class Distributor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "distributor_id")
    private int distributorId;
    @Column(name = "name")
    private String name;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "address")
    private String address;
    @Column(name = "email")
    private String email;
    @Column(name = "date")
    private Date date;
    @Column(name = "isActive")
    private boolean isActive;


    @OneToMany(mappedBy = "distributor", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<StockCustomer> stockCustomerList;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_code", unique = true, nullable = true, insertable = true, updatable = true)
    private Account distributorAccount;

    public int getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(int distributorId) {
        this.distributorId = distributorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<StockCustomer> getStockCustomerList() {
        return stockCustomerList;
    }

    public void setStockCustomerList(List<StockCustomer> stockCustomerList) {
        this.stockCustomerList = stockCustomerList;
    }

    public Account getDistributorAccount() {
        return distributorAccount;
    }

    public void setDistributorAccount(Account distributorAccount) {
        this.distributorAccount = distributorAccount;
    }
}
