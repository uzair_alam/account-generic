package com.hellokoding.account.model;

import java.util.ArrayList;
import java.util.List;

public class VoucherList {


    public VoucherList(){

        this.voucherList = new ArrayList<Voucher>();
    }

    public List<Voucher> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<Voucher> voucherList) {
        this.voucherList = voucherList;
    }

    private List <Voucher> voucherList;


    public void add(Voucher voucher){
        this.voucherList.add(voucher);
    }

}
