package com.hellokoding.account.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name="calibration_info")
public class CalibrationInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "info_id")
    private int infoId;
    @Column(name = "date")
    private Date date;
    @Column(name = "remarks")
    private String remarks;
    @Column(name = "actual_quantity")
    private Double actualQuantity;
    @Column(name = "tanks_quantity")
    private Double tanksQuantity;
    @Column(name = "is_calibrated")
    private Boolean isCalibrated;


    @ManyToOne()
    @JoinColumn(name = "stock_id", nullable = true)
    private Stock calibrationStock;

    public int getInfoId() {
        return infoId;
    }

    public void setInfoId(int infoId) {
        this.infoId = infoId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Double getActualQuantity() {
        return actualQuantity;
    }

    public void setActualQuantity(Double actualQuantity) {
        this.actualQuantity = actualQuantity;
    }

    public Double getTanksQuantity() {
        return tanksQuantity;
    }

    public void setTanksQuantity(Double tanksQuantity) {
        this.tanksQuantity = tanksQuantity;
    }

    public Stock getCalibrationStock() {
        return calibrationStock;
    }

    public void setCalibrationStock(Stock calibrationStock) {
        this.calibrationStock = calibrationStock;
    }

    public Boolean getCalibrated() {
        return isCalibrated;
    }

    public void setCalibrated(Boolean calibrated) {
        isCalibrated = calibrated;
    }
}
